﻿using cerepairs;
using hme;
using System;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Authentication
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Remove insecure protocols (SSL3, TLS 1.0, TLS 1.1)
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Ssl3;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls;
            ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Tls11;
            // Add TLS 1.2
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                if (authTicket != null && !authTicket.Expired)
                {
                    var roles = authTicket.UserData.Split(',');
                    HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new FormsIdentity(authTicket), roles);
                    var new_authTicket = new FormsAuthenticationTicket(1, authTicket.Name, DateTime.Now, DateTime.Now.AddMinutes(30), false, authTicket.UserData);
                    string encryptedTicket = FormsAuthentication.Encrypt(new_authTicket);
                    var new_authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    HttpContext.Current.Response.Cookies.Add(new_authCookie);

                    //// Issue new ticket if there is less than 7 minutes remaining on current one.
                    //if ((authTicket.Expiration - DateTime.Now) <= TimeSpan.FromMinutes(7))
                    //{
                    //    var new_authTicket = new FormsAuthenticationTicket(1, authTicket.Name, DateTime.Now, DateTime.Now.AddMinutes(15), false, authTicket.UserData);
                    //    string encryptedTicket = FormsAuthentication.Encrypt(new_authTicket);
                    //    var new_authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    //    HttpContext.Current.Response.Cookies.Add(new_authCookie);
                    //}
                }
            }
        }
        protected void Session_Start(Object sender, EventArgs e)
        {
            Session["Session_UID"] = null;
            Session["User_UID"] = null;
            string http_host = Request.ServerVariables["HTTP_HOST"];
            if (http_host.ToLower().Contains("cerepairs.com"))
            {
                Session["cmp"] = "cerepairs";
                Session["Company_Master"] = "cel";
                Session["Company_Master_Access"] = "cel";
                Session["Company_Master_Email"] = "mbehymer@cerepairs.com";
            }
            //if (http_host.ToLower().Contains("newcerepairs.com"))
            //{
            //    Session["cmp"] = "cerepairs";
            //    Session["Company_Master"] = "cel";
            //    Session["Company_Master_Access"] = "cel";
            //    Session["Company_Master_Email"] = "mbehymer@cerepairs.com";
            //}
            if (http_host.ToLower().Contains("hme.com"))
            {
                Session["cmp"] = "hme";
                Session["Company_Master"] = "hsc";
                Session["Company_Master_Access"] = "hsc";
                Session["Company_Master_Email"] = "InvoiceReceipts@hme.com";
            }
            if (http_host.ToLower().Contains("jtech.com"))
            {
                Session["cmp"] = "jtech";
                Session["Company_Master"] = "jtch";
                Session["Company_Master_Access"] = "jtch";
                Session["Company_Master_Email"] = "##_Invoice_Receipts_JTECH@hme.com";
            }
            if (http_host.ToLower().Contains("clearcom.com"))
            {
                Session["cmp"] = "clearcom";
                Session["Company_Master"] = "clrc";
                Session["Company_Master_Access"] = "clrc";
                Session["Company_Master_Email"] = "clearcomar@clearcom.com";
            }
            if (http_host.ToLower().Contains("clearcom.com"))
            {
                Session["cmp"] = "clearcom";
                Session["Company_Master"] = "clrc";
                Session["Company_Master_Access"] = "clrc";
                Session["Company_Master_Email"] = "clearcomar@clearcom.com";
            }
        }
    }
}
