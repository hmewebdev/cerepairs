﻿using System;
using System.Collections.Generic;
using System.Linq;
using RazorEngine;
using RazorEngine.Templating;

namespace RazorEngineClass
{
    public class RazorEngine
    {
        public static string RunCompile(string viewpath, string templatekey, object model)
        {
            string result = string.Empty;
            var template = System.IO.File.ReadAllText(viewpath);
            if (string.IsNullOrEmpty(templatekey))
            {
                templatekey = Guid.NewGuid().ToString();
            }
            result = Engine.Razor.RunCompile(template, templatekey, null, model);
            //result = Engine.Razor.RunCompile(template, null, model);
            return result;
        }
    }
}