﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace qalab
{
    public class CacheClass
    {
        public static int CacheLoginAttempts(string ip)
        {
            Cache cache = new Cache();
            int count = 0;
            if (cache[ip] != null)
            {
                count = (int)cache.Get(ip);
                cache.Remove(ip);
            }
            int newcount = count + 1;
            if (newcount < 5)
            {
                cache.Insert(ip, newcount, null, DateTime.Now.AddSeconds(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            else
            {
                newcount = 99;
                cache.Insert(ip + "delay", newcount, null, DateTime.Now.AddMinutes(2), System.Web.Caching.Cache.NoSlidingExpiration);

            }
            return newcount;
        }
        public static int GetLoginAttempts(string ip)
        {
            Cache cache = new Cache();
            if (cache[ip] != null)
            {
                return (int)cache.Get(ip);
            }
            else
                return 0;
        }

        public static void ClearCache(string handle)
        {
            Cache cache = new Cache();
            if (cache[handle] != null)
            {
                cache.Remove(handle);
            }
        }
        public static void CacheForgotPassword(string uid)
        {
            Cache cache = new Cache();
            if (cache[uid] != null)
            {
                cache.Remove(uid);
            }
            cache.Insert(uid, 0, null, DateTime.Now.AddMinutes(120), System.Web.Caching.Cache.NoSlidingExpiration);
        }
        public static bool CacheIsForgotPassword(string uid)
        {
            Cache cache = new Cache();
            if (cache[uid] != null)
            {
                cache.Remove(uid);
                return true;
            }
            return false;
        }

        public static void CacheSecurityToken(string token, string json)
        {
            Cache cache = new Cache();
            if (cache[token] != null)
            {
                cache.Remove(token);
            }
            cache.Insert(token, json, null, DateTime.Now.AddMinutes(30), System.Web.Caching.Cache.NoSlidingExpiration);
        }

        public static string CacheGetSecurityToken(string token)
        {
            Cache cache = new Cache();
            if (cache[token] != null)
            {
                return (string)cache.Get(token);
            }
            else
                return null;
        }
    }
}