﻿using HME.SQLSERVER.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace cerepairs.Classes
{
    public abstract partial class BaseModels
    {
        public dbSQL db = null;
        public string sqlMessage = string.Empty;
        public DataSet ds;
        public List<SqlParameter> sp = new List<SqlParameter>();


        public BaseModels()
        {
            //db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            //Session["ContinueShopping"] = Request.Url.PathAndQuery;
        }

        public string User_IP
        {
            get { return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]; }
        }

        public string User_Agent
        {
            get { return HttpContext.Current.Request.UserAgent; }
        }
    }
    public static class Extensions
    {
        public static void SetProperty(this object obj, string propertyName, object value)
        {
            var propertyInfo = obj.GetType().GetProperty(propertyName);
            if (propertyInfo == null) return;
            propertyInfo.SetValue(obj, value);
        }
    }
}