﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;


namespace axapi
{
    public class AXapi
    {
        public async Task<List<StoreItemsModel>> GetStoreItems(string company)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authentication", "3f0403d7e097a981d068c1a6a394f0d4216c3c0b");
            var url = "http://dynaxapi.com/api/AXapi/AXpost";
            var payload = new Dictionary<string, string>
            {
              {"request", "StoreItems"},
              {"company", company},
              {"status", ""},
              {"message", ""},
              {"pagenumber", "2"},
              {"itemperpage", "20"},
              {"sortorder", ""},
              {"sortcolumn", ""}
            };

            HttpResponseMessage response = await httpClient.PostAsJsonAsync(url, payload);
            response.EnsureSuccessStatusCode();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string str = await response.Content.ReadAsStringAsync();
                var storeitems = json.Deserialize<List<StoreItemsModel>>(str);
                return storeitems;
            }
            else
            {
                return null;
            }
        }

        public async Task<List<cerepairs.AXServices.CustomerModel>> Get_CustomerInfo(string company, string customerid)
        {
            cerepairs.AXServices.custreq customerInfoRequest = new cerepairs.AXServices.custreq();
            customerInfoRequest.company = company;
            customerInfoRequest.itemperpage = "20";
            customerInfoRequest.pagenumber = "2";
            customerInfoRequest.request = "CustomerInfo";
            customerInfoRequest.customerAcct = customerid;
            cerepairs.AXServices.AXWebServiceSoapClient AX = new cerepairs.AXServices.AXWebServiceSoapClient();
            //cerepairs.AXServices.AuthHeader authentication = new cerepairs.AXServices.AuthHeader();
            //authentication.UserName = "test";
            //authentication.Password = "test";

            List<cerepairs.AXServices.CustomerModel> customerInfo = await Task.Factory.StartNew(() => AX.GetCustomerInfo(customerInfoRequest));
            return customerInfo;
        }

        public async Task<List<CustomerModel>> GetCustomerInfo(string company, string customerid)
        {

            cerepairs.ServiceReference1.IService1 cc = new cerepairs.ServiceReference1.Service1Client();
            cerepairs.ServiceReference1.custreq req  = new cerepairs.ServiceReference1.custreq();
            List<cerepairs.ServiceReference1.CustomerModel> cm = await cc.GetCustomerInfoAsync(req);

            //cerepairs.ServiceReference1.Service1Client client = new cerepairs.ServiceReference1.Service1Client();
            //client.ClientCredentials.UserName.UserName = "administrator";
            //client.ClientCredentials.UserName.Password = "abcd1234!";
            //client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.PeerOrChainTrust;

            //List<cerepairs.ServiceReference1.CustomerModel> ccm = cc.GetCustomerInfo(req);



            cerepairs.AXServices.custreq customerInfoRequest = new cerepairs.AXServices.custreq();
            customerInfoRequest.company = company;
            customerInfoRequest.itemperpage = "20";
            customerInfoRequest.pagenumber = "2";
            customerInfoRequest.request = "CustomerInfo";
            customerInfoRequest.customerAcct = customerid;
            cerepairs.AXServices.AXWebServiceSoapClient AX = new cerepairs.AXServices.AXWebServiceSoapClient();
            List<cerepairs.AXServices.CustomerModel> customerInfo = AX.GetCustomerInfo(customerInfoRequest);

            JavaScriptSerializer json = new JavaScriptSerializer();
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authentication", "3f0403d7e097a981d068c1a6a394f0d4216c3c0b");
            var url = "http://dynaxapi.com/api/AXapi/GetCustomerInfo";
            var payload = new Dictionary<string, string>
            {
              {"request", "CustomerInfo"},
              {"company", company},
              {"status", ""},
              {"message", ""},
              {"pagenumber", "2"},
              {"itemperpage", "20"},
              {"sortorder", ""},
              {"sortcolumn", ""},
              {"customerAcct", customerid}
            };

            HttpResponseMessage response = await httpClient.PostAsJsonAsync(url, payload);
            response.EnsureSuccessStatusCode();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                string str = await response.Content.ReadAsStringAsync();
                var customer = json.Deserialize<List<CustomerModel>>(str);
                return customer;
            }
            else
            {
                return null;
            }
        }

    }

    public class ServicePortalModels
    {
    }

    public class StoreItemsList
    {
        public List<StoreItemsModel> request { get; set; }
    }

    [Serializable]
    public class StoreItemsModel
    {
        public string CATEGORYID { get; set; }
        public string CustomerID { get; set; }
        public string SUBCATEGORYID { get; set; }
        public string SUBCATEGORY { get; set; }
        public string FILENAME { get; set; }
        public string FILETYPE { get; set; }
        public string ITEMID { get; set; }
        public string ITEMNAME { get; set; }
        public string SALESPRICE { get; set; }
        public string COMPANY { get; set; }
    }

    [Serializable]
    public class CustomerList
    {
        public List<CustomerModel> customer { get; set; }
    }
    public class CustomerModel
    {
        public string CustomerId { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
        public string LocationName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string CustomerPhone { get; set; }
    }
}