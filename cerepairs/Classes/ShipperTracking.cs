﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace shipper
{
    public class ShipperTracking
    {
        public static async Task<Shippo_Tracking_Response> shippo(string carrier, string tracking_number, string purchase_order)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("ShippoToken", "3f0403d7e097a981d068c1a6a394f0d4216c3c0b");
            var url = "https://api.goshippo.com/v1/tracks/";
            //Uri uri = new Uri("https://api.goshippo.com/tracks");
            var payload = new Dictionary<string, string>
            {
              {"carrier", carrier},
              {"tracking_number", tracking_number},
              {"metadata", carrier + "|" + purchase_order}
            };
            string strPayload = json.Serialize(payload);
            HttpContent content = new StringContent(strPayload, Encoding.UTF8, "application/json");
            var result = await httpClient.PostAsync(url, content);
            if (result.StatusCode == HttpStatusCode.Created)
            {
                string str = await result.Content.ReadAsStringAsync();
                Shippo_Tracking_Response tracking = json.Deserialize<Shippo_Tracking_Response>(str);
                return tracking;
            }
            else
            {
                return null;
            }
        }
    }

    public class Tracking_History_dto
    {
        public string tracking_number { get; set; }
        public string carrier { get; set; }
        public string service { get; set; }
        public string message { get; set; }
        public string status { get; set; }
        public Shippo_Tracking_Response tracking { get; set; }
    }
    public class Shippo_Tracking_Response
    {
        public string message { get; set; }
        public string carrier { get; set; }
        public string tracking_number { get; set; }
        public Shippo_Tracking_Address address_from { get; set; }
        public Shippo_Tracking_Address address_to { get; set; }
        public string eta { get; set; }
        public string original_eta { get; set; }
        public Shippo_ServiceLevel servicelevel { get; set; }
        public string metadata { get; set; }
        public Shippo_Tracking_Status tracking_status { get; set; }
        public List<Shippo_Tracking_History> tracking_history { get; set; }
        public string transaction { get; set; }
        public string test { get; set; }
    }
    public class Shippo_Tracking_Address
    {
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
    }
    public class Shippo_ServiceLevel
    {
        public string token { get; set; }
        public string name { get; set; }
    }
    public class Shippo_Tracking_Status
    {
        public string object_created { get; set; }
        public string object_updated { get; set; }
        public string object_id { get; set; }
        public string status { get; set; }
        public string status_details { get; set; }
        public string status_date { get; set; }
        public Shippo_Tracking_Address location { get; set; }
    }
    public class Shippo_Tracking_History
    {
        public string object_created { get; set; }
        public string object_id { get; set; }
        public string status { get; set; }
        public string status_details { get; set; }
        public string status_date { get; set; }
        public Shippo_Tracking_Address location { get; set; }
    }
}