﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hmeUtil;
using HME.SQLSERVER.DAL;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using cerepairs.Classes;
using System.Net;
using static hme.SecurityController;
using System.Web.Script.Serialization;
using hme;

namespace cerepairs
{
    public abstract class BaseController : Controller
    {
        private string salt = "fwesdfsfs904sdkw-ldfgpsd-wgf";
        public  string commonSalt = "kds45kdfdgmckeYy$m*Hke>";

        private Encryptor encryptor = new Encryptor();
        public dbSQL db = null;
        public string sqlMessage = string.Empty;
        private string updateType = string.Empty;
        public bool isGoogleBolt = false;
        public string ip = string.Empty;
        public string pc = string.Empty;
        public string[] userInfo;
        public User_Info user_info;

        public BaseController()
        {
            //string bbb = Session.SessionID.ToString();
        }
        public BaseController(HttpSessionStateBase session)
        {
            string bbb = session.SessionID.ToString();
        }

        public void SessionManagement()
        {
            string urlpath = Request.Url.PathAndQuery;
            if (!urlpath.Contains("/cart") && !urlpath.Contains("keepalive") && !urlpath.Contains("/Payment"))
            {
                Session["ContinueShopping"] = Request.Url.PathAndQuery;
            }
            string user_cookie = string.Empty;
            string history_cookie = string.Empty;
            hmeUtil.Encryptor encryptor = new Encryptor();
            JavaScriptSerializer json = new JavaScriptSerializer();
            if (Session["Cart_Session_UID"] == null)
            {
                Session["Cart_Session_UID"] = Guid.NewGuid().ToString("N");
            }
            if (Session["Cart_User_UID"] == null)
            {
                Session["Cart_User_UID"] = Guid.NewGuid().ToString("N");
            }
            if (Request.Cookies["User_Cookie"] != null)
            {
                user_cookie = Request.Cookies["User_Cookie"].Value;
                bool isEncrypted = Request.Cookies["User_Cookie"].Value.Substring(0, 2) == "e_" ? true : false;
                int cookieLength = Request.Cookies["User_Cookie"].Value.Length;
                if (isEncrypted)
                {
                    user_cookie = encryptor.Decrypt(user_cookie.Substring(2, cookieLength - 2), salt);
                    User_Cookie = user_cookie;
                }
                else
                {
                    User_Cookie = user_cookie;
                }
            }
            else
            {
                user_cookie = Guid.NewGuid().ToString("N");
                User_Cookie = user_cookie;
            }
            Session["User_Cookie"] = User_Cookie;

            if (Request.Cookies["History_Cookie"] != null)
            {
                history_cookie = Request.Cookies["History_Cookie"].Value;
                bool isEncrypted = Request.Cookies["History_Cookie"].Value.Substring(0, 2) == "e_" ? true : false;
                int cookieLength = Request.Cookies["History_Cookie"].Value.Length;
                if (isEncrypted)
                {
                    history_cookie = encryptor.Decrypt(history_cookie.Substring(2, cookieLength - 2), salt);
                    History_Cookie = history_cookie;
                }
                else
                {
                    History_Cookie = history_cookie;
                }
            }
            else
            {
                history_cookie = Guid.NewGuid().ToString("N");
                History_Cookie = history_cookie;
            }

            //Session["User_Cookie"] = User_Cookie;

            //if (Session["User_Cookie"] == null)
            //{
            //    Session["User_Cookie"] = user_cookie;
            //    //clean up cart
            //    //update db
            //}
            db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("@User_Session_UID", User_Session_UID));
            sp.Add(new SqlParameter("@User_Cookie", User_Cookie));
            sp.Add(new SqlParameter("@User_IP", User_IP));
            SqlParameter status = new SqlParameter("@status", SqlDbType.NVarChar, 10)
            {
                Direction = ParameterDirection.Output
            };
            sp.Add(status);

            ds = db.executeStoredProcedure("updateSession", sp, out sqlMessage);
            if (sqlMessage == "OK")
            {
                UpdateType = status.Value.ToString();
                if (UpdateType == "insert")
                {

                }
            }
            db.Dispose();
            ds.Dispose();
            ip = Request.ServerVariables["REMOTE_ADDR"];
            //pc = Dns.GetHostEntry(ip).HostName;

            ViewBag.isAdmin = false;
            ViewBag.isFullAdmin = false;
            ViewBag.isUser = false;
            if (User != null && User.Identity.IsAuthenticated)
            {
                user_info = json.Deserialize<User_Info>(encryptor.Decrypt(User.Identity.Name, SecurityController.salt));
                ViewBag.isAdmin = user_info.isadmin.ToLower() == "true" ? true : false;
                ViewBag.isFullAdmin = user_info.isfulladmin.ToLower() == "true" ? true : false;
                ViewBag.isUser = user_info.logintype.ToLower() == "customerservice" ? true : false;
            }
        }

        public string User_Session_UID
        {
            get { return Session.SessionID.ToString(); }
        }

        public string Cart_Session_UID
        {
            get
            {
                if (Session["Cart_Session_UID"] == null)
                {
                    Session["Cart_Session_UID"] = Guid.NewGuid().ToString("N");
                }
                return Session["Cart_Session_UID"].ToString();
            }
        }

        public string Cart_User_UID
        {
            get { return Session["Cart_User_UID"].ToString(); }
        }


        public string User_Cookie
        {
            get
            {
                if (Request.Cookies["User_Cookie"] != null)
                {
                    string user_cookie = Request.Cookies["User_Cookie"].Value;
                    bool isEncrypted = Request.Cookies["User_Cookie"].Value.Substring(0, 2) == "e_" ? true : false;
                    int cookieLength = Request.Cookies["User_Cookie"].Value.Length;
                    if (isEncrypted)
                    {
                        return Request.Cookies["User_Cookie"].Value;
                        //return encryptor.Decrypt(user_cookie.Substring(2, cookieLength - 2), salt);
                    }
                    else
                    {
                        return Request.Cookies["User_Cookie"].Value;
                    }
                }
                else
                {
                    string cookie = Guid.NewGuid().ToString("N");
                    User_Cookie = cookie;
                    return cookie;
                }
            }
            set
            {
                HttpCookie User_Cookie = new HttpCookie("User_Cookie");
                //User_Cookie.Value = "e_" + encryptor.Encrypt(value, salt);
                User_Cookie.Value = value;
                User_Cookie.Expires = DateTime.MaxValue;
                Response.SetCookie(User_Cookie);
            }
        }

        public string History_Cookie
        {
            get
            {
                if (Request.Cookies["History_Cookie"] != null)
                {
                    string history_cookie = Request.Cookies["History_Cookie"].Value;
                    bool isEncrypted = Request.Cookies["History_Cookie"].Value.Substring(0, 2) == "e_" ? true : false;
                    int cookieLength = Request.Cookies["History_Cookie"].Value.Length;
                    if (isEncrypted)
                    {
                        return encryptor.Decrypt(history_cookie.Substring(2, cookieLength - 2), salt);
                    }
                    else
                    {
                        return Request.Cookies["History_Cookie"].Value;
                    }
                }
                else
                {
                    string cookie = Guid.NewGuid().ToString("N");
                    History_Cookie = cookie;
                    return cookie;
                }
            }
            set
            {
                HttpCookie History_Cookie = new HttpCookie("History_Cookie");
                //History_Cookie.Value = "e_" + encryptor.Encrypt(value, salt);
                History_Cookie.Value = value;
                Response.SetCookie(History_Cookie);
            }
        }

        public string UpdateType
        {
            get { return updateType; }
            set { updateType = value; }
        }

        public string User_IP
        {
            get { return Request.ServerVariables["REMOTE_ADDR"]; }
        }

        public string User_Agent
        {
            get { return Request.UserAgent; }
        }

        public bool IsGoogleBolt()
        {
            isGoogleBolt = false;// User_Agent.ToLower().Contains("googlebot");
            ViewBag.isGoogleBolt = isGoogleBolt;
            return isGoogleBolt;
        }

        public void GetCompanyInfo()
        {
            string http_host = Request.ServerVariables["HTTP_HOST"];

            if (http_host.ToLower().Contains("cerepairs.com"))
            {
                Session["cmp"] = "cerepairs";
                Session["Company_Master"] = "ce";
                Session["Company_Master_Access"] = "ce";
                Session["Company_Master_Email"] = "mbehymer@cerepairs.com";
            }
            else if (http_host.ToLower().Contains("hme.com"))
            {
                Session["cmp"] = "hme";
                Session["Company_Master"] = "hsc";
                Session["Company_Master_Access"] = "hsc";
                Session["Company_Master_Email"] = "InvoiceReceipts@hme.com";
            }
            else if (http_host.ToLower().Contains("jtech.com"))
            {
                Session["cmp"] = "jtech";
                Session["Company_Master"] = "jtch";
                Session["Company_Master_Access"] = "jtch";
                Session["Company_Master_Email"] = "##_Invoice_Receipts_JTECH@hme.com";
            }
            else if (http_host.ToLower().Contains("clearcom.com"))
            {
                Session["cmp"] = "clearcom";
                Session["Company_Master"] = "clrc";
                Session["Company_Master_Access"] = "clrc";
                Session["Company_Master_Email"] = "clearcomar@clearcom.com";
            }
        }

        public string Company_Master
        {
            get { return Session["Company_Master"].ToString(); }
        }

        public string Company
        {
            get { return Session["cmp"].ToString(); }
        }

        public void getPayZero_Account()
        {
            if (ConfigurationManager.AppSettings["Environment"].ToString().ToLower() == "test")
            {
                Session["tokenizationkey"] = encryptor.Decrypt(ConfigurationManager.AppSettings["test tokenization-key"], commonSalt);
                Session["payxero-authorization"] = encryptor.Decrypt(ConfigurationManager.AppSettings["test authorization"], commonSalt);
                Session["key-hash"] = encryptor.Decrypt(ConfigurationManager.AppSettings["test key-hash"], commonSalt);
            }
            else
            {
                var yy = ConfigurationManager.AppSettings["cerepairs " + "authorization"];
                var xx = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs " + "authorization"], commonSalt); ;
                Session["tokenizationkey"] = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs " + "tokenization-key"], commonSalt);
                Session["payxero-authorization"] = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs " + "authorization"], commonSalt);
                Session["key-hash"] = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs " + "key-hash"], commonSalt);

            }
        }
        public PaypalInfo GetPaypalInfo(string currency)
        {
            PaypalInfo info = new PaypalInfo();
            info.Currency = currency;

            var url = Request.Url.AbsoluteUri.Split(new[] { '?' })[0];
            url = url.Substring(0, url.LastIndexOf('/'));
            string appBaseURL = url.Substring(0, url.LastIndexOf('/'));

            info.ReturnURL = appBaseURL + "/Payment/Transaction";
            info.CancelURL = appBaseURL + "/Payment/PaymentFailure";
            info.ErrorURL = appBaseURL + "/Payment/PaymentFailure";
            info.Partner = "PayPal";
            info.Vendor = "CommercialElectronics";
            info.User = "WebSupport";
            info.Password = "k1hlZ4wflH";

            //info.Company = company;
            return info;
        }
    }
}