﻿using hmeUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace cerepairs.Classes
{
    public class token
    {
        public  string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public  string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string salt = "@0d4k?@8j*f%1l93sdf903jh^^oower&";
        public ts_dto TimeStamp()
        {
            DateTime date = DateTime.Now;
            long ticks = date.Ticks;
            ts_dto ts_dto = new ts_dto();
            ts_dto.ts = ticks.ToString();
            return ts_dto;
        }
        public Token ValidateToken(string token, int elapsed, string timeFrame)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            bool did_elapse = false;
            DateTime date = DateTime.Now;
            Encryptor encryptor = new Encryptor();
            Token dto = json.Deserialize<Token>(encryptor.Decrypt(token, salt)); // json.Deserialize<Token>(encryptor.Decrypt(token, salt));
            long ticks = date.Ticks;
            try
            {
                string t = dto.ts.ts.ToString();
                long token_ticks = long.Parse(dto.ts.ts.ToString());
                long elapsedTicks = ticks - token_ticks;
                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                double elapsedSeconds = elapsedSpan.TotalSeconds;
                double elapsedMinutes = elapsedSpan.Minutes;
                double elapsedHours = elapsedSpan.Hours;
                double elapsedDays = elapsedSpan.Days;
                double elapsedMS = elapsedSpan.Milliseconds;
                if (elapsed != 0)
                {
                    switch (timeFrame)
                    {
                        case "milliseconds":
                            did_elapse = elapsedDays > elapsed;
                            break;
                        case "seconds":
                            did_elapse = elapsedSeconds > elapsed;
                            break;
                        case "minutes":
                            did_elapse = elapsedMinutes > elapsed;
                            break;
                        case "hours":
                            did_elapse = elapsedHours > elapsed;
                            break;
                        case "days":
                            did_elapse = elapsedDays > elapsed;
                            break;
                    }
                    dto.status = !did_elapse;
                    return dto;
                }
            }
            catch (Exception ex) { var msg = ex.Message; dto.status = false; }
            return dto;
        }
        public string CreateToken(string obj)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            Encryptor encryptor = new Encryptor();
            Token token = new Token();
            token.obj = obj;
            token.ts = TimeStamp();
            token.status = false;
            return encryptor.Encrypt(json.Serialize(token), salt); // encryptor.Encrypt(json.Serialize(token), salt);
        }
    }
    [Serializable]
    public class Token
    {
        public string obj { get; set; }
        public ts_dto ts { get; set; }
        public bool status { get; set; }
    }
    public class ts_dto
    {
        public string ts { get; set; }
    }
}