﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace ups
{
    public class upsLabes
    {
        #region methods

        #region ReturnLabel
        public ShipmentResponseObj ReturnLabel(UpsLabelModels.upsLabelModel shippinginfo)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            ShippingLabelRequest shippingLabelRequest = new ShippingLabelRequest();


            shippingLabelRequest.ShipmentRequest = BuildShipmentRequest(shippinginfo);
            shippingLabelRequest.UPSSecurity = new UPSSecurity();
            shippingLabelRequest.UPSSecurity.ServiceAccessToken = new ServiceAccessToken();
            shippingLabelRequest.UPSSecurity.UsernameToken = new UsernameToken();

            RequestShippingLabel RequestShippingLabel = new RequestShippingLabel();
            RequestShippingLabel.ShippingLabelRequest = shippingLabelRequest;

            Cerepair cerepair = setdestination(shippinginfo);

            RequestShippingLabel.ShippingLabelRequest.UPSSecurity.ServiceAccessToken.AccessLicenseNumber = cerepair.accessLicensenumber;
            RequestShippingLabel.ShippingLabelRequest.UPSSecurity.UsernameToken.Password = cerepair.password;
            RequestShippingLabel.ShippingLabelRequest.UPSSecurity.UsernameToken.Username = cerepair.username;

            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.LabelSpecification.LabelImageFormat.Code = "GIF";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.LabelSpecification.LabelImageFormat.Description = "GIF";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.LabelSpecification.HTTPUserAgent = "Mozilla/4.5";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.ReturnService.Code = "9";
 
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Request.RequestOption = "validate";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Request.TransactionReference.CustomerContext = "Your Customer Context";

            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Description = "RETURN SERVICE";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.ShipperNumber = cerepair.accountnumber;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.TaxIdentificationNumber = "123456";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Service.Code = "03";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Service.Description = "Ground";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipmentRatingOptions.NegotiatedRatesIndicator = "0";

            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.Description = shippinginfo.products.Length > 30 ?shippinginfo.products.Substring(0, 30): shippinginfo.products;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.Packaging.Code = "02";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.Packaging.Description = shippinginfo.products.Length > 30 ? shippinginfo.products.Substring(0, 30) : shippinginfo.products;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.Dimensions.Height = "2";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.Dimensions.Length = "7";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.Dimensions.Width = "5";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.Dimensions.UnitOfMeasurement.Code = "IN";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.Dimensions.UnitOfMeasurement.Description = "Inches";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.PackageWeight.UnitOfMeasurement.Code = "LBS";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.PackageWeight.UnitOfMeasurement.Description = "Pounds";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.PackageWeight.Weight = shippinginfo.shippingweight;
            if (!string.IsNullOrEmpty(shippinginfo.rmanumber))
            {
                RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.ReferenceNumber.Code = "RZ";
                RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.ReferenceNumber.Value = shippinginfo.rmanumber;
                RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.ReferenceNumber.ReferenceNumber = shippinginfo.phonenumber;
            }
            if (shippinginfo.destination != "cerepairs")
            {
                RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.PackageServiceOptions.DeclaredValue.CurrencyCode = "USD";
                RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Package.PackageServiceOptions.DeclaredValue.MonetaryValue = "99.00";
            }

            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.PaymentInformation.ShipmentCharge.Type = "01";
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.PaymentInformation.ShipmentCharge.BillShipper.AccountNumber = cerepair.accountnumber;//must be the sme as ShipperNumber 

            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.Name = shippinginfo.storename + ":" + shippinginfo.storenumber;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.AttentionName = shippinginfo.firstname + " " + shippinginfo.lastname;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.Address.AddressLine = shippinginfo.address1;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.Address.City = shippinginfo.city;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.Address.CountryCode = shippinginfo.country;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.Address.PostalCode = shippinginfo.zipcode;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.Address.StateProvinceCode = shippinginfo.state;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.Shipper.Phone.Number = shippinginfo.phonenumber;


            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.AttentionName = shippinginfo.attention;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.Name = cerepair.location;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.Address.AddressLine = cerepair.street;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.Address.City = cerepair.province;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.Address.CountryCode = cerepair.countrycode;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.Address.PostalCode = cerepair.postalcode;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.Address.StateProvinceCode = cerepair.stateprovincecode;
            //RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.FaxNumber = cerepair.faxnumber;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.Phone.Number = cerepair.phonenumber;

            //RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo. = cerepair.faxnumber;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipFrom.Name = shippinginfo.firstname + " " + shippinginfo.lastname;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipFrom.Address.AddressLine = shippinginfo.address1;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipFrom.Address.City = shippinginfo.city;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipFrom.Address.CountryCode = shippinginfo.country;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipFrom.Address.PostalCode = shippinginfo.zipcode;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipFrom.Address.StateProvinceCode = shippinginfo.state;
            //RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipTo.FaxNumber = cerepair.faxnumber;
            RequestShippingLabel.ShippingLabelRequest.ShipmentRequest.Shipment.ShipFrom.Phone.Number = shippinginfo.phonenumber;



            string json_shippingLabelRequest = json.Serialize(RequestShippingLabel.ShippingLabelRequest);


            var req = "{\"UPSSecurity\": { \"UsernameToken\": {\"Username\": \"CENRoeder\",";
            req += "\"Password\": \"CEHME2010\"},";
            req += "\"ServiceAccessToken\": {\"AccessLicenseNumber\": \"1C57A07278A3E128\"}},";
            req += "\"ShipmentRequest\": { \"Request\": {\"RequestOption\": \"validate\",";
            req += "\"TransactionReference\": { \"CustomerContext\": \"Your Customer Context\"}},";
            req += "\"Shipment\": {\"Description\": \"Description\",";
            req += "\"Shipper\": { \"Name\": \"Shipper Name\",";
            req += "\"AttentionName\": \"Shipper Attn Name\",";
            req += "\"TaxIdentificationNumber\": \"123456\",";
            req += "\"Phone\": {  \"Number\": \"1234567890\",";
            req += "\"Extension\": \"1\"  },";
            req += "\"ShipperNumber\": \"679382\",";
            req += "\"FaxNumber\": \"1234567890\",";
            req += "\"Address\": {  \"AddressLine\": \"111 Avenida Amigo\",";
            req += "\"City\": \"San Marcos\",";
            req += "\"StateProvinceCode\": \"CA\",";
            req += "\"PostalCode\": \"92069\",";
            req += "\"CountryCode\": \"US\"  } },";
            req += "\"ShipTo\": { \"Name\": \"Ship To Name\",";
            req += "\"AttentionName\": \"Ship To Attn Name\",";
            req += "\"Phone\": {  \"Number\": \"1234567890\"  },";
            req += "\"Address\": {  \"AddressLine\": \"741 Avenida Amigo\",";
            req += "\"City\": \"San Marcos\",";
            req += "\"StateProvinceCode\": \"CA\",";
            req += "\"PostalCode\": \"92069\",";
            req += "\"CountryCode\": \"US\"  } },";
            req += "\"ShipFrom\": { \"Name\": \"Bill Jones\",";
            req += "\"AttentionName\": \"Ship From Attn Bill Jones\",";
            req += "\"Phone\": {  \"Number\": \"1234567890\"  },";
            req += "\"FaxNumber\": \"1234567890\",";
            req += "\"Address\": {  \"AddressLine\": \"888 Avenida Amigo\",";
            req += "\"City\": \"San Marcos\",";
            req += "\"StateProvinceCode\": \"CA\",";
            req += "\"PostalCode\": \"92069\",";
            req += "\"CountryCode\": \"US\"  } },";
            req += "\"PaymentInformation\": { \"ShipmentCharge\": {  \"Type\": \"01\",";
            req += "\"BillShipper\": {   \"AccountNumber\": \"679382\"   }  } },";
            req += "\"Service\": { \"Code\": \"03\",";
            req += "\"Description\": \"Express\" },";
            req += "\"Package\": { \"Description\": \"Description\",";
            req += "\"Packaging\": {  \"Code\": \"02\",";
            req += "\"Description\": \"Description\"  },";
            req += "\"Dimensions\": {  \"UnitOfMeasurement\": {   \"Code\": \"IN\",";
            req += "\"Description\": \"Inches\"   },";
            req += "\"Length\": \"7\",";
            req += "\"Width\": \"5\",";
            req += "\"Height\": \"2\"  },";
            req += "\"PackageWeight\": {  \"UnitOfMeasurement\": {   \"Code\": \"LBS\",";
            req += "\"Description\": \"Pounds\"   },";
            req += "\"Weight\": \"10\"  } }},";
            req += "\"LabelSpecification\": {\"LabelImageFormat\": { \"Code\": \"GIF\",";
            req += "\"Description\": \"GIF\" },";
            req += "\"HTTPUserAgent\": \"Mozilla/4.5\"}}}";

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["upsLabelURL"]]);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json; charset=utf-8";
            httpWebRequest.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept");
            httpWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");

            using (var sw = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                sw.Write(json_shippingLabelRequest);
            }

            string response = string.Empty;
            HttpWebResponse ups = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(ups.GetResponseStream()))
            {
                response = streamReader.ReadToEnd();
            }

            ShipmentResponseObj ShipmentResponseObj = new ShipmentResponseObj();

            ShipmentResponseObj = (ShipmentResponseObj)json.Deserialize<ShipmentResponseObj>(response);

            return ShipmentResponseObj;
        }
        #endregion

        #region BuildShippingObject
        public ShipmentRequest BuildShipmentRequest(UpsLabelModels.upsLabelModel shippinginfo)
        {
            ShipmentRequest shipmentRequest = new ShipmentRequest();
            shipmentRequest.ReturnService = new ReturnService();
            shipmentRequest.Request = new Request();
            shipmentRequest.Request.TransactionReference = new TransactionReference();
            shipmentRequest.Shipment = new Shipment();
            shipmentRequest.Shipment.Shipper = new Shipper();
            shipmentRequest.Shipment.Shipper.Address = new Address();
            shipmentRequest.Shipment.Shipper.Phone = new Phone();

            shipmentRequest.Shipment.ShipTo = new ShipTo();
            shipmentRequest.Shipment.ShipTo.Address = new Address();
            shipmentRequest.Shipment.ShipTo.Phone = new Phone();

            shipmentRequest.Shipment.ShipFrom = new ShipFrom();
            shipmentRequest.Shipment.ShipFrom.Address = new Address();
            shipmentRequest.Shipment.ShipFrom.Phone = new Phone();

            shipmentRequest.Shipment.Service = new Service();
            shipmentRequest.Shipment.ShipmentRatingOptions = new ShipmentRatingOptions();

            shipmentRequest.Shipment.Package = new Package();
            shipmentRequest.Shipment.Package.Dimensions = new Dimensions();
            shipmentRequest.Shipment.Package.Dimensions.UnitOfMeasurement = new UnitOfMeasurement();
            shipmentRequest.Shipment.Package.Packaging = new Packaging();
            shipmentRequest.Shipment.Package.PackageWeight = new PackageWeight();
            shipmentRequest.Shipment.Package.PackageWeight.UnitOfMeasurement = new UnitOfMeasurement2();
            if (!string.IsNullOrEmpty(shippinginfo.rmanumber))
            {
                shipmentRequest.Shipment.Package.ReferenceNumber = new Reference_Number();
            }
            if (shippinginfo.destination != "cerepairs")
            {
                shipmentRequest.Shipment.Package.PackageServiceOptions = new PackageServiceOptions();
                shipmentRequest.Shipment.Package.PackageServiceOptions.DeclaredValue = new Declared_Value();
            }

            shipmentRequest.Shipment.PaymentInformation = new PaymentInformation();
            shipmentRequest.Shipment.PaymentInformation.ShipmentCharge = new ShipmentCharge();
            shipmentRequest.Shipment.PaymentInformation.ShipmentCharge.BillShipper = new BillShipper();

            shipmentRequest.LabelSpecification = new LabelSpecification();
            shipmentRequest.LabelSpecification.LabelImageFormat = new LabelImageFormat();


            return shipmentRequest;
        }
        #endregion

        #region setdestination
        private Cerepair setdestination(UpsLabelModels.upsLabelModel shippinginfo)
        {
            Cerepair cerepair = new Cerepair();
            switch (shippinginfo.destination)
            {
                case "cerepairs":
                    cerepair.location = "Commercial Electronics Repair";
                    cerepair.phonenumber = "8777310334";
                    cerepair.street = "3787 Rider Trail S.";
                    cerepair.province = "Earth City";
                    cerepair.stateprovincecode = "MO";
                    cerepair.postalcode = "63045";
                    cerepair.countrycode = "US";
                    cerepair.faxnumber = "";
                    cerepair.shippernumber = "679382";
                    cerepair.username = "CENRoeder";
                    cerepair.password = "CEHME2010";
                    cerepair.accessLicensenumber = "1C57A07278A3E128";
                    cerepair.accountnumber = "679382";
                    break;
                case "west":
                    cerepair.location = "HME Factory Service Center - West";
                    cerepair.phonenumber = "8585356000";
                    cerepair.street = "2848 Whiptail Loop";
                    cerepair.province = "Carlsbad";
                    cerepair.stateprovincecode = "CA";
                    cerepair.postalcode = "92010";
                    cerepair.countrycode = "US";
                    cerepair.faxnumber = "";
                    cerepair.shippernumber = "90AY20";
                    cerepair.username = "CENRoeder";
                    cerepair.password = "CEHME2010";
                    cerepair.accessLicensenumber = "1C57A07278A3E128";
                    cerepair.accountnumber = "679382";
                    break;
                case "east":
                    cerepair.location = "HME Factory Service Center - East";
                    cerepair.phonenumber = "8008484468";
                    cerepair.street = "3787 Rider Trail S.";
                    cerepair.province = "Earth City";
                    cerepair.stateprovincecode = "MO";
                    cerepair.postalcode = "63045";
                    cerepair.countrycode = "US";
                    cerepair.faxnumber = "";
                    cerepair.shippernumber = "2R9347";
                    cerepair.username = "CENRoeder";
                    cerepair.password = "CEHME2010";
                    cerepair.accessLicensenumber = "1C57A07278A3E128";
                    cerepair.accountnumber = "679382";
                    break;
            };
            return cerepair;
        }
        #endregion

        #region resizeImage
        public Image resizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }
        #endregion

        #endregion

    }


    #region properties

    [Serializable]
    public class Cerepair
    {
        public string location { get; set; }
        public string phonenumber { get; set; }
        public string street { get; set; }
        public string province { get; set; }
        public string stateprovincecode { get; set; }
        public string postalcode { get; set; }
        public string countrycode { get; set; }
        public string faxnumber { get; set; }
        public string shippernumber { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string accessLicensenumber { get; set; }
        public string accountnumber { get; set; }
    }

    [Serializable]
    public class labelinfo_dto
    {
        public labelinfo labelinfo { get; set; }
        public string label { get; set; }
        public string messsage { get; set; }
        public string status { get; set; }
        public string id { get; set; }

    }

    [Serializable]
    public class labelinfo
    {
        public string companyname { get; set; }
        public string storenumber { get; set; }
        public string storename { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string country { get; set; }
        public string rmanumber { get; set; }
        public string destination { get; set; }
        public string products { get; set; }
        public string weight { get; set; }
        public string phone { get; set; }
        public string attention { get; set; }
    }

    [Serializable]
    public class UsernameToken
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    [Serializable]
    public class ServiceAccessToken
    {
        public string AccessLicenseNumber { get; set; }
    }

    [Serializable]
    public class UPSSecurity
    {
        public UsernameToken UsernameToken { get; set; }
        public ServiceAccessToken ServiceAccessToken { get; set; }
    }

    [Serializable]
    public class TransactionReference
    {
        public string CustomerContext { get; set; }
    }

    [Serializable]
    public class Request
    {
        public string RequestOption { get; set; }
        public TransactionReference TransactionReference { get; set; }
    }

    [Serializable]
    public class Phone
    {
        public string Number { get; set; }
        public string Extension { get; set; }
    }

    [Serializable]
    public class Address
    {
        public string AddressLine { get; set; }
        public string City { get; set; }
        public string StateProvinceCode { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
    }

    [Serializable]
    public class Shipper
    {
        public string Name { get; set; }
        public string AttentionName { get; set; }
        public string TaxIdentificationNumber { get; set; }
        public Phone Phone { get; set; }
        public string ShipperNumber { get; set; }
        public string FaxNumber { get; set; }
        public Address Address { get; set; }
    }

    [Serializable]
    public class ShipTo
    {
        public string Name { get; set; }
        public string AttentionName { get; set; }
        public Phone Phone { get; set; }
        public Address Address { get; set; }
    }

    [Serializable]
    public class ShipFrom
    {
        public string Name { get; set; }
        public string AttentionName { get; set; }
        public Phone Phone { get; set; }
        public string FaxNumber { get; set; }
        public Address Address { get; set; }
    }

    [Serializable]
    public class BillShipper
    {
        public string AccountNumber { get; set; }
    }

    [Serializable]
    public class ShipmentCharge
    {
        public string Type { get; set; }
        public BillShipper BillShipper { get; set; }
    }

    [Serializable]
    public class PaymentInformation
    {
        public ShipmentCharge ShipmentCharge { get; set; }
    }

    [Serializable]
    public class Service
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    [Serializable]
    public class Packaging
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    [Serializable]
    public class UnitOfMeasurement
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    [Serializable]
    public class Dimensions
    {
        public UnitOfMeasurement UnitOfMeasurement { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
    }

    [Serializable]
    public class UnitOfMeasurement2
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    [Serializable]
    public class PackageWeight
    {
        public UnitOfMeasurement2 UnitOfMeasurement { get; set; }
        public string Weight { get; set; }
    }

    [Serializable]
    public class Package
    {
        public string Description { get; set; }
        public Packaging Packaging { get; set; }
        public Dimensions Dimensions { get; set; }
        public PackageWeight PackageWeight { get; set; }
        public Reference_Number ReferenceNumber { get; set; }
        public PackageServiceOptions PackageServiceOptions { get; set; }
    }

    public class Reference_Number
    {
        public string ReferenceNumber { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }

    public class PackageServiceOptions
    {
        public Declared_Value DeclaredValue { get; set; }
    }
    public class Declared_Value
    {
        public string CurrencyCode { get; set; }
        public string MonetaryValue { get; set; }
    }

    [Serializable]
    public class Shipment
    {
        public string Description { get; set; }
        public Shipper Shipper { get; set; }
        public ShipTo ShipTo { get; set; }
        public ShipFrom ShipFrom { get; set; }
        public PaymentInformation PaymentInformation { get; set; }
        public ShipmentRatingOptions ShipmentRatingOptions { get; set; }
        public Service Service { get; set; }
        public Package Package { get; set; }
    }

    [Serializable]
    public class ShipmentRatingOptions
    {
        public string NegotiatedRatesIndicator { get; set; }
    }

    [Serializable]
    public class LabelImageFormat
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    [Serializable]
    public class LabelSpecification
    {
        public LabelImageFormat LabelImageFormat { get; set; }
        public string HTTPUserAgent { get; set; }
    }

    [Serializable]
    public class ShipmentRequest
    {
        public Request Request { get; set; }
        public Shipment Shipment { get; set; }
        public LabelSpecification LabelSpecification { get; set; }
        public ReturnService ReturnService { get; set; }
    }

    [Serializable]
    public class ShippingLabelRequest
    {
        public UPSSecurity UPSSecurity { get; set; }
        public ShipmentRequest ShipmentRequest { get; set; }
    }

    [Serializable]
    public class RequestShippingLabel
    {
        public ShippingLabelRequest ShippingLabelRequest { get; set; }
    }

    [Serializable]
    public class ShipmentResponseObj
    {
        public ShipmentResponse ShipmentResponse { get; set; }
    }

    public class ShipmentResponse
    {
        public Response Response { get; set; }
        public ShipmentResults ShipmentResults { get; set; }
    }

    [Serializable]
    public class Response
    {
        public ResponseStatus ResponseStatus { get; set; }
        public TransactionReference TransactionReference { get; set; }
    }

    [Serializable]
    public class ResponseStatus
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    [Serializable]
    public class ShipmentResults
    {
        public ShipmentCharges ShipmentCharges { get; set; }
        public NegotiatedRateCharges NegotiatedRateCharges { get; set; }
        public BillingWeight BillingWeight { get; set; }
        public string ShipmentIdentificationNumber { get; set; }
        public PackageResults PackageResults { get; set; }
    }

    [Serializable]
    public class ShipmentCharges
    {
        public Charges TransportationCharges { get; set; }
        public Charges ServiceOptionsCharges { get; set; }
        public Charges TotalCharges { get; set; }
    }

    [Serializable]
    public class NegotiatedRateCharges
    {
        public Charges TotalCharge { get; set; }
    }

    [Serializable]
    public class Charges
    {
        public string CurrencyCode { get; set; }
        public string MonetaryValue { get; set; }
    }

    [Serializable]
    public class BillingWeight
    {
        public UnitOfMeasurement UnitOfMeasurement { get; set; }
        public string Weight { get; set; }
    }

    [Serializable]
    public class PackageResults
    {
        public string TrackingNumber { get; set; }
        public Charges ServiceOptionsCharges { get; set; }
        public ShippingLabel ShippingLabel { get; set; }
    }

    [Serializable]
    public class ShippingLabel
    {
        public ImageFormat ImageFormat { get; set; }
        public string GraphicImage { get; set; }
        public string HTMLImage { get; set; }
    }

    [Serializable]
    public class ImageFormat
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    [Serializable]
    public class ReturnService
    {
        public string Code { get; set; }
    }

    #endregion
}

