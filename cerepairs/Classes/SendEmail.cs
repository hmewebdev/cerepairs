﻿using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using hmeUtil;
using System.IO;

namespace cerepairs
{
    public class SendEmail
    {
        #region private void sendenotifymail()
        public static bool sendenotifymail(string imgpath, string viewpath, string header, string message, string url, string linktag, string to, string subject, string siteURL, string siteURLLogin)
        {
            //EmailTemplateModels.renderEmail model = new EmailTemplateModels.renderEmail();
            //model.header1 = header;
            //model.footer1 = "877-731-0334";// ConfigurationManager.AppSettings["FooterPhoneNumber"];
            //model.footer2 = "www.cerepairs.com";// ConfigurationManager.AppSettings["FooterrURL"];
            //model.message = message;
            //model.guid = url;
            //model.email = to;


            dynamic model = new
            {
                email = to,
                header1 = header,
                footer1 = ConfigurationManager.AppSettings["celFooterPhoneNumber"],
                footer2 = ConfigurationManager.AppSettings["celFooterrURL"],
                message = message,
                siteurl = siteURL,
                siteurllogin = siteURLLogin,
                guid = url
            };
            //string viewpath = HttpContext.Current.Server.MapPath(@"~/Views/emailtemplate.cshtml");
            var template = System.IO.File.ReadAllText(viewpath);
            string emailbody = Razor.Parse(template, model);

            RunCompile(viewpath, null, model);
            //var emailbody = RenderRazorViewToString(@"emailtemplate", model);

            email email = new email();
            email.emailaddress emailaddress;
            email.attachment attachment;

            email.from = "no-reply@hme.com";
            email.subject = subject;

            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            //emailaddress.address = "hscott@hme.com";
            emailaddress.address = to;
            email.to.Add(emailaddress);

            #region attachments
            email.attachments = new List<email.attachment>();
            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = imgpath + @"\email_header_cel.png";
            attachment.inline = true;
            attachment.inlineid = "~attachment1~";
            email.attachments.Add(attachment);
            for (int i = 1; i < 10; i++)
            {
                attachment = new email.attachment();
                attachment.type = email.AttachmentType.filePath;
                attachment.filepath = imgpath + @"\PT_MFAN_OuterShadowTL" + i.ToString() + ".png";
                attachment.inline = true;
                attachment.inlineid = "~PT_MFAN_OuterShadowTL" + i.ToString() + "~";
                email.attachments.Add(attachment);

            }
            for (int i = 1; i < 10; i++)
            {
                attachment = new email.attachment();
                attachment.type = email.AttachmentType.filePath;
                attachment.filepath = imgpath + @"\PT_MFAN_OuterShadowTR" + i.ToString() + ".png";
                attachment.inline = true;
                attachment.inlineid = "~PT_MFAN_OuterShadowTR" + i.ToString() + "~";
                email.attachments.Add(attachment);

            }
            for (int i = 1; i < 10; i++)
            {
                attachment = new email.attachment();
                attachment.type = email.AttachmentType.filePath;
                attachment.filepath = imgpath + @"\PT_MFAN_OuterShadowBL" + i.ToString() + ".png";
                attachment.inline = true;
                attachment.inlineid = "~PT_MFAN_OuterShadowBL" + i.ToString() + "~";
                email.attachments.Add(attachment);
            }
            for (int i = 1; i < 10; i++)
            {
                attachment = new email.attachment();
                attachment.type = email.AttachmentType.filePath;
                attachment.filepath = imgpath + @"\PT_MFAN_OuterShadowBR" + i.ToString() + ".png";
                attachment.inline = true;
                attachment.inlineid = "~PT_MFAN_OuterShadowBR" + i.ToString() + "~";
                email.attachments.Add(attachment);
            }

            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = imgpath + @"\R.gif";
            attachment.inline = true;
            attachment.inlineid = "~R~";
            email.attachments.Add(attachment);

            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = imgpath + @"\S.gif";
            attachment.inline = true;
            attachment.inlineid = "~S~";
            email.attachments.Add(attachment);

            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = imgpath + @"\H.gif";
            attachment.inline = true;
            attachment.inlineid = "~H~";
            email.attachments.Add(attachment);


            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = imgpath + @"\PT_MFAN_OuterShadowBottomBR.png";
            attachment.inline = true;
            attachment.inlineid = "~PT_MFAN_OuterShadowBottomBR~";
            email.attachments.Add(attachment);


            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = imgpath + @"\PT_MFAN_OuterShadowBottomBL.png";
            attachment.inline = true;
            attachment.inlineid = "~PT_MFAN_OuterShadowBottomBL~";
            email.attachments.Add(attachment);
            #endregion

            email.body = emailbody;
            bool ret = email.sendemail();
            email.Dispose();
            return ret;
        }
        #endregion

         public static string RunCompile(string viewpath, string templatekey, object model)
        {
            string result = string.Empty;
            var template = System.IO.File.ReadAllText(viewpath);
            if (string.IsNullOrEmpty(templatekey))
            {
                templatekey = Guid.NewGuid().ToString();
            }
            result = Engine.Razor.RunCompile(template, templatekey, null, model);
            //result = Engine.Razor.RunCompile(template, null, model);
            return result;
        }
    }
}