﻿using System;
using System.Runtime.Caching;

namespace qalab
{
    public class RuntimeCaching
    {
        public static int CacheLoginAttempts(string ip)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            int count = 0;
            if (cache[ip] != null)
            {
                count = (int)cache.Get(ip);
                cache.Remove(ip);
            }
            int newcount = count + 1;
            if (newcount < 5)
            {
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddSeconds(30);
                ClearCache(ip);
                cache.Add(ip, newcount, cacheItemPolicy);
                //cache.Insert(ip, newcount, null, DateTime.Now.AddSeconds(30), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            else
            {
                newcount = 99;
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(2);
                ClearCache(ip);
                cache.Add(ip + "delay", newcount, cacheItemPolicy);
                //cache.Insert(ip + "delay", newcount, null, DateTime.Now.AddMinutes(2), System.Web.Caching.Cache.NoSlidingExpiration);

            }
            return newcount;
        }
        public static int GetLoginAttempts(string ip)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[ip] != null)
            {
                return (int)cache.Get(ip);
            }
            else
                return 0;
        }

        public static void ClearCache(string handle)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[handle] != null)
            {
                cache.Remove(handle);
            }
        }
        public static void CacheForgotPassword(string uid)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[uid] != null)
            {
                cache.Remove(uid);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(uid, "0", cacheItemPolicy);
            //cache.Insert(uid, 0, null, DateTime.Now.AddMinutes(120), System.Web.Caching.Cache.NoSlidingExpiration);
        }
        public static bool CacheIsForgotPassword(string uid)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[uid] != null)
            {
                cache.Remove(uid);
                return true;
            }
            return false;
        }

        public static void CacheSecurityToken(string token, string json)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[token] != null)
            {
                //cache.Remove(token);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(token, json, cacheItemPolicy);
            string xx = cache[token].ToString();
        }

        public static string CacheGetSecurityToken(string token)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[token] != null)
            {
                string json = (string)cache.Get(token);
                //cache.Remove(token);
                return json;
            }
            else
                return null;
        }


        public static void CacheSaveCurrency(string id, string currency)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[id] != null)
            {
                //cache.Remove(id);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(id, currency, cacheItemPolicy);
        }
        public static string CacheGetCurrency(string id)
        {
            ObjectCache cache = MemoryCache.Default;
            string currency = string.Empty;
            if (cache[id] != null)
            {
                currency = cache.Get(id).ToString();
                //cache.Remove(id);
                return currency;
            }
            return null;
        }

        public static void CacheSaveIdenity(string id, string idenity)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[id] != null)
            {
                //cache.Remove(id);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(id, idenity, cacheItemPolicy);
        }
        public static string CacheGetIdenity(string id)
        {
            ObjectCache cache = MemoryCache.Default;
            string idenity = string.Empty;
            if (cache[id] != null)
            {
                idenity = cache.Get(id).ToString();
                //cache.Remove(id);
                return idenity;
            }
            return null;
        }

        public static void CacheResponseToken(string token, string json)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[token] != null)
            {
                //cache.Remove(token);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(token, json, cacheItemPolicy);
            string xx = cache[token].ToString();
        }
        public static string CacheGetResponseToken(string token)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[token] != null)
            {
                string json = (string)cache.Get(token);
                //cache.Remove(token);
                return json;
            }
            else
                return null;
        }

        public static void CacheFailureToken(string token, string json)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[token] != null)
            {
                //cache.Remove(token);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(token, json, cacheItemPolicy);
            string xx = cache[token].ToString();
        }
        public static string CacheGetFailureToken(string token)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[token] != null)
            {
                string json = (string)cache.Get(token);
                //cache.Remove(token);
                return json;
            }
            else
                return null;
        }

        public static void CacheInvoiceToken(string token, string json)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[token] != null)
            {
                //cache.Remove(token);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(token, json, cacheItemPolicy);
            string xx = cache[token].ToString();
        }
        public static string CacheGetInvoiceToken(string token)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[token] != null)
            {
                string json = (string)cache.Get(token);
                //cache.Remove(token);
                return json;
            }
            else
                return null;
        }


        public static void CacheButtonCommandsToken(string token, string json)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[token] != null)
            {
                //cache.Remove(token);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(token, json, cacheItemPolicy);
            string xx = cache[token].ToString();
        }
        public static string CacheGetButtonCommandsToken(string token)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[token] != null)
            {
                string json = (string)cache.Get(token);
                //cache.Remove(token);
                return json;
            }
            else
                return null;
        }

        public static void CacheDownloadToken(string token, string json)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[token] != null)
            {
                //cache.Remove(token);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(token, json, cacheItemPolicy);
            string xx = cache[token].ToString();
        }
        public static string CacheGetDownloadToken(string token)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[token] != null)
            {
                string json = (string)cache.Get(token);
                //cache.Remove(token);
                return json;
            }
            else
                return null;
        }

        public static void CacheBillingLogToken(string token, string json)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[token] != null)
            {
                //cache.Remove(token);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(token, json, cacheItemPolicy);
        }
        public static string CacheGetBillingLogToken(string token)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[token] != null)
            {
                string json = (string)cache.Get(token);
                //cache.Remove(token);
                return json;
            }
            else
                return null;
        }

        public static void CacheSession(string token, string json)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[token] != null)
            {
                //cache.Remove(token);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(20);
            cache.Add(token, json, cacheItemPolicy);
        }
        public static string CacheGetSession(string token)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[token] != null)
            {
                string json = (string)cache.Get(token);
                //cache.Remove(token);
                return json;
            }
            else
                return null;
        }

        public static void CacheDupTrans(string token, string json)
        {
            ObjectCache cache = MemoryCache.Default;
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            if (cache[token] != null)
            {
                //cache.Remove(token);
            }
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddMinutes(10);
            cache.Add(token, json, cacheItemPolicy);
        }
        public static string CacheGetDupTrans(string token)
        {
            ObjectCache cache = MemoryCache.Default;
            if (cache[token] != null)
            {
                string json = (string)cache.Get(token);
                //cache.Remove(token);
                return json;
            }
            else
                return null;
        }

    }


}