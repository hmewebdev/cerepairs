﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Web.Script.Serialization;

namespace signalR_Hub
{
    public class cerepairsHub
    {
        [HubName("cerepairsHub")]
        public class ceHub : Hub
        {
            public void Push(string chatid, hubdto dto)
            {
                IHubContext hub = GlobalHost.ConnectionManager.GetHubContext<ceHub>();
                hub.Clients.Client(chatid).broadcastMessage(dto);
                //hub.Clients.All.broadcastMessage(dto);
                //Clients.All.dbMonitor(posttype, table, data);
            }
        }
    }
        public class hubdto
        {
            public string request { get; set; }
            public string chatid { get; set; }
            public string data { get; set; }
            public string paymenttype { get; set; }
            public string paypal { get; set; }
            public string success { get; set; }
            public string status { get; set; }
    }
}