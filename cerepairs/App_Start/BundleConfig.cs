﻿using System.Web;
using System.Web.Optimization;

namespace hme
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/react").Include(
             "~/Scripts/react.js",
             "~/Scripts/react-dom.js",
             "~/Scripts/react-dom-server.js",
             "~/Scripts/react-with-addons.js"
             ));
            bundles.Add(new ScriptBundle("~/bundles/static").Include(
             "~/css/main.css"
             ));

            bundles.Add(new ScriptBundle("~/bundles/cerepair").Include(
            "~/Scripts/vendor.js",
            "~/Scripts/main.js",
            "~/Scripts/cerepairs.js",
            "~/Scripts/alasql.min.js"
            //"~/Scripts/tracking.js"
            //"~/Scripts/popmessage.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/detail").Include(
             "~/Scripts/demo.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery-3.2.1.min.js",
            "~/Scripts/1.11.4_jquery-ui.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            "~/Scripts/bootstrap.js",
            //"~/Scripts/popper.min.js",

            "~/Scripts/respond.js")
            );

            bundles.Add(new StyleBundle("~/Content/css").Include(
            "~/Content/3.3.5_bootstrap.min.css",
            "~/Content/site.css",
            "~/Content/4.7.0-font-awesome.min.css",
            "~/Content/jquery-ui.css",
            "~/Content/cerepairs.css",
            "~/css/style.box.css",
            "~/css/style.cer.css",
            "~/css/style.ddm.css",
            "~/css/ui-custom/jquery-ui.css")
            );

            bundles.Add(new StyleBundle("~/Content/Login").Include(
            "~/Content/3.3.5_bootstrap.min.css",
            "~/Content/4.7.0-font-awesome.min.css",
            "~/Content/jquery-ui.css",
            "~/Content/customerservice/main.css",
            "~/Content/animate.min.css",
            "~/Content/customerservice/dropdown.css?v=4",
            "~/Content/customerservice/jquery.cookieBar.css"
            ));

            bundles.Add(new StyleBundle("~/Content/base").Include(
             //"~/Content/tracking.css",
             //"~/Content/popmessage.css",
             "~/Content/jquery-ui.css"

            ));

            bundles.Add(new StyleBundle("~/css/base").Include(
             "~/css/main.css"
            //"~/Content/popmessage.css"
             ));

            bundles.Add(new ScriptBundle("~/bundles/Login").Include(
            "~/Scripts/jquery-3.2.1.min.js",
            "~/Scripts/1.11.4_jquery-ui.min.js",
            "~/Scripts/customerservice/jquery.simplePagination.js",
            "~/Scripts/customerservice/salvattore.js",
            "~/Scripts/customerservice/main.js",
            "~/Scripts/vendor.js",
            "~/Scripts/main.js",
            "~/Scripts/ceuser.js",
            "~/Scripts/popmessages.js"
            //"~/Scripts/customerservice/1.11.4_jquery-ui.min.js"
            ));

            //BundleTable.EnableOptimizations = true;
        }
    }
}
