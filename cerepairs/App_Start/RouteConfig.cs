﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace hme
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            // To enable route attribute in controllers
            //routes.MapMvcAttributeRoutes();

            //routes.MapRoute(
            //   name: "drive-thru-repair-help-center2",
            //   url: "drive-thru-repair-help-center/Link",
            //   defaults: new { controller = "Home", action = "Index" }                                                                                                   //constraints: new { product = @"\d+" }
            //   );


            routes.MapRoute(
                  name: "InvoicetransactionID",
                  url: "Invoice-transactionID/{transactionID}",
                  defaults: new { controller = "Invoice", action = "InvoiceTransactionID", transactionID = UrlParameter.Optional }
             );
            routes.MapRoute(
                   name: "Invoice-SessionUID",
                   url: "Invoice-SessionUID/{sessionUID}",
                   defaults: new { controller = "Invoice", action = "InvoiceSessionUID", sessionUID = UrlParameter.Optional }
              );
            routes.MapRoute(
                  name: "Catalog",
                  url: "Catalog/{req}/{p1}/{p2}/{p3}",
                  defaults: new { controller = "Catalog", action = "Catitems", p1 = UrlParameter.Optional, p2 = UrlParameter.Optional, p3 = UrlParameter.Optional }
             );
            routes.MapRoute(
                   name: "Search",
                   url: "Search/{req}/{p1}/{p2}/{p3}",
                   defaults: new { controller = "Search", action = "Search", p1 = UrlParameter.Optional, p2 = UrlParameter.Optional, p3 = UrlParameter.Optional }
              );

            routes.MapRoute(
                 name: "ProductDefault",
                 url: "Product",
                 defaults: new { controller = "Product", action = "Index" }
            );

            routes.MapRoute(
             name: "Products",
             url: "Product/{pmu}/Products/{p1}/{p2}/{p3}",
             defaults: new { controller = "Product", action = "ManufacturerProducts", pmu = UrlParameter.Optional, p1 = UrlParameter.Optional, p2 = UrlParameter.Optional, p3 = UrlParameter.Optional }
             );

            routes.MapRoute(
            name: "ProductSystems",
            url: "Product/Systems/{pmu}",
            defaults: new { controller = "Product", action = "ProductSystems", pmu = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "Product",
             url: "Product/{ptu}/{p1}/{p2}/{p3}",
             defaults: new { controller = "Product", action = "Products", ptu = UrlParameter.Optional, p1 = UrlParameter.Optional, p2 = UrlParameter.Optional, p3 = UrlParameter.Optional }                                                                                                      //constraints: new { product = @"\d+" }
             );


            //routes.MapRoute(
            // name: "Product2",
            // url: "contact-ce-drive-thru-repairs/{query}",
            // defaults: new { controller = "Product", action = "Products" }
            // );


            routes.MapRoute(
            name: "ProductItem",
            url: "Product/{ptu}/{ptl}",
            defaults: new { controller = "Product", action = "Product", ptu = UrlParameter.Optional, ptl = UrlParameter.Optional }//,
            //constraints: new { product = @"\d+" }
            );

            routes.MapRoute(
             name: "Detail",
             url: "Detail/{pdt}",
             defaults: new { controller = "Detail", action = "Details", pdt = UrlParameter.Optional }                                                                                                     //constraints: new { product = @"\d+" }
             );

            routes.MapRoute(
             name: "SendRepair",
             url: "SendRepair",
             defaults: new { controller = "Home", action = "SendInARepair" }                                                                                                   //constraints: new { product = @"\d+" }
             );
            routes.MapRoute(
             name: "about-ce",
             url: "about-ce",
             defaults: new { controller = "Home", action = "About" }                                                                                                   //constraints: new { product = @"\d+" }
             );

            routes.MapRoute(
             name: "multistorediscount",
             url: "multistorediscount",
             defaults: new { controller = "Home", action = "Multistorediscount" }
             );

            routes.MapRoute(
              name: "bonusbucks",
              url: "bonusbucks",
              defaults: new { controller = "Home", action = "Bonusbucks" }
              );
            routes.MapRoute(
              name: "bonus-bucks",
              url: "bonus-bucks",
              defaults: new { controller = "Home", action = "Bonusbucks" }
              );
            routes.MapRoute(
             name: "about-our-repairs",
             url: "about-our-repairs",
             defaults: new { controller = "Home", action = "OurRepairs" }                                                                                                   //constraints: new { product = @"\d+" }
             );
            routes.MapRoute(
             name: "drive-thru-warranty-and-emas",
             url: "drive-thru-warranty-and-emas",
             defaults: new { controller = "Home", action = "RepairWarranty" }                                                                                                   //constraints: new { product = @"\d+" }
             );
            routes.MapRoute(
            name: "tech-support",
            url: "tech-support",
            defaults: new { controller = "Home", action = "TechSupport" }                                                                                                   //constraints: new { product = @"\d+" }
            );
            routes.MapRoute(
             name: "same-day-service",
             url: "same-day-service",
             defaults: new { controller = "Home", action = "SameDay" }                                                                                                   //constraints: new { product = @"\d+" }
             );
            routes.MapRoute(
             name: "protection-plan",
             url: "protection-plan",
             defaults: new { controller = "Home", action = "Protection_Plan" }                                                                                                   //constraints: new { product = @"\d+" }
             );
            routes.MapRoute(
             name: "ce-ema",
             url: "ce-ema",
             defaults: new { controller = "Home", action = "Protection_Plan" }                                                                                                   //constraints: new { product = @"\d+" }
             );

            routes.MapRoute(
             name: "advance-exchange",
             url: "advance-exchange",
             defaults: new { controller = "Home", action = "Advance_Exchange" }                                                                                                   //constraints: new { product = @"\d+" }
             );
            routes.MapRoute(
                 name: "advanceexchange",
                 url: "advanceexchange",
                 defaults: new { controller = "Home", action = "Advance_Exchange" }                                                                                                   //constraints: new { product = @"\d+" }
                 );
            routes.MapRoute(
                  name: "usermanuals",
                  url: "drive-thru-user-manuals",
                  defaults: new { controller = "Home", action = "usermanuals" }                                                                                                   //constraints: new { product = @"\d+" }
                  );

            routes.MapRoute(
              name: "nationwide-installation",
              url: "nationwide-installation",
              defaults: new { controller = "Home", action = "Nationwide_Installation" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "4-month-warranty",
              url: "4-month-warranty",
              defaults: new { controller = "Home", action = "Four_month_warranty" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "Drive-Thru-Repairs-Frequency-Asked-Questions",
              url: "Drive-Thru-Repairs-Frequency-Asked-Questions",
              defaults: new { controller = "Home", action = "FAQ" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "drive-thru-faqss",
              url: "drive-thru-faq",
              defaults: new { controller = "Home", action = "FAQ" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "drive-thru-faq_s",
              url: "faq",
              defaults: new { controller = "Home", action = "FAQ" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "drive-thru-faq_ss",
              url: "faqs",
              defaults: new { controller = "Home", action = "FAQ" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "Contact",
              url: "Contact",
              defaults: new { controller = "Home", action = "Contact" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "ContactOld",
              url: "contact-ce-drive-thru-repairs",
              defaults: new { controller = "Home", action = "Contact" }                                                                                                   //constraints: new { product = @"\d+" }
              );

            routes.MapRoute(
              name: "Special-Offers",
              url: "Special-Offers",
              defaults: new { controller = "Home", action = "Special_Offers" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "current-customer-specials",
              url: "current-customer-specials",
              defaults: new { controller = "Home", action = "Special_Offers" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "pricematching",
              url: "pricematching",
              defaults: new { controller = "Home", action = "PriceMatching" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "price-matching",
              url: "price-matching",
              defaults: new { controller = "Home", action = "PriceMatching" }                                                                                                   //constraints: new { product = @"\d+" }
              );



            routes.MapRoute(
              name: "free-battery",
              url: "free-battery",
              defaults: new { controller = "Home", action = "freebattery" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
               name: "freebattery",
               url: "freebattery",
               defaults: new { controller = "Home", action = "freebattery" }                                                                                                   //constraints: new { product = @"\d+" }
               );
            routes.MapRoute(
              name: "Send-In-A-Repair",
              url: "Send-In-A-Repair",
              defaults: new { controller = "Home", action = "SendInARepair" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "send-in-a-repair-free-shipping",
              url: "send-in-a-repair-free-shipping",
              defaults: new { controller = "Home", action = "SendInARepair" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "freeshipping",
              url: "freeshipping",
              defaults: new { controller = "Home", action = "SendInARepair" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "Factory-Authorized-Repairs",
              url: "Factory-Authorized-Repairs",
              defaults: new { controller = "Home", action = "FactoryAuthorizedRepairs" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
              name: "Help-Center",
              url: "Help-Center",
              defaults: new { controller = "Home", action = "HelpCenter" }                                                                                                   //constraints: new { product = @"\d+" }
              );
            routes.MapRoute(
               name: "drive-thru-repair-help-center",
               url: "drive-thru-repair-help-center",
               defaults: new { controller = "Home", action = "HelpCenter" }                                                                                                   //constraints: new { product = @"\d+" }
               );
            routes.MapRoute(
              name: "Technical-Support",
              url: "Technical-Support",
              defaults: new { controller = "Home", action = "TechnicalSupport" }                                                                                                   //constraints: new { product = @"\d+" }
              );

            routes.MapRoute(
               name: "Survey",
               url: "Survey",
               defaults: new { controller = "Home", action = "Survey" }                                                                                                   //constraints: new { product = @"\d+" }
               );

            routes.MapRoute(
                name: "Feedback",
                url: "feedback",
                defaults: new { controller = "Home", action = "Survey" }                                                                                                   //constraints: new { product = @"\d+" }
                );

            routes.MapRoute(
               name: "CustomerSurvey",
               url: "CustomerSurvey",
               defaults: new { controller = "Home", action = "Survey" }                                                                                                   //constraints: new { product = @"\d+" }
               );


            routes.MapRoute(
                name: "Privacy",
                url: "Privacy",
                defaults: new { controller = "Home", action = "Privacy" }                                                                                                   //constraints: new { product = @"\d+" }
                );
            routes.MapRoute(
                name: "eos-hd",
                url: "eos-hd",
                defaults: new { controller = "Home", action = "eos_hd" }                                                                                                   //constraints: new { product = @"\d+" }
                );
            //routes.MapRoute(
            //    name: "ce-ema",
            //    url: "ce-ema",
            //    defaults: new { controller = "Home", action = "ce_ema" }                                                                                                   //constraints: new { product = @"\d+" }
            //    );
            routes.MapRoute(
                name: "cookies-policy",
                url: "cookies-policy",
                defaults: new { controller = "Home", action = "cookies_policy" }                                                                                                   //constraints: new { product = @"\d+" }
                );
            routes.MapRoute(
                name: "half-off-aio-repairs",
                url: "half-off-aio-repairs",
                defaults: new { controller = "Home", action = "half_off_aio_repairs" }                                                                                                   //constraints: new { product = @"\d +" }
                );
 
            routes.MapRoute(
                name: "new-customer-specials",
                url: "new-customer-specials",
                defaults: new
                {
                    controller = "Home",
                    action = "new_customer_specials" }                                                                                                   //constraints: new { product = @"\d + " }
                );

            routes.MapRoute(
                name: "current_customer_specials",
                url: "current_customer_specials",
                defaults: new
                {
                    controller = "Home",
                    action = "current-customer-specials"
                }                                                                                                   //constraints: new { product = @"\d + " }

            );
            routes.MapRoute(
                name: "cebatteries",
                url: "cebatteries",
                defaults: new
                {
                    controller = "Home",
                    action = "cebatteries"
                }                                                                                                   //constraints: new { product = @"\d + " }

            );
            routes.MapRoute(
              name: "testpage",
              url: "testpage",
              defaults: new { controller = "Home", action = "testpage" }
              );

            routes.MapRoute(
                 name: "free-headset-repair",
                 url: "free-headset-repair",
                 defaults: new
                 {
                     controller = "Home",
                     action = "free_headset_repair"
                 }                                                                                                  

             );
            routes.MapRoute("Pages", "{type}/{id}",
                 new { controller = "Home", action = "Page", id = UrlParameter.Optional },
                 new RouteValueDictionary
                 {{ "type", ConfigurationManager.AppSettings["pages"]}
            });

            //routes.MapRoute(
            //     name: "DefaultPages",
            //     url: "{page}",
            //     defaults: new { controller = "Home", action = "Page", page = UrlParameter.Optional }
            // );

            //routes.MapRoute(
            //     name: "Default",
            //     url: "{controller}/{action}/{id}",
            //     defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            // );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}/{displayType}/{brand}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, displayType = UrlParameter.Optional, brand = UrlParameter.Optional }
            );


        }
    }
}
