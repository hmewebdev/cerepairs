﻿using cerepairs.Classes;
using HME.SQLSERVER.DAL;
using hmeUtil;
using qalab;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.DirectoryServices;


namespace hme.Models
{
    public class AccountViewModels
    {
        public string User_Tbl_Update(RegisterViewModel model)
        {
            RegisterViewModel_List registerViewModel_List = new RegisterViewModel_List();
            registerViewModel_List.ViewModel_List = new List<RegisterViewModel>();
            registerViewModel_List.ViewModel_List.Add(model);

            ListToDataTable lsttodt = new ListToDataTable();
            DataTable dt = lsttodt.ToDataTable(registerViewModel_List.ViewModel_List);

            dbSQL axsql = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;

            sp.Add(new SqlParameter("registration", dt));
            sp.Add(new SqlParameter("request", "Insert"));
            SqlParameter status = new SqlParameter("status", SqlDbType.NVarChar, 50);
            status.Direction = ParameterDirection.Output;
            sp.Add(status);
            SqlParameter msg = new SqlParameter("msg", SqlDbType.NVarChar, 250);
            msg.Direction = ParameterDirection.Output;
            sp.Add(msg);

            ds = axsql.executeStoredProcedure("UserUpdate", sp, out sqlMessage);
            axsql.Dispose();

            if (sqlMessage != "OK")// || ds.Tables[0].Rows.Count == 0
            {
                ds.Dispose();
                return sqlMessage;
            }
            if (sqlMessage == "OK")
            {
                if ((string)status.Value != "OK")
                {
                    ds.Dispose();
                    return (string)msg.Value;
                }
            }
            ds.Dispose();
            return sqlMessage;
        }

        public UserLogin User_Login(string emailaddress, string password)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;
            sp.Add(new SqlParameter("emailaddress", emailaddress));
            sp.Add(new SqlParameter("isactive", 1));

            ds = db.executeStoredProcedure("UserLogin", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage == "OK")
            {
                UserLogin userLogin = new UserLogin();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string User_PasswordHash = TokenHash.GenerateSHA512String(password + ds.Tables[0].Rows[0]["User_PasswordSalt"].ToString());
                    for (int i = 1; i <= 2048; i++)
                    {
                        User_PasswordHash = TokenHash.GenerateSHA512String(User_PasswordHash + ds.Tables[0].Rows[0]["User_PasswordSalt"].ToString());
                    }
                    if (User_PasswordHash != ds.Tables[0].Rows[0]["User_PasswordHash"].ToString())
                    {
                        ds.Dispose();
                        return null;
                    }

                    userLogin.User_PasswordSalt = ds.Tables[0].Rows[0]["User_PasswordSalt"].ToString();
                    userLogin.User_PasswordHash = ds.Tables[0].Rows[0]["User_PasswordHash"].ToString();
                    userLogin.User_Cust_ID = ds.Tables[0].Rows[0]["User_Cust_ID"].ToString();
                    userLogin.UserType_ID = ds.Tables[0].Rows[0]["UserType_ID"].ToString();
                    userLogin.TypeRole_ID = ds.Tables[0].Rows[0]["TypeRole_ID"].ToString();
                    userLogin.User_UID = ds.Tables[0].Rows[0]["User_UID"].ToString();
                    userLogin.CN = ds.Tables[0].Rows[0]["User_FirstName"].ToString() + " " + ds.Tables[0].Rows[0]["User_LastName"].ToString();
                    userLogin.SN = ds.Tables[0].Rows[0]["User_LastName"].ToString();
                    userLogin.Givenname = ds.Tables[0].Rows[0]["User_FirstName"].ToString();
                    userLogin.User_Company = ds.Tables[0].Rows[0]["User_Company"].ToString();
                    userLogin.User_CompanyName = ds.Tables[0].Rows[0]["User_CompanyName"].ToString();


                    userLogin.User_Address_Line1 = ds.Tables[0].Rows[0]["User_Address_Line1"].ToString();
                    userLogin.User_Address_Line2 = ds.Tables[0].Rows[0]["User_Address_Line2"].ToString();
                    userLogin.User_Phone_Number = ds.Tables[0].Rows[0]["User_Phone_Number"].ToString();
                    userLogin.User_Locality = ds.Tables[0].Rows[0]["User_Locality"].ToString();
                    userLogin.User_Region = ds.Tables[0].Rows[0]["User_Region"].ToString();
                    userLogin.User_PostCode = ds.Tables[0].Rows[0]["User_PostCode"].ToString();
                    userLogin.User_Country = ds.Tables[0].Rows[0]["User_Country"].ToString();





                    ds.Dispose();
                    return userLogin;
                }
            }
            ds.Dispose();
            return null;
        }

        public string CheckCustomerID(RegisterViewModel model)
        {
            dbSQL avanteDB = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Avante"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;

            sp.Add(new SqlParameter("idlist", model.User_Cust_ID));
            ds = avanteDB.executeStoredProcedure("CheckCustomerID", sp, out sqlMessage);
            avanteDB.Dispose();

            if (sqlMessage != "OK")// || ds.Tables[0].Rows.Count == 0
            {
                ds.Dispose();
                return sqlMessage;
            }
            string[] list = model.User_Cust_ID.Split(',');
            if (ds.Tables[0].Rows.Count != list.Length)
            {
                sqlMessage = "Customer ID Invalid. One or more of the Customer IDs entered are invalid.";
            }
            ds.Dispose();
            return sqlMessage;
        }

        public bool IsEmailAddressInUse(string emailaddress)
        {
            //return false;
            dbSQL axsql = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
            DataSet sql_ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;

            sp.Add(new SqlParameter("emailaddress", emailaddress));
            sql_ds = axsql.executeStoredProcedure("ValidateEmailAddress", sp, out sqlMessage);
            axsql.Dispose();

            if (sqlMessage != "OK" || sql_ds.Tables[0].Rows.Count > 0)
            {
                sql_ds.Dispose();
                return true;
            }
            sql_ds.Dispose();

            DirectoryEntry entry = new DirectoryEntry("LDAP://powdc01.HME.COM", "hscott@hme.com", "January2019!");
            DirectorySearcher ds = new DirectorySearcher(entry);
            ds.SearchRoot = new DirectoryEntry("LDAP://dc=hme,dc=com");
            ds.PropertiesToLoad.Add("cn");
            ds.PropertiesToLoad.Add("department");
            ds.PropertiesToLoad.Add("sn");
            ds.PropertiesToLoad.Add("mail");
            ds.PropertiesToLoad.Add("givenname");
            ds.PropertiesToLoad.Add("SamAccountname");
            ds.PropertiesToLoad.Add("physicalDeliveryOfficeName");
            ds.PropertiesToLoad.Add("memberOf");
            ds.PropertiesToLoad.Add("postalcode");
            ds.PropertiesToLoad.Add("streetaddress");
            ds.PropertiesToLoad.Add("telephonenumber");
            ds.PropertiesToLoad.Add("c");
            ds.PropertiesToLoad.Add("st");
            ds.PropertiesToLoad.Add("l");
            ds.PropertiesToLoad.Add("o");
            ds.PropertiesToLoad.Add("homephone");
            ds.PropertiesToLoad.Add("employeeID");
            ds.PropertiesToLoad.Add("title");
            ds.PropertiesToLoad.Add("company");
            ds.Filter = "(&(objectclass=user)(mail=" + emailaddress + "))";
            SearchResultCollection src = ds.FindAll();

            if (src.Count > 0)
            {
                return true;
            }

            return false;
        }

        public string Get_User_UID(string emailaddress)
        {
            dbSQL axsql = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;

            sp.Add(new SqlParameter("emailaddress", emailaddress));
            ds = axsql.executeStoredProcedure("ValidateEmailAddress", sp, out sqlMessage);
            axsql.Dispose();

            if (sqlMessage == "OK")
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string uid = ds.Tables[0].Rows[0]["user_uid"].ToString();
                    ds.Dispose();
                    return uid;
                }
            }
            ds.Dispose();
            return null;
        }

        public void Pass_Reset_Insert(string resetuid, string useruid, string resetip, string reset_Agent)
        {
            dbSQL axsql = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;
            sp.Add(new SqlParameter("resetuid", resetuid));
            sp.Add(new SqlParameter("useruid", useruid));
            sp.Add(new SqlParameter("resetip", resetip));
            sp.Add(new SqlParameter("resetagent", reset_Agent));
            ds = axsql.executeStoredProcedure("Insert_Pass_Reset", sp, out sqlMessage);
            axsql.Dispose();
            ds.Dispose();
        }

        public string Update_Pass_Reset(string resetuid)
        {
            dbSQL axsql = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;
            sp.Add(new SqlParameter("resetuid", resetuid));
            ds = axsql.executeStoredProcedure("Update_Pass_Reset", sp, out sqlMessage);
            axsql.Dispose();
            if (sqlMessage == "OK")
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string user_uid = ds.Tables[0].Rows[0]["user_uid"].ToString();
                    ds.Dispose();
                    return user_uid;
                }
            }
            ds.Dispose();
            return null;
        }
    }

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel_List
    {
        public List<RegisterViewModel> ViewModel_List { get; set; }
    }

    public class RegisterViewModel
    {
        public string User_ID { get; set; }
        public string User_UID { get; set; }

        [Required]
        [Display(Name = "Customer ID:")]
        public string User_Cust_ID { get; set; }

        public int User_IsVerified { get; set; }
        public int User_IsActive { get; set; }
        public int User_IsDenied { get; set; }

        [Required]
        [Display(Name = "First Name:")]
        public string User_FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name:")]
        public string User_LastName { get; set; }

        [Required]
        [Display(Name = "Email Address:")]
        public string User_EmailAddress { get; set; }

        [Required]
        [Display(Name = "Company Address 1:")]
        public string User_Address_Line1 { get; set; }

        [Display(Name = "Company Address 2:")]
        public string User_Address_Line2 { get; set; }

        [Required]
        [Display(Name = "Phone Number:")]
        public string User_Phone_Number { get; set; }

        [Required]
        [Display(Name = "Company City/Locality:")]
        public string User_Locality { get; set; }

        [Required]
        [Display(Name = "Company State/Region:")]
        public string User_Region { get; set; }

        [Required]
        [Display(Name = "Company Postal Code:")]
        public string User_PostCode { get; set; }

        [Required]
        [Display(Name = "Company Country:")]
        public string User_Country { get; set; }

        [Display(Name = "Company Name:")]
        public string User_Company { get; set; }
        [Required]
        [Display(Name = "User_CompanyName")]
        public string User_CompanyName { get; set; }
        public string User_PasswordHash { get; set; }
        public string User_PasswordQuestion { get; set; }
        public string User_PasswordAnswerHash { get; set; }
        public DateTime User_Created_DTS { get; set; }
        public string User_Created_By { get; set; }

        [Display(Name = "Company Brand:")]
        public string User_Brand { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password:")]
        public string User_Password1 { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password Confirm:")]
        public string User_Password2 { get; set; }
        public string User_PasswordSalt { get; set; }

    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string User_EmailAddress { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class UserLogin
    {
        public string User_UID { get; set; }
        public string User_PasswordHash { get; set; }
        public string User_PasswordSalt { get; set; }
        public string UserType_ID { get; set; }
        public string TypeRole_ID { get; set; }
        public string User_Cust_ID { get; set; }
        public string CN { get; set; }
        public string SN { get; set; }
        public string Givenname { get; set; }
        public string User_Company { get; set; }
        public string User_CompanyName { get; set; }

        public string User_Address_Line1 { get; set; }
        public string User_Address_Line2 { get; set; }
        public string User_Phone_Number { get; set; }
        public string User_Locality { get; set; }
        public string User_Region { get; set; }
        public string User_PostCode { get; set; }
        public string User_Country { get; set; }


    }

    [Serializable]
    public class ldapUsers
    {
        public List<ldapUser> users { get; set; }
        public string status { get; set; }
    }

    [Serializable]
    public class ldapUser
    {
        public string cn { get; set; }
        public string department { get; set; }
        public string sn { get; set; }
        public string mail { get; set; }
        public string givenname { get; set; }
        public string SamAccountname { get; set; }
        public string physicalDeliveryOfficeName { get; set; }
        public List<memberOf> memberOf { get; set; }
        public List<reportto> manager { get; set; }
        public List<employee> directReports { get; set; }
        public List<ldapUser> managersignoff { get; set; }
        public string employeeID { get; set; }
        public string title { get; set; }
        public string company { get; set; }
    }

    [Serializable]
    public class memberOf
    {
        public string memberof { get; set; }
    }

    [Serializable]
    public class employee
    {
        public string name { get; set; }
        public ldapUser info { get; set; }
    }

    [Serializable]
    public class reportto
    {
        public string manager { get; set; }
    }

    [Serializable]
    public class Token
    {
        public string guid { get; set; }
        public string user_uid { get; set; }
        public string ip { get; set; }
        public string pc { get; set; }
        public string ts { get; set; }
        public string email { get; set; }
    }

}
