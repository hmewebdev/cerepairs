﻿using cerepairs.Classes;
using HME.SQLSERVER.DAL;
using qalab;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;


namespace cerepairs.Models
{
    public class FreeShippingModels : BaseModels
    {
        public string UpdateUPS(UPS_Insert list)
        {
            string sqlMessage = string.Empty;
            //DataSet ds;
            ListToDataTable lsttodt = new ListToDataTable();
            DataTable dt = lsttodt.ToDataTable(list.ups);

            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["HMEUPS"]].ConnectionString);
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("dt", dt));
            db.executeStoredProcedure("update_hmeups", sp, out sqlMessage);
            db.Dispose();
            //ds.Dispose();
            return sqlMessage;
        }

        public upsget GetUPS(string tracking_number)
        {
            string sqlMessage = string.Empty;
            DataSet ds;
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["HMEUPS"]].ConnectionString);
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("tracking_number", tracking_number));
            ds = db.executeStoredProcedure("get_hmeups", sp, out sqlMessage);
            db.Dispose();
            upsget upsupdate = new upsget();
            foreach (DataColumn dc in ds.Tables[0].Columns)
            {
                switch (dc.ColumnName.ToString())
                {
                    case "Created_DTS":
                        upsupdate.SetProperty(dc.ColumnName, ds.Tables[0].Rows[0][dc.ColumnName]);
                        break;
                    case "Comments":
                        string xx = ds.Tables[0].Rows[0][dc.ColumnName].ToString();
                        upsupdate.SetProperty("comments", xx);
                        break;
                    default:
                        upsupdate.SetProperty(dc.ColumnName, ds.Tables[0].Rows[0][dc.ColumnName].ToString());
                        break;
                }
            }
            ds.Dispose();
            return upsupdate;
        }

    }

    public class UPS_Insert
    {
        public List<upsupdate> ups { get; set; }
    }

    public class upsupdate
    {
        public string store_name { get; set; }
        public string store_number { get; set; }
        public string contact_name { get; set; }
        public string phone_number { get; set; }
        public string rma_number { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip_code { get; set; }
        public string email { get; set; }
        public string products { get; set; }
        public string product_desc { get; set; }
        public string tracking_number { get; set; }
        public string Packing_Label { get; set; }
        public string HME_Company { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string comments { get; set; }
    }
    public class upsget
    {
        public DateTime Created_DTS { get; set; }
        public string store_name { get; set; }
        public string store_number { get; set; }
        public string contact_name { get; set; }
        public string phone_number { get; set; }
        public string rma_number { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip_code { get; set; }
        public string email { get; set; }
        public string products { get; set; }
        public string product_desc { get; set; }
        public string tracking_number { get; set; }
        public string Packing_Label { get; set; }
        public string HME_Company { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string comments { get; set; }
    }
}