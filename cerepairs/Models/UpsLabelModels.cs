﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ups
{
    public class UpsLabelModels
    {
        public class upsLabelModel
        {
            //[Required]
            //[Display(Name = "First Name")]
            //public string firstname { get; set; }
            public string image { get; set; }
            public string imageb64 { get; set; }
            public string label { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string storename { get; set; }
            public string email { get; set; }
            public string storenumber { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string zipcode { get; set; }
            public string chain { get; set; }
            public string HME_EOS_Battery_Charge { get; set; }
            public string All_in_one_headset { get; set; }
            public string Communicator { get; set; }
            public string Timer_SYS30 { get; set; }
            public string Timer_DASH { get; set; }
            public string Timer_Zoom { get; set; }
            public string Zoom_TSP { get; set; }
            public string Battery_Charger { get; set; }
            public string Other { get; set; }
            public string Weight { get; set; }
            public string destination { get; set; }
            public string rmanumber { get; set; }
            public string attention { get; set; }
            public string products { get; set; }
            public string companyname { get; set; }
            public string country { get; set; }
            public string phonenumber { get; set; }
            public string status { get; set; }
            public string shippingweight { get; set; }
            public string captcharesponse { get; set; }
            public string comments { get; set; }
            public string trackingnumber { get; set; }
        }
    }
}