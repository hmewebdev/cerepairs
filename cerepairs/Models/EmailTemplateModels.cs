﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cerepairs.Models
{
    public class EmailTemplateModels
    {
        public class renderEmail
        {
            public string header1 { get; set; }
            public string header2 { get; set; }
            public string footer1 { get; set; }
            public string footer2 { get; set; }
            public string copyright { get; set; }
            public string url { get; set; }
            public string message { get; set; }
            public string linktag { get; set; }
            public string email { get; set; }
            public string guid { get; set; }
            public List<content> maincontent { get; set; }
        }
        public class content
        {
            public string row { get; set; }
            public int order { get; set; }
        }
    }
}