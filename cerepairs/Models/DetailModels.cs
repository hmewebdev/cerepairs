﻿using cerepairs.Classes;
using HME.SQLSERVER.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using cart.Models;
using System.Text.RegularExpressions;
using System.Net;

namespace cerepairs.DetailsModels
{
    public class DetailModels : BaseModels
    {
        public ProductDetail_dto GetProductDetail(string pdt)
        {
            ProductDetail_dto dto = new ProductDetail_dto();
            dto.productdetails = new List<ProductDetail>();
            dto.relatedproducts = new List<RelatedProducts>();
            dto.recommendedproducts = new List<RecommendedProducts>();
            dto.recentlyviewed = new List<RecentlyViewedProducts>();
            bool byname = pdt.Contains("_") || pdt.Contains("-") ? true : false;
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);


            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter(byname ? "Product_Name" : "Product_UID", pdt));
            sp.Add(new SqlParameter("Product_IsActive", 1));
            if( HttpContext.Current.Request.Cookies["User_Cookie"].Value.ToString() != null)
            {
                string usercookie = HttpContext.Current.Request.Cookies["User_Cookie"].Value.ToString();
                sp.Add(new SqlParameter("@usercookie", usercookie));

            }
            if (HttpContext.Current.Request.Cookies["History_Cookie"].Value.ToString() != null)
            {
                string historycookie = HttpContext.Current.Request.Cookies["History_Cookie"].Value.ToString();
                sp.Add(new SqlParameter("@historycookie", historycookie));

            }
            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("products_select", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage != "OK" || ds.Tables[0].Rows.Count == 0)
            {
                return dto;
            }
        
            ProductDetail productDetail = new ProductDetail();
            foreach (DataColumn dc in ds.Tables[0].Columns)
            {
                switch (dc.ColumnName)
                {
                    case "Product_Warranty":
                        break;
                    default:
                        productDetail.SetProperty(dc.ColumnName, ds.Tables[0].Rows[0][dc.ColumnName].ToString());
                        break;
                }
            }
            dto.productdetails.Add(productDetail);
            dto.productdetails[0].Product_Long_Description = WebUtility.HtmlEncode(dto.productdetails[0].Product_Long_Description);// HttpUtility.HtmlEncode(dto.productdetails[0].Product_Long_Description);
            dto.productdetails[0].Product_Short_Description = WebUtility.HtmlEncode(dto.productdetails[0].Product_Short_Description);// HttpUtility.HtmlEncode(dto.productdetails[0].Product_Short_Description);
            //            dto.productdetails[0].Product_Long_Description = "";

            IList<PropertyInfo> properties = typeof(RelatedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[2].Rows)
            {
                RelatedProducts relatedproducts = new RelatedProducts();
                foreach (var property in properties)
                {
                    relatedproducts.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.relatedproducts.Add(relatedproducts);

            }
            properties = typeof(RecentlyViewedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[3].Rows)
            {
                RecentlyViewedProducts recentlyviewed = new RecentlyViewedProducts();
                foreach (var property in properties)
                {
                    recentlyviewed.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.recentlyviewed.Add(recentlyviewed);
            }
            return dto;
        }

        public void GetProductWarranty(ProductDetail_dto dto)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXWebPortal"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            string sqlMessage = string.Empty;
            sp.Add(new SqlParameter("dataareaid", dto.productdetails[0].Product_Manufacturer_Name));
            sp.Add(new SqlParameter("itemid", dto.productdetails[0].Manufacturer_Part_Number));
            ds = db.executeStoredProcedure("item_warranty ", sp, out sqlMessage);
            db.Dispose();
        }

        public void GetProductDetailByShortDescription(ProductDetail_dto dto)
        {
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("Product_Description", dto.shortdescription));
            sp.Add(new SqlParameter("Product_IsActive", 1));

            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("products_select", sp, out sqlMessage);

            dto.productdetails = new List<ProductDetail>();
            if (sqlMessage == "OK")
            {
                ProductDetail productDetail = new ProductDetail();
                //IList<PropertyInfo> properties = typeof(ProductDetail).GetProperties().ToList();
                //foreach (var property in properties)
                //{
                //    productDetail.SetProperty(property.Name, ds.Tables[0].Rows[0][property.Name].ToString());
                //}
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    productDetail.SetProperty(dc.ColumnName, ds.Tables[0].Rows[0][dc.ColumnName].ToString());
                }

                dto.productdetails.Add(productDetail);

                History_dto historydto = new History_dto();
                historydto.product_uid = dto.productdetails[0].Product_UID;
                historydto.user_cookie = dto.user_cookie;
                historydto.user_sessionuid = dto.user_sessionuid;
                UpdateHistory(historydto);

                //sp = new List<SqlParameter>();
                //sp.Add(new SqlParameter("@History_User_Cookie", dto.user_cookie));
                //sp.Add(new SqlParameter("@History_Session_UID", dto.user_sessionuid));
                //sp.Add(new SqlParameter("@History_Wishlist", "0"));
                //sp.Add(new SqlParameter("@History_Product_UID", dto.productdetails[0].Product_UID));
                //sp.Add(new SqlParameter("@History_User_IP", User_IP));
                //sp.Add(new SqlParameter("@History_User_Agent", User_Agent));
                //sqlMessage = string.Empty;
                //db.executeStoredProcedure("log_history", sp, out sqlMessage);

                dto.productclasses = new List<Product_Class>();
                dto.productclasses = ds.Tables[1].AsEnumerable().Select(m => new Product_Class()
                {
                    ProductClass_ID = Convert.ToString(m["ProductClass_ID"]),
                    ProductClass_UID = Convert.ToString(m["ProductClass_UID"]),
                    ProductClass = Convert.ToString(m["ProductClass"]),
                    ProductClass_Created_DTS = Convert.ToString(m["ProductClass_Created_DTS"]),
                    ProductClass_CreatedBy = Convert.ToString(m["ProductClass_CreatedBy"]),
                }).ToList();
            }

            db.Dispose();
            dto.status = sqlMessage;
        }

        public void UpdateHistory(History_dto dto)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("@History_User_Cookie", dto.user_cookie));
            sp.Add(new SqlParameter("@History_Session_UID", dto.user_sessionuid));
            sp.Add(new SqlParameter("@History_Wishlist", "0"));
            sp.Add(new SqlParameter("@History_Product_UID", dto.product_uid));
            sp.Add(new SqlParameter("@History_User_IP", User_IP));
            sp.Add(new SqlParameter("@History_User_Agent", User_Agent));
            sqlMessage = string.Empty;
            db.executeStoredProcedure("log_history", sp, out sqlMessage);
            db.Dispose();
        }

        public void GetPromo(ProductDetail_dto dto)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("@Product_UID", dto.product_uid));
            sp.Add(new SqlParameter("@User_Cookie", dto.user_cookie));

            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("detail_promo", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage == "OK")
            {
                dto.relatedproducts = new List<RelatedProducts>();
                dto.offers = new List<Offers>();
                dto.recommendedproducts = new List<RecommendedProducts>();

                RelatedProducts relatedProduct;
                //<RelatedProducts> relatedProducts = new List<RelatedProducts>();
                Offers offer;
                //List<Offers> offers = new List<Offers>();
                //ProductDetail productDetail;

                IList<PropertyInfo> properties;
                properties = typeof(RelatedProducts).GetProperties().ToList();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    relatedProduct = new RelatedProducts();
                    foreach (var property in properties)
                    {
                        relatedProduct.SetProperty(property.Name, row[property.Name].ToString());
                    }
                    dto.relatedproducts.Add(relatedProduct);

                }

                properties = typeof(Offers).GetProperties().ToList();
                foreach (DataRow row in ds.Tables[1].Rows)
                {
                    offer = new Offers();
                    foreach (var property in properties)
                    {
                        offer.SetProperty(property.Name, row[property.Name].ToString());
                    }
                    dto.offers.Add(offer);

                }

                properties = typeof(RecommendedProducts).GetProperties().ToList();
                foreach (DataRow row in ds.Tables[2].Rows)
                {
                    RecommendedProducts recommendedproducts = new RecommendedProducts();
                    foreach (var property in properties)
                    {
                        recommendedproducts.SetProperty(property.Name, row[property.Name].ToString());
                    }
                    dto.recommendedproducts.Add(recommendedproducts);

                }
            }
            dto.status = sqlMessage;
        }

        public ProductDetail_dto GetHistory(string usercookie)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("@usercookie", usercookie));
            sp.Add(new SqlParameter("@Product_IsActive", 1));
            
             sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("GetHistory", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage != "OK" || ds.Tables[0].Rows.Count == 0)
            {
                return null;
            }
            ProductDetail_dto dto = new ProductDetail_dto();
            dto.relatedproducts = new List<RelatedProducts>();
            dto.recommendedproducts = new List<RecommendedProducts>();
            dto.recentlyviewed = new List<RecentlyViewedProducts>();

 
            IList<PropertyInfo> properties = typeof(RelatedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                RelatedProducts relatedproducts = new RelatedProducts();
                foreach (var property in properties)
                {
                    relatedproducts.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.relatedproducts.Add(relatedproducts);

            }
            properties = typeof(RecentlyViewedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[1].Rows)
            {
                RecentlyViewedProducts recentlyviewed = new RecentlyViewedProducts();
                foreach (var property in properties)
                {
                    recentlyviewed.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.recentlyviewed.Add(recentlyviewed);
            }
            ds.Dispose();
            return dto;
        }


        [Serializable]
        public class ProductDetail_dto
        {
            public List<ProductDetail> productdetails { get; set; }
            public List<Product_Class> productclasses { get; set; }
            public List<Offers> offers { get; set; }
            public List<RelatedProducts> relatedproducts { get; set; }
            public List<RecentlyViewedProducts> recentlyviewed { get; set; }
            public List<RecommendedProducts> recommendedproducts { get; set; }
            public string shortdescription { get; set; }
            public string product_uid { get; set; }
            public string user_cookie { get; set; }
            public string history_cookie { get; set; }
            public string user_sessionuid { get; set; }
            public string status { get; set; }
            public List<CartDTO> cartitems { get; set; }

        }

        [Serializable]
        public class History_dto
        {
            public string product_uid { get; set; }
            public string user_cookie { get; set; }
            public string user_sessionuid { get; set; }
        }


        [Serializable]
        public class ProductDetail
        {
            public string Product_ID { get; set; }
            public string Product_UID { get; set; }
            public string Product_IsActive { get; set; }
            public string Product_Class_ID { get; set; }
            public string Product_Short_Description { get; set; }
            public string Product_Long_Description { get; set; }
            public string Product_Image_Full { get; set; }
            public string Product_Image_Med { get; set; }
            public string Product_Image_Thum { get; set; }
            public string Supplier_Part_Number { get; set; }
            public string Manufacturer_Part_Number { get; set; }
            public string Manufacturer_ID { get; set; }
            public string Currency_Code { get; set; }
            public string Repair_Price { get; set; }
            public string Repair_Price_High { get; set; }
            public string Exchange_Price { get; set; }
            public string Exchange_Price_High { get; set; }
            public string Recondition_Price { get; set; }
            public string Recondition_Price_High { get; set; }
            public string Refurbished_Price { get; set; }
            public string Retail_Price { get; set; }
            public string Additional_Freight { get; set; }
            public string UOM { get; set; }
            public string UNSPSC { get; set; }
            public string Product_LastMod_DTS { get; set; }
            public string Product_Created_DTS { get; set; }
            public string Product_CreatedBy { get; set; }
            public string thisCategory { get; set; }
            public string ProductClass { get; set; }
            public string Product_Name { get; set; }
            public string Product_Warranty { get; set; }
            public string Product_Manufacturer_Name { get; set; }

            public string h1 { get; set; }
            public string title { get; set; }
            public string metaDescription { get; set; }
            public string metaKeywords { get; set; }

        }

        [Serializable]
        public class Product_Class
        {
            public string ProductClass_ID { get; set; }
            public string ProductClass_UID { get; set; }
            public string ProductClass { get; set; }
            public string ProductClass_Created_DTS { get; set; }
            public string ProductClass_CreatedBy { get; set; }
        }

        public class Offers
        {
            public string Offer_ID { get; set; }
            public string Offer_UID { get; set; }
            public string Offer_IsActive { get; set; }
            public string Offer_ValidFrom { get; set; }
            public string Offer_ValidTo { get; set; }
            public string Offer_Description { get; set; }
            public string Offer_Qty { get; set; }
            public string Offer_DiscountQty { get; set; }
            public string Offer_DiscountAmount { get; set; }
            public string Offer_Type { get; set; }
            public string Offer_Created_DTS { get; set; }
            public string Offer_CreatedBy { get; set; }
            public string Offer_PromoCode { get; set; }
            public string Offer_IsDefault { get; set; }
            public string Offer_PromoPage { get; set; }
        }

        public class RelatedProducts
        {
            public string Product_UID { get; set; }
            public string Product_Image_Thum { get; set; }
            public string Product_Name { get; set; }
            public string Product_Short_Description { get; set; }
            public string Repair_Price { get; set; }
            public string Retail_Price { get; set; }
            public string Refurbished_Price { get; set; }
            public string Exchange_Price { get; set; }
            public string Recondition_Price { get; set; }
        }

        public class RecommendedProducts
        {
            public string Product_UID { get; set; }
            public string Product_Image_Thum { get; set; }
            public string Product_Name { get; set; }
            public string Product_Short_Description { get; set; }
            public string Repair_Price { get; set; }
            public string Retail_Price { get; set; }
            public string Refurbished_Price { get; set; }
            public string Exchange_Price { get; set; }
            public string Recondition_Price { get; set; }
        }

        public class RecentlyViewedProducts
        {
            public string Product_UID { get; set; }
            public string Product_Image_Thum { get; set; }
            public string Product_Name { get; set; }
            public string Product_Short_Description { get; set; }
            public string Repair_Price { get; set; }
            public string Retail_Price { get; set; }
            public string Refurbished_Price { get; set; }
            public string Exchange_Price { get; set; }
            public string Recondition_Price { get; set; }
        }

    }
}