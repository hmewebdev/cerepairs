﻿using cerepairs.Classes;
using HME.SQLSERVER.DAL;
using qalab;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace cerepairs.Models
{
    public class UserManagementModels
    {
        public async Task<UserManagementDTO> UsersSearch(UserManagementDTO dto)
        {
            await Task.Run(() =>
            {
                dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
                DataSet ds;
                List<SqlParameter> sp = new List<SqlParameter>();
                string sqlMessage = string.Empty;
                Type myClassType = dto.usersearchrequest.GetType();
                PropertyInfo[] properties = myClassType.GetProperties();
                object value = string.Empty;
                foreach (PropertyInfo property in properties as IEnumerable)
                {
                    switch (property.Name)
                    {
                        case "count":
                            break;
                        default:
                            value = property.GetValue(dto.usersearchrequest, null);
                            sp.Add(new SqlParameter(property.Name, value));
                            break;
                    }
                }
                SqlParameter count = new SqlParameter("count", SqlDbType.Int);
                count.Direction = ParameterDirection.Output;
                sp.Add(count);
                ds = db.executeStoredProcedure("userlookup", sp, out sqlMessage);
                db.Dispose();

                if (sqlMessage == "OK")
                {
                    dto.usersearchrequest.count = (int)count.Value;
                    dto.usermanagementusers = new List<UserManagementUser>();
                    dto.dropdowns = new List<Dropdowns>();
                    if (ds.Tables.Count > 0)
                    {
                        dto.status = sqlMessage;
                        IList<PropertyInfo> propertiesList;
                        propertiesList = typeof(UserManagementUser).GetProperties().ToList();
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            UserManagementUser record = new UserManagementUser();
                            foreach (var property in propertiesList)
                            {
                                switch (property.Name)
                                {
                                    case "status":
                                    case "message":
                                        break;
                                    case "apply_credit":
                                    //case "apply_deposit":
                                    case "permission_isactive":
                                    case "purchaseorder":
                                        record.SetProperty(property.Name, row[property.Name].ToString() == "True" ? "1" : "0");
                                        break;
                                    default:
                                        record.SetProperty(property.Name, row[property.Name].ToString());
                                        break;
                                }

                            }
                            dto.usermanagementusers.Add(record);
                        }
                    }
                    if (ds.Tables.Count == 2)
                    {
                        IList<PropertyInfo> propertiesList;
                        propertiesList = typeof(Dropdowns).GetProperties().ToList();
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            Dropdowns record = new Dropdowns();
                            foreach (var property in propertiesList)
                            {
                                switch (property.Name)
                                {
                                    case "key":
                                    case "isactive":
                                        break;
                                    default:
                                        record.SetProperty(property.Name, row[property.Name].ToString());
                                        break;
                                }
                            }
                            dto.dropdowns.Add(record);
                        }
                    }
                }
                else
                {
                    dto.status = "Failed";
                    dto.message = sqlMessage;
                }
            });
            return dto;
        }

        public async Task<UserManagementUser> SingleUsersSearch(UserSearchRequest searchrequest)
        {
            UserManagementUser record = new UserManagementUser();
            await Task.Run(() =>
            {
                dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
                DataSet ds;
                List<SqlParameter> sp = new List<SqlParameter>();
                string sqlMessage = string.Empty;
                Type myClassType = searchrequest.GetType();
                PropertyInfo[] properties = myClassType.GetProperties();
                object value = string.Empty;
                foreach (PropertyInfo property in properties as IEnumerable)
                {
                    switch (property.Name)
                    {
                        case "count":
                            break;
                        default:
                            value = property.GetValue(searchrequest, null);
                            sp.Add(new SqlParameter(property.Name, value));
                            break;
                    }
                }
                ds = db.executeStoredProcedure("singleuserlookup", sp, out sqlMessage);
                db.Dispose();

                if (sqlMessage == "OK")
                {
                    if (ds.Tables.Count > 0)
                    {
                        IList<PropertyInfo> propertiesList;
                        propertiesList = typeof(UserManagementUser).GetProperties().ToList();
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            foreach (var property in propertiesList)
                            {
                                switch (property.Name)
                                {
                                    case "status":
                                    case "message":
                                    case "user_companies":
                                    case "user_select_apps":
                                    case "apply_credit":
                                    case "apply_deposit":
                                    case "user_type":
                                    case "permission_isactive":
                                    case "po_id":
                                    case "purchaseorder":
                                        break;
                                    default:
                                        record.SetProperty(property.Name, row[property.Name].ToString());
                                        break;
                                }

                            }
                        }
                    }
 
                }
            });
            return record;
        }

        public async Task<string> GetUserSalt(string user_uid)
        {
            string salt = null;
            await Task.Run(() =>
            {
                dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
                DataSet ds;
                List<SqlParameter> sp = new List<SqlParameter>();
                string sqlMessage = string.Empty;
                sp.Add(new SqlParameter("user_uid", user_uid));
                ds = db.executeStoredProcedure("GeteuserSalt", sp, out sqlMessage);
                db.Dispose();
                if (sqlMessage == "OK")
                {
                    if (ds.Tables.Count > 0)
                    {
                        salt = ds.Tables[0].Rows[0]["User_PasswordSalt"].ToString();
                        ds.Dispose();
                    }
                }
            });
            return salt;
        }

        public async Task<string> SetNewPassword(string user_uid, string user_password_hash)
        {
            string resp = null;
            await Task.Run(() =>
            {
                dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
                DataSet ds;
                List<SqlParameter> sp = new List<SqlParameter>();
                string sqlMessage = string.Empty;
                sp.Add(new SqlParameter("user_uid", user_uid));
                sp.Add(new SqlParameter("user_password_hash", user_password_hash));
                ds = db.executeStoredProcedure("SetNewPassword", sp, out sqlMessage);
                db.Dispose();
                ds.Dispose();
                if (sqlMessage == "OK")
                {
                    resp = sqlMessage;
                }
            });
            return resp;
        }

        public async Task<UserManagementDTO> PendingUsers(UserManagementDTO dto)
        {
            await Task.Run(() =>
            {
                dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
                DataSet ds;
                List<SqlParameter> sp = new List<SqlParameter>();
                string sqlMessage = string.Empty;
                Type myClassType = dto.usersearchrequest.GetType();
                PropertyInfo[] properties = myClassType.GetProperties();
                object value = string.Empty;
                foreach (PropertyInfo property in properties as IEnumerable)
                {
                    switch (property.Name)
                    {
                        case "count":
                            break;
                        default:
                            value = property.GetValue(dto.usersearchrequest, null);
                            sp.Add(new SqlParameter(property.Name, value));
                            break;
                    }
                }
                SqlParameter count = new SqlParameter("count", SqlDbType.Int);
                count.Direction = ParameterDirection.Output;
                sp.Add(count);
                ds = db.executeStoredProcedure("userlookup", sp, out sqlMessage);
                db.Dispose();
                if (sqlMessage == "OK")
                {
                    dto.usersearchrequest.count = (int)count.Value;
                    dto.usermanagementusers = new List<UserManagementUser>();
                    dto.dropdowns = new List<Dropdowns>();
                    if (ds.Tables.Count > 0)
                    {
                        dto.status = sqlMessage;
                        IList<PropertyInfo> propertiesList;
                        propertiesList = typeof(UserManagementUser).GetProperties().ToList();
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            UserManagementUser record = new UserManagementUser();
                            foreach (var property in propertiesList)
                            {
                                switch (property.Name)
                                {
                                    case "status":
                                    case "message":
                                        break;
                                    case "apply_credit":
                                    //case "apply_deposit":
                                    case "permission_isactive":
                                    case "purchaseorder":
                                        record.SetProperty(property.Name, row[property.Name].ToString() == "True" ? "1" : "0");
                                        break;
                                    default:
                                        record.SetProperty(property.Name, row[property.Name].ToString());
                                        break;
                                }

                            }
                            dto.usermanagementusers.Add(record);
                        }
                    }
                    if (ds.Tables.Count == 2)
                    {
                        IList<PropertyInfo> propertiesList;
                        propertiesList = typeof(Dropdowns).GetProperties().ToList();
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            Dropdowns record = new Dropdowns();
                            foreach (var property in propertiesList)
                            {
                                switch (property.Name)
                                {
                                    case "key":
                                    case "isactive":
                                        break;
                                    default:
                                        record.SetProperty(property.Name, row[property.Name].ToString());
                                        break;
                                }
                            }
                            dto.dropdowns.Add(record);
                        }
                    }
                }
                else
                {
                    dto.status = "Failed";
                    dto.message = sqlMessage;
                }
            });
            return dto;
        }

        public async Task<UserManagementUser> VerifyUpdate(UserManagementUser dto, string request)
        {
            await Task.Run(() =>
            {
                string sqlMessage = string.Empty;
                dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
                DataSet ds;
                List<SqlParameter> sp = new List<SqlParameter>();
                sp.Add(new SqlParameter("requestype", request));
                sp.Add(new SqlParameter("apply_credit", dto.apply_credit));
                sp.Add(new SqlParameter("apply_deposit", dto.apply_deposit));
                sp.Add(new SqlParameter("user_select_apps", dto.user_select_apps));
                sp.Add(new SqlParameter("user_type", dto.user_type));
                sp.Add(new SqlParameter("user_cust_id", dto.user_cust_id));
                sp.Add(new SqlParameter("user_uid", dto.user_uid));
                sp.Add(new SqlParameter("user_id", dto.user_id));
                sp.Add(new SqlParameter("user_created_by", dto.user_created_by));
                ds = db.executeStoredProcedure("VerifyUpdate", sp, out sqlMessage);
                //dto.user_emailaddress = ds.Tables[0].Rows[0]["user_emailaddress"].ToString();
                db.Dispose();
                if (sqlMessage == "OK")
                {
                    dto.status = "OK";
                }
                else
                {
                    dto.status = "Failed";
                    dto.message = sqlMessage;
                }
            });
            return dto;
        }

        public async Task<UserManagementUser> UserUpdate(UserManagementUser dto, string request)
        {
            await Task.Run(() =>
            {
                string sqlMessage = string.Empty;
                dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["AXsql"]].ConnectionString);
                DataSet ds;
                UserManagementDTO _dto = new UserManagementDTO();
                _dto.usermanagementusers = new List<UserManagementUser>();
                _dto.usermanagementusers.Add(dto);
                ListToDataTable lsttodt = new ListToDataTable();
                DataTable dt = lsttodt.ToDataTable(_dto.usermanagementusers);

                List<SqlParameter> sp = new List<SqlParameter>();
                sp.Add(new SqlParameter("requestype", request));
                sp.Add(new SqlParameter("user", dt));
                ds = db.executeStoredProcedure("UpdateUser", sp, out sqlMessage);
                db.Dispose();
                if (sqlMessage == "OK")
                {
                    dto.status = "OK";
                }
                else
                {
                    dto.status = "Failed";
                    dto.message = sqlMessage;
                }
            });
            return dto;
        }
    }

    [Serializable]
    public class UserManagementDTO
    {
        public UserSearchRequest usersearchrequest { get; set; }
        public List<UserManagementUser> usermanagementusers { get; set; }
        public List<Dropdowns> dropdowns { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }

    [Serializable]
    public class UserManagementUser
    {
        public string user_id { get; set; }
        public string user_uid { get; set; }
        public string user_cust_id { get; set; }
        public string user_isverified { get; set; }
        public string user_isactive { get; set; }
        public string user_isdenied { get; set; }
        public string user_firstname { get; set; }
        public string user_lastname { get; set; }
        public string user_emailaddress { get; set; }
        public string user_address_line1 { get; set; }
        public string user_address_line2 { get; set; }
        public string user_phone_number { get; set; }
        public string user_locality { get; set; }
        public string user_region { get; set; }
        public string user_postcode { get; set; }
        public string user_country { get; set; }
        public string user_company { get; set; }
        public string user_companyname { get; set; }
        public string user_created_dts { get; set; }
        public string user_created_by { get; set; }
        public string user_brand { get; set; }
        //public string company { get; set; }
        public string user_companies { get; set; }
        public string user_select_apps { get; set; }
        public string apply_credit { get; set; }
        public string apply_deposit { get; set; }
        public string user_type { get; set; }
        public string permission_isactive { get; set; }
        public string po_id { get; set; }
        public string purchaseorder { get; set; }
        public string status { get; set; }
        public string message { get; set; }

    }

    [Serializable]
    public class UserSearchRequest
    {
        public int pagenumber { get; set; }
        public int itemsperpage { get; set; }
        public int count { get; set; }
        public string sortcolumn { get; set; }
        public string sortorder { get; set; }
        public string custom { get; set; }
        public string countonly { get; set; }
        public string user_id { get; set; }
        public string user_uid { get; set; }
        public string user_cust_id { get; set; }
        public string user_firstname { get; set; }
        public string user_lastname { get; set; }
        public string user_emailaddress { get; set; }
        public string session_user_emailaddress { get; set; }
        public string user_isverified { get; set; }
        public string user_company { get; set; }
    }

    [Serializable]
    public class Dropdowns
    {
        public string key { get; set; }
        public string id { get; set; }
        public string value { get; set; }
        public string text { get; set; }
        public string usertype_id { get; set; }
        public string company_code { get; set; }
        public string isactive { get; set; }
    }
}