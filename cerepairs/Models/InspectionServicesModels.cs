﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web;
using System.Web.Mvc;
using HME.SQLSERVER.DAL;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;

using hmeUtil;

namespace InspectionServices.Models
{
    public class InspectionServicesModels
    {
        public InspectionServices_dto NewInspectionRequest(InspectionServices_dto dto, string userinfo)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            InspectionServices inspectionServices = new InspectionServices();
            inspectionServices = json.Deserialize<InspectionServices>(dto.jsonobj);
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();

            var table = CreateDataTable(inspectionServices);

            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("@InspectionServices", table));
            SqlParameter idenity = new SqlParameter("@idenity", SqlDbType.Int);
            idenity.Direction = ParameterDirection.Output;
            sp.Add(idenity);

            ds = db.executeStoredProcedure("fai_InspectionServicesInsert", sp, out sqlMessage);
            if (sqlMessage == "OK")
            {
                int key = Convert.ToInt32(idenity.Value);
                dto.fai = key.ToString();
                foreach (var file in dto.files)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        byte[] data;
                        using (Stream inputStream = file.InputStream)
                        {
                            MemoryStream memoryStream = inputStream as MemoryStream;
                            if (memoryStream == null)
                            {
                                memoryStream = new MemoryStream();
                                inputStream.CopyTo(memoryStream);
                            }
                            data = memoryStream.ToArray();
                        }

                        sp = new List<SqlParameter>();
                        sp.Add(new SqlParameter("@file", data));
                        sp.Add(new SqlParameter("@id", key));
                        sp.Add(new SqlParameter("@fileName", file.FileName));
                        sp.Add(new SqlParameter("@prefix", Path.GetExtension(file.FileName)));
                        SqlParameter guid = new SqlParameter("@guid", SqlDbType.NVarChar, 150);
                        guid.Direction = ParameterDirection.Output;
                        sp.Add(guid);
                        sqlMessage = string.Empty;
                        ds = db.executeStoredProcedure("uploadAttachment", sp, out sqlMessage);

                        //file.SaveAs(Path.Combine(Server.MapPath("/uploads"), Guid.NewGuid() + Path.GetExtension(file.FileName)));
                    }
                }

                email email = new email();
                email.emailaddress emailaddress;
                email.from = "no-reply@hme.com";
                email.subject = "Inspection Services Request Submitted";


                email.to = new List<email.emailaddress>();
                emailaddress = new email.emailaddress();
                emailaddress.address = userinfo.Split('|')[0];
                email.to.Add(emailaddress);

                var body = "<table>";

                body += "<tr><td>Inspection Services Request entered by " + userinfo.Split('|')[1] + ". FAI id: " + key.ToString() + "</td></tr>";

                body += "</table>";
                email.body = body;
                bool ret = email.sendemail();
                email.Dispose();


                email = new email();
                email.from = "no-reply@hme.com";
                email.subject = "Inspection Services Request - FAI id:" + key.ToString();

                email.to = new List<email.emailaddress>();
                emailaddress = new email.emailaddress();
                emailaddress.address = "Michael Howard <mhoward@hme.com>";
                email.to.Add(emailaddress);

                email.cc = new List<email.emailaddress>();
                emailaddress = new email.emailaddress();
                emailaddress.address = "David Day <DDay@hme.com>";
                email.cc.Add(emailaddress);


                body = "<table>";

                body += "<tr><td>Inspection Services Request entered by " + userinfo.Split('|')[1] + ". FAI id: " + key.ToString() + "</td></tr>";

                body += "</table>";
                email.body = body;
                ret = email.sendemail();
                email.Dispose();
            }
            db.Dispose();
            return dto;
        }

        private DataTable CreateDataTable(InspectionServices inspectionServices)
        {
            DataTable table = new DataTable();
            table.Columns.Add("requestorname", typeof(string));
            table.Columns.Add("itemnumber", typeof(string));
            table.Columns.Add("revision", typeof(string));
            table.Columns.Add("datereceived", typeof(string));
            table.Columns.Add("needdate", typeof(string));
            table.Columns.Add("project", typeof(string));
            table.Columns.Add("riqtyinspected", typeof(string));
            table.Columns.Add("po", typeof(string));
            table.Columns.Add("qtyprovided", typeof(string));
            table.Columns.Add("rmonumber", typeof(string));
            table.Columns.Add("engineeringwo", typeof(string));
            table.Columns.Add("previousevid", typeof(string));
            table.Columns.Add("limtedduration", typeof(string));
            table.Columns.Add("lotnumberonly", typeof(string));
            table.Columns.Add("scar", typeof(string));
            table.Columns.Add("engineeringwock", typeof(byte));
            table.Columns.Add("previousevidck", typeof(byte));
            table.Columns.Add("scarck", typeof(byte));
            table.Columns.Add("limteddurationck", typeof(byte));
            table.Columns.Add("lotnumberonlyck", typeof(byte));
            table.Columns.Add("pilot", typeof(byte));
            table.Columns.Add("inspectallck", typeof(byte));
            table.Columns.Add("inspectcriticalck", typeof(byte));
            table.Columns.Add("inspectcircledck", typeof(byte));
            table.Columns.Add("oktodestroyck", typeof(byte));
            table.Columns.Add("ingorepkgrohsfitcheckck", typeof(byte));
            table.Columns.Add("ignorenotesck", typeof(byte));
            table.Columns.Add("visualck", typeof(byte));
            table.Columns.Add("sortingonlyck", typeof(byte));
            table.Columns.Add("contactuponreceiptck", typeof(byte));
            table.Columns.Add("removedtsck", typeof(byte));
            table.Columns.Add("description", typeof(string));
            table.Columns.Add("additionalinspectioninstructions", typeof(string));
            table.Columns.Add("reasonforfai", typeof(string));
            table.Columns.Add("faitype", typeof(string));
            table.Columns.Add("failevel", typeof(string));
            table.Columns.Add("distributormanufacturer", typeof(string));
            table.Columns.Add("buyer", typeof(string));
            table.Columns.Add("supplierprovidedreportck", typeof(byte));
            table.Columns.Add("company1", typeof(string));
            table.Columns.Add("reportrequiredck", typeof(string));

            DataRow row = table.NewRow();
            inspectionServices.GetType().GetProperties().ToList().ForEach(f =>
            {
                try
                {
                    row[f.Name] = f.GetValue(inspectionServices, null);
                }
                catch { }
            });
            table.Rows.Add(row);
            return table;
        }

        public Dropdowns Get_Dropdowns()
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            ds = db.executeStoredProcedure("supplier_buyer", out sqlMessage);
            db.Dispose();

            Dropdowns dropdowns = new Dropdowns();

            List<Supplier> suppliers = new List<Supplier>();
            suppliers = ds.Tables[0].AsEnumerable().Select(m => new Supplier()
            {
                supplier = Convert.ToString(m["supplier"]),
            }).ToList();

            List<Buyer> buyers = new List<Buyer>();
            buyers = ds.Tables[1].AsEnumerable().Select(m => new Buyer()
            {
                buyer = Convert.ToString(m["buyer"]),
            }).ToList();

            dropdowns.suppliers = suppliers;
            dropdowns.buyers = buyers;

            return dropdowns;
        }
        [Serializable]
        public class InspectionServices_dto
        {
            public string fai { get; set; }
            public string jsonobj { get; set; }
            public IEnumerable<HttpPostedFileBase> files { get; set; }
        }
        [Serializable]
        public class InspectionServices
        {
            //[Required(ErrorMessage = "File Type required")]
            public string requestorname { get; set; }
            public string itemnumber { get; set; }
            public string revision { get; set; }
            public string datereceived { get; set; }
            public string needdate { get; set; }
            public string project { get; set; }
            public string riqtyinspected { get; set; }
            public string po { get; set; }
            public string qtyprovided { get; set; }
            public string rmonumber { get; set; }
            public string engineeringwo { get; set; }
            public string previousevid { get; set; }
            public string limtedduration { get; set; }
            public string lotnumberonly { get; set; }
            public string scar { get; set; }
            public string engineeringwock { get; set; }
            public string previousevidck { get; set; }
            public string scarck { get; set; }
            public string limteddurationck { get; set; }
            public string lotnumberonlyck { get; set; }
            public string pilot { get; set; }
            public string inspectallck { get; set; }
            public string inspectcriticalck { get; set; }
            public string inspectcircledck { get; set; }
            public string oktodestroyck { get; set; }
            public string ingorepkgrohsfitcheckck { get; set; }
            public string ignorenotesck { get; set; }
            public string visualck { get; set; }
            public string sortingonlyck { get; set; }
            public string contactuponreceiptck { get; set; }
            public string removedtsck { get; set; }
            public string description { get; set; }
            public string additionalinspectioninstructions { get; set; }
            public string company1 { get; set; }
            public string reasonforfai { get; set; }
            public string faitype { get; set; }
            public string failevel { get; set; }
            public string distributormanufacturer { get; set; }
            public string buyer { get; set; }
            public string supplierprovidedreportck { get; set; }
            public string reportrequiredck { get; set; }
            //[Required(ErrorMessage = "file required")]
        }
        public class Supplier
        {
            public string supplier { get; set; }
        }
        public class Buyer
        {
            public string buyer { get; set; }
        }
        public class Dropdowns
        {
            public List<Buyer> buyers { get; set; }
            public List<Supplier> suppliers { get; set; }
        }

    }
}