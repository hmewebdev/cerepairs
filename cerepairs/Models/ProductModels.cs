﻿using cerepairs.Classes;
using HME.SQLSERVER.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;

namespace cerepairs.ProductsModels
{
    public class ProductModels : BaseModels
    {
        public Manufacturer GetManufacturerName(string pmu)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("Manufacturer_IsActive", 1));
            sp.Add(new SqlParameter("Manufacturer_UID", pmu));

            ds = db.executeStoredProcedure("getManufacturerName", sp, out sqlMessage);
            db.Dispose();

            Manufacturer manufacturer = new Manufacturer();

            foreach (DataColumn dc in ds.Tables[0].Columns)
            {
                manufacturer.SetProperty(dc.ColumnName, ds.Tables[0].Rows[0][dc.ColumnName].ToString());
            }

            ds.Dispose();
            return manufacturer;
        }

        public void GetManufacturerProducts(ManufacturerProduct_dto dto)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("Manufacturer_IsActive", 1));
            sp.Add(new SqlParameter("ProductType_IsActive", 1));
            sp.Add(new SqlParameter("Manufacturer_Name", dto.manufacturername));

            ds = db.executeStoredProcedure("getManufacturerProducts", sp, out sqlMessage);
            db.Dispose();
            if (sqlMessage != "OK" || ds.Tables[0].Rows.Count == 0)
            {
            }
            dto.manufacturerProducts = new List<ManufacturerProduct>();
            dto.recommendedproducts = new List<RecommendedProducts>();
            dto.recentlyviewed = new List<RecentlyViewedProducts>();
            dto.relatedproducts = new List<RelatedProducts>();
            dto.manufacturerProducts = ds.Tables[0].AsEnumerable().Select(m => new ManufacturerProduct()
            {
                Manufacturer_Name = Convert.ToString(m["Manufacturer_Name"]),
                Manufacturer_ID = Convert.ToString(m["Manufacturer_ID"]),
                Manufacturer_Part_Number = Convert.ToString(m["Manufacturer_Part_Number"]),
                Product_Long_Description = WebUtility.HtmlEncode(Convert.ToString(m["Product_Long_Description"])),
                Product_Short_Description = HttpUtility.HtmlEncode(Convert.ToString(m["Product_Short_Description"])),
                Product_Image_Full = Convert.ToString(m["Product_Image_Full"]),
                Product_Image_Med = Convert.ToString(m["Product_Image_Med"]),
                Product_Image_Thum = Convert.ToString(m["Product_Image_Thum"]),
                Product_UID = Convert.ToString(m["Product_UID"]),
                Supplier_Part_Number = Convert.ToString(m["Supplier_Part_Number"]),
                Currency_Code = Convert.ToString(m["Currency_Code"]),
                Repair_Price = Math.Round(Convert.ToDecimal(m["Repair_Price"]), 2),
                Repair_Price_High = Math.Round(Convert.ToDecimal(m["Repair_Price_High"]), 2),
                Exchange_Price = Math.Round(Convert.ToDecimal(m["Exchange_Price"]), 2),
                Exchange_Price_High = Math.Round(Convert.ToDecimal(m["Exchange_Price_High"]), 2),
                Recondition_Price = Math.Round(Convert.ToDecimal(m["Recondition_Price"]), 2),
                Recondition_Price_High = Math.Round(Convert.ToDecimal(m["Recondition_Price_High"]), 2),
                Refurbished_Price = Math.Round(Convert.ToDecimal(m["Refurbished_Price"]), 2),
                Retail_Price = Math.Round(Convert.ToDecimal(m["Retail_Price"]), 2),
                Product_Name = Convert.ToString(m["Product_Name"]),
                ProductType = Convert.ToString(m["ProductType"])
            }).ToList();

            //dto.manufacturerProducts = ds.Tables[0].AsEnumerable().Select(m => new ManufacturerProduct()
            //{
            //    Manufacturer_UID = Convert.ToString(m["Manufacturer_UID"]),
            //    Manufacturer_Name = Convert.ToString(m["Manufacturer_Name"]),
            //    Manufacturer_Logo = Convert.ToString(m["Manufacturer_Logo"]),
            //    Manufacturer_ID = Convert.ToString(m["Manufacturer_ID"]),
            //    ProductType = Convert.ToString(m["ProductType"]),
            //    ProductType_Order = Convert.ToString(m["ProductType_Order"]),
            //    ProductType_Image = Convert.ToString(m["ProductType_Image"]),
            //    Product_ID = Convert.ToString(m["Product_ID"]),
            //    Product_UID = Convert.ToString(m["ProductType_UID"]),
            //}).ToList();

            int skipRecords = 2;
            int takeRecords = 5;

            var xx = dto.manufacturerProducts
            .Skip(skipRecords)
            .Take(takeRecords)
            .ToList();

            dto.manufacturerdescription = ds.Tables[1].Rows[0]["Manufacturer_Description"].ToString();

            IList<PropertyInfo> properties = typeof(RelatedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[2].Rows)
            {
                RelatedProducts relatedproducts = new RelatedProducts();
                foreach (var property in properties)
                {
                    relatedproducts.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.relatedproducts.Add(relatedproducts);

            }
            properties = typeof(RecentlyViewedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[3].Rows)
            {
                RecentlyViewedProducts recentlyviewed = new RecentlyViewedProducts();
                foreach (var property in properties)
                {
                    recentlyviewed.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.recentlyviewed.Add(recentlyviewed);
            }
        }

        public void Search(ManufacturerProduct_dto dto, string searchText)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("search", searchText));
            sp.Add(new SqlParameter("isactive", 1));

            ds = db.executeStoredProcedure("Search", sp, out sqlMessage);
            db.Dispose();
            dto.relatedproducts = new List<RelatedProducts>();
            dto.recommendedproducts = new List<RecommendedProducts>();
            dto.recentlyviewed = new List<RecentlyViewedProducts>();
            dto.searchProducts = new List<Search_Products>();

            if (sqlMessage == "OK")
            {
                IList<PropertyInfo> properties = typeof(Search_Products).GetProperties().ToList();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Search_Products searchProduct = new Search_Products();
                    foreach (var property in properties)
                    {
                        switch (property.Name)
                        {
                            case "Product_Long_Description":
                                //searchProduct.SetProperty(property.Name, HttpUtility.HtmlEncode(row[property.Name].ToString()));
                                break;
                            case "Product_Short_Description":
                                searchProduct.SetProperty(property.Name, HttpUtility.HtmlEncode(row[property.Name].ToString()));
                                break;
                            default:
                                searchProduct.SetProperty(property.Name, row[property.Name].ToString());
                                break;
                        }
                    }
                    dto.searchProducts.Add(searchProduct);
                }
            }
        }

        public void SelectCatalog(ManufacturerProduct_dto dto)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("isactive", 1));
            sp.Add(new SqlParameter("category", dto.category));
            sp.Add(new SqlParameter("usercookie", dto.user_cookie));
            sp.Add(new SqlParameter("manufacturername", dto.manufacturername));

            ds = db.executeStoredProcedure("category_select", sp, out sqlMessage);
            db.Dispose();
            if (sqlMessage != "OK" || ds.Tables[0].Rows.Count == 0)
            {
            }
            dto.relatedproducts = new List<RelatedProducts>();
            dto.recommendedproducts = new List<RecommendedProducts>();
            dto.recentlyviewed = new List<RecentlyViewedProducts>();

            dto.manufacturerProducts = new List<ManufacturerProduct>();
            dto.manufacturerProducts = ds.Tables[0].AsEnumerable().Select(m => new ManufacturerProduct()
            {
                Manufacturer_Name = Convert.ToString(m["Manufacturer_Name"]),
                Manufacturer_ID = Convert.ToString(m["Manufacturer_ID"]),
                Manufacturer_Part_Number = Convert.ToString(m["Manufacturer_Part_Number"]),
                //Product_Long_Description = HttpUtility.HtmlEncode(Convert.ToString(m["Product_Long_Description"])),
                Product_Short_Description = HttpUtility.HtmlEncode(Convert.ToString(m["Product_Short_Description"])),
                Product_Image_Full = Convert.ToString(m["Product_Image_Full"]),
                Product_Image_Med = Convert.ToString(m["Product_Image_Med"]),
                Product_Image_Thum = Convert.ToString(m["Product_Image_Thum"]),
                Product_UID = Convert.ToString(m["Product_UID"]),
                Supplier_Part_Number = Convert.ToString(m["Supplier_Part_Number"]),
                Currency_Code = Convert.ToString(m["Currency_Code"]),
                Repair_Price = Math.Round(Convert.ToDecimal(m["Repair_Price"]), 2),
                Repair_Price_High = Math.Round(Convert.ToDecimal(m["Repair_Price_High"]), 2),
                Exchange_Price = Math.Round(Convert.ToDecimal(m["Exchange_Price"]), 2),
                Exchange_Price_High = Math.Round(Convert.ToDecimal(m["Exchange_Price_High"]), 2),
                Recondition_Price = Math.Round(Convert.ToDecimal(m["Recondition_Price"]), 2),
                Recondition_Price_High = Math.Round(Convert.ToDecimal(m["Recondition_Price_High"]), 2),
                Refurbished_Price = Math.Round(Convert.ToDecimal(m["Refurbished_Price"]), 2),
                Retail_Price = Math.Round(Convert.ToDecimal(m["Retail_Price"]), 2),
                Product_Name = Convert.ToString(m["Product_Name"]),
                ProductType = Convert.ToString(m["ProductType"]),
                //OrderBy = Convert.ToInt32(m["OrderBY"])
            }).ToList();

            IList<PropertyInfo> properties = typeof(RelatedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[1].Rows)
            {
                RelatedProducts relatedproducts = new RelatedProducts();
                foreach (var property in properties)
                {
                    relatedproducts.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.relatedproducts.Add(relatedproducts);

            }
            properties = typeof(RecentlyViewedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[2].Rows)
            {
                RecentlyViewedProducts recentlyviewed = new RecentlyViewedProducts();
                foreach (var property in properties)
                {
                    recentlyviewed.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.recentlyviewed.Add(recentlyviewed);
            }

            //int skipRecords = 2;
            //int takeRecords = 5;

            //var xx = dto.manufacturerProducts
            //.Skip(skipRecords)
            //.Take(takeRecords)
            //.ToList();

            //dto.manufacturerdescription = ds.Tables[1].Rows[0]["Manufacturer_Description"].ToString();
        }

        public void GetCatalog(ManufacturerProduct_dto dto)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("Manufacturer_IsActive", 1));
            sp.Add(new SqlParameter("ProductType_IsActive", 1));
            sp.Add(new SqlParameter("Manufacturer_Name", dto.manufacturername));

            ds = db.executeStoredProcedure("GetCatalog", sp, out sqlMessage);
            db.Dispose();
            if (sqlMessage != "OK" || ds.Tables[0].Rows.Count == 0)
            {
            }
            dto.manufacturerProducts = new List<ManufacturerProduct>();
            dto.manufacturerProducts = ds.Tables[0].AsEnumerable().Select(m => new ManufacturerProduct()
            {
                Manufacturer_UID = Convert.ToString(m["Manufacturer_UID"]),
                Manufacturer_Name = Convert.ToString(m["Manufacturer_Name"]),
                Manufacturer_Logo = Convert.ToString(m["Manufacturer_Logo"]),
                Manufacturer_ID = Convert.ToString(m["Manufacturer_ID"]),
                ProductType = Convert.ToString(m["ProductType"]),
                ProductType_Order = Convert.ToString(m["ProductType_Order"]),
                ProductType_Image = Convert.ToString(m["ProductType_Image"])
            }).ToList();

            int skipRecords = 2;
            int takeRecords = 5;

            var xx = dto.manufacturerProducts
            .Skip(skipRecords)
            .Take(takeRecords)
            .ToList();

            //dto.manufacturerdescription = ds.Tables[1].Rows[0]["Manufacturer_Description"].ToString();
        }

        public void ProductType(Product_dto dto, string userCookie)
        {
            db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("ProductType_UID", dto.ProductType_UID));
            sp.Add(new SqlParameter("historycookie", userCookie));
            if (dto.ProductClass_ID != null)
            {
                sp.Add(new SqlParameter("ProductClass_ID", dto.ProductClass_ID));
            }
            ds = db.executeStoredProcedure("getProductType", sp, out sqlMessage);
            db.Dispose();
            if (sqlMessage != "OK" || ds.Tables[0].Rows.Count == 0)
            {
                return;
            }
            dto.product_type = new Product_Type();
            IList<PropertyInfo> properties = typeof(Product_Type).GetProperties().ToList();
            foreach (var property in properties)
            {
                dto.product_type.SetProperty(property.Name, ds.Tables[0].Rows[0][property.Name].ToString());
            }
            if (dto.ProductClass_ID != null && ds.Tables[1].Rows.Count > 0)
            {
                dto.ProductClass = ds.Tables[1].Rows[0]["ProductClass"].ToString();
            }
        }

        public void ProductsByProductType(ManufacturerProduct_dto dto, string historycookie)
        {
            db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("urlProductType", dto.producttype));
            if (dto.ProductClass != null)
            {
                sp.Add(new SqlParameter("ProductClass", dto.ProductClass));
            }
            sp.Add(new SqlParameter("historycookie", historycookie));
            sp.Add(new SqlParameter("Product_IsActive", 1));
            ds = db.executeStoredProcedure("getProductsByProductType", sp, out sqlMessage);
            db.Dispose();
            if (sqlMessage != "OK")
            {
                return;
            }
            ManufacturerProduct product;
            dto.manufacturerProducts = new List<ManufacturerProduct>();
            dto.relatedproducts = new List<RelatedProducts>();
            dto.recommendedproducts = new List<RecommendedProducts>();
            dto.recentlyviewed = new List<RecentlyViewedProducts>();
            dto.productclasses = new List<Product_Class>();
            IList<PropertyInfo> properties;
            properties = typeof(ManufacturerProduct).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                product = new ManufacturerProduct();
                foreach (var property in properties)
                {
                    switch (property.Name)
                    {
                        //case "ProductType":
                        case "ProductType_Order":
                        case "ProductType_Image":
                            break;
                        case "":
                        case "Repair_Price":
                        case "Repair_Price_High":
                        case "Exchange_Price":
                        case "Exchange_Price_High":
                        case "Recondition_Price":
                        case "Recondition_Price_High":
                        case "Refurbished_Price":
                        case "Retail_Price":
                            product.SetProperty(property.Name, Math.Round(Convert.ToDecimal(row[property.Name].ToString()), 2));
                            break;
                        case "Product_Long_Description":
                            product.SetProperty(property.Name, HttpUtility.HtmlEncode(row[property.Name].ToString()));
                            break;
                        case "Product_Short_Description":
                            product.SetProperty(property.Name, HttpUtility.HtmlEncode(row[property.Name].ToString()));
                            break;
                        default:
                            product.SetProperty(property.Name, row[property.Name].ToString());
                            break;
                    }
                }
                dto.manufacturerProducts.Add(product);
            }
            dto.ProductType_Description = ds.Tables[1].Rows[0]["ProductType_Description"].ToString();

            //properties = typeof(Product_Class).GetProperties().ToList();
            //foreach (DataRow row in ds.Tables[3].Rows)
            //{
            //    Product_Class productclass = new Product_Class();
            //    foreach (var property in properties)
            //    {
            //        switch (property.Name)
            //        {
            //            case "ProductClass_Created_DTS":
            //            case "ProductClass_CreatedBy":
            //                break;
            //            default:
            //                productclass.SetProperty(property.Name, row[property.Name].ToString());
            //                break;
            //        }
            //    }
            //    dto.productclasses.Add(productclass);
            //}

            properties = typeof(RelatedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[3].Rows)
            {
                RelatedProducts relatedproducts = new RelatedProducts();
                foreach (var property in properties)
                {
                    relatedproducts.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.relatedproducts.Add(relatedproducts);

            }
            properties = typeof(RecentlyViewedProducts).GetProperties().ToList();
            foreach (DataRow row in ds.Tables[4].Rows)
            {
                RecentlyViewedProducts recentlyviewed = new RecentlyViewedProducts();
                foreach (var property in properties)
                {
                    recentlyviewed.SetProperty(property.Name, row[property.Name].ToString());
                }
                dto.recentlyviewed.Add(recentlyviewed);
            }
        }

        [Serializable]
        public class Product_dto
        {
            public List<ProductByProductType> products { get; set; }
            public string producttype { get; set; }
            public string ProductType_Description { get; set; }
            public string ProductType_UID { get; set; }
            public string ProductClass_ID { get; set; }
            public string ProductClass { get; set; }
            public Product_Type product_type { get; set; }

        }

        [Serializable]
        public class Manufacturer
        {
            public string Manufacturer_ID { get; set; }
            public string Manufacturer_UID { get; set; }
            public string Manufacturer_IsActive { get; set; }
            public string Manufacturer_Name { get; set; }
            public string Manufacturer_Address1 { get; set; }
            public string Manufacturer_Address2 { get; set; }
            public string Manufacturer_City { get; set; }
            public string Manufacturer_State { get; set; }
            public string Manufacturer_County { get; set; }
            public string Manufacturer_Zip { get; set; }
            public string Manufacturer_Zip4 { get; set; }
            public string Manufacturer_Country { get; set; }
            public string Manufacturer_Website { get; set; }
            public string Manufacturer_Email { get; set; }
            public string Manufacturer_Phone { get; set; }
            public string Manufacturer_Fax { get; set; }
            public string Manufacturer_Logo { get; set; }
            public string Manufacturer_Description { get; set; }
            public string Manufacturer_CreatedBy { get; set; }
        }

        [Serializable]
        public class ManufacturerProduct
        {
            public string Manufacturer_UID { get; set; }
            public string Manufacturer_Name { get; set; }
            public string Manufacturer_Logo { get; set; }
            public string Manufacturer_ID { get; set; }
            public string Manufacturer_Part_Number { get; set; }
            public string ProductType { get; set; }
            public string Product_ID { get; set; }
            public string Product_UID { get; set; }
            public string ProductType_UID { get; set; }
            public string ProductType_Order { get; set; }
            public string ProductType_Image { get; set; }
            public string Product_Short_Description { get; set; }
            public string Product_Name { get; set; }
            public string Product_Long_Description { get; set; }
            public string Product_Image_Full { get; set; }
            public string Product_Image_Med { get; set; }
            public string Product_Image_Thum { get; set; }
            public string ProductClass { get; set; }
            public string Supplier_Part_Number { get; set; }
            public string Currency_Code { get; set; }
            public decimal Repair_Price { get; set; }
            public decimal Repair_Price_High { get; set; }
            public decimal Exchange_Price { get; set; }
            public decimal Exchange_Price_High { get; set; }
            public decimal Recondition_Price { get; set; }
            public decimal Recondition_Price_High { get; set; }
            public decimal Refurbished_Price { get; set; }
            public decimal Retail_Price { get; set; }
            //public int OrderBy { get; set; }
        }

        [Serializable]
        public class ManufacturerProduct_dto
        {
            public List<ManufacturerProduct> manufacturerProducts { get; set; }
            public List<Search_Products> searchProducts { get; set; }
            public List<Offers> offers { get; set; }
            public List<RelatedProducts> relatedproducts { get; set; }
            public List<RecentlyViewedProducts> recentlyviewed { get; set; }
            public List<RecommendedProducts> recommendedproducts { get; set; }
            public List<Product_Class> productclasses { get; set; }
            public string manufacturername { get; set; }
            public string manufacturerdescription { get; set; }
            public string category { get; set; }
            public string user_cookie { get; set; }
            public string user_sessionuid { get; set; }
            public string status { get; set; }
            public string producttype { get; set; }
            public string ProductClass { get; set; }
            public string ProductType_Description { get; set; }
        }

        [Serializable]
        public class Product_Type
        {
            public string Product_ID { get; set; }
            public string ProductType_ID { get; set; }
            public string ProductType_UID { get; set; }
            public string ProductCategory_ID { get; set; }
            public string ProductManufacturer_ID { get; set; }
            public string ProductType_IsActive { get; set; }
            public string ProductType { get; set; }
            public string urlProductType { get; set; }
            public string ProductType_Description { get; set; }
            public string ProductType_Image { get; set; }
            public string ProductType_Order { get; set; }
            public string ProductType_LastMod_DTS { get; set; }
            public string ProductType_Created_DTS { get; set; }
            public string ProductType_CreatedBy { get; set; }
        }

        [Serializable]
        public class ProductByProductType
        {
            public string ProductClass { get; set; }
            public string Manufacturer_UID { get; set; }
            public string Manufacturer_Name { get; set; }
            public string Manufacturer_Logo { get; set; }
            public string Product_UID { get; set; }
            public string Product_Short_Description { get; set; }
            public string Supplier_Part_Number { get; set; }
            public string Manufacturer_Part_Number { get; set; }
            public string UOM { get; set; }
            public string Product_Image_Thum { get; set; }
            public string Retail_Price { get; set; }
            public string Exchange_Price { get; set; }
            public string Repair_Price { get; set; }
            public string Refurbished_Price { get; set; }
            public string Product_IsActive { get; set; }
            public string ProductType_UID { get; set; }
            public string thisCategory { get; set; }
        }

        public class Offers
        {
            public string Offer_ID { get; set; }
            public string Offer_UID { get; set; }
            public string Offer_IsActive { get; set; }
            public string Offer_ValidFrom { get; set; }
            public string Offer_ValidTo { get; set; }
            public string Offer_Description { get; set; }
            public string Offer_Qty { get; set; }
            public string Offer_DiscountQty { get; set; }
            public string Offer_DiscountAmount { get; set; }
            public string Offer_Type { get; set; }
            public string Offer_Created_DTS { get; set; }
            public string Offer_CreatedBy { get; set; }
            public string Offer_PromoCode { get; set; }
            public string Offer_IsDefault { get; set; }
            public string Offer_PromoPage { get; set; }
        }

        public class RelatedProducts
        {
            public string Product_UID { get; set; }
            public string Product_Image_Thum { get; set; }
            public string Product_Name { get; set; }
            public string Product_Short_Description { get; set; }
            public string Repair_Price { get; set; }
            public string Retail_Price { get; set; }
            public string Refurbished_Price { get; set; }
            public string Exchange_Price { get; set; }
            public string Recondition_Price { get; set; }
        }

        public class RecommendedProducts
        {
            public string Product_UID { get; set; }
            public string Product_Image_Thum { get; set; }
            public string Product_Name { get; set; }
            public string Product_Short_Description { get; set; }
            public string Repair_Price { get; set; }
            public string Retail_Price { get; set; }
            public string Refurbished_Price { get; set; }
            public string Exchange_Price { get; set; }
            public string Recondition_Price { get; set; }
        }

        public class RecentlyViewedProducts
        {
            public string Product_UID { get; set; }
            public string Product_Image_Thum { get; set; }
            public string Product_Name { get; set; }
            public string Product_Short_Description { get; set; }
            public string Repair_Price { get; set; }
            public string Retail_Price { get; set; }
            public string Refurbished_Price { get; set; }
            public string Exchange_Price { get; set; }
            public string Recondition_Price { get; set; }
        }

        public class Search_Products
        {
            public string Product_ID { get; set; }
            public string Product_UID { get; set; }
            public string Product_IsActive { get; set; }
            public string Product_Class_ID { get; set; }
            public string Product_Short_Description { get; set; }
            public string Product_Long_Description { get; set; }
            public string Product_Image_Full { get; set; }
            public string Product_Image_Med { get; set; }
            public string Product_Image_Thum { get; set; }
            public string Supplier_Part_Number { get; set; }
            public string Manufacturer_Part_Number { get; set; }
            public string Manufacturer_ID { get; set; }
            public string Currency_Code { get; set; }
            public string Repair_Price { get; set; }
            public string Repair_Price_High { get; set; }
            public string Exchange_Price { get; set; }
            public string Exchange_Price_High { get; set; }
            public string Recondition_Price { get; set; }
            public string Recondition_Price_High { get; set; }
            public string Refurbished_Price { get; set; }
            public string Retail_Price { get; set; }
            public string Additional_Freight { get; set; }
            public string UOM { get; set; }
            public string UNSPSC { get; set; }
            //public string Product_LastMod_DTS { get; set; }
            //public string Product_Created_DTS { get; set; }
            //public string Product_CreatedBy { get; set; }
            public string Product_Name { get; set; }
            public string Manufacturer_Name { get; set; }
            public string ProductCategory_ID { get; set; }
            public string ProductCategory { get; set; }
            public string ProductClass { get; set; }
        }

        public class Product_Class
        {
            public string ProductClass_ID { get; set; }
            public string ProductClass_UID { get; set; }
            public string ProductClass { get; set; }
        }
        public class Product_Classes
        {
            public List<Product_Class> Classes { get; set; }
        }

    }
    //public static class Extensions
    //{
    //    public static void SetProperty(this object obj, string propertyName, object value)
    //    {
    //        var propertyInfo = obj.GetType().GetProperty(propertyName);
    //        if (propertyInfo == null) return;
    //        propertyInfo.SetValue(obj, value);
    //    }
    //}
}