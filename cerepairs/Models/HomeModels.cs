﻿using cerepairs.Classes;
using HME.SQLSERVER.DAL;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using hmeUtil;

namespace cerepairs.HomeModel
{
    public class HomeModels: BaseModels
    {
        public Page GetPage(string page_URL)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("Page_URL", page_URL));
            sp.Add(new SqlParameter("Page_IsActive", 1));

            ds = db.executeStoredProcedure("getPage", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage != "OK" || ds.Tables[0].Rows.Count == 0)
            {
                return null;
            }

            Page page = new Page();
            IList<PropertyInfo> properties = typeof(Page).GetProperties().ToList();
            foreach (var property in properties)
            {
                page.SetProperty(property.Name, ds.Tables[0].Rows[0][property.Name].ToString());
            }
            page.Page_Content = page.Page_Content.Replace("http://devcerepairs.hme.com", "").Replace("http://uatcerepairs.hme.com", "").Replace("http://www.cerepairs.com", "");
            return page;
        }

        public string SaveContactInfo(ContactInfoDTO dto,string path,string imagepath)
        {
            //var template = System.IO.File.ReadAllText(path);
            //string content = RunCompile(path, null, dto);
            var template = System.IO.File.ReadAllText(path);
            string content = Razor.Parse(template, dto);

            email email = new email();
            email.emailaddress emailaddress;
            email.attachment attachment;

            email.from = "no-reply@hme.com";
            email.subject = "CE Customer Contact Information";

            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = "info@ceinstl.com";
            //emailaddress.address = to;
            email.to.Add(emailaddress);
            email.attachments = new List<email.attachment>();
            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = imagepath + @"ce_shipping.jpg";
            attachment.inline = true;
            attachment.inlineid = "~attachment1~";
            //email.attachments.Add(attachment);
            email.body = content;
            bool ret = email.sendemail();
            email.Dispose();

            return "OK";
        }

        public string EmailSpecialOffer(SignUpDTO dto, string path, string imagepath)
        {
            //var template = System.IO.File.ReadAllText(path);
            //string content = RunCompile(path, null, dto);
            var template = System.IO.File.ReadAllText(path);
            string content = Razor.Parse(template, dto);

            email email = new email();
            email.emailaddress emailaddress;
            email.attachment attachment;

            email.from = "no-reply@hme.com";
            email.subject = "CE Sign Up for Special Email Offers";

            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = "cespecialoffers@cerepairs.com";
            //emailaddress.address = to;
            email.to.Add(emailaddress);
            email.attachments = new List<email.attachment>();
            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = imagepath + @"ce_shipping.jpg";
            attachment.inline = true;
            attachment.inlineid = "~attachment1~";
            //email.attachments.Add(attachment);
            email.body = content;
            bool ret = email.sendemail();
            email.Dispose();

            return "OK";
        }

        public string Survey(Servey dto)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("chain",dto.chain ));
            sp.Add(new SqlParameter("comments",dto.comments ));
            sp.Add(new SqlParameter("drivethru",dto.drivethru ));
            sp.Add(new SqlParameter("email", dto.email));
            sp.Add(new SqlParameter("expectations",dto.expectations ));
            sp.Add(new SqlParameter("goodvalue", dto.goodvalue));
            sp.Add(new SqlParameter("name", dto.name));
            sp.Add(new SqlParameter("phone", dto.phone));
            sp.Add(new SqlParameter("quickly", dto.quickly));
            sp.Add(new SqlParameter("receivenews", dto.receivenews));
            sp.Add(new SqlParameter("storenumber", dto.storenumber));

            ds = db.executeStoredProcedure("insert_ceSurvey", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage != "OK" || ds.Tables[0].Rows.Count == 0)
            {
                ds.Dispose();
                return null;
            }
            ds.Dispose();
            return sqlMessage;
        }

        public class UserInfo
        {
            public bool isAdmin { get; set; }
            public bool isFullAdmin { get; set; }
            public bool isUser { get; set; }
        }

        public static string RunCompile(string viewpath, string templatekey, object model)
        {
            string result = string.Empty;
            var template = System.IO.File.ReadAllText(viewpath);
            if (string.IsNullOrEmpty(templatekey))
            {
                templatekey = Guid.NewGuid().ToString();
            }
            result = Engine.Razor.RunCompile(template, templatekey, null, model);
            //result = Engine.Razor.RunCompile(template, null, model);
            return result;
        }


    }
    [Serializable]
    public class Servey
    {
        public string chain { get; set; }
        public string comments { get; set; }
        public string drivethru { get; set; }
        public string email { get; set; }
        public string expectations { get; set; }
        public string goodvalue { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string quickly { get; set; }
        public string receivenews { get; set; }
        public string storenumber { get; set; }
        public string results { get; set; }
    }

    [Serializable]
    public class Page
    {
        public string Page_ID { get; set; }
        public string Page_UID { get; set; }
        public string Page_IsActive { get; set; }
        public string Page_Title { get; set; }
        public string Page_Description { get; set; }
        public string Page_Content { get; set; }
        public string Page_Template { get; set; }
        public string Page_BackgroundImage { get; set; }
        public string Page_URL_String { get; set; }
        public string Page_MetaTitle { get; set; }
        public string Page_MetaKeywords { get; set; }
        public string Page_MetaDescription { get; set; }
        public string Page_LastMod_DTS { get; set; }
        public string Page_Created_DTS { get; set; }
        public string Page_CreatedBy { get; set; }
    }

    [Serializable]
    public class ContactInfoDTO
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phonenumber { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string comments { get; set; }
    }

    [Serializable]
    public class SignUpDTO
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
    }
    //public static class Extensions
    //{
    //    public static void SetProperty(this object obj, string propertyName, object value)
    //    {
    //        var propertyInfo = obj.GetType().GetProperty(propertyName);
    //        if (propertyInfo == null) return;
    //        propertyInfo.SetValue(obj, value);
    //    }
    //}
}