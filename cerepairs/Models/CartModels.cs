﻿using cerepairs.Classes;
using HME.SQLSERVER.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;

namespace cart.Models
{
    public class CartModels : BaseModels
    {
        public float AdditionalFreight(string item, int itemCount)
        {
            float tmpAdditionalFreight = 0;
            float AdditionalFreight = 0;
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("Item", item));
            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("GetAdditionalFreight", sp, out sqlMessage);
            db.Dispose();
            if (ds.Tables[0].Rows.Count > 0)
            {
                int multipurchase = (int)ds.Tables[0].Rows[0]["MultiPurchase"];
                float multicharge = float.Parse(ds.Tables[0].Rows[0]["MultiCharge"].ToString());

                tmpAdditionalFreight = float.Parse(ds.Tables[0].Rows[0]["AdditionalCharge"].ToString());
                if (multipurchase > 0 && itemCount > 1)
                {
                    tmpAdditionalFreight += (itemCount - 1) * multicharge;
                }
            }
            ds.Dispose();
            return tmpAdditionalFreight;
        }
        public CartLookup GetCart(string sessionUID, string UserCookie, string Cart_Promo)
        {
            CartLookup cartDTO = new CartLookup();
            CartLookup cartlookup = new CartLookup();
            cartDTO.cartitems = new List<CartDTO>();
            cartDTO.freight = new List<Freight>();
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("sessionUID", sessionUID));
            sp.Add(new SqlParameter("cartUserId", UserCookie));
            sp.Add(new SqlParameter("promocode", Cart_Promo));

            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("getcartitems", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage != "OK" || ds.Tables[0].Rows.Count == 0)
            {
                return cartlookup;
            }
            IList<PropertyInfo> properties = typeof(CartDTO).GetProperties().ToList();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    CartDTO cartitem = new CartDTO();
                    foreach (var property in properties)
                    {
                        switch (property.Name)
                        {
                            case "Cart_ID":
                            case "Cart_Product_QTY":
                            case "Cart_Product_Price_SubTotal":
                            case "Cart_Product_Additional_Freight":
                            case "Cart_Promo_Discount":
                                //case "Cart_DTS":
                                cartitem.SetProperty(property.Name, row[property.Name].ToString());
                                break;
                            default:
                                cartitem.SetProperty(property.Name, row[property.Name]);
                                break;
                        }
                    }
                    cartDTO.cartitems.Add(cartitem);
                }
                cartDTO.totalcartitems = cartDTO.cartitems.Sum(r => (Int32.Parse(r.Cart_Product_QTY) + Int32.Parse(r.Cart_Promo_Discount))).ToString();
                if (ds.Tables[1].Rows.Count > 0)
                {
                    cartDTO.promo = new Promo();
                    cartDTO.promo.promoqty = ds.Tables[1].Rows[0]["Promo_QTY"].ToString();
                    cartDTO.promo.discount = ds.Tables[1].Rows[0]["Discount"].ToString();
                    cartDTO.promo.productqty = ds.Tables[1].Rows[0]["Cart_Product_QTY"].ToString();
                    cartDTO.promo.offerqty = ds.Tables[1].Rows[0]["Offer_Qty"].ToString();
                    cartDTO.promo.fromdate = ds.Tables[1].Rows[0]["Offer_ValidFrom"].ToString();
                    cartDTO.promo.todate = ds.Tables[1].Rows[0]["Offer_ValidTo"].ToString();
                    cartDTO.promo.discription = ds.Tables[1].Rows[0]["Offer_Description"].ToString();
                    cartDTO.promo.discountamount = ds.Tables[1].Rows[0]["Offer_DiscountAmount"].ToString();
                    cartDTO.promo.productprice = ds.Tables[1].Rows[0]["Cart_Product_Price_SubTotal"].ToString();
                    cartDTO.promo.promomessage = ds.Tables[1].Rows[0]["promomessage"].ToString();
                    cartDTO.promo.Cart_Promo_Discount = ds.Tables[1].Rows[0]["Cart_Promo_Discount"].ToString();
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    properties = typeof(Freight).GetProperties().ToList();
                    cartlookup.freight = new List<Freight>();
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        Freight freight = new Freight();
                        foreach (var property in properties)
                        {
                            switch (property.Name)
                            {
                                case "ProductFreight_UID":
                                case "ProductFreight_ID":
                                    freight.SetProperty(property.Name, row[property.Name].ToString());
                                    break;
                                default:
                                    freight.SetProperty(property.Name, Math.Round((decimal)row[property.Name], 2));
                                    break;
                            }
                        }
                        cartDTO.freight.Add(freight);
                    }
                }
            }
            ds.Dispose();
            CartModels model = new CartModels();
            float AdditionalFreight = 0;
            cartDTO.AdditionalFreight = 0;
            foreach (var item in cartDTO.cartitems)
            {
                AdditionalFreight += model.AdditionalFreight(item.Manufacturer_Part_Number, Convert.ToInt32(item.Cart_Product_QTY));
            }
            cartDTO.AdditionalFreight += Convert.ToDecimal(AdditionalFreight);
            return cartDTO;
        }

        public bool UpdateCart(string sessionUID, string UserCookie, AddProductDTO dto)
        {
            List<CartDTO> cartDTO = new List<CartDTO>();
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("User_Session_UID", sessionUID));
            sp.Add(new SqlParameter("User_Cookie", UserCookie));
            sp.Add(new SqlParameter("Cart_Product_UID", dto.productuid));
            sp.Add(new SqlParameter("Cart_Product_QTY", dto.count));
            sp.Add(new SqlParameter("Cart_Product_Price_SubTotal", dto.amount));
            sp.Add(new SqlParameter("Cart_Product_Additional_Freight", dto.freight));
            sp.Add(new SqlParameter("Cart_Product_Price_Type", dto.productpricetype));
            sp.Add(new SqlParameter("Cart_ID", dto.cartid));
            sp.Add(new SqlParameter("request", dto.request));

            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("updatecart", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage != "OK")
            {
                //return null;
            }
            return true;
        }

        public PromoDTO PromoLookupJson(PromoDTO dto, string sessionUID, string UserCookie)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("userssessionuid", sessionUID));
            sp.Add(new SqlParameter("usercookie", dto.usercookie));
            sp.Add(new SqlParameter("prmocode", dto.prmocode));
            sp.Add(new SqlParameter("productuid", dto.pormo_productUID));
            string sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("PromoLookup", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage != "OK")
            {
                //return null;
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                DateTime from = (DateTime)ds.Tables[0].Rows[0]["Offer_ValidFrom"];
                DateTime to = (DateTime)ds.Tables[0].Rows[0]["Offer_ValidTo"];
                DateTime now = DateTime.Now;
                int offerQty = (int)ds.Tables[0].Rows[0]["Offer_Qty"];
                int cartQty = (int)ds.Tables[0].Rows[0]["Cart_Product_QTY"];
                string Cart_Product_UID = ds.Tables[0].Rows[0]["Cart_Product_UID"].ToString();
                dto.offerqty = offerQty.ToString();
                if (now >= from && now <= to)
                {
                    if (cartQty >= offerQty)
                    {
                        dto.message = "OK";
                        dto.promomessage = ds.Tables[0].Rows[0]["Offer_Description"].ToString();
                        int modcount = 0;
                        if (cartQty == (cartQty / offerQty) * offerQty)
                        {
                            modcount = (cartQty / offerQty);
                        }
                        else
                        {
                            modcount = (cartQty / offerQty);
                        }
                        if (modcount > 0)
                        {
                            sp = new List<SqlParameter>();
                            sp.Add(new SqlParameter("userssessionuid", sessionUID));
                            sp.Add(new SqlParameter("@User_Cookie", UserCookie));
                            sp.Add(new SqlParameter("Cart_Product_UID", Cart_Product_UID));
                            sp.Add(new SqlParameter("Cart_Product_QTY", cartQty));
                            sp.Add(new SqlParameter("Cart_Promo_Discount", modcount));
                            ds = db.executeStoredProcedure("updateCartPromo", sp, out sqlMessage);
                            if (sqlMessage == "OK")
                            {
                            }
                            ds.Dispose();
                        }
                    }
                    else
                    {
                        dto.message = "OK";
                        int modcount = cartQty % offerQty;
                        dto.promomessage = "The mininum quantity for the product is not met";
                    }
                }
                else
                {
                    dto.message = "Failed";
                    dto.promomessage = "The promo code has expired";
                }
            }
            else
            {
                dto.message = "Failed";
                dto.promomessage = "The promo code is invalid";
            }
            ds.Dispose();
            return dto;
        }
        public void PromoLookup(string prmocode, string sessionUID, string UserCookie)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("userssessionuid", sessionUID));
            sp.Add(new SqlParameter("usercookie", UserCookie));
            sp.Add(new SqlParameter("prmocode", prmocode));
            string sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("PromoLookup", sp, out sqlMessage);
            db.Dispose();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    DateTime from = (DateTime)row["Offer_ValidFrom"];
                    DateTime to = (DateTime)row["Offer_ValidTo"];
                    DateTime now = DateTime.Now;
                    int offerQty = (int)row["Offer_Qty"];
                    int cartQty = (int)row["Cart_Product_QTY"];
                    string Cart_Product_UID = row["Cart_Product_UID"].ToString();
                    if (now >= from && now <= to)
                    {
                        if (offerQty > 0 && cartQty >= offerQty)
                        {
                            int modcount = 0;
                            if (cartQty == (cartQty / offerQty) * offerQty)
                            {
                                modcount = (cartQty / offerQty);
                            }
                            else
                            {
                                modcount = (cartQty / offerQty);
                            }

                            if (modcount > 0)
                            {
                                sp = new List<SqlParameter>();
                                sp.Add(new SqlParameter("userssessionuid", sessionUID));
                                sp.Add(new SqlParameter("@User_Cookie", UserCookie));
                                sp.Add(new SqlParameter("Cart_Product_UID", Cart_Product_UID));
                                sp.Add(new SqlParameter("Cart_Product_QTY", cartQty));
                                sp.Add(new SqlParameter("Cart_Promo_Discount", modcount));
                                ds = db.executeStoredProcedure("updateCartPromo", sp, out sqlMessage);
                                if (sqlMessage == "OK")
                                {
                                }
                                ds.Dispose();
                            }
                        }
                    }
                }

            }
            ds.Dispose();
        }

        public bool CartUpdate(string sessionUID, string UserCookie, AddProductDTO dto, string Cart_Promo)
        {
            List<CartDTO> cartDTO = new List<CartDTO>();
            PromoDTO promodto = new PromoDTO();
            int modcount = 0;
            int cartQty = 0; Convert.ToInt32(dto.count);
            if (Cart_Promo != null)
            {
                promodto.prmocode = Cart_Promo;
                promodto.pormo_productUID = dto.productuid;
                PromoDTO promo = PromoLookupJson(promodto, sessionUID, UserCookie);
                cartQty = Convert.ToInt32(dto.count);
                int offerQty = Convert.ToInt32(promo.offerqty);
                if (offerQty > 0 && cartQty >= offerQty)
                {
                    if (cartQty == (cartQty / offerQty) * offerQty)
                    {
                        modcount = (cartQty / offerQty);
                    }
                    else
                    {
                        modcount = (cartQty / offerQty);
                    }
                }
            }
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("User_Session_UID", sessionUID));
            sp.Add(new SqlParameter("User_Cookie", UserCookie));
            sp.Add(new SqlParameter("Cart_Product_UID", dto.productuid));
            sp.Add(new SqlParameter("Cart_Product_QTY", (Convert.ToInt32(dto.count)))); /*+ Convert.ToInt32(modcount)).ToString())); */
            sp.Add(new SqlParameter("Cart_Product_Price_SubTotal", dto.amount));
            sp.Add(new SqlParameter("Cart_Product_Additional_Freight", dto.freight));
            sp.Add(new SqlParameter("Cart_Product_Price_Type", dto.productpricetype));
            sp.Add(new SqlParameter("Cart_Promo_Discount", modcount));
            sp.Add(new SqlParameter("Cart_ID", dto.cartid));
            sp.Add(new SqlParameter("request", dto.request));

            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("updatecart", sp, out sqlMessage);
            db.Dispose();

            if (sqlMessage != "OK")
            {
                //return null;
            }
            return true;
        }

    }

    public class AddProductDTO
    {
        public string product { get; set; }
        public string count { get; set; }
        public string category { get; set; }
        public string status { get; set; }
        public string amount { get; set; }
        public string productuid { get; set; }
        public string freight { get; set; }
        public string productpricetype { get; set; }
        public string cartid { get; set; }
        public string request { get; set; }
        public List<CartDTO> cartitems { get; set; }
        public PromoDTO promodto { get; set; }
        public string totalcartitems { get; set; }
    }

    public class CartLookup
    {
        public List<CartDTO> cartitems { get; set; }
        public Promo promo { get; set; }
        public List<Freight> freight { get; set; }
        public string totalcartitems { get; set; }
        public decimal AdditionalFreight { get; set; }
    }
    public class CartDTO
    {
        public string Cart_ID { get; set; }
        public string Cart_Product_UID { get; set; }
        public string Cart_Product_QTY { get; set; }
        public string Cart_Product_Price_SubTotal { get; set; }
        public string Cart_Product_Additional_Freight { get; set; }
        public string Cart_Product_Price_Type { get; set; }
        public string Cart_DTS { get; set; }
        public string Cart_Promo_Discount { get; set; }
        public string Product_Image_Thum { get; set; }
        public string Product_Image_Full { get; set; }
        public string Product_Image_Med { get; set; }
        public string Product_Short_Description { get; set; }
        public string Product_Long_Description { get; set; }
        public string Manufacturer_Part_Number { get; set; }
        public string Supplier_Part_Number { get; set; }
        public string Product_Name { get; set; }
    }

    public class PromoDTO
    {
        public string signalr { get; set; }
        public string usercookie { get; set; }
        public string prmocode { get; set; }
        public string request { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string promomessage { get; set; }
        public string offerqty { get; set; }
        public string newqty { get; set; }
        public string pormo_productUID { get; set; }
        public Promo promo { get; set; }
        public PromoDTO promodto { get; set; }
    }

    public class Promo
    {
        public string promoqty { get; set; }
        public string discount { get; set; }
        public string productqty { get; set; }
        public string prmocode { get; set; }
        public string offerqty { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
        public string discription { get; set; }
        public string discountamount { get; set; }
        public string productprice { get; set; }
        public string promomessage { get; set; }
        public string Cart_Promo_Discount { get; set; }
    }

    public class Freight
    {
        public string ProductFreight_UID { get; set; }
        public string ProductFreight_ID { get; set; }
        public decimal ProductFreight_Low { get; set; }
        public decimal ProductFreight_High { get; set; }
        public decimal ProductFreight { get; set; }
    }

}