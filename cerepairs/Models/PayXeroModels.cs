﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayXero.Models
{
    public class PayXeroModels
    {
        public PayXeroCharge BuildCharge(PayXeroFromResponse response)
        {
            PayXeroCharge PayXeroCharge = new PayXeroCharge();
            PayXeroCharge.order = new orderCharge();
            //PayXeroCharge.order.amount = float.Parse(response.order.amount);
            PayXeroCharge.order.discount = response.order.discount;
            PayXeroCharge.order.shipping = response.order.shipping;
            PayXeroCharge.order.tax = response.order.tax;
            PayXeroCharge.order.poNumber = response.order.poNumber;
            PayXeroCharge.order.amount = response.order.amount;
            PayXeroCharge.amount = response.amount;
            PayXeroCharge.contact = response.contact;
            PayXeroCharge.billingAddress = response.billingAddress;
            PayXeroCharge.shippingAddress = response.shippingAddress;
            PayXeroCharge.capture = true;
            PayXeroCharge.card = new cardCharge();
            PayXeroCharge.card.name = response.card.name;
            //PayXeroCharge.card.paymentToken = "00000000-000000-000000-000000000000";// response.card.paymentToken;
            PayXeroCharge.card.paymentToken = response.card.paymentToken;
            PayXeroCharge.card.number = response.card.number.number;
            PayXeroCharge.sendReceipt = false;
            return PayXeroCharge;
        }
    }
}

public class PayXeroPaymentResp
{
    public string statusCode { get; set; }
    public string timestamp { get; set; }
    public string path { get; set; }
    public PayXeroPaymentErrorResp response { get; set; }
    public string status { get; set; }
    public string message { get; set; }
    public PayXeroPaymentRespData data { get; set; }

    //public class PayXeroPaymentRespData
    //{
    //    public string currency { get; set; }
    //    public string paid { get; set; }
    //    public string amountRefunded { get; set; }
    //    public string surchargeRefunded { get; set; }
    //    public string voidable { get; set; }
    //    public string capturable { get; set; }
    //    public string refundable { get; set; }
    //    public string error { get; set; }
    //    public string transactionErrorMsg { get; set; }
    //    public string sendReceipt { get; set; }
    //    public string settled { get; set; }
    //}

    public class PayXeroPaymentErrorResp
    {
        public string statusCode { get; set; }
        public string message { get; set; }
    }

}

public class PayXeroChargeDataDeclined
{
    public int statusCode { get; set; }
    public DateTime timestamp { get; set; }
    public string path { get; set; }
    public string response { get; set; }
    public int status { get; set; }
    public string message { get; set; }
    public PayXeroChargeResponse data { get; set; }
    public string actionstatus { get; set; }
}
public class PayXeroChargeData
{
    public int statusCode { get; set; }
    public DateTime timestamp { get; set; }
    public string path { get; set; }
    public Response response { get; set; }
    public int status { get; set; }
    public string actionstatus { get; set; }
    public string message { get; set; }
    public string token { get; set; }
    public string currency { get; set; }
    public string paymentstatus { get; set; }
    public string PaymentCreditCardNumber { get; set; }
    public string AdditionalEmails { get; set; }
    public string content { get; set; }
    public PayXeroChargeResponse data { get; set; }
}
public class PayXeroChargeResponse
{
    public string statusCode { get; set; }
    public string id { get; set; }
    public string @object { get; set; }
    public string type { get; set; }
    public string amount { get; set; }
    public string baseAmount { get; set; }
    public string surcharge { get; set; }
    public bool capture { get; set; }
    public string transactionId { get; set; }
    public bool error { get; set; }
    public string createdAt { get; set; }
    public string transactionAt { get; set; }
    public bool settled { get; set; }
    public string device { get; set; }
    public string paymentId { get; set; }
    public string createdBy { get; set; }
    public string updatedBy { get; set; }
    public string confirmationNumber { get; set; }
}

public class ResponseDeclined
{
    public int statusCode { get; set; }
    public DateTime timestamp { get; set; }
    public string path { get; set; }
    public string response { get; set; }
    public int status { get; set; }
    public string message { get; set; }

}
//public class Response
//{
//    public int statusCode { get; set; }
//    public IList<string> message { get; set; }
//    public string error { get; set; }

//}

public class Response
{
    public string code { get; set; }
    public string text { get; set; }

}

public class Dup
{
    public string Amount { get; set; }
    public string CreditCard { get; set; }
    public string date { get; set; }
}
public class ccError
{
    public int statusCode { get; set; }
    public DateTime timestamp { get; set; }
    public string path { get; set; }
    public Response response { get; set; }
    public int status { get; set; }
    public string message { get; set; }

}


public class PayXeroFromResponse
{
    public string clientId { get; set; }
    public double amount { get; set; }
    public double surcharge { get; set; }
    public bool sendReceipt { get; set; }
    //public bool bypassSurcharge { get; set; }
    public contact contact { get; set; }
    public billingAddressPZ billingAddress { get; set; }
    public shippingAddressPZ shippingAddress { get; set; }
    public order order { get; set; }
    public string capture { get; set; }
    public card card { get; set; }
    public string SECURETOKENID { get; set; }
    public string SECURETOKEN { get; set; }
    public string actionstatus { get; set; }
    public string message { get; set; }
}

public class PayXeroCharge
{
    public double amount { get; set; }
    public contact contact { get; set; }
    public billingAddressPZ billingAddress { get; set; }
    public shippingAddressPZ shippingAddress { get; set; }
    public orderCharge order { get; set; }
    public bool capture { get; set; }
    public cardCharge card { get; set; }
    public bool sendReceipt { get; set; }
}
public class contact
{
    public string email { get; set; }
    public string phone { get; set; }
}
public class billingAddressPZ
{
    public string address { get; set; }
    public string city { get; set; }
    public string country { get; set; }
    public string state { get; set; }
    public string zipCode { get; set; }
}
public class shippingAddressPZ
{
    public string address { get; set; }
    public string city { get; set; }
    public string country { get; set; }
    public string state { get; set; }
    public string zipCode { get; set; }
}
public class order
{
    public double amount { get; set; }
    public double shipping { get; set; }
    public double tax { get; set; }
    public string poNumber { get; set; }
    public double discount { get; set; }
}
public class orderCharge
{
    public double amount { get; set; }
    public double shipping { get; set; }
    public double tax { get; set; }
    public string poNumber { get; set; }
    public double discount { get; set; }
}
public class card
{
    public string name { get; set; }
    public string zipCode { get; set; }
    public string paymentToken { get; set; }
    public cnumber number { get; set; }
}
public class cardCharge
{
    public string name { get; set; }
    public string paymentToken { get; set; }
    public string number { get; set; }
}
public class cnumber
{
    public string number { get; set; }
    public string bin { get; set; }
    public string exp { get; set; }
    public string hash { get; set; }
    public string type { get; set; }
}

public class CreditCardRate
{
    public int statusCode { get; set; }
    public DateTime timestamp { get; set; }
    public string path { get; set; }
    public Response response { get; set; }
    public int status { get; set; }
    public string actionstatus { get; set; }
    public string message { get; set; }
    public ratedata data { get; set; }
    public string jsonData { get; set; }
}
public class CreditCardRateDTO
{
    public ratedata data { get; set; }
    public string jsonData { get; set; }
}
public class ratedata
{
    public string clientId { get; set; }
    public int amount { get; set; }
    public string creditRate { get; set; }
    public int debitRate { get; set; }
    public string brand { get; set; }
    public string cardType { get; set; }
    public int totalAmount { get; set; }
    public int surcharge { get; set; }
    public string formattotalAmount { get; set; }
    public string formatsurcharge { get; set; }
    public string formatamount { get; set; }

}


public class Card
{
    public string vaultId { get; set; }
    public string name { get; set; }
    public string number { get; set; }
    public string network { get; set; }
    public string exp { get; set; }
    public string paymentToken { get; set; }
    public string type { get; set; }
    public string last4 { get; set; }

}
public class Contact
{
    public string phone { get; set; }
    public string email { get; set; }
}
public class CustomValues
{
    public string value { get; set; }
    public string key { get; set; }
    public string label { get; set; }
}
public class Order
{
    public int shipping { get; set; }
    public int federalTaxes { get; set; }
    public double amount { get; set; }
    public int tax { get; set; }
    public int discount { get; set; }
    //public IList<undefined> products { get; set; }
    public IList<CustomValues> customValues { get; set; }

}
public class BillingAddressPZ
{
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string company { get; set; }
    public string country { get; set; }
    public string address { get; set; }
    public string address2 { get; set; }
    public string city { get; set; }
    public string state { get; set; }
    public string zipCode { get; set; }
    public string phoneNumber { get; set; }
    public string email { get; set; }

}
public class ShippingAddressPZ
{
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string company { get; set; }
    public string country { get; set; }
    public string address { get; set; }
    public string address2 { get; set; }
    public string city { get; set; }
    public string state { get; set; }
    public string zipCode { get; set; }
    public string phoneNumber { get; set; }
    public string email { get; set; }

}
public class SettlementInfo
{
    public DateTime date { get; set; }
    public string batchId { get; set; }
    public string processorBatchId { get; set; }

}
public class Transactions
{
    public bool settled { get; set; }
    public DateTime createdAt { get; set; }
    public DateTime updatedAt { get; set; }
    public string _id { get; set; }
    public string id { get; set; }
    public string @object { get; set; }
    public string type { get; set; }
    public int amount { get; set; }
    public int baseAmount { get; set; }
    public int surcharge { get; set; }
    public string currency { get; set; }
    public string captured { get; set; }
    public string transactionId { get; set; }
    public bool error { get; set; }
    public string userAgent { get; set; }
    public string device { get; set; }
    public string paymentId { get; set; }
    public string createdBy { get; set; }
    public string updatedBy { get; set; }
    public string owner { get; set; }
    public string ownerGroup { get; set; }
    public SettlementInfo settlementInfo { get; set; }

}
public class PayXeroPaymentRespData
{
    public string currency { get; set; }
    public bool paid { get; set; }
    public int amountRefunded { get; set; }
    public int surchargeRefunded { get; set; }
    public bool voidable { get; set; }
    public bool capturable { get; set; }
    public bool refundable { get; set; }
    public bool error { get; set; }
    public string transactionErrorMsg { get; set; }
    public IList<string> statusList { get; set; }
    public bool sendReceipt { get; set; }
    public bool settled { get; set; }
    public string clientId { get; set; }
    public string clientName { get; set; }
    public Card card { get; set; }
    public Contact contact { get; set; }
    public int amount { get; set; }
    public int surcharge { get; set; }
    public double percent { get; set; }
    public int paidAmount { get; set; }
    public int paidAmountSurcharge { get; set; }
    public string status { get; set; }
    public Order order { get; set; }
    public BillingAddressPZ billingAddress { get; set; }
    public ShippingAddressPZ shippingAddress { get; set; }
    public IList<Transactions> transactions { get; set; }
    public string from { get; set; }
    public string fromId { get; set; }
    public IList<object> paymentRequestId { get; set; }
    public string transactionId { get; set; }
    public string createdBy { get; set; }
    public string updatedBy { get; set; }
    public string owner { get; set; }
    public IList<object> ownerGroup { get; set; }
    public DateTime createdAt { get; set; }
    public DateTime updatedAt { get; set; }
    public string id { get; set; }

}

public class Meta
{
    public int count { get; set; }
    public int totalPages { get; set; }
    public int page { get; set; }
    public int limit { get; set; }

}
public class CustomerCard
{
    public string name { get; set; }
    public string number { get; set; }
    public string network { get; set; }
    public string exp { get; set; }
    public string last4 { get; set; }
    public string type { get; set; }
    public string customerVaultId { get; set; }

}
public class CustomerBillingAddress
{
    public string company { get; set; }
    public string country { get; set; }
    public string address { get; set; }
    public string address2 { get; set; }
    public string city { get; set; }
    public string state { get; set; }

}
public class CustomerShippingAddress
{
    public string company { get; set; }
    public string country { get; set; }
    public string address { get; set; }
    public string address2 { get; set; }
    public string city { get; set; }
    public string state { get; set; }

}
public class Data
{
    public string id { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string companyName { get; set; }
    public IList<CustomerCard> card { get; set; }
    public string phoneNumber { get; set; }
    public string email { get; set; }
    public string zipCode { get; set; }
    public CustomerBillingAddress billingAddress { get; set; }
    public CustomerShippingAddress shippingAddress { get; set; }
    public IList<object> deletedAt { get; set; }

}
public class customerResp
{
    public Meta meta { get; set; }
    public IList<Data> data { get; set; }

}
