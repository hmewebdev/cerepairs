﻿using ce.InvoiceModels;
using cerepairs.Classes;
using HiQPdf;
using HME.SQLSERVER.DAL;
using qalab;
using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;

namespace PaymentModel.Models
{
    public class UpdateSales : BaseModels
    {
        public void UpdateSalesInfo(ProcessPayment dto)
        {
            PayPalResponse paypalresp = dto.presp;
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("sessionUID", dto.Cart_Session_UID));
            sp.Add(new SqlParameter("cartUserId", dto.User_Cookie));
            sp.Add(new SqlParameter("Invoice_Transaction_ID", dto.model.Invoice_Transaction_ID));
            sp.Add(new SqlParameter("Invoice_Created_IP", dto.User_Created_IP));
            sp.Add(new SqlParameter("Invoice_Correlation_ID", dto.model.Invoice_Correlation_ID));
            sp.Add(new SqlParameter("promocode", dto.model.promo));
            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("getcartproducts", sp, out sqlMessage);
            db.Dispose();
            if (sqlMessage != "OK") { }

            ListToDataTable lsttodt = new ListToDataTable();
            List<PaymentModel> paymentModelList = new List<PaymentModel>();
            paymentModelList.Add(dto.model);
            DataTable additionalInfo = lsttodt.ToDataTable(paymentModelList);
            DataTable billinginfo = lsttodt.ToDataTable(dto.billinginfo.billinginfo);
            DataTable billingaddress = lsttodt.ToDataTable(dto.billinginfo.billingaddress);
            DataTable shippingaddress = lsttodt.ToDataTable(dto.billinginfo.shippingaddress);

            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("billinginfo", billinginfo));
            sp.Add(new SqlParameter("billingaddress", billingaddress));
            sp.Add(new SqlParameter("shippingaddress", shippingaddress));
            sp.Add(new SqlParameter("payment_info", additionalInfo));
            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("updatebilling", sp, out sqlMessage);
            db.Dispose();
            if (sqlMessage != "OK"){}
            ds.Dispose();
            string appPath = HttpRuntime.AppDomainAppPath;// HttpContext.Current.Server.MapPath(@"~/Views/invoice.cshtml");
            string path = appPath + "Views/invoice.cshtml";
            string guid = Guid.NewGuid().ToString("N");
            InvoiceModels model = new InvoiceModels();

            Invoices inv = model.GetInvoice(paymentModelList[0].confirmationNumber, "conformation");
            string cart_session_uid = inv.Invoice_Session_UID;
            string user_cookie = inv.Invoice_User_Cookie;
            string transaction_id = inv.Invoice_Transaction_ID;
            string comformation_id = paymentModelList[0].confirmationNumber;
            ce.InvoiceModels.User user = model.GetUser(inv.Invoice_Transaction_ID);
            var first = DateTime.Parse(inv.invoices.Take(1).FirstOrDefault().Invoice_Created_DTS).ToString("MM/dd/yyyy");
            string page = RazorEngineClass.RazorEngine.RunCompile(path, null, inv);
            byte[] pdfBuffer = null;
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(page, ConfigurationManager.AppSettings["ceURL"]);
            Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
            pdf.LoadFromBytes(pdfBuffer);

            for (int i = 0; i < pdf.Pages.Count; i++)
            {
                PdfPageBase originalPage = pdf.Pages[i];
                if (originalPage.IsBlank())
                {
                    pdf.Pages.Remove(originalPage);
                    i--;
                }
            }

            MemoryStream PDFstream = new MemoryStream();
            pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
            byte[] bytes = PDFstream.ToArray();
            model.SavePDF(bytes, cart_session_uid, user_cookie, transaction_id, comformation_id);

            bool ret = false;
            int count = 0;
            while (count < 5)
            {
                ret = model.SendInvoiceEmail("Invoice of Order -" + comformation_id, transaction_id, comformation_id, appPath + @"images", "CEORDERS@hme.com", "Web_Support@hme.com", null, pdf, dto.billinginfo.shippingaddress[0].company);
                if (ret)
                {
                    break;
                }
                Thread.Sleep(500);
                count++;
            }
            count = 0;
            while (count < 5)
            {
                //ret = model.SendInvoiceEmail("Invoice of Order -" + dto.dto.confirmationNumber, dto.dto.SECURETOKENID, dto.dto.confirmationNumber, dto.directoryPath, dto.dto.User_Email, null, null, dto.pdf, dto.dto.CompanyName);
                ret = model.SendInvoiceEmail("Invoice of Order -" + comformation_id, transaction_id, comformation_id, appPath + @"images", dto.billinginfo.billinginfo[0].email, null, null, pdf, dto.billinginfo.shippingaddress[0].company);
                if (ret)
                {
                    break;
                }
                Thread.Sleep(500);
                count++;
            }
        }

    }

    [Serializable]
    public class Sessions
    {
        public string ContinueShopping { get; set; }
        public string User_Cookie { get; set; }
        public string Cart_Session_UID { get; set; }
        public string Cart_Promo { get; set; }
        public string CartCount { get; set; }
        public string CartActive { get; set; }
        public string PromoDiscount { get; set; }
        public string Cart_Promo_Message { get; set; }
        public string Promo_ProductUID { get; set; }
        public string payflowresponse { get; set; }
    }

    [Serializable]
    public class ButtonCommands
    {
        public securetokendto token { get; set; }
        public string request { get; set; }
        public string response { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }
    [Serializable]
    public class securetokendto
    {
        public string SECURETOKENID { get; set; }
        public string Cart_Session_UID { get; set; }
        public string User_Cookie { get; set; }
    }

    public class PaymentRequest
    {
        public string Amount { get; set; }
        public string CustomerId { get; set; }
        public string PaymentNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string s_InvoiceDate { get; set; }
        public string InvoiceBalance { get; set; }
        public string BalanceOwed { get; set; }
        public string PaymentAmount { get; set; }//REMOVE
        public string Currency { get; set; }
        public string SignalR { get; set; }
        public string PaymentType { get; set; }
        public List<CreditPayments> CreditPayments { get; set; }
        public List<PaymentRequest> CreditInvoces { get; set; }

    }

    public class CreditPayments
    {
        public string BalanceOwed { get; set; }
        public string CreditApplied { get; set; }
        public string InvoicePayment { get; set; }
        public string PaymentNumber { get; set; }
    }

    //Insert into DB
    public class PaymentRequestModel
    {
        public List<PaymentRequest> PaymentRequests { get; set; }
        public string Comments { get; set; }
        public string Currency { get; set; }
        public string Company { get; set; }
        public string PaymentRefNum { get; set; }
        public string PaymentLabel { get; set; }
        public string PaymentType { get; set; }
        public string PaymentCardType { get; set; }
        public string PaymentCardNumber { get; set; }
        public string PaymentNumber { get; set; }
        public string PaymentValues { get; set; }
        public decimal TotalAmount { get; set; }
        public string PayPalURL { get; set; }
        public string ConfirmationMessage { get; set; }
        public string BCCEmail { get; set; }
        public string PaymentSessionUserId { get; set; }
        public string UserIPaddress { get; set; }
        public string PaymentConfirmation { get; set; }
        public DateTime? PaymentCreatedDateTime { get; set; }
        public string SignalR { get; set; }
    }

    // getting payment details from DB
    public class PaymentModel
    {
        public string PaymentID { get; set; }
        public string SessionUserId { get; set; }
        public string PaymentUId { get; set; }
        public string PaymentCurrency { get; set; }
        public string PaymentCompany { get; set; }
        public string CustomerId { get; set; }
        public string PaymentConfirmation { get; set; }
        public string PaymentAmount { get; set; }
        //public string PaymentSubTotal { get; set; }
        public string PaymentNumber { get; set; }
        public string PaymentType { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string IsPaymentApplied { get; set; }
        public DateTime PaymentCreatedDateTime { get; set; }
        public string PaymentCreatedBy { get; set; }
        public string PaymentTransactionId { get; set; }
        public string PaymentJournal { get; set; }
        public string PaymentCardType { get; set; }
        public string PaymentMemo { get; set; }
        public string PaymentComplete { get; set; }
        public string PaymentToken { get; set; }
        public string PaymentTokenId { get; set; }
        public string JsonObj { get; set; }
        public string User_Cookie { get; set; }
        public string Invoice_Session_UID { get; set; }
        public string Invoice_User_Cookie { get; set; }
        public string confirmationNumber { get; set; }
        public string promo { get; set; }
        public string promo_productUID { get; set; }
        public string freight { get; set; }
        public string grandtotal { get; set; }
        public string discount { get; set; }
        public string surcharge { get; set; }
        public string taxrate { get; set; }
        public string tax { get; set; }
        public string ipAddress { get; set; }
        public string Invoice_Transaction_ID { get; set; }
        public string Invoice_Correlation_ID { get; set; }
        public string User_Created_Agent { get; set; }
        public string Invoice_Product_Discount { get; set; }
        public string Invoice_Promo_Code { get; set; }
        //public string AdditionalFreight { get; set; }
        //public PayPalTransactionResponseDTO paypalresponse { get; set; }
        //public ProcessPayment dto { get; set; }
    }

    //Updating the DB and 
    public class PaymentResponseModel
    {
        public string PaymentID { get; set; }
        public bool IsPaymentApplied { get; set; }
        public string PaymentTransactionId { get; set; }
        public string PaymentJournal { get; set; }
        public string PaymentTokenId { get; set; }
        public bool PaymentComplete { get; set; }
        public string PaymentConfirmation { get; set; }
        public DateTime PaymentCreatedDateTime { get; set; }
        public string PaymentCardType { get; set; }

        public string PaymentCurrency { get; set; }
        public string PaymentCompany { get; set; }
        public string CustomerId { get; set; }
        public string PaymentAmount { get; set; }
        public string PaymentType { get; set; }
    }

    public class Confirmation_Number
    {
        public string ConfirmationNumber { get; set; }
        public string SignalR { get; set; }
        public string PaymentType { get; set; }
        public string Status { get; set; }
    }

    public class BillingInfoLog
    {
        public string grandtotal { get; set; }
        public string tax { get; set; }
        public string freight { get; set; }
        public BillingInfo billinginfo { get; set; }
        public BillingAddress billingaddress { get; set; }
        public ShippingAddressLog shippingaddress { get; set; }
        public string signalr { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }
    public class ProcessPayment
    {
        public string confirmationnumber { get; set; }
        public string signalr { get; set; }
        public string paymenttype { get; set; }
        public string status { get; set; }
        public string request { get; set; }
        public string usercookie { get; set; }
        public string amount { get; set; }
        public string tax { get; set; }
        public BillingInfoDTO billinginfo { get; set; }
        public string Invoice_Session_UID { get; set; }
        public string Invoice_User_Cookie { get; set; }
        public string PaymentUId { get; set; }
        public PayPalTransactionResponseDTO paypalresponse { get; set; }
        public PaymentModel model { get; set; }
        public string User_Session_UID { get; set; }
        public string Cart_Session_UID { get; set; }
        public string User_Cookie { get; set; }
        public string user { get; set; }
        public string currency { get; set; }
        public string Token { get; set; }
        public string TokenID { get; set; }
        public string User_Created_IP { get; set; }
        public string User_Created_Agent { get; set; }
        public string Promo { get; set; }
        public string Promo_ProductUID { get; set; }
        public PayPalResponse presp { get; set; }
    }

    public class InvoicePayment
    {
        public string confirmationnumber { get; set; }
        public string signalr { get; set; }
        public string paymenttype { get; set; }
        public string status { get; set; }
        public string request { get; set; }
        public string usercookie { get; set; }
        public string amount { get; set; }
        public BillingInfoDTO billinginfo { get; set; }
        public string Invoice_Session_UID { get; set; }
        public string Invoice_User_Cookie { get; set; }
        public string confirmationNumber { get; set; }
        public PayPalTransactionResponseDTO paypalresponse { get; set; }
    }

    [Serializable]
    public class PayPalTransactionResponseDTO
    {
        public string AVSDATA { get; set; }
        public string AMT { get; set; }
        public string ACCT { get; set; }
        public string AUTHCODE { get; set; }
        public string ZIP { get; set; }
        public string EXPDATE { get; set; }
        public string RESPMSG { get; set; }
        public string CARDTYPE { get; set; }
        public string AVSZIP { get; set; }
        public string AVSADDR { get; set; }
        public string IAVS { get; set; }
        public string PROCAVS { get; set; }
        public string RESULT { get; set; }

    }

    [Serializable]
    public class PayPalTransactionResponse
    {
        public string response { get; set; }
        public string conformation { get; set; }

    }

    public class BillingInfoDTO
    {
        public List<BillingInfo> billinginfo { get; set; }
        public List<BillingAddress> billingaddress { get; set; }
        public List<ShippingAddress> shippingaddress { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }

    public class BillingInfo
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string phonenumber { get; set; }
        public string email { get; set; }
    }

    public class BillingAddress
    {
        public string address { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
    }

    public class ShippingAddress
    {
        public string company { get; set; }
        public string address { get; set; }
        //public string address { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
    }
    public class ShippingAddressLog
    {
        public string tax { get; set; }
        public string grandtotal { get; set; }
        public string freight { get; set; }
        public string company { get; set; }
        public string address { get; set; }
        //public string address { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
    }

    public class MoreInfoList
    {
        public List<MoreInfo> moreinfo { get; set; }
    }

    public class MoreInfo
    {
        public string Token { get; set; }
        public string TokenID { get; set; }
        public string Cart_Session_UID { get; set; }
        public string User_Cookie { get; set; }
        public string User_Created_IP { get; set; }
        public string User_Created_Agent { get; set; }
        public string TaxRate { get; set; }
        public string Promo { get; set; }
        public string Freight { get; set; }
        public string Discount { get; set; }
        public string GrandTotal { get; set; }
        public string ipAddress { get; set; }
        public string promo_productUID { get; set; }
        public string PaymentConfirmation { get; set; }

    }
 
    //public class InvoiceUpdate
    //{
    //    public string Discount { get; set; }
    //    public string Cart_Product_QTY { get; set; }
    //    public string Offer_Qty { get; set; }
    //    //public string Offer_ValidFrom { get; set; }
    //    ////public string Offer_ValidTo { get; set; }
    //    //public string Offer_Description { get; set; }
    //    public string Offer_DiscountAmount { get; set; }
    //    public string Cart_Product_Price_SubTotal { get; set; }
    //    public string Cart_Product_UID { get; set; }
    //    //public string PrmoMessage { get; set; }

    //    //public string Cart_ID { get; set; }
    //    public string Cart_IsCompleted { get; set; }
    //    public string Cart_Session_UID { get; set; }
    //    public string Cart_User_Cookie { get; set; }
    //    //public string Cart_Product_UID { get; set; }
    //    //public string Cart_Product_QTY { get; set; }
    //    //public string c.Cart_Product_Price_SubTotal { get; set; }
    //    public string Cart_Product_Additional_Freight { get; set; }
    //    public string Cart_Product_Price_Type { get; set; }
    //    //public string Cart_DTS { get; set; }

    //    //public string Product_ID { get; set; }
    //    //public string Product_UID { get; set; }
    //    //public string Product_IsActive { get; set; }
    //    //public string Product_Class_ID { get; set; }
    //    public string Product_Short_Description { get; set; }
    //    //public string Product_Long_Description { get; set; }
    //    //public string Product_Image_Full { get; set; }
    //    //public string Product_Image_Med { get; set; }
    //    //public string Product_Image_Thum { get; set; }
    //    //public string Supplier_Part_Number { get; set; }
    //    public string Manufacturer_Part_Number { get; set; }
    //    //public string Manufacturer_ID { get; set; }
    //    //public string Currency_Code { get; set; }
    //    //public string Repair_Price { get; set; }
    //    //public string Repair_Price_High { get; set; }
    //    //public string Exchange_Price { get; set; }
    //    //public string Exchange_Price_High { get; set; }
    //    //public string Recondition_Price { get; set; }
    //    //public string Recondition_Price_High { get; set; }
    //    //public string Refurbished_Price { get; set; }
    //    //public string Retail_Price { get; set; }
    //    //public string Additional_Freight { get; set; }
    //    public string UOM { get; set; }
    //    public string UNSPSC { get; set; }
    //    //public string Product_LastMod_DTS { get; set; }
    //    //public string Product_Created_DTS { get; set; }
    //    //public string Product_CreatedBy { get; set; }
    //    //public string Product_Name { get; set; }
    //}
}