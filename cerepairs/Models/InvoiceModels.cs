﻿using cerepairs.Classes;
using HME.SQLSERVER.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using HiQPdf;
using hmeUtil;
using System.Threading;
using Spire.Pdf;
using Spire.Pdf.Graphics;
using cart.Models;
using System.IO;
using qalab;
using System.Web.Script.Serialization;

namespace ce.InvoiceModels
{
    public class InvoiceModels : BaseModels
    {
        public User GetUser(string TokenID = null, string Invoice_Session_UID = null, string User_Cookie = null, string conformationid = null)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("Invoice_Session_UID", User_Cookie));
            sp.Add(new SqlParameter("Invoice_User_Cookie", Invoice_Session_UID));
            sp.Add(new SqlParameter("Invoice_Transaction_ID", TokenID));
            ds = db.executeStoredProcedure("getuser", sp, out sqlMessage);
            db.Dispose();
            IList<PropertyInfo> properties = typeof(User).GetProperties().ToList();
            User user = new User();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                foreach (var property in properties)
                {
                    user.SetProperty(property.Name, row[property.Name].ToString());
                }
            }
            ds.Dispose();
            return user;
        }
        public Invoices GetInvoice(string filtervalue, string filter)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            string sqlMessage = string.Empty;
            DataSet ds;
            List<SqlParameter> sp = new List<SqlParameter>();
            switch (filter)
            {
                case "conformation":
                    sp.Add(new SqlParameter("Invoice_Conformation_ID", filtervalue));
                    break;
                case "transactionID":
                    sp.Add(new SqlParameter("Invoice_Transaction_ID", filtervalue));
                    break;
                case "invoicesessionID":
                    sp.Add(new SqlParameter("Session_UID", filtervalue));
                    break;
            }
            ds = db.executeStoredProcedure("getinvoice", sp, out sqlMessage);
            db.Dispose();

            IList<PropertyInfo> properties = typeof(Invoice).GetProperties().ToList();
            Invoices invoices = new Invoices();
            Invoice invoice = new Invoice();
            invoices.user = new User();
            invoices.invoices = new List<Invoice>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                invoice = new Invoice();
                foreach (var property in properties)
                {
                    try
                    {
                        invoice.SetProperty(property.Name, row[property.Name].ToString());
                    }
                    catch { }
                }
                invoices.invoices.Add(invoice);

            }
            foreach (DataColumn dc in ds.Tables[1].Columns)
            {
                try
                {
                    invoices.user.SetProperty(dc.ColumnName, ds.Tables[1].Rows[0][dc.ColumnName].ToString());
                }
                catch { }
            }

            invoices.Invoice_Session_UID = ds.Tables[0].Rows[0]["Invoice_Session_UID"].ToString();
            invoices.Invoice_Transaction_ID = ds.Tables[0].Rows[0]["Invoice_Transaction_ID"].ToString();
            invoices.Invoice_User_Cookie = ds.Tables[0].Rows[0]["Invoice_User_Cookie"].ToString();
            invoices.Invoice_Conformation_ID = ds.Tables[0].Rows[0]["Invoice_Conformation_ID"].ToString();

            ds.Dispose();
            return invoices;
        }

        public void ProcessInvoice(string transcationid, string viewPath)
        {
            InvoiceModels invoice = new InvoiceModels();
            Invoices inv = invoice.GetInvoice(transcationid, "transactionID");
            string base64 = GetInvoiceB64(inv, viewPath);
        }

        private string GetInvoiceB64(Invoices inv, string viewPath)
        {
            var first = DateTime.Parse(inv.invoices.Take(1).FirstOrDefault().Invoice_Created_DTS).ToString("MM/dd/yyyy");
            //var date = first.Invoice_Created_DTS;
            //ViewBag.Title = "Invoice";
            //string viewpath = Server.MapPath(@"~/Views/invoice.cshtml");
            string page = RazorEngineClass.RazorEngine.RunCompile(viewPath, null, inv);
            byte[] pdfBuffer = null;
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            htmlToPdfConverter.Document.PageSize = HiQPdf.PdfPageSize.A4;
            htmlToPdfConverter.Document.PageOrientation = HiQPdf.PdfPageOrientation.Portrait;
            pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(page, ConfigurationManager.AppSettings["ceURL"]);
            string base64 = Convert.ToBase64String(pdfBuffer);
            return base64;
        }

        public bool SendInvoiceEmail(string subject, string tokenid, string conformationid, string directoryPath, string to, string bcc = null, byte[] pdfBuffer = null, Spire.Pdf.PdfDocument pdf = null, string companyName = null)
        {
            //MemoryStream PDFstream = new MemoryStream();
            //if (pdfBuffer != null)
            //{
            //    pdf.LoadFromBytes(pdfBuffer);
            //}else
            //{
            //    PDFstream = new MemoryStream();
            //    pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
            //    pdfBuffer = PDFstream.ToArray();
            //}
            //byte[] xpdfBuffer = null;

            //HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            //htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            ////htmlToPdfConverter.Document.Margins = new PdfMargins(5);
            //htmlToPdfConverter.Document.PageSize = HiQPdf.PdfPageSize.A4;
            //htmlToPdfConverter.Document.PageOrientation = HiQPdf.PdfPageOrientation.Portrait;
            //xpdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(body, ConfigurationManager.AppSettings["ceURL"]);

            //List<pdfImage> pages = GetPDFpages(pdf);
            //email email = new email();
            //email.emailaddress emailaddress;
            //email.attachments = new List<email.attachment>();
            //email.from = "no-reply@cerepairs.com";
            //email.subject = subject;
            //email.to = new List<email.emailaddress>();
            //emailaddress = new email.emailaddress();
            //emailaddress.address = to;
            //email.to.Add(emailaddress);
            ////foreach (pdfImage pg in pages)
            ////{
            ////    email.body += "<div><img src='" + pg.base64 + "' style='width: 100%;'></div>";
            ////}
            ////email.body = "<html><head></head><body>"+body+"</body></html>";
            //email.attachment mailattachment = new email.attachment();
            //mailattachment = new email.attachment();
            //mailattachment.type = email.AttachmentType.byteArrayPDF;
            //mailattachment.bytearray = xpdfBuffer;
            //mailattachment.filename = "Invoice_" + string.Format("{0:M.d.yyyy_HH.mm.ss_ffff}", DateTime.Now) + ".pdf";
            //email.attachments.Add(mailattachment);


            //mailattachment = new email.attachment();
            //mailattachment.type = email.AttachmentType.memoryStream;
            //mailattachment.bytearray = pdfBuffer;
            //mailattachment.inline = true;
            //mailattachment.inlineid = "~attachment1~";
            //email.attachments.Add(mailattachment);
            //email.body = "<img src='~attachment1~'>";
            //bool ret = email.sendemail();
            //email.Dispose();


            InvoiceModels model = new InvoiceModels();
            //Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
            //byte[] bytes;
            //byte[] pdfBuffer;
            string xbody = string.Empty;
            string pngImages = string.Empty;
            string header = string.Empty;
            string urlPath = directoryPath;
            byte[] ximage;
            MemoryStream PDFstream = new MemoryStream();
            pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
            pdfBuffer = PDFstream.ToArray();

            System.Drawing.Image image = pdf.SaveAsImage(0, PdfImageType.Bitmap, 296, 296);
            List<pdfImage> pdfpages = new List<pdfImage>();
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ximage = ms.ToArray();
            }
            try
            {
                pdfpages = model.GetPDFpages(pdf);
            }
            catch { return false; }
            email email = new email();
            email.emailaddress emailaddress;
            email.attachments = new List<email.attachment>();
            email.from = "no-reply@cerepairs.com";
            //transcationid
            //"Invoice of Order - BQPP6E6473DE"
            email.subject = "Invoice of Order - " + (companyName != null ? companyName : "") + " " + conformationid;  //subject;// "Invoice of Order -" + transcationid;
            email.to = new List<email.emailaddress>();
            if (to != null)
            {
                string[] toemail;
                try
                {
                    toemail = to.Split(',');
                }
                catch { return false; }
                foreach (string em in toemail)
                {
                    emailaddress = new email.emailaddress();
                    emailaddress.address = em;
                    email.to.Add(emailaddress);
                }
            }
            else
            {
                User user = GetUser(tokenid);
                emailaddress = new email.emailaddress();
                emailaddress.address = user.Contact_Email;
                email.to.Add(emailaddress);
            }
            if (bcc != null)
            {
                email.bcc = new List<email.emailaddress>();
                emailaddress = new email.emailaddress();
                emailaddress.address = bcc;
                email.bcc.Add(emailaddress);

            }
            //string base64 = Convert.ToBase64String(pdfBuffer);
            email.body = string.Empty;
            email.attachment mailattachment = new email.attachment();
            mailattachment = new email.attachment();
            mailattachment.type = email.AttachmentType.byteArrayPDF;
            mailattachment.bytearray = pdfBuffer;
            mailattachment.filename = "Invoice_" + (companyName != null ? companyName : "") + "_" + conformationid + "_" + string.Format("{0:M.d.yyyy_HH.mm.ss_ffff}", DateTime.Now) + ".pdf";
            email.attachments.Add(mailattachment);

            int count = 0;
            try
            {
                foreach (var p in pdfpages)
                {
                    mailattachment = new email.attachment();
                    mailattachment.type = email.AttachmentType.byteArray;
                    mailattachment.bytearray = ximage;
                    mailattachment.inline = true;
                    mailattachment.inlineid = "~attachment" + count.ToString() + "~";
                    email.attachments.Add(mailattachment);
                    pngImages += "<div><img src='cid:~attachment" + count.ToString() + "~' style='width: 100%;'></div>";
                    xbody += "<div><img src='" + p.base64 + "' style='width: 100%;'></div>";
                }
            }
            catch { return false; }

            mailattachment = new email.attachment();
            mailattachment.type = email.AttachmentType.filePath;
            mailattachment.filepath = urlPath + @"\celogo.png";
            mailattachment.inline = true;
            mailattachment.inlineid = "~logo~";
            //email.attachments.Add(mailattachment);

            header = "";// "<div style='text-align:left;margin-left:10px;'><img src='cid:~logo~'/><div style='margin-left:10px;font-size:19px;'>Invoice</div></div>";
            email.body = header + pngImages;
            bool ret = email.sendemail();
            email.Dispose();
            return ret;
        }

        public bool SendInvoiceEmailToAdmins(string subject, string tokenid, string conformationid, string directoryPath, string to, string bcc = null, byte[] pdfBuffer = null, Spire.Pdf.PdfDocument pdf = null, string companyName = null)
        {
            InvoiceModels model = new InvoiceModels();
            string xbody = string.Empty;
            string pngImages = string.Empty;
            string header = string.Empty;
            string urlPath = directoryPath;
            byte[] ximage;
            MemoryStream PDFstream = new MemoryStream();
            pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
            pdfBuffer = PDFstream.ToArray();

            System.Drawing.Image image = pdf.SaveAsImage(0, PdfImageType.Bitmap, 296, 296);
            List<pdfImage> pdfpages = new List<pdfImage>();
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ximage = ms.ToArray();
            }
            try
            {
                pdfpages = model.GetPDFpages(pdf);
            }
            catch { return false; }
            email email = new email();
            email.emailaddress emailaddress;
            email.attachments = new List<email.attachment>();
            email.from = "no-reply@cerepairs.com";
            email.subject = "Invoice of Order - " + (companyName != null ? companyName : "") + " " + conformationid;  //subject;// "Invoice of Order -" + transcationid;
            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = to;
            email.to.Add(emailaddress);
            if (bcc != null)
            {
                email.bcc = new List<email.emailaddress>();
                emailaddress = new email.emailaddress();
                emailaddress.address = bcc;
                email.bcc.Add(emailaddress);

            }
            email.body = string.Empty;
            email.attachment mailattachment = new email.attachment();
            mailattachment = new email.attachment();
            mailattachment.type = email.AttachmentType.byteArrayPDF;
            mailattachment.bytearray = pdfBuffer;
            mailattachment.filename = "Invoice_" + (companyName != null ? companyName : "") + "_" + conformationid + "_" + string.Format("{0:M.d.yyyy_HH.mm.ss_ffff}", DateTime.Now) + ".pdf";
            email.attachments.Add(mailattachment);

            int count = 0;
            try
            {
                foreach (var p in pdfpages)
                {
                    mailattachment = new email.attachment();
                    mailattachment.type = email.AttachmentType.byteArray;
                    mailattachment.bytearray = ximage;
                    mailattachment.inline = true;
                    mailattachment.inlineid = "~attachment" + count.ToString() + "~";
                    email.attachments.Add(mailattachment);
                    pngImages += "<div><img src='cid:~attachment" + count.ToString() + "~' style='width: 100%;'></div>";
                    xbody += "<div><img src='" + p.base64 + "' style='width: 100%;'></div>";
                }
            }
            catch { return false; }

            mailattachment = new email.attachment();
            mailattachment.type = email.AttachmentType.filePath;
            mailattachment.filepath = urlPath + @"\celogo.png";
            mailattachment.inline = true;
            mailattachment.inlineid = "~logo~";
            //email.attachments.Add(mailattachment);

            header = "";// "<div style='text-align:left;margin-left:10px;'><img src='cid:~logo~'/><div style='margin-left:10px;font-size:19px;'>Invoice</div></div>";
            email.body = header + pngImages;
            bool ret = email.sendemail();
            email.Dispose();
            return ret;
        }

        public List<pdfImage> GetPDFpages(Spire.Pdf.PdfDocument pdf)
        {
            pdfImage pdfImage = new pdfImage();
            List<pdfImage> pdfImages = new List<pdfImage>();
            int page = 0;
            for (int i = 0; i < pdf.Pages.Count; i++)
            {
                System.Drawing.Image image = pdf.SaveAsImage(i, PdfImageType.Bitmap, 296, 296);
                pdfImage = new pdfImage();
                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    pdfImage.image = ms.ToArray();
                    pdfImage.base64 = String.Format("data:image/pdf;base64,{0}", Convert.ToBase64String(pdfImage.image));
                    pdfImage.page = page;
                    page++;
                }
                pdfImages.Add(pdfImage);
            }
            return pdfImages;
        }

        public void SavePDF(byte[] pdf, string cart_session_uid = null, string user_cookie = null, string transaction_id = null, string comformation_id = null)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("cart_session_uid", cart_session_uid));
            sp.Add(new SqlParameter("user_cookie", user_cookie));
            sp.Add(new SqlParameter("transaction_id", transaction_id));
            sp.Add(new SqlParameter("comformation_id", comformation_id));
            sp.Add(new SqlParameter("pdf", pdf));
            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("save_invoicePDF", sp, out sqlMessage);
            ds.Dispose();
            db.Dispose();
        }

        public byte[] GetPDF(string cart_session_uid = null, string user_cookie = null, string transaction_id = null, string comformation_id = null)
        {
            dbSQL db = new dbSQL(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["Datasource"]].ConnectionString);
            sp = new List<SqlParameter>();
            sp.Add(new SqlParameter("cart_session_uid", cart_session_uid));
            sp.Add(new SqlParameter("user_cookie", user_cookie));
            sp.Add(new SqlParameter("transaction_id", transaction_id));
            sp.Add(new SqlParameter("comformation_id", comformation_id));
            sqlMessage = string.Empty;
            ds = db.executeStoredProcedure("getPDF", sp, out sqlMessage);
            db.Dispose();
            if (sqlMessage == "OK")
            {
                byte[] pdf = (byte[])ds.Tables[0].Rows[0]["pdf"];
                db.Dispose();
                return pdf;
            }
            else
            {
                db.Dispose();
                return null;
            }

        }

        //InvoiceThread
        //public void ConformationInvoice(string guid, PayPalTransactionResponse dto, string viewpath, string directoryPath)
        public void ConformationInvoice(InvoiceThread dto)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            //Invoices inv = GetInvoice(dto.dto.SECURETOKENID, "transactionID");
            //var first = DateTime.Parse(inv.invoices.Take(1).FirstOrDefault().Invoice_Created_DTS).ToString("MM/dd/yyyy");
            ////var date = first.Invoice_Created_DTS;
            //string page = RazorEngineClass.RazorEngine.RunCompile(dto.viewpath, null, inv);
            //byte[] pdfBuffer = null;
            //HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            //htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            //pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(page, ConfigurationManager.AppSettings["ceURL"]);
            //Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
            //pdf.LoadFromBytes(pdfBuffer);

            //for (int i = 0; i < pdf.Pages.Count; i++)
            //{
            //    PdfPageBase originalPage = pdf.Pages[i];
            //    if (originalPage.IsBlank())
            //    {
            //        pdf.Pages.Remove(originalPage);
            //        i--;
            //    }
            //}

            //MemoryStream PDFstream = new MemoryStream();
            //pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
            //byte[] bytes = PDFstream.ToArray();
            //dto.bytes = bytes;
            //dto.pdf = pdf;
            //InvoiceModels model = new InvoiceModels();
            //model.SavePDF(dto.bytes, dto.dto.Cart_Session_UID, dto.dto.User_Cookie, dto.dto.SECURETOKENID, dto.dto.confirmationNumber);

            InvoiceThreading invoiceThreading = new InvoiceThreading();
            invoiceThreading.InvoiceConformation(dto);
            //Thread invoice_Thread = new Thread(invoiceThreading.InvoiceConformation);
            //invoice_Thread.Start(dto);

            //new Thread(() =>
            //{
            //    SavePDF(bytes, dto.dto.Cart_Session_UID, dto.dto.User_Cookie, dto.dto.SECURETOKENID, dto.dto.confirmationNumber);
            //    buttonCommands bcommands = new buttonCommands();
            //    bcommands.Invoice_Conformation_ID = dto.dto.confirmationNumber;
            //    bcommands.Invoice_Session_UID = dto.dto.Cart_Session_UID;
            //    bcommands.Invoice_Transaction_ID = dto.dto.SECURETOKENID;
            //    bcommands.Invoice_User_Cookie = dto.dto.User_Cookie;
            //    //dto. = dto.User_Cookie;
            //    string security = json.Serialize(bcommands);
            //    RuntimeCaching.CacheButtonCommandsToken(dto.guid, security);
            //    bool ret = false;
            //    int count = 0;
            //    while (count < 5)
            //    {
            //        ret = SendInvoiceEmail("Invoice of Order -" + dto.dto.confirmationNumber, dto.dto.SECURETOKENID, dto.dto.confirmationNumber, dto.directoryPath, "CEORDERS@hme.com", null, null, pdf, dto.dto.CompanyName);
            //        if (ret)
            //        {
            //            break;
            //        }
            //        Thread.Sleep(500);
            //        count++;
            //    }
            //    count = 0;
            //    while (count < 5)
            //    {
            //        ret = SendInvoiceEmail("Invoice of Order -" + dto.dto.confirmationNumber, dto.dto.SECURETOKENID, dto.dto.confirmationNumber, dto.directoryPath, dto.dto.User_Email, null, null, pdf, dto.dto.CompanyName);
            //        if (ret)
            //        {
            //            break;
            //        }
            //        Thread.Sleep(500);
            //        count++;
            //    }
            //    //ret = SendInvoiceEmailToAdmins("Invoice of Order -" + dto.confirmationNumber, dto.SECURETOKENID, dto.confirmationNumber, directoryPath, "CEORDERS@hme.com", null, null, pdf, dto.CompanyName);
            //}).Start();
            //RuntimeCaching.CacheInvoiceToken(guid, dto.confirmationNumber);
        }
        public string Print(buttonCommands securintObjs)
        {
            InvoiceModels model = new InvoiceModels();
            JavaScriptSerializer json = new JavaScriptSerializer();
            Invoices inv = model.GetInvoice(securintObjs.Invoice_Transaction_ID, "transactionID");
            Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
            byte[] bytes = null;
            string invoiceImage = string.Empty;
            for (int i = 0; i < 10; i++)
            {
                bytes = model.GetPDF(securintObjs.Invoice_Session_UID, securintObjs.Invoice_User_Cookie, securintObjs.Invoice_Transaction_ID);
                if (bytes == null) { Thread.Sleep(500); } else { break; }
            }
            //bytes = model.GetPDF(securintObjs.Invoice_Session_UID, securintObjs.Invoice_User_Cookie, securintObjs.Invoice_Transaction_ID);
            if (bytes == null)
            {
                return null;
            }
            else
            {
                pdf.LoadFromBytes(bytes);
                List<pdfImage> pdfpages = model.GetPDFpages(pdf);
                foreach (pdfImage p in pdfpages)
                {
                    invoiceImage += "<div><img src='" + p.base64 + "' style='width: 100%;'></div>";
                }
                return invoiceImage;
            }
        }
    }

    public class InvoiceThreading
    {
        public void InvoiceConformation(object data)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            InvoiceThread dto = (InvoiceThread)data;
            InvoiceModels model = new InvoiceModels();
            //model.SavePDF(dto.bytes, dto.dto.Cart_Session_UID, dto.dto.User_Cookie, dto.dto.SECURETOKENID, dto.dto.confirmationNumber);
            buttonCommands bcommands = new buttonCommands();
            bcommands.Invoice_Conformation_ID = dto.dto.confirmationNumber;
            bcommands.Invoice_Session_UID = dto.dto.Cart_Session_UID;
            bcommands.Invoice_Transaction_ID = dto.dto.SECURETOKENID;
            bcommands.Invoice_User_Cookie = dto.dto.User_Cookie;
            //dto. = dto.User_Cookie;
            string security = json.Serialize(bcommands);
            RuntimeCaching.CacheButtonCommandsToken(dto.guid, security);
            bool ret = false;
            int count = 0;
            while (count < 5)
            {
                ret = model.SendInvoiceEmail("Invoice of Order -" + dto.dto.confirmationNumber, dto.dto.SECURETOKENID, dto.dto.confirmationNumber, dto.directoryPath, "CEORDERS@hme.com", "Web_Support@hme.com", null, dto.pdf, dto.dto.CompanyName);
                if (ret)
                {
                    break;
                }
                Thread.Sleep(500);
                count++;
            }
            count = 0;
            while (count < 5)
            {
                ret = model.SendInvoiceEmail("Invoice of Order -" + dto.dto.confirmationNumber, dto.dto.SECURETOKENID, dto.dto.confirmationNumber, dto.directoryPath, dto.dto.User_Email, null, null, dto.pdf, dto.dto.CompanyName);
                if (ret)
                {
                    break;
                }
                Thread.Sleep(500);
                count++;
            }
        }
    }
    public class pdfImages
    {
        public List<pdfImage> images { get; set; }
    }


    public class additionalEmails
    {
        public string guid { get; set; }
        public string emails { get; set; }
        public string status { get; set; }
        public string Invoice_User_Cookie { get; set; }
        public string Invoice_Session_UID { get; set; }
        public string Invoice_Conformation_ID { get; set; }
    }
    public class bcmd
    {
        public string guid { get; set; }
        public string status { get; set; }
        public string print { get; set; }
        public string Invoice_User_Cookie { get; set; }
        public string Invoice_Session_UID { get; set; }
        public string Invoice_Conformation_ID { get; set; }
        
    }

    public class pdfImage
    {
        public byte[] image { get; set; }
        public string base64 { get; set; }
        public int page { get; set; }
    }

    [Serializable]
    public class buttonCommands
    {
        public string Invoice_Transaction_ID { get; set; }
        public string Invoice_User_Cookie { get; set; }
        public string Invoice_Session_UID { get; set; }
        public string Invoice_Conformation_ID { get; set; }
        public string CompanyName { get; set; }
    }

    public class InvoiceThread
    {
        public string guid { get; set; }
        public string viewpath { get; set; }
        public string directoryPath { get; set; }
        public byte[] bytes { get; set; }
        public Spire.Pdf.PdfDocument pdf { get; set; }
        public PayPalTransactionResponse dto { get; set; }
        public string path { get; set; }
    }

    [Serializable]
    public class Invoices
    {
        public List<Invoice> invoices { get; set; }
        public User user { get; set; }
        public string Invoice_Transaction_ID { get; set; }
        public string Invoice_User_Cookie { get; set; }
        public string Invoice_Session_UID { get; set; }
        public string Invoice_Conformation_ID { get; set; }
    }

    [Serializable]
    public class Invoice
    {
        public string Invoice_Session_UID { get; set; }
        public string Invoice_User_Cookie { get; set; }
        public string Invoice_Product_UID { get; set; }
        public string Invoice_Manufacturer_Part_Number { get; set; }
        public string Invoice_Product_Description { get; set; }
        public string Invoice_Product_Unit_Price { get; set; }
        public string Invoice_Product_Price { get; set; }
        public string Invoice_Product_Discount { get; set; }
        public string Invoice_Product_Qty { get; set; }
        public string Invoice_UOM { get; set; }
        public string Invoice_UNSPSC { get; set; }
        public string Invoice_Order { get; set; }
        public string Invoice_Created_DTS { get; set; }
        public string Invoice_Product_Price_Type { get; set; }
        public string Invoice_Transaction_ID { get; set; }
        public string Invoice_Correlation_ID { get; set; }
        public string Invoice_Cart_Promo_Discount { get; set; }
    }
    [Serializable]
    public class InvoiceUpdate
    {
        public string Invoice_Session_UID { get; set; }
        public string Invoice_User_Cookie { get; set; }
        public string Invoice_Product_UID { get; set; }
        public string Invoice_Manufacturer_Part_Number { get; set; }
        public string Invoice_Product_Description { get; set; }
        public string Invoice_Product_Unit_Price { get; set; }
        public string Invoice_Product_Price { get; set; }
        public string Invoice_Product_Price_Type { get; set; }
        public string Invoice_Product_Discount { get; set; }
        public string Invoice_Product_Qty { get; set; }
        public string Invoice_UOM { get; set; }
        public string Invoice_UNSPSC { get; set; }
        public string Invoice_Promo_Code { get; set; }
        public string Invoice_Order { get; set; }
        public string Invoice_Created_DTS { get; set; }
        public string Invoice_Created_IP { get; set; }
        public string Invoice_Transaction_ID { get; set; }
        public string Invoice_Correlation_ID { get; set; }
        public string Invoice_Cart_Promo_Discount { get; set; }

    }
    [Serializable]
    public class User
    {
        public string User_ID { get; set; }
        public string Invoice_Session_UID { get; set; }
        public string Token { get; set; }
        public string TokenID { get; set; }
        public string User_From { get; set; }
        public string User_To { get; set; }
        public string Buyer_Cookie { get; set; }
        public string Browser_FormPost { get; set; }
        public string User_Cookie { get; set; }
        public string Contact_FirstName { get; set; }
        public string Contact_LastName { get; set; }
        public string Contact_Email { get; set; }
        public string Contact_Phone { get; set; }
        public string Bill_Address1 { get; set; }
        public string Bill_Address2 { get; set; }
        public string Bill_City { get; set; }
        public string Bill_State { get; set; }
        public string Bill_Zip { get; set; }
        public string Bill_Country { get; set; }
        public string Ship_Company { get; set; }
        public string Ship_Address1 { get; set; }
        public string Ship_Address2 { get; set; }
        public string Ship_City { get; set; }
        public string Ship_State { get; set; }
        public string Ship_Zip { get; set; }
        public string Ship_Country { get; set; }
        public string User_Created_DTS { get; set; }
        public string User_Created_IP { get; set; }
        public string User_Created_Agent { get; set; }
    }
    [Serializable]
    public class transinfo
    {
        public string Invoice_User_Cookie { get; set; }
        public string Invoice_Session_UID { get; set; }
        public string Invoice_Transaction_ID { get; set; }
    }
}
