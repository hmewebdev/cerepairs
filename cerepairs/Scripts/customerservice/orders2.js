$(function() {
	
	/*if ($("#SO_Search_Form").length){
		if(getUrlParameter('cust') != undefined) {
			if(getUrlParameter('cust').length){
				getSalesOrders(getUrlParameter('cust'));
			}
		}
	}*/
	
	$( "#Date_From" ).click(function(e) {
        $(this).datepicker('setDate', null);
    });
	
	$( "#Date_To" ).click(function(e) {
        $(this).datepicker('setDate', null);
    });
	
	$( "#Date_From" ).datepicker({
		onSelect: function(){
			if(!$("#Date_To").val().length){
				$("#Date_To").val($(this).val());
			}
		}	
	});
	
	$( "#Date_To" ).datepicker({
		onSelect: function(){
			if(!$("#Date_From").val().length){
				$("#Date_From").val($(this).val());
			}
		}	
	});
	
	if($('#Cust_Number_Anchor').val().length){
		getSalesOrders();
	}
	
	$("#SO_Search_Form").submit(function(e){
        e.preventDefault();
		getSalesOrders();
    });
});

/* Set Pagination Defaults */
var Records_Per_Page = 5;
var Page_Start = 0;
var Page_End = Records_Per_Page;
var Paginated = false;

function getSalesOrders(){
	var Cust_Number = "";
	
	/*if (CustId != undefined) {
		Cust_Number = CustId;
	}*/
	
	if($('#Cust_Number_Anchor').val().length){
		Cust_Number = $('#Cust_Number_Anchor').val();	
	}
	
	/* Set Sales Order Defaults */
	var SO_Result_Rows = "";
	var SO_Number = $('#SO_Number').val();
	var PO_Number = $('#PO_Number').val();
	var INV_Number = $('#INV_Number').val();
	var Part_Number = $('#Part_Number').val();
	var Date_From = $('#Date_From').val();
	var Date_To = $('#Date_To').val();
	
	/* Get Sales Order Records */
	$.ajax({ 
		async: false,
		type: 'POST', 
		url: './proxy.cfm?service=OrderInfo&method=getOrders&returnFormat=json', 
		data: {Cust_Number: Cust_Number, SO_Number: SO_Number, PO_Number: PO_Number, INV_Number: INV_Number, Part_Number: Part_Number, Date_From: Date_From, Date_To: Date_To},
		cache: false, 
		dataType: 'json',
		success: function(soInfo) {
			var so_temp = JSON.parse(soInfo);
			tempSONumber = so_temp.DATA.SALESORDERNUMBER;
			tempPONumber = so_temp.DATA.PURCHASEORDERNUMBER;
			tempDate = so_temp.DATA.SALESORDERDATE;
		}
	});
	
	$('#record-count').text("("+tempSONumber.length+")");
	
	/* Clear Sales Order Table */
	$("table#SO_Results tbody").remove(); 
	
	/* Populate Sales Order Table */
	for(order=Page_Start;order<Page_End;order++){
		if (tempSONumber[order] != undefined) {
			
			SO_Result_Rows += '<tr class="SO_Result_Rows SO_Result_Row_'+order+'" id="'+order+'" data-toggle="collapse" data-target=".order-line-'+order+'" onclick="updateStatus('+order+');">'
			+'	<td><a href="javascript:void(0);" onclick="pauseStatus(\''+tempSONumber[order]+'\')">'+tempSONumber[order]+'</a></td>'
			+'	<td>'+tempPONumber[order]+'</td>'
			+'	<td>'+tempDate[order]+'</td>'
			+'  <td style="text-align:right;"><span class="plus'+order+' glyphicon glyphicon-plus-sign"></span></td>'
			+'</tr>';
			
			SO_Result_Rows += '<tr class="ord-row ord-row-'+order+'">'
			+'	<td></td>'
			+'	<td colspan="3">'
			+'		<div class="order-line-'+order+' collapse">'
			+'		<table class="table-striped">'
			+'			<thead>'
			+'			<tr>'
			+'				<th>LINE</th>'
			+'				<th>PART NUMBER</th>'
			+'				<th>DESCRIPTION</th>'
			+'				<th>QUANTITY</th>'
			+'				<th>UNIT PRICE</th>'
			+'				<th>TOTAL PRICE</th>'
			+'			</tr>'
			+'			</thead>'
			+'			<tbody>'
			+'			<tr>'
			+'				<td>001</td>'
			+'				<td>453G016</td>'
			+'				<td>COM6000,HEADSET, 1 BAT41</td>'
			+'				<td>6</td>'
			+'				<td>$20.00</td>'
			+'				<td>$120.00</td>'
			+'			</tr>'
			+'			<tr>'
			+'				<td>002</td>'
			+'				<td>453G017</td>'
			+'				<td>COM6000,HEADSET, 1 BAT41</td>'
			+'				<td>1</td>'
			+'				<td>$20.00</td>'
			+'				<td>$20.00</td>'
			+'			</tr>'
			+'			<tr>'
			+'				<td>003</td>'
			+'				<td>453G018</td>'
			+'				<td>COM6000,HEADSET, 1 BAT41</td>'
			+'				<td>2</td>'
			+'				<td>$20.00</td>'
			+'				<td>$40.00</td>'
			+'			</tr>'
			+'			</tbody>'
			+'		</table>'
			+'		</div>'
			+'	</td>'
			+'</tr>';
		}
	}
	
	$('.no-records').remove();

	if (!tempSONumber.length) {
		$("<p class='no-records'>No Records Found.</p>").insertAfter($('#SO_Results'));
		$('ul.pagination').hide();	
	} else {
		$('ul.pagination').show();	
	}
	
	$('<tbody>'+SO_Result_Rows+'</tbody>').insertAfter($('table thead#SO_Results_Header'));
	
	/* Set to Page 1 for new search */
	if(Paginated){
		$('ul.pagination').pagination('selectPage', 1);
	}
	
	/* Generate Pagination */
	$('ul.pagination').pagination({
        items: tempSONumber.length,
        itemsOnPage: Records_Per_Page,
		onPageClick: function(pageNumber) {
			
			/* Clear Sales Order Table */
			$("table#SO_Results tbody").remove(); 
			
			/* Set Pagination Settings */
			SO_Result_Rows = "";
			Page_Start = Records_Per_Page * (pageNumber - 1);
			Page_End = Page_Start + Records_Per_Page;
	
			/* Populate Customer Table */
			for(order=Page_Start;order<Page_End;order++){
				if (tempSONumber[order] != undefined) {
					SO_Result_Rows += '<tr class="SO_Result_Rows SO_Result_Row_'+order+'" id="'+order+'" data-toggle="collapse" data-target=".order-line-'+order+'" onclick="updateStatus('+order+');">'
					+'	<td><a href="javascript:void(0);" onclick="pauseStatus(\''+tempSONumber[order]+'\')">'+tempSONumber[order]+'</a></td>'
					+'	<td>'+tempPONumber[order]+'</td>'
					+'	<td>'+tempDate[order]+'</td>'
					+'  <td style="text-align:right;"><span class="plus'+order+' glyphicon glyphicon-plus-sign"></span></td>'
					+'</tr>';
					
					SO_Result_Rows += '<tr class="ord-row ord-row-'+order+'">'
					+'	<td></td>'
					+'	<td colspan="3">'
					+'		<div class="order-line-'+order+' collapse">'
					+'		<table class="table-striped">'
					+'			<thead>'
					+'			<tr>'
					+'				<th>LINE</th>'
					+'				<th>PART NUMBER</th>'
					+'				<th>DESCRIPTION</th>'
					+'				<th>QUANTITY</th>'
					+'				<th>UNIT PRICE</th>'
					+'				<th>TOTAL PRICE</th>'
					+'			</tr>'
					+'			</thead>'
					+'			<tbody>'
					+'			<tr>'
					+'				<td>001</td>'
					+'				<td>453G016</td>'
					+'				<td>COM6000,HEADSET, 1 BAT41</td>'
					+'				<td>6</td>'
					+'				<td>$20.00</td>'
					+'				<td>$120.00</td>'
					+'			</tr>'
					+'			<tr>'
					+'				<td>002</td>'
					+'				<td>453G017</td>'
					+'				<td>COM6000,HEADSET, 1 BAT41</td>'
					+'				<td>1</td>'
					+'				<td>$20.00</td>'
					+'				<td>$20.00</td>'
					+'			</tr>'
					+'			<tr>'
					+'				<td>003</td>'
					+'				<td>453G018</td>'
					+'				<td>COM6000,HEADSET, 1 BAT41</td>'
					+'				<td>2</td>'
					+'				<td>$20.00</td>'
					+'				<td>$40.00</td>'
					+'			</tr>'
					+'			</tbody>'
					+'		</table>'
					+'		</div>'
					+'	</td>'
					+'</tr>';
				}
			}
		
			$('<tbody>'+SO_Result_Rows+'</tbody>').insertAfter($('table thead#SO_Results_Header'));
			Paginated = true;
        }
    });
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function updateStatus(Row) {
	$('.ord-row-'+Row).on('shown.bs.collapse', function() {$(".plus"+Row).addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');});
	$('.ord-row-'+Row).on('hidden.bs.collapse', function() {$(".plus"+Row).addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');});
}

/*function pauseStatus(Row) {
	$('.SO_Result_Rows').on('click', function (e) { e.stopPropagation(); })
}*/

function pauseStatus(Row){
	$('.SO_Result_Rows').on('click', function (e) { e.stopPropagation(); })
	$('#SO_Number_Anchor').val(Row);
	$('#Cust_Anchor_Form').submit();
}