
$(function() {
	if($('#Payment_Card_Number').length){
		getCartCardType();
	}

	// Payment Validation
	$('#Cart_Payment_Confirm').click(function(e) {
		var hasError = false;

		/*if($('#Payment_Option').val() == "Credit"){
			if($('#Payment_Card_Name').val().length < 5 ){
				$('.Payment_Card_Name_Row').addClass('has-error has-feedback');
				hasError = true;
			} else {
				$('.Payment_Card_Name_Row').removeClass('has-error');	
			}
			
			if($('#Payment_Card_Number').val().length != 16 ){
				$('.Payment_Card_Number_Row').addClass('has-error');
				hasError = true;
			} else {
				$('.Payment_Card_Number_Row').removeClass('has-error');	
			}
			
			if($('#Payment_Card_Exp').val().length == 0){
				$('.Payment_Card_Exp_Row').addClass('has-error');
				hasError = true;
			} else {
				$('.Payment_Card_Exp_Row').removeClass('has-error');	
			}
			
			if($('#Payment_Card_Postal').val().length < 5){
				$('.Payment_Card_Postal_Row').addClass('has-error');
				hasError = true;
			} else {
				$('.Payment_Card_Postal_Row').removeClass('has-error');	
			}
			
			if($('#Payment_Card_CVC').val().length < 3 ){
				$('.Payment_Card_CVC_Row').addClass('has-error');
				hasError = true;
			} else {
				$('.Payment_Card_CVC_Row').removeClass('has-error');	
			}
		}*/
		
		if(!hasError){
			$('#payConfirmStore').modal("show");
			setTimeout(function(){
				$('form').unbind('submit').submit();
			}, 5000);
		}
    });
	
	// Add/Remove Hover Class to Cart Count
	$('.product-shopping-cart').hover(function() {
			$('.product-cart-count-symbol').addClass('product-cart-count-symbol-hover');
	}, function() {
		$('.product-cart-count-symbol').removeClass('product-cart-count-symbol-hover');
    });
	
	// Hide Cart Count
	$('.product-cart-count-symbol').hide();
	
	// Get Cart Count
	$.ajax({ 
		async: false,
		type: 'POST', 
		url: './proxy.cfm?service=CartMgmt&method=GetCartCount&returnFormat=json', 
		cache: false, 
		dataType: 'json',
		success: function(CartCount) {
			if(CartCount > 0){
				$('.product-shopping-cart').addClass('product-cart-full');
				$('.product-cart-count-symbol').text(CartCount);
				$('.product-cart-count-symbol').show();
			} else {
				$('.product-cart-count-symbol').hide();
				$('.product-cart-count-symbol').text("0");
				$('.product-shopping-cart').removeClass('product-cart-full');	
			}
		}
	});
	
	// Toggle items per page
	$('#Product_Per_Page').change(function(e) {
		$('#Product_Search_Page').val($('#Product_Search').val());
		$('#Product_Current_Page').val(1);
        $('#Product_Page_Form').submit();
    });
	
	// Paginate to Next page
	$('#Product_Next_Page').click(function(e) {
        $('#Product_Current_Page').val(parseInt($('#Product_Current_Page').val())+1);
		$('#Product_Search_Page').val($('#Product_Search').val());
    });
	
	// Paginate to Previous page
	$('#Product_Prev_Page').click(function(e) {
        $('#Product_Current_Page').val(parseInt($('#Product_Current_Page').val())-1);
		$('#Product_Search_Page').val($('#Product_Search').val());
    });
	
	// Dynamically set modal width to image
	$('.product-detail-cont img').click(function(e) {
        $('#Product_Image_Modal .modal-dialog').css("width", $('#Product_Image_Modal .modal-dialog img').width());
    });
	
	// Remove Item from Cart
	$('.Product_Cart_Remove').click(function(e) {
        var Product_Cart_Item = $(this).attr('class').split(' ')[1].split('_')[3];
		var Cart_Count = $('.product-cart-count').text();
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=RemoveCartItem&returnFormat=json', 
			data: {ItemUID: Product_Cart_Item},
			cache: false, 
			dataType: 'json',
			success: function(removeStatus) {
				
				$.ajax({ 
					async: false,
					type: 'POST', 
					url: './proxy.cfm?service=CartMgmt&method=GetCartCount&returnFormat=json', 
					cache: false, 
					dataType: 'json',
					success: function(CartCount) {
						Cart_Count = CartCount;
					}
				});
				
				if(Cart_Count > 0){
					$('#'+Product_Cart_Item).fadeOut();
					
					setTimeout(function(){
						$('#'+Product_Cart_Item).remove();
					}, 3000);
					
					// Set Cart Count
					$('.product-cart-count-symbol').text(Cart_Count);
				} else {
					$('.product-cart-count-symbol').hide();
					$('.product-cart-count-symbol').text("0");
					$('.product-shopping-cart').removeClass('product-cart-full');
					$('.product-cart-cont-show').hide();
					$('.product-cart-empty-hidden').fadeIn();	
					$('body').css("overflow","scroll");
                    $('div.container.container-main').css("width","700px");
				}
				
			}
		});
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=GetCartSubtotal&returnFormat=json', 
			cache: false, 
			dataType: 'json',
			success: function(subtotal) {
				$('.product-cart-count').text(Cart_Count);
				$('.product-cart-subtotal strong').text(subtotal);
			}
		});
    });
	
	// Update Item in Cart
	$('.Product_Cart_Qty').change(function(e) {
        var Product_Qty_Item = $(this).attr('class').split(' ')[3].split('_')[3];
		var Product_Qty = $(this).val();
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=UpdateCartItem&returnFormat=json', 
			data: {Company: 'hsc', ItemUID: Product_Qty_Item, ItemQty: Product_Qty},
			cache: false, 
			dataType: 'json',
			success: function(updateStatus) {
				$('.Product_Cart_Price_'+Product_Qty_Item).text(updateStatus);
			}
		});
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=GetCartSubtotal&returnFormat=json', 
			cache: false, 
			dataType: 'json',
			success: function(subtotal) {
				$('.product-cart-subtotal strong').text(subtotal);
			}
		});
		
    });
	
	// Clear Items from Cart
	$('.Product_Cart_Clear').click(function(e) {
		var Product_Items = [];
		
		$('.Product_Cart_Remove').each(function(cart_item) {
            var Product_Cart_Clear_Item = $(this).attr('class').split(' ')[1].split('_')[3];
			Product_Items[cart_item] = Product_Cart_Clear_Item;
        });
		
		$.ajax({ 
			async: false,
			type: 'POST', 
			url: './proxy.cfm?service=CartMgmt&method=RemoveCartItem&returnFormat=json', 
			data: {ItemUID: JSON.stringify(Product_Items)},
			cache: false, 
			dataType: 'json',
			success: function(removeStatus) {
				$('.product-cart-cont-show').hide();
				$('.product-cart-empty-hidden').fadeIn();
			}
		});
		
		// Set Cart Count
		$('.product-cart-count-symbol').hide();
		$('.product-cart-count-symbol').text("0");
		$('.product-shopping-cart').removeClass('product-cart-full');
	});
	
	// Toggle Payment Radio
	$('.payment-method-toggle').change(function(e) {
		var PaymentOptionSelected = $(this).attr('id').split(/[_ ]+/).pop();
		var PaymentOptionType = $(this).attr('class').split(' ')[1];
		togglePaymentBox(PaymentOptionSelected,PaymentOptionType);
    });
	
	// Set Default Ship/Bill Address
	$('#Cart_Shipping_Address_ID_Submit').val($('#Cart_Ship_Address_Cust_ID_'+1).val());
	$('#Cart_Shipping_Address_Name_Submit').val($('#Cart_Ship_Address_Name_'+1).val());
	$('#Cart_Billing_Address_ID_Submit').val($('#Cart_Ship_Address_Cust_ID_'+1).val());
	$('#Cart_Billing_Address_Name_Submit').val($('#Cart_Ship_Address_Name_'+1).val());
	
	// Toggle Ship/Bill Radio
	$('.cart-toggle').change(function(e) {
		var OptionSelected = $(this).attr('id').split(/[_ ]+/).pop();
		var OptionType = $(this).attr('class').split(' ')[1];
		var OptionRow = $(this).attr('id').split('_')[4];
		toggleBox(OptionSelected,OptionType,OptionRow);
    });
	
	//Toggle Shipping Address Btn
	$('.cart-toggle-ship-btn').click(function(e) {
		$(".address-shipnew").trigger("click");
    });
	
	//Toggle Billing Address Btn
	$('.cart-toggle-bill-btn').click(function(e) {
		$(".address-billnew").trigger("click");
    });
	
	// Check if New Ship Address selected
	if(!$('.address-shipnew').is(':checked')){
		$('.cart-toggle-content-ship').hide();
	}
	
	// Check if New Bill Address selected
	if(!$('.address-billnew').is(':checked')){
		$('.cart-toggle-content-bill').hide();
	}
});

function toggleBox(OptionSelected,OptionType,OptionRow) {
	
	var classType = "";
	
	if(OptionType === "address-ship" || OptionType === "address-shipnew"){
		classType = "ship";
	} else {
		classType = "bill";	
	}
	
	$('.checkout-box').each(function(index, element) {
		if($(this).hasClass("address-"+classType+"-cont")){
        	$(this).removeClass("checkout-address-selected");
		}
    });
	
	if(classType === "ship"){
		if(OptionType === "address-shipnew"){
			$('.cart-toggle-content-ship').fadeIn();
		} else {
			$('.cart-toggle-content-ship').hide();
		}
		
		$('#Cart_Shipping_Address_ID_Submit').val($('#Cart_Ship_Address_Cust_ID_'+OptionRow).val());
		$('#Cart_Shipping_Address_Name_Submit').val($('#Cart_Ship_Address_Name_'+OptionRow).val());
	}
	
	if(classType === "bill"){
		if(OptionType === "address-billnew"){
			$('.cart-toggle-content-bill').fadeIn();
		} else {
			$('.cart-toggle-content-bill').hide();
		}
		
		$('#Cart_Billing_Address_ID_Submit').val($('#Cart_Bill_Address_Cust_ID_'+OptionRow).val());
		$('#Cart_Billing_Address_Name_Submit').val($('#Cart_Bill_Address_Name_'+OptionRow).val());
	}
	
	$('.checkout-box-'+OptionSelected).addClass("checkout-address-selected");
	
}

function togglePaymentBox(PaymentOptionSelected,PaymentOptionType) {
	
	$('.payment-box').each(function(index, element) {
    	$(this).removeClass("payment-method-selected");
    });
	
	if(PaymentOptionType === "payment-method-cardnew"){
		$('.payment-method-toggle-content').fadeIn();
		$('#Payment_Option').val("Credit");
	} else {
		$('.payment-method-toggle-content').hide();
		$('#Payment_Option').val("Invoice");
	}
	
	$('.payment-box-'+PaymentOptionSelected).addClass("payment-method-selected");
	
}

function getCartCardType() {
	var Card_ID = $('#Payment_Card_Number').val();
	var Visa = new RegExp('^4[0-9]{6,}$');
	var MasterCard = new RegExp('^5[1-5][0-9]{5,}$');
	var AmericanExpress = new RegExp('^3[47][0-9]{5,}$');
	var Discover = new RegExp('^6(?:011|5[0-9]{2})[0-9]{3,}$');
	
	if(Card_ID.length >=6) {
		$('.card-img img').css("box-shadow","0px 2px 2px -2px #000000");
		$('.card-img').show();
		$('.card-img img').show();
		
		if(Visa.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Visa.png");
			$('#Card_Type').val("visa");
		} else if(MasterCard.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Mastercard.png");	
			$('#Card_Type').val("mastercard");
		} else if(AmericanExpress.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/AmericanExpress.png");	
			$('#Card_Type').val("amex");
		} else if(Discover.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Discover.png");
			$('#Card_Type').val("discover");
		} else {
			$('.card-img img').hide();
		}
	} else {
		$('.card-img img').hide();	
	}
}