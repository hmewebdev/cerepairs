
$(function() {
	
	 $('span.invoice_frame').empty();

	/* Datepicker Defaults */
	$( "#INV_Date_From" ).click(function(e) {
        $(this).datepicker('setDate', null);
    });
	
	$( "#INV_Date_To" ).click(function(e) {
        $(this).datepicker('setDate', null);
    });
	
	$( "#INV_Date_From" ).datepicker({
		onSelect: function(){
			if(!$("#INV_Date_To").val().length){
				$("#INV_Date_To").val($(this).val());
			}
		}	
	});
	
	$( "#INV_Date_To" ).datepicker({
		onSelect: function(){
			if(!$("#INV_Date_From").val().length){
				$("#INV_Date_From").val($(this).val());
			}
		}	
	});
	/* Datepicker Defaults */
	
	// Toggle items per page
	$('#CSP_Per_Page').change(function(e) {
		//$('#CSP_Search_Page').val($('#Product_Search').val());
		$('#CSP_Current_Page').val(1);
        $('#INV_Search_Form').submit();
    });
	
	// Set to Page 1 if new search
	$('#INV_Search_Submit').click(function(e) {
        $('#CSP_Current_Page').val(1);
    });
	
});

function updateStatus(Row) {
	$('.inv-row-'+Row).on('shown.bs.collapse', function() {$(".plus"+Row).addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');});
	$('.inv-row-'+Row).on('hidden.bs.collapse', function() {$(".plus"+Row).addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');});
}

function pauseStatus(Row, Cust_ID, Cust_Company) {
	$('.INV_Result_Rows').on('click', function (e) { e.stopPropagation(); })
	$('#Cust_Number_Anchor').val(Cust_ID);
	$('#Cust_Number_Anchor_Company').val(Cust_Company);
	$('#INV_Number_Anchor').val(Row);
	$('#Cust_Anchor_Form').submit();
}

function printInvoice() {
	var inv_param = $('#INV_Number').val();
	var cna_param = $('#Cust_Number_Anchor').val();
	var cnac_param = $('#Cust_Number_Anchor_Company').val();
	var prnt_url = './?pg=Invoices&st=Print&cna='+cna_param+'&cnac='+cnac_param+'&inv='+inv_param
	
	$.ajax({ 
		async: false,
		type: 'POST', 
		url: prnt_url
	});
	
	$('#invoicePrint').modal("show");
}