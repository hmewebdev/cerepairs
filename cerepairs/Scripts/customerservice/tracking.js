function toggleTrackingHistory(obj){
	obj.html(obj.html() == 'Show History' ? 'Hide History' : 'Show History')		
	$('.tracking-activities').toggle();
}

function tracking_dropdown_selected(obj){
	displayTrackingInfo(obj.value,$(obj).find('option:selected').text());
}

function tracking_history(order_number){
	var trackingNumbers = jQuery.grep(jsonTracking, function(obj) {
    	return obj.salesid === order_number;
	});
	
	if(trackingNumbers.length > 0){	
		if(trackingNumbers.length == 1){	
			$('#trackingHeader').html('');
			$('#tracking_number_dropdown_container').hide();
			
			if(trackingNumbers[0].shipcarriername.length > 0){
				ShipCarrierName = trackingNumbers[0].shipcarriername;
			} else {
				if(trackingNumbers[0].trackingnumber.indexOf("Z") == -1){
					ShipCarrierName = "FEDEX";
				} else {
					ShipCarrierName = "UPS";
				}
			}
			
			displayTrackingInfo(ShipCarrierName, trackingNumbers[0].trackingnumber);
		}else{
			var dropdown=$('.tracking-number-dropdown');
			dropdown.find("option:gt(0)").remove();
			$.each(trackingNumbers, function(index, trackingInfo) {
				dropdown.append($('<option>', {value: trackingInfo.shipcarriername, text: trackingInfo.trackingnumber}, '</option>'))		
			});
			dropdown[0].selectedIndex = 0;
			$('#tracking_number_dropdown_container').show();
			tracking_modal_setup('Select a tracking number from the dropdown below.');
		}
	}else{
		tracking_modal_setup('No tracking number(s) are available at this time.');
	}
}

function tracking_modal_setup(msg){
	$('#trackingHeader').html(msg);
	$('#trackingModalHistoryBtn').hide();
	$('.tracking-activities').hide();
	$('#tracking_history_container').hide();
	$('#trackingModal').modal('show');
}

var tracking_spinner;
var service = 'shippo';
//var service = 'serviceProvider';
function displayTrackingInfo(carrier, trackingNumber){
	var target = document.getElementById('tracking_header');
	var opts = spinnerOptions();
	tracking_spinner  = new Spinner(opts).spin(target);
	console.log('{"service":"' + service + '", "carrier":"' + carrier + '", "trackingNumber":"' + trackingNumber + '"}')
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "tracking_service.cfm",
		data: '{"service":"' + service + '", "carrier":"' + carrier + '", "trackingNumber":"' + trackingNumber + '"}',
		dataType: "json",
		success: function (data) {					
			tracking_spinner.stop();
			if (data !== null) {
				//"184200".upsTime();
				//"20160921".upsDate('M dd, yy', true)
				console.log(data);
				switch(service){
					case "shippo":
						if( typeof data.carrier == 'string' && data.tracking_status != null){
							shippo(new shippoClass(data));
							$('#tracking_unavailable_container').hide();
							$('#tracking_history_container').show();
							$('#trackingModalHistoryBtn').show();
						}
						else{
							$('#tracking_history_container').hide();
							$('#trackingModalHistoryBtn').hide();
							$('#tracking_unavailable_container').show();
							$('#trackingHeader').html('Invalid Tracking Number!');
						}
						break;
					case "serviceProvider":
						$('#trackingModalHistoryBtn').show();
						switch(data.carrier){
							case "fedex":
								break;
							case "ups":
								break;
						}
						break;
				}
				//$('#trackingModalHistoryBtn').show();
				$('#trackingModalHistoryBtn').html('Show History');
				$('.tracking-activities').hide();
				$('#trackingModal').modal('show');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			tracking_spinner.stop();
			console.log(thrownError);
			window.location = '/security/login/';
		}
	});	
}

/*
$('#trackingModal').on('shown.bs.modal', function () {
		if($('#tracking_status_right').prop('clientHeight') > $('#tracking_status_left').prop('clientHeight'))
			$('#tracking_status_left').height($('#tracking_status_right').height());
		else
			$('#tracking_status_right').height($('#tracking_status_left').height());
})
*/

function shippo(carrier){
	jsonObj = carrier.json;
	var date = '', time = '';
	$('#trackingHeader').html(carrier.carrier.toUpperCase() + ' - ' + carrier.tracking_number);
	if(carrier.tracking_status != null){
		date = (carrier.tracking_status_date).formatDate('M dd, yy', true);
		time = (carrier.tracking_status_date).timeFormat();
		$('#tracking_status_left').html(carrier.tracking_status_status + ' <div><strong>' + date + '</strong></div><div class="lite-font">'+ time + '</div>');
		if(carrier.tracking_status_status == 'DELIVERED'){
			$('#tracking_status_right').html(carrier.shipTo());
		}else{
			$('#tracking_status_right').html(carrier.tracking_status_status_details + '<div class="lite-font">' + carrier.history_location_address(0) + '</div>');
		}
		shippoHistory(carrier);
		carrier = null;
	}else{
		$('#tracking_status_left').html('Tracking number unknown');
		$('#tracking_status_right').html('');
	}
}

function shippoHistory(carrier){
	var li, div;
	
	$('#carrier_link').html('');
	console.log(carrier);
	switch(carrier.carrier){
		case 'ups':
			$('#carrier_link').html('<a href="https://wwwapps.ups.com/tracking/tracking.cgi?tracknum=' + carrier.tracking_number+'" target="_blank">Go to '+carrier.carrier.toUpperCase()+' Web Site</a>');
			break;
		case 'fedex':
			$('#carrier_link').html('<a href="https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber=' + carrier.tracking_number+'&cntry_code=us" target="_blank">Go to '+carrier.carrier.toUpperCase()+' Web Site</a>');
			break;
		case 'dhl_express':
			$('#carrier_link').html('<a href="http://www.dhl.com/en/express/tracking.html?AWB=' + carrier.tracking_number+'&brand=DHL" target="_blank">Go to '+carrier.carrier.toUpperCase()+' Web Site</a>');
			break;
	}
	
	$('#trackingHistory').html('');
	$.each(carrier.history, function(i, item) {
		carrier.date = carrier.history_status_date(i);
		carrier.time = carrier.history_status_time(i);

		li = $('<li>',{'class':'tracking-activity'})
		div = $('<div>',{
			'class':'tracking-activity-time',
			'html':"<strong>" + carrier.dateFormat() + "</strong><div class='lite-font'>" + carrier.timeFormat() + "</div>"
		}).appendTo(li);
		switch(carrier.history_tracking_status_icon(i)){
			case 'D':
				div = $('<div>',{
					'class':'tracking-status-img delivered tracking-status-icon',
					'html':"<i class='fa fa-check' style='font-size:15px; color:#fff;'></i>"
				}).appendTo(li);
				break;
			case 'F':
				div = $('<div>',{
					'class':'tracking-status-img exception tracking-status-icon',
					'html':"<i class='fa fa-exclamation' style='font-size:15px; color:#fff;'></i>"
				}).appendTo(li);
				break;
			case 'OFD':
				div = $('<div>',{
					'class':'tracking-status-img outfordelivery tracking-status-icon',
					'html':"<i class='fa fa-truck' style='font-size:15px; color:#fff;'></i>"
				}).appendTo(li);
				break;
			case 'BIR':
				div = $('<div>',{
					'class':'tracking-status-img inforeceived tracking-status-icon',
					'html':"<i class='fa fa-file-text-o' style='font-size:15px; color:#fff;'></i>"
				}).appendTo(li);
				break;
			default:
				div = $('<div>',{
					'class':'tracking-status-img intransit tracking-status-icon',
					'html':"<i class='fa fa-check' style='font-size:15px; color:#fff;'></i>"
				}).appendTo(li);
				break;
		}
		div = $('<div>',{
			'class':'tracking-activity-content',
			'html':"<strong>" + carrier.history_status_details(i) + "</strong><div class='lite-font'>" + carrier.history_location_address(i) + "</div>"
		}).appendTo(li);		
		li.appendTo($('#trackingHistory'));
	});
}

String.prototype.timeFormat = function (){
	var date = new Date(this);
	if (date.getHours() >= 12){
		var hour = parseInt(date.getHours()) - 12;
		var amPm = "PM";
	} else {
		var hour = date.getHours(); 
		var amPm = "AM";
	}
	minutes = parseInt(date.getMinutes());
	hour = (hour < 10 ? '0' : '') + hour;  
	minutes = (minutes < 10 ? '0' : '') + minutes;  
	var time = hour + ":" + minutes + " " + amPm;
	return time;
}

String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); 
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

	hours = (hours < 10 ? '0' : '') + hours;  
	minutes = (minutes < 10 ? '0' : '') + minutes;  
	seconds = (seconds < 10 ? '0' : '') + seconds; 
	
    return hours + ':' + minutes + ':' + seconds;
}

String.prototype.upsTime = function () {
	var amPm = ' AM';
   	var hh = parseInt(this.substr(0,2));
   	var mm = parseInt(this.substr(2,2));
   	var ss = parseInt(this.substr(4,2));

	if(hh > 12){
		amPm = ' PM';
		hh -= 12;
	}
	
	hh = (hh < 10 ? '0' : '') + hh;  
	mm = (mm < 10 ? '0' : '') + mm;  
	ss = (ss < 10 ? '0' : '') + ss; 
	 
    return hh+':'+mm+':'+amPm;
}

String.prototype.upsDate = function (format, toUpper) {
   	var mm = this.substr(4,2);
   	var dd = this.substr(6,2);
   	var yy = this.substr(0,4);
	var date = mm + '/' + dd + '/' + yy;
	var dateFormat = date.formatDate(format, toUpper);

    return dateFormat;
}

String.prototype.formatDate = function (format, toUpper) {
	//var xdate = convertUTCDateToLocalDate(new Date(this));
	//console.log(xdate);	
	var date = $.datepicker.formatDate(format, new Date(this));
	if(toUpper)
    	return date.toUpperCase();
	else
    	return date;
}

function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;   
}

function fedexClass() {
    this.date = '';
    this.time = '';
    this.dateFormat = function(){return this.date.formatDate('M dd, yy', true);}
    this.timeFormat = function(){return this.time.timeFormat();}
}

function shippoClass(_json) {
    this.json = _json;
    this.date = '';
    this.time = '';
    this.dateFormat = function(){return this.date.formatDate('M dd, yy', true);}
    this.timeFormat = function(){return this.time.timeFormat();}
	this.history = this.json.tracking_history;
	this.history_status_details = function(i){return this.history[i].status_details;}
	this.history_status = function(i){return this.history[i].status;}
	this.history_status_date = function(i){return this.history[i].status_date;}
	this.history_status_time = function(i){return this.history[i].status_date;}
	this.tracking_status = this.json.tracking_status;
	this.tracking_status_date = this.json.tracking_status.status_date;
	this.tracking_status_status = this.json.tracking_status.status;
	this.tracking_status_status_details = this.json.tracking_status.status_details;
	this.history_tracking_status_icon = function(i)
		{
			var status = '';
			switch(this.history[i].status){
				case 'FAILURE':
					status = 'F';
					break;
				case 'DELIVERED':
					status = 'D';
					break;
				case 'TRANSIT':
					switch(this.history[i].status_details){
						case 'OUT FOR DELIVERY':
							status = 'OFD';
							break;
						case 'On FedEx vehicle for delivery':
							status = 'OFD';
							break;
						default:
							break;
					}
					break;
				case 'UNKNOWN':
					switch(this.history[i].status_details){
						case 'BILLING INFORMATION RECEIVED':
							status = 'BIR';
							break;
						case 'Shipment information sent to FedEx':
							status = 'BIR';
							break;
						default:
							break;
					}
					break;
				default:
					break;
			}
			return status;
		}
	this.shipTo = function()
		{
			var address = '';
			if(this.json.address_to != null){
				address_to = this.json.address_to;
				if('city' in address_to)
					address += address_to.city == null ? '' : address_to.city;
				if('state' in address_to)
					address += address_to.state == null ? '' : (address != '' ? ', ' : '') + address_to.state;
				if('zip' in address_to)
					address += address_to.zip == null ? '' : (address != '' ? ' ' : '') + address_to.zip;
				if('country' in address_to)
					address += address_to.country == null ? '' : (address != '' ? ' ' : '') + address_to.country;
			}
			return address;
		}
	this.shipFrom = function()
		{
			var address = '';
			if(this.json.address_from != null){
				address_from = this.json.address_from;
				if('city' in address_from)
					address += address_from.city == null ? '' : address_from.city;
				if('state' in address_from)
					address += address_from.state == null ? '' : (address != '' ? ', ' : '') + address_from.state;
				if('zip' in address_from)
					address += address_from.zip == null ? '' : (address != '' ? ' ' : '') + address_from.zip;
				if('country' in address_from)
					address += address_from.country == null ? '' : (address != '' ? ' ' : '') + address_from.country;
			}
			return address;
		}
	//this.json.address_to.city + ', ' + this.json.address_to.state + ' ' + this.json.address_to.country;
	this.carrier = this.json.carrier;
	this.tracking_number = this.json.tracking_number;
	this.history_location_address = function(i)
		{
			var address = '';
			if(this.history[i].status_details == 'BILLING INFORMATION RECEIVED'){
				address = this.shipFrom();
			}else{
				if(this.history[i].location != null){
					var location = this.history[i].location;
					if('city' in location)
						address += location.city == null ? '' : location.city;
					if('state' in location)
						address += location.state == null ? '' : (address != '' ? ', ' : '') + location.state;
					if('zip' in location)
						address += location.zip == null ? '' : (address != '' ? ' ' : '') + location.zip;
					if('country' in location)
						address += location.country == null ? '' : (address != '' ? ' ' : '') + location.country;
				}
			}
			return address;
		}

	_json.tracking_history.reverse();
}

function upsClass() {
    this.date = '';
    this.time = '';
    this.dateFormat = function(){return this.date.formatDate('M dd, yy', true);}
    this.timeFormat = function(){return this.time.timeFormat();}
}