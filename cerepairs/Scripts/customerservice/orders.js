$(function() {
	
	$( "#Date_From" ).click(function(e) {
        $(this).datepicker('setDate', null);
    });
	
	$( "#Date_To" ).click(function(e) {
        $(this).datepicker('setDate', null);
    });
	
	$( "#Date_From" ).datepicker({
		onSelect: function(){
			if(!$("#Date_To").val().length){
				$("#Date_To").val($(this).val());
			}
		}	
	});
	
	$( "#Date_To" ).datepicker({
		onSelect: function(){
			if(!$("#Date_From").val().length){
				$("#Date_From").val($(this).val());
			}
		}	
	});
	
	if($('#Cust_Number_Anchor').val().length){
		$('.cust-field').hide();
		$('.no-cust-field').show();
	} else {
		$('.no-cust-field').hide();	
		$('.cust-field').show();
	}
	
	// Toggle items per page
	$('#CSP_Per_Page').change(function(e) {
		//$('#CSP_Search_Page').val($('#Product_Search').val());
		$('#CSP_Current_Page').val(1);
        $('#SO_Search_Form').submit();
    });
	
	// Set to Page 1 if new search
	$('#SO_Search_Submit').click(function(e) {
        $('#CSP_Current_Page').val(1);
    });
});

function updateStatus(Row) {
	$('.ord-row-'+Row).on('shown.bs.collapse', function() {$(".plus"+Row).addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');});
	$('.ord-row-'+Row).on('hidden.bs.collapse', function() {$(".plus"+Row).addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');});
}

function pauseStatus(Row){
	$('.SO_Result_Rows').on('click', function (e) { e.stopPropagation(); })
	$('#SO_Number_Anchor').val(Row);
	$('#Cust_Anchor_Form').submit();
}