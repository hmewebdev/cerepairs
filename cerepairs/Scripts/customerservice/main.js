var checkList = [];
var checkValue = '';
var checkAmounts = [];
var checkAmount = '';
var checkCusts = [];
var checkCust = '';

$(function() {
	var company_box = [];
	var apps_box = [];
	
	$('.company_checkbox').change(function(e) {
		 if(this.checked){
			company_box.push(" " + $(this).attr('id')); 
		 } else {
			company_box.splice( $.inArray($(this).attr('id'), company_box), 1);
		 }
		 
		 $('.selected_company').text(company_box);
    });
	
	$('.apps_checkbox').change(function(e) {
		 if(this.checked){
			apps_box.push(" " + $(this).attr('id')); 
		 } else {
			apps_box.splice( $.inArray($(this).attr('id'), apps_box), 1);
		 }
		 
		 $('.selected_apps').text(apps_box);
    });
	
	/* Pagination */
	
	// Paginate to Next page
	//$('#CSP_Next_Page').click(function(e) {
    //    $('#CSP_Current_Page').val(parseInt($('#CSP_Current_Page').val())+1);
    //});
	
	// Paginate to Previous page
	//$('#CSP_Prev_Page').click(function(e) {
    //    $('#CSP_Current_Page').val(parseInt($('#CSP_Current_Page').val())-1);
    //});
	
	/* Email Receipt Recipients Form */
	$('#payRecipients').click(function() {
		$('.send-receipt-msg').hide();
		$('.send-receipt-err').hide();
		$('.send-receipt-cont').show();
		$('#sendReceipt').show();
		$('#Recipient_List').val("");
		$('#payReciept').modal("show");
	});
	
	/* Email Receipt Status Message */
	$('#sendReceipt').click(function() {
		$('.send-receipt-cont').hide();
		$('#sendReceipt').hide();
		
		if($('#Recipient_List').val()){
			
			var Recipient_List = $('#Recipient_List').val();
			var Payment_Confirmation = $('.pay-confirm-number').text();
			
			$.ajax({ 
				async: false,
				type: 'POST', 
				url: './proxy.cfm?service=OrderInfo&method=sendEmailReceipt&returnFormat=json', 
				data: {Email_To: Recipient_List, Payment_Confirmation: Payment_Confirmation},
				cache: false, 
				dataType: 'json',
				success: function(payInfo) {
					//var pay_temp = JSON.parse(payInfo);
					//Payment_Message = pay_temp.PAYMENT_MESSAGE;
					//Payment_Confirmation = pay_temp.PAYMENT_CONFIRMATION;
				}
			});
			
			$('.send-receipt-msg').fadeIn();
		} else {
			$('.send-receipt-err').fadeIn();	
		}
	});
	
	/* On payment window closes */
	$('#payMessage').on('hidden.bs.modal', function () {
		$('#Card_Number').val("");
		$('#Card_Zip').val("");
		$('#Card_Memo').val("");
		checkList = [];
		checkAmounts = [];
	});
	
	/* Payment Back Button */
	$('#payConfirm_back').click(function(e) {
        $('.panel-ccinfo').hide();
		$('.panel-pmtinfo').fadeIn();
		$('#payConfirm_back').hide();
		$('#payConfirm_submit').show();
		$("#Card_Holder").prop('required',false);
		$("#Card_Number").prop('required',false);
		$("#Card_Zip").prop('required',false);
		$("#Card_Exp").prop('required',false);
		$("#Card_CVC").prop('required',false);
    });
	
	/* Pay Deposits */
	$('.pay-deposit').click(function() {
		$('.paySpinner').hide();
		$('.payMessage').show();
		$('.paySpinnerBtns').show();
		
		var cust_info = $(this).attr("id").split("-");
		var card_company = cust_info[0];
		var card_cust = cust_info[1];
		var card_curr = cust_info[2];
		
		$('#Payment_Currency').val(card_curr);
		$('#Cust_Number_Anchor').val(card_cust);
		$('#Cust_Number_Anchor_Company').val(card_company);
		
		$('.cart_row').remove();
	
		var Deposit_Payment_Rows = '<tr class="cart_row">'
			+'	<td class="text-center"></td>'
			+'	<td><span class="glyphicon glyphicon-check"></span> '+card_cust+'</td>'
			+'	<td><input type="text" value="'+currencyFormat(0)+'" size="13" onBlur="setPaymentDetails();"></td>'
			+'</tr>';
			
		$(Deposit_Payment_Rows).insertBefore($('tr.total-row'));
		$('span.card-count').text("1");
		$('span.card-amount').text(currencyFormat(0));
		$('.card-amount-confirm').text(currencyFormat(0));
		$('.card-curr-confirm').text($('#Payment_Currency').val());

		$('.no-payments-cont').hide();
		$('.payments-cont').show();
		$('#payConfirm_submit').show();
	});

	/* Pay Invoices */
	$('#pay-invoice').click(function() {	
		$('.paySpinner').hide();
		$('.payMessage').show();
		$('.paySpinnerBtns').show();
		
		if(getUrlParameter("st") == "View"){
			checkCusts = [];
			checkList = [];
			checkAmounts = [];
			
			checkCusts.push($('span.cust-id-view').text());
			checkList.push($('#INV_Number').val());
			checkAmounts.push($('.total-amt').text().replace("$",""));
		} else {
        	getCheckedBoxes();
		}
		
		setCheckedRows();
		
		if(checkList.length){
			$('.no-payments-cont').hide();
			$('.payments-cont').show();
			$('#payConfirm_submit').show();
		} else {
			$('.payments-cont').hide();
			$('#payConfirm_submit').hide();
			$('.no-payments-cont').show();
		}
		
		setPaymentDetails();
    });

	$("#Card_Confirm").submit(function(){
		//$('#payConfirm_submit').attr("disabled", "disabled");
		$('.paySpinner').show();
		$('.payMessage').hide();
		$('.paySpinnerBtns').hide();
		
		var Cust_Number = "";
		var Cust_Number_Company = "";
		
		if($('#Cust_Number_Anchor').val().length){
			Cust_Number = $('#Cust_Number_Anchor').val();	
		}
		
		var Payment_Message = "";
		var Payment_Confirmation = "";
		var Payment_Type = $('.modal-title span').text();
		var Payment_Cust_Numbers = checkCusts;
		var Payment_Numbers = checkList;
		var Payment_Amounts = checkAmounts;
		var Payment_Company = $('#INV_Company').val();
		var Payment_Total = $('.card-amount-confirm').text();
		var Payment_Currency = $('.card-curr-confirm').text();
		var Payment_Memo = $('#Payment_Memo').val();
		var hasCredit = false;
		var hasBalance = false;
		
		// Check for Credits
		$('.cart_row input').each(function(index, element) {
			if($(this).is(":disabled")){
				hasCredit = true;
			} else {
				hasBalance = true;	
			}
		});
		
		if((!hasCredit && parseInt($('.card-amount').text().replace("$","")) == 0) || (!hasBalance && hasCredit)){
			alert('Total Payment must be greater than $0.00.');
			return false;	
		} else {
			if(Payment_Type === "Deposit" && Payment_Numbers.length === 0){
				
				if($('#Cust_Number_Anchor_Company').val().length){
					Cust_Number_Company = $('#Cust_Number_Anchor_Company').val();	
				}
				
				// Make Deposit Payment
				$.ajax({ 
					async: false,
					type: 'POST', 
					url: './proxy.cfm?service=OrderInfo&method=setDeposits&returnFormat=json', 
					data: {Payment_Cust_Number: Cust_Number, Payment_Company: Cust_Number_Company, Payment_Total: Payment_Total, Payment_Currency: Payment_Currency, Payment_Memo: Payment_Memo},
					cache: false, 
					dataType: 'json',
					success: function(payInfo) {
						var pay_temp = JSON.parse(payInfo);
						Payment_Message = pay_temp.PAYMENT_MESSAGE;
						Payment_Confirmation = pay_temp.PAYMENT_CONFIRMATION;
						Payment_Token = pay_temp.PAYMENT_TOKEN;
						Payment_TokenID = pay_temp.PAYMENT_TOKENID;
						$('#PP_Payment_Token').val(Payment_Token);
						$('#PP_Payment_TokenID').val(Payment_TokenID);
					}
				});
				
			} else {
			
				// Make Invoice Payment
				$.ajax({ 
					async: false,
					type: 'POST', 
					url: './proxy.cfm?service=OrderInfo&method=setPayments&returnFormat=json', 
					data: {Payment_Cust_Numbers: JSON.stringify(Payment_Cust_Numbers), Payment_Numbers: JSON.stringify(Payment_Numbers), Payment_Amounts: JSON.stringify(Payment_Amounts), Payment_Type: Payment_Type, Payment_Company: Payment_Company, Payment_Total: Payment_Total, Payment_Currency: Payment_Currency, Payment_Memo: Payment_Memo},
					cache: false, 
					dataType: 'json',
					success: function(payInfo) {
						var pay_temp = JSON.parse(payInfo);
						Payment_Message = pay_temp.PAYMENT_MESSAGE;
						Payment_Confirmation = pay_temp.PAYMENT_CONFIRMATION;
						Payment_Token = pay_temp.PAYMENT_TOKEN;
						Payment_TokenID = pay_temp.PAYMENT_TOKENID;
						$('#PP_Payment_Token').val(Payment_Token);
						$('#PP_Payment_TokenID').val(Payment_TokenID);
					}
				});
				
			}

			return true;
		}

    });
	
	/* Pay Invoices */
	$('.card-img img').hide();
	
	$('.selectINV_Cred').click(function(e) {
       	$('#creditMsg').modal("show");
    });
	
	$('#Payment_Currency').change(function(e) {
       	$('.card-curr-confirm').text($('#Payment_Currency').val());
    });
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function currencyFormat(amount) {
	var dollarAmount;
	
	if(amount.length > 7){
		amount = amount.toString().replace(",","");
	}
	
	dollarAmount = '$' + parseFloat(amount, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
	
	return dollarAmount;
}

function dateFormat(date) {
	var dateFormat;
	
	 dateFormat = $.datepicker.formatDate('mm/dd/yy', new Date(date));
	
	return dateFormat;
}

function getIndexList(payment_array, payment_value){
    var indexMatches = [];
	
	for(index=0;index<payment_array.length;index++){
		if(payment_value == payment_array[index]){
			indexMatches.push(index);
		}
	}
	
    return indexMatches;
}

function checkAllBoxes(){
	if($('#selectINV_All').is(":checked")){
		$('.selectCHK').each(function(index, element) {
			if(!$(this).is(":disabled")){
				$(this).prop("checked", true);
			}
		});
	} else {
		$('.selectCHK').each(function(index, element) {
			$(this).prop("checked", false);	
		});
	}
	
	getCheckedBoxes();
}

function getCheckedBoxes(){
	
	var isDeposit = false;
	
	if(getUrlParameter("pg") == "Orders" || getUrlParameter("pg") == "Customers"){
		isDeposit = true;
	}
	
	// List Invoices First
	$('.selectCHK').each(function(index, element) {
		
		checkValueObj = JSON.parse($(this).val());
		checkValue = checkValueObj.VAL;
		checkCust = checkValueObj.CID;
		checkCurr = checkValueObj.CUR;
		checkAmount = Number(checkValueObj.AMT);
		
		if(checkAmount > 0 || isDeposit){
			if(this.checked){
				if($.inArray(checkValue, checkList) == -1){
					checkList.push(checkValue);
					checkAmounts.push(checkAmount);
					checkCusts.push(checkCust);
				}
				
				$('#Payment_Currency').val(checkCurr);
			} else {
				if($.inArray(checkValue, checkList) !== -1){
					checkList.splice($.inArray(checkValue, checkList), 1);
					checkAmounts.splice($.inArray(checkAmount, checkAmounts), 1);
					checkCusts.splice($.inArray(checkCust, checkCusts), 1);
				}
			}
		}
		
	});
	
	// List Credit Second
	$('.selectCHK').each(function(index, element) {
		
		checkValueObj = JSON.parse($(this).val());
		checkValue = checkValueObj.VAL;
		checkCust = checkValueObj.CID;
		checkCurr = checkValueObj.CUR;
		checkAmount = Number(checkValueObj.AMT);
		
		if(checkAmount < 0 && !isDeposit){
			if(this.checked){
				if($.inArray(checkValue, checkList) == -1){
					checkList.push(checkValue);
					checkAmounts.push(checkAmount);
					checkCusts.push(checkCust);
				}
				
				$('#Payment_Currency').val(checkCurr);
			} else {
				if($.inArray(checkValue, checkList) !== -1){
					checkList.splice($.inArray(checkValue, checkList), 1);
					checkAmounts.splice($.inArray(checkAmount, checkAmounts), 1);
					checkCusts.splice($.inArray(checkCust, checkCusts), 1);
				}
			}
		}
		
	});
	
	return checkList;
}

function setCheckedRows(){
	var Cart_Payment_Rows = "";
	var Cart_Total = 0;
	
	$("tr.cart_row").remove(); 
	
	if(checkList.length){
		for(cart_val=0;cart_val<checkList.length;cart_val++){
			
			Cart_Payment_Rows += '<tr class="cart_row">'
			+'	<td class="text-center"></td>'
			
			if(checkAmounts[cart_val] < 0){
				Cart_Payment_Rows += '<td class="cred_row"><span class="glyphicon glyphicon-minus"></span> '+checkList[cart_val]+'</td>'
				+'	<td class="cred_row"><input type="text" value="-'+currencyFormat(checkAmounts[cart_val]).replace("-","")+'" size="13" disabled="disabled"></td>'
			} else {
				Cart_Payment_Rows += '<td><span class="glyphicon glyphicon-check"></span> '+checkList[cart_val]+'</td>'
				+'	<td><input type="text" value="'+currencyFormat(checkAmounts[cart_val])+'" size="13" onBlur="setPaymentDetails();"></td>'	
			}
			
			Cart_Payment_Rows += '</tr>'
				
		}
	}
	
	if(checkAmounts.length){
		for(cart_amt=0;cart_amt<checkAmounts.length;cart_amt++){
			Cart_Total += checkAmounts[cart_amt];
		}
	}

	$(Cart_Payment_Rows).insertBefore($('tr.total-row'));
	$('span.card-count').text(checkList.length);
	if(Cart_Total < 0){
		$('span.card-amount').text("$0.00");
		$('.card-amount-confirm').text("$0.00");
	} else {
		$('span.card-amount').text(currencyFormat(Cart_Total));
		$('.card-amount-confirm').text(currencyFormat(Cart_Total));
	}
	$('.card-curr-confirm').text($('#Payment_Currency').val());
}

function setPaymentDetails(){
	var Cart_Total = 0;
	var temp_amount = 0;
	
	checkAmounts = [];
	
	$('.cart_row input').each(function(index, element) {
		temp_amount = parseFloat($(this).val().replace("$","").replace(",",""));
		
		if(isNaN(temp_amount)){
			$(this).val("$0.00");
			checkAmounts.push(0.00);
			Cart_Total += 0.00;
		} else {
			Cart_Total += temp_amount;
			if(temp_amount > 0){
				$(this).val(currencyFormat(temp_amount));
				checkAmounts.push(temp_amount);
			} else {
				if($(this).is(":disabled")){
					$(this).val("-"+currencyFormat(temp_amount).replace("-",""));	
					checkAmounts.push(temp_amount);
				} else {
					$(this).val("$0.00");
					checkAmounts.push(0.00);
				}
			}
		}
    });
	
	if(Cart_Total < 0){
		$('span.card-amount').text("$0.00");
		$('.card-amount-confirm').text("$0.00");
	} else {
		$('span.card-amount').text(currencyFormat(Cart_Total));
		$('.card-amount-confirm').text(currencyFormat(Cart_Total));
	}
	$('.card-curr-confirm').text($('#Payment_Currency').val());
	$('#PP_Payment_Amount').val(Cart_Total);
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	
	return true;
}

function numbersOnly(){
	var card_num = $('#Card_Number').val();
	card_num = card_num.replace(/[^0-9]/g, '');
	$('#Card_Number').val(card_num);
}

function triggerAnimation(element, value) {
	$(element).removeClass().addClass(value + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
	  $(this).removeClass();
	});
}

function getCardType() {
	var Card_ID = $('#Card_Number').val();
	var Visa = new RegExp('^4[0-9]{6,}$');
	var MasterCard = new RegExp('^5[1-5][0-9]{5,}$');
	var MasterCard2 = Card_ID.substring(0,6);
	var AmericanExpress = new RegExp('^3[47][0-9]{5,}$');
	var Discover = new RegExp('^6(?:011|5[0-9]{2})[0-9]{3,}$');
	
	if(Card_ID.length >=6) {
		$('.card-img img').show();
		
		if(Visa.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Visa.png");
			$('#Card_Type').val("visa");
		} else if(MasterCard.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Mastercard.png");	
			$('#Card_Type').val("mastercard");
		} else if(Number(MasterCard2) >= 222100 && Number(MasterCard2) <= 272099) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Mastercard.png");	
			$('#Card_Type').val("mastercard");
		} else if(AmericanExpress.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/AmericanExpress.png");	
			$('#Card_Type').val("amex");
		} else if(Discover.test(Card_ID)) {
			triggerAnimation(".card-img img", "flipInX");
			$('.card-img img').attr("src","./images/Discover.png");
			$('#Card_Type').val("discover");
		} else {
			$('.card-img img').hide();
		}
	} else {
		$('.card-img img').hide();	
	}
}

//function getUserTypeRoleList() {
//	var UserType = $('#User_Type').val(); 
//	var UserRoles = [];
//	var UserApps = [];
	
//	if(UserType === "Customer"){
//		$('.admin-perm-cont').hide(); 
//		$('#Apply_Credit').prop("checked", false);
//		$('#Apply_Deposit').prop("checked", false);
//	} else {
//		if(UserType === "Full Admin"){
//			$('#Apply_Credit').prop("checked", true);
//			$('#Apply_Deposit').prop("checked", true);
//		}
//		$('.admin-perm-cont').show(); 	
//	}
	
//	$.ajax({ 
//		async: false,
//		type: 'POST', 
//		url: './proxy.cfm?service=UserPermission&method=getUserRoleByType&returnFormat=json', 
//		data: {UserType: UserType},
//		cache: false, 
//		dataType: 'json',
//		success: function(roleInfo) {
//			$('#User_Role option').remove();
			
//			for(i=0;i<roleInfo.DATA.TYPEROLE_ID.length;i++){
//				UserRoles.push(roleInfo.DATA.TYPEROLE_NAME[i]);
				
//				$('<option value="'+roleInfo.DATA.TYPEROLE_ID[i]+'">'+roleInfo.DATA.TYPEROLE_NAME[i]+'</option>').appendTo('#User_Role');
//			}
//		}
//	});

//	$.ajax({ 
//		async: false,
//		type: 'POST', 
//		url: './proxy.cfm?service=UserPermission&method=getApplicationByType&returnFormat=json', 
//		data: {UserType: UserType},
//		cache: false, 
//		dataType: 'json',
//		success: function(appInfo) {
			
//			$('#User_Select_Apps option').remove();
//			$('.apps ul li').remove();
			
//			$('<li class="disabled"><span><input type="checkbox" disabled=""><label></label>Choose your option</span></li>').appendTo('.apps ul');
//			$('<option value="" disabled="" selected="">Choose your option</option>').appendTo('#User_Select_Apps');
			
//			for(i=0;i<appInfo.DATA.APP_FULLNAME.length;i++){
//				UserApps.push(appInfo.DATA.APP_FULLNAME[i]);
				
//				$('<li class=""><span><input type="checkbox"><label></label> '+appInfo.DATA.APP_FULLNAME[i]+'</span></li>').appendTo('.apps ul');
//				$('<option value="'+appInfo.DATA.APP_FULLNAME[i]+'">'+appInfo.DATA.APP_FULLNAME[i]+'</option>').appendTo('#User_Select_Apps');
//			}
			
//			$('.companies.apps').material_select('destroy');
//			$('.companies.apps').material_select();
//		}
//	});
//}

//---------------------------------------------------
// >>> CHATLIO
//---------------------------------------------------
	var TECH_WID = 'dbab36fc-7c35-4ade-41fc-94576814d759';
	var SALES_WID = '0fb1459f-d546-4b8b-5b36-7ff109a345c6';
	var SELECT_DEPT_STR = 	'<select id="chatlio-select-dept">' +
								'<option value="sales">Sales</option>' +
								'<option value="tech">Tech Support</option>' +
							'</select>';
	localStorage.curDept = localStorage.curDept || 'sales';

	function loadChatlio(){ 
		//console.log("localS: " + localStorage.curDept);
		var widgetId = localStorage.curDept === 'tech' ? TECH_WID : SALES_WID;
		var el = document.getElementById("chatlio-widget");

		/* Remove previous instances of chatlio */
		if (el) {el.parentNode.removeChild(el);}

	    //console.log(widgetId);

		/* Chatlio Script Begins */
		window._chatlio = [];
		var t=document.getElementById("chatlio-widget-embed");if(t&&window.ChatlioReact&&_chatlio.init)return void _chatlio.init(t,ChatlioReact);for(var e=function(t){return function(){_chatlio.push([t].concat(arguments)) }},i=["configure","identify","track","show","hide","isShown","isOnline"],a=0;a<i.length;a++)_chatlio[i[a]]||(_chatlio[i[a]]=e(i[a]));var n=document.createElement("script"),c=document.getElementsByTagName("script")[0];n.id="chatlio-widget-embed",n.src="https://w.chatlio.com/w.chatlio-widget.js",n.async=!0,n.setAttribute("data-embed-version","2.1");
	   n.setAttribute('data-widget-id', widgetId);
	   c.parentNode.insertBefore(n,c);
	}

	
	document.addEventListener('chatlio.ready', function (e) {
		$(".chatlio-offline-message-container").children("p").after( SELECT_DEPT_STR );
		var el = document.getElementById("chatlio-select-dept");

		$(el).change(function(){
			localStorage.curDept =  $(el).val();
			//alert(localStorage.curDept);
			loadChatlio();
			_chatlio.show({expanded: true});
		});

		if (el) { 
			var idx = localStorage.curDept === 'tech' ? 1 : 0;
			el.options.selectedIndex = idx; 
		}		
	});	   

	$("#btn-reset").click(function(){
		_chatlio.reset();
		localStorage.curDept = null;

	});

function adURL(content){
	return content.replace(/\"/g,'').replace(/'/g,'');
}