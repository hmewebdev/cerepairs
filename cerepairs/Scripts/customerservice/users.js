$(function() {
	
	// Toggle items per page
	$('#CSP_Per_Page').change(function(e) {
		//$('#CSP_Search_Page').val($('#Product_Search').val());
		$('#CSP_Current_Page').val(1);
        $('#User_Search_Form').submit();
    });
	
	// Set to Page 1 if new search
	$('#User_Search_Submit').click(function(e) {
        $('#CSP_Current_Page').val(1);
    });
	
	if($('#User_IsActive_Label').val() == "Active"){
		$('#User_IsActive_Label').removeClass("btn-danger");
		$('#User_IsActive_Label').addClass("btn-success");
		$('#User_IsActive').val(1);
	} else {
		$('#User_IsActive_Label').removeClass("btn-success");
		$('#User_IsActive_Label').addClass("btn-danger");
		$('#User_IsActive').val(0);
	}
	
	$('#User_IsActive_Label').click(function(e) {
		if($(this).val() == "Active"){
			$(this).val("Inactive");
			$(this).removeClass("btn-success");
			$(this).addClass("btn-danger");
			$('#User_IsActive').val(0);
		} else {
			$(this).val("Active");
			$(this).removeClass("btn-danger");
			$(this).addClass("btn-success");
			$('#User_IsActive').val(1);
		}
    });

});