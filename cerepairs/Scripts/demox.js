$('#actionButton').hide();
$('#phoneNo').hide();
$('.price').hide();

function hideAll() {
    $('#actionButton').hide();
    $('#phoneNo').hide();
    $('.price').hide();
}


$('.checkbox').checkbox(
	{
	    onChecked: function () {
	        var str = '';
	        var opt = [];
	        hideAll();
	        var radioValue = $('input[name=\'fruit\']:checked').val();
	        str += '<i class="dropdown icon"></i>';
	        str += '<div class="default text">Select Condition</div>';
	        str += '<div class="menu">';
	        if (radioValue === 'repair') {
	            opt = [];
	            if (Exchange_Price > 0) {
	                opt.push({
	                    name: 'Advanced Exchange',
	                    value: 'advanced',
	                    selected: true
	                });
	            }
	            if (Recondition_Price > 0 && Repair_Price > 0) {
	                var standard = {
	                    name: 'Standard/Recondition',
	                    value: 'standard'
	                };
	                if (Recondition_Price > 0 && Repair_Price > 0) {
	                    standard.selected = true;

	                }
	                opt.push(standard);
	            }
	        } else if (radioValue === 'new') {
	            opt = [];
	            if (Retail_Price > 0) {
	                opt.push({
	                    name: 'New',
	                    value: 'new',
	                    selected: true
	                });
	            }
	            if (Refurbished_Price > 0) {
	                var ref = {
	                    name: 'Refurbished',
	                    value: 'ref'
	                }
	                if (Retail_Price == 0) {
	                    ref.selected = true;
	                }
	                opt.push(ref);
	            }

	        }
	        str += '</div>';

	        $('.ui.dropdown')
			  .dropdown({
			      values: opt,
			      onChange: function (value, name) {
			          updatePrice(value);
			          updateModal(value, name)
			      }
			  });


	    }
	}

);

function updatePrice(prodCond) {
    var price = '';
    switch (prodCond) {
        case 'advanced':
            price = parseFloat(Exchange_Price).toFixed(2);
            $('.price').show();
            $('#phoneNo').show();
            $('#actionButton').hide();
            break;
        case 'standard':
            price = parseFloat(Repair_Price).toFixed(2) + '-' + parseFloat(Recondition_Price).toFixed(2);
            $('.price').show();
            $('#phoneNo').hide();
            $('#actionButton').html('Send It In');
            $('#actionButton').show();
            break;
        case 'new':
            price = parseFloat(Retail_Price).toFixed(2);
            $('#phoneNo').hide();
            $('.price').show();
            $('#actionButton').html('Add to Cart');
            $('#actionButton').show();
            break;
        case 'ref':
            price = parseFloat(Refurbished_Price).toFixed(2);
            $('.price').show();
            $('#phoneNo').hide();
            $('#actionButton').html('Add to Cart');
            $('#actionButton').show();
            break;
    }

    $('.amount').html(price);
}

function updateModal(prodCond, name) {
    var msg = '';

    switch (prodCond) {
        case 'advanced':
            msg = 'If you can\'t wait for a repair, CE will overnight you a refurbished unit in exchange for your broken unit. Once you receive the replacement unit, return your broken unit in the same box. A minimum $35 shipping fee will apply. Additional charges may apply for expedited shipping needs.';
            break;
        case 'standard':
            msg = 'Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician\'s assessment. In either case, a reconditioned replacement item may be returned to you.';
            break;
        case 'new':
            msg = 'These products are straight from the factory and carry the OEM warranty. CE is a fully authorized distributor of this equipment.';
            break;
        case 'ref':
            msg = 'This is a previously used product. Our technicians thoroughly check, repair, and upgrade to ensure like-new operation. Your purchase of these products includes a 4-month warranty. Please note that all refurbished items are subject to equipment availability.';
            break;
    }

    $('.ui.modal .header').html(name);
    $('.ui.modal .content').html(msg);
}

$('.details').on('click', function () {
    $('.tiny.modal').modal('show');

});

$(document).ready(function () {
    var arr = [];
    if (Exchange_Price > 0 || Recondition_Price > 0 || Repair_Price > 0) {
        if (Exchange_Price > 0) {
            arr.push({
                name: 'Advanced Exchange',
                value: 'advanced',
                selected: true
            });
        }
        if (Recondition_Price > 0 && Repair_Price > 0) {
            var standard = {
                name: 'Standard/Recondition',
                value: 'standard'
            }
            if (Exchange_Price == 0) {
                standard.selected = true;
            }
            arr.push(standard);
        }
    } else {
        $('input[name=\'fruit\'][value=new]').prop('checked', true);
        opt = [];
        arr.push({
            name: 'New',
            value: 'new',
            selected: true
        });
        if (Refurbished_Price > 0) {
            arr.push({
                name: 'Refurbished',
                value: 'ref'
            });
        }
    }
    $('.ui.dropdown')
      .dropdown({
          action: 'activate',
          values: arr,
          placeholder: 'Select Condition',
          onChange: function (value, name) {
              updatePrice(value);
              updateModal(value, name);
          }
      });

    $('#actionButton').on('click', function () {
        //console.log('#actionButton');
        //console.log($('.ui.form .price .amount').html());
        //console.log($('.ui.form .menu .item.active.selected').html());
        //console.log($('.ui.form #actionButton').attr('productUID'));
        //console.log($('.ui.form #actionButton').attr('freight'));
        var cartdto = {};
        cartdto.product = '';
        cartdto.count = '1';
        cartdto.category = '';
        cartdto.amount = $('.ui.form .price .amount').html();
        cartdto.productuid = product.Product_UID;
        cartdto.freight = parseFloat(product.Additional_Freight).toFixed(2).toString();
        cartdto.productpricetype = $('.ui.form .menu .item.active.selected').html();
        cartdto.status = '';
        cartdto.request = 'insert';

        $.ajax({
            type: "POST",
            //url: '/Payment/ProcessPayment',
            url: '/cart/AddToCart',
            data: JSON.stringify(cartdto),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                window.location = '/cart';
                //console.log(data);
                //$('.payment').html(data);
            }
        });
    });
});
