﻿var tryingToReconnect = false;
var chat;
var chatid;
$(document).ready(function () {
    console.log('$.connection.hub.start()');
    chat = $.connection.cerepairsHub;
    chat.client.broadcastMessage = function (dto) {
        console.log(dto);
        var url = '/Payment/PaymentConfirmationAjax';
        switch (dto.request) {
            case 'paypal':
                $(window).scrollTop(0);
                $('.paypal.payment').html(dto.data);
                if (dto.success == "true") {
                    $.ajax({
                        type: "POST",
                        url: '/Payment/ClearSession',
                        //data: JSON.stringify(dto),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            console.log(data)
                        }
                    });
                }
                break;
            case 'ccConformation':
                $('#ccProcess').hide();
                $('span .PaymentConfirmationCode').html(dto.data);
                $('#payment-conformation').show();
                break;
            case 'CreditInvoiceConformation':
                $('#payment').hide();
                $('span .PaymentConfirmationCode').html(dto.data);
                $('#payment-conformation').show();
                break;
        }
    };
    $.connection.hub.start().done(function () {
        tryingToReconnect = false;
        console.log('$.connection.hub.start().done');
        chatid = $.connection.hub.id;
        console.log(chatid);
    });

    $.connection.hub.reconnecting(function () {
        console.log('reconnecting');
        tryingToReconnect = true;
    });

    $.connection.hub.reconnected(function () {
        console.log('reconnected');
        tryingToReconnect = false;
    });

    $.connection.hub.disconnected(function () {
        if ($.connection.hub.lastError)
        { console.log("Disconnected. Reason: " + $.connection.hub.lastError.message); }
        console.log('disconnected');
        if (tryingToReconnect) {
            notifyUserOfDisconnect(); // Your function to notify user.
            setTimeout(function () {
                console.log('Trying to reconnect');
                $.connection.hub.start()
                 .done(function () {
                     tryingToReconnect = false;
                     console.log('$.connection.hub.start().done');
                     chatid = $.connection.hub.id;
                     console.log(chatid);
                 })
                .fail(function () {
                    console.log('Could not Connect!');
                });
            }, 5000); // Restart connection after 5 seconds.

        }
        else {
            console.log('$.connection.hub.start_2');
            setTimeout(function () {
                console.log('Trying to reconnect');
                $.connection.hub.start()
                 .done(function () {
                     tryingToReconnect = false;
                     console.log('$.connection.hub.start().done');
                     chatid = $.connection.hub.id;
                     console.log(chatid);
                 });
            }, 5000); // Restart connection after 5 seconds.
        }

    });

    $.connection.hub.connectionSlow(function () {
        notifyUserOfConnectionProblem(); // Your function to notify user.
    });
});

function notifyUserOfDisconnect() {
    console.log('notifyUserOfDisconnect');

}

function notifyUserOfConnectionProblem() {
    console.log('notifyUserOfConnectionProblem');

}

