﻿var obligatorys = [];
$(document).ready(function () {
    $('.register').on('click', function () {
        $('#User_Cust_ID').val($('#User_Cust_ID').val().replace(/\s/g, ''));
        obligatorys = [];
        $('.pop-msg2').remove();
        $('.regform form .red').removeClass('red');
        $('.regform form .obligatory').each(function (index, item) {
            var objID = $(this).attr('name');
            var label_text = $('label[for=' + objID + ']').html();
            var label_obj = $('label[for=' + objID + ']');
            switch ($(this).prop('type')) {
                case 'checkbox':
                    break;
                case 'hidden':
                    break;
                case 'select':
                    if ($(this).val() == null) {
                        $(label_obj).addClass('red');
                        obligatorys.push(label_text);
                    }
                    break;
                case 'text':
                case 'textarea':
                case 'password':
                    if ($(this).val() == '') {
                        $(label_obj).addClass('red');
                        obligatorys.push(label_text);
                    }
                    break;
            }
        });
        if (obligatorys.length > 0) {
            var json = { obj: $('.registration-header'), msg: 'Please complete the items in RED', left: 200, top: 10, append: $('.registration-header'), _class: 'pop-msg2' };
            popup_msg(json);
            $(window).scrollTop(0);
            return false;
        }

        if (!validateemail($('#User_EmailAddress').val())) {
            var json = { obj: $('.registration-header'), msg: 'Please provide a valid email address.', left: 200, top: 10, append: $('.registration-header'), _class: 'pop-msg2' };
            $('label[for=User_EmailAddress]').addClass('red');
            popup_msg(json);
            $(window).scrollTop(0);
            return false;
        }

        if ($('#User_Password1').val() != $('#User_Password2').val()) {
            $('label[for=User_Password1]').addClass('red');
            $('label[for=User_Password2').addClass('red');
            var json = { obj: $('.registration-header'), msg: 'Passwords do not match', left: 200, top: 10, append: $('.registration-header'), _class: 'pop-msg2' };
            popup_msg(json);
            $(window).scrollTop(0);
            return false;
        }
        var vpassword = passwordValidation($('#User_Password1').val());
        if (vpassword != 'OK') {
            $('label[for=User_Password1]').addClass('red');
            $('label[for=User_Password2').addClass('red');
            var json = { obj: $('.registration-header'), msg: vpassword, left: 200, top: 10, append: $('.registration-header'), _class: 'pop-msg2' };
            popup_msg(json);
            $(window).scrollTop(0);
            return false;
        }
    });

    $('#resetpassword').on('click', function () {
        if ($('#Password').val() == '') {
            $('label[for=Password]').addClass('red');
            var json = { obj: $('.resetpassword-header'), msg: 'Please complete the items in RED', left: 200, top: 10, append: $('.resetpassword-header'), _class: 'pop-msg2' };
            popup_msg(json);
            $(window).scrollTop(0);
            return false;
        }

        if ($('#Password').val() != $('#ConfirmPassword').val()) {
            $('label[for=Password]').addClass('red');
            $('label[for=ConfirmPassword').addClass('red');
            var json = { obj: $('.resetpassword-header'), msg: 'Passwords do not match', left: 200, top: 10, append: $('.resetpassword-header'), _class: 'pop-msg2' };
            popup_msg(json);
            $(window).scrollTop(0);
            return false;
        }

        var vpassword = passwordValidation($('#Password').val());
        if (vpassword != 'OK') {
            $('label[for=Password]').addClass('red');
            $('label[for=ConfirmPassword').addClass('red');
            var json = { obj: $('.resetpassword-header'), msg: vpassword, left: 200, top: 10, append: $('.resetpassword-header'), _class: 'pop-msg2' };
            popup_msg(json);
            $(window).scrollTop(0);
            return false;
        }
    });
});

function passwordValidation(pwd) {
    re = /\s/;
    if (re.test(pwd)) {
        return 'Error: Password cannot contain spaces.';
    }
    if (pwd.length < 8) {
        return 'Error: Password must contain at least eight (8) characters.';
    }
    if (pwd.length > 16) {
        return 'Error: Password cannot contain more than sixteen (16) characters.';
    }
    re = /[0-9]/;
    if (!re.test(pwd)) {
        return 'Error: Password must contain at least one number (0-9).';
    }
    re = /[a-z]/;
    if (!re.test(pwd)) {
        return 'Error: Password must contain at least one lowercase letter (a-z).';
    }
    re = /[A-Z]/;
    if (!re.test(pwd)) {
        return 'Error: Password must contain at least one uppercase letter (A-Z).';
    }
    return 'OK';
}

function validateemail(email) {
    //var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d!#$%&'*+\-\/=?^_`{|}~\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d!#$%&'*+\-\/=?^_`{|}~\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

    return pattern.test(email);
}
//!#$%&'*+-/=?^_`{|}~