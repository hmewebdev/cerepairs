$('.ui.dropdown')
	.dropdown()
;

$('.ui.radio.checkbox')
  .checkbox()
;

$('.ui.checkbox')
  .checkbox()
;

$('.info.segment .item')
  .tab({
    context: '.info.segment'
  })
;

$('.ui.accordion')
  .accordion({
    exclusive: false
  })
;

var mySwiper = new Swiper ('.swiper-container', {
	// Optional parameters

	loop: true,
slidesPerView: 1,
	// If we need pagination
	pagination: {
	  el: '.swiper-pagination',
	  clickable: true
	},

	// Navigation arrows
	navigation: {
	  nextEl: '.swiper-button-next',
	  prevEl: '.swiper-button-prev',
	},

	// And if we need scrollbar
	scrollbar: {
	  el: '.swiper-scrollbar',
	},
});

$('.counter .button').on('click', function() {

	var $button = $(this);
	var oldValue = parseInt($button.parent().find('input').val());
	var $dec = $button.parent().find('.dec');
	var newVal = 0;

	if ($button.hasClass('inc')) {
		//Any NaN will be converted to 1
		newVal = isNaN(oldValue) ? 1 : oldValue + 1;			
		if ($dec.hasClass('disable')){
			$dec.toggleClass('disable');
		}
	} else if ($button.hasClass('dec')) {
		// Don't allow decrementing below zero
		if (oldValue > 0) {
			newVal = oldValue - 1;
		} else {
			newVal = 0;
		}
		if(newVal === 0) {$button.addClass('disable');}
	}

  	$button.parent().find('input').val(newVal);

});


function rows(columns){

	var items = $('.grid .cell').length;
	var lastRowLen = items % columns;
	var itemIdx =  items - lastRowLen; 

	for(var i = 0; i < lastRowLen; i++ ){
		$('.grid .cell:nth('+ (i + itemIdx) +')').addClass('last-row');
	}

}

rows(3);




$('#actionButton').hide();
$('#phoneNo').hide();
$('.price').hide();


function hideAll(){
	$('#actionButton').hide();
	$('#phoneNo').hide();
	$('.price').hide();
}


$('.ce.radio').on('click', function() {


	console.log($(this).val());
    var radioValue = $(this).find('input[type=\'radio\']').val();

    if(radioValue === 'repair'){
    $('.fields').removeClass('visible');
    	$('#repair').addClass('visible');
    } else if(radioValue === 'buy') {
    $('.fields').removeClass('visible');
    	$('#buy').addClass('visible');
    }

    updatePrice(radioValue);
    updateDetails(radioValue, 'name');

});

		


function updatePrice (prodCond){
	var price = '';
	switch(prodCond){
		case 'advance':
			price = '$185.00';
			$('.price').show();
			$('#phoneNo').show();
			$('#actionButton').hide();
			break;
		case 'standard':
			price = '$134.00-$175.00';
			$('.price').show();
			$('#phoneNo').hide();
			$('#actionButton').html('Send it In');		
			$('#actionButton').show();
			break;
		case 'new':
			price = '$399.00';
			$('#phoneNo').hide();
			$('.price').show();
			$('#actionButton').html('Add to Cart');			
			$('#actionButton').show();			
			break;
		case 'refurbished':
			price = '$249.00';
			$('.price').show();
			$('#phoneNo').hide();
			$('#actionButton').html('Add to Cart');			
			$('#actionButton').show();				
			break;		
	}

	$('.amount').html(price);
}

function updateDetails(prodCond, name){
	var msg = '';

	switch(prodCond){
		case 'advance':
			msg = 'If you can\'t wait for a repair, CE will overnight you a refurbished unit in exchange for your broken unit. Once you receive the replacement unit, return your broken unit in the same box. A minimum $35 shipping fee will apply. Additional charges may apply for expedited shipping needs.';
			break;
		case 'standard':
			msg = 'Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician\'s assessment. In either case, a reconditioned replacement item may be returned to you.';
			break;
		case 'new':
			msg = 'These products are straight from the factory and carry the OEM warranty. CE is a fully authorized distributor of this equipment.';
			break;
		case 'refurbished':
			msg = 'This is a previously used product. Our technicians thoroughly check, repair, and upgrade to ensure like-new operation. Your purchase of these products includes a 4-month warranty. Please note that all refurbished items are subject to equipment availability.';
			break;		
	}

	$('.ui.modal .header').html(name);
	$('.details').html(msg);
}


// Scroll to a certain element
$('.condition .ce.radio').on('click', function(){
	// alert('hi');
	document.querySelector('.condition').scrollIntoView({ 
	  behavior: 'smooth' 
	});
});
