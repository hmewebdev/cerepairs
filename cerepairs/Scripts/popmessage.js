﻿function popup_msg(json) {
    var ele = create_element(json.msg, json._class);
    var position = getxy($(json.obj));
    $(ele).css({ top: position.top + (json.top), left: position.left + json.left });
    $(json.append).append(ele);
    var thread = new handlemsg();
    thread.do(ele);
}

function popup_msg_html(json) {
    var ele = create_element_span(json.msg, json._class);
    var position = getxy($(json.obj));
    //$(ele).css({ top: position.top + json.top, left: position.left + json.left });
    $(json.append).append(ele);
    //var thread = new handlemsg();
    //thread.do(ele);
}

function handlemsg() {
    if (delay > 0) {
    this.do = function (obj) {
        setTimeout(function () {
            $(obj).remove();
        }, 5000);
    };
}

function create_element(msg, _class) {
    return $('<div/>', {
        text: msg,
        class: _class
    });
}

function create_element_span(msg, _class) {
    var $div = $('<span/>', {
        class: _class
    });
    return $div.html(msg);
}

function create_element_html(msg, _class) {
    var $div = $('<div/>', {
        class: _class
    });
    return $div.html(msg);
}

function getxy(obj) {
    var position = {};
    position.left = $(obj).position().left;
    position.top = $(obj).position().top;
    position.x = $(obj).position().left;
    position.y = $(obj).position().top;
    return position;
}
