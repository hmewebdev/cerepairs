﻿var isgooglebolt = false;
var useBillingUnclick = false;
var shippinglabelimg = '';
var labeldto = null;
var PromoLookupJson = {};
var checkoutdto = {
    billinginfo: [],
    billingaddress: [],
    shippingaddress: [],
};
var fromFailure = false;
var salesTax = '';
var grandTotal = '';
$(document).ready(function () {
    isgooglebolt = navigator.userAgent.toLowerCase().indexOf("googlebot") != -1 ? true : false;
    console.log("googlebolt? " + isgooglebolt);
    if (getCookie('cookieConsent') == null || getCookie('cookieConsent') == '') {
        setTimeout(function () {
            $("#cookieConsent").fadeIn(200);
        }, 2000);
    }
    $("#closeCookieConsent, .cookieConsentOK").click(function () {
        setCookie('cookieConsent', '1');
        $("#cookieConsent").fadeOut(200);
    });

    pagination();

    $('.brand-link a').on('click touch tap', function (e) {
        $(this).parent().parent().find('input[type=checkbox]').click();
        productFilter($(this));
        e.stopImmediatePropagation();
        return false;
    });

    var ckobj = $(this);
    $('.accordion.menu .checkbox label').on('click touch tap', function (e) {
       
        ckobj = $(this);
        console.log($(this))
        setTimeout(function () {
            productFilter($(ckobj).find('a'));
        }, 1);
        //e.stopImmediatePropagation();

    });

    $('.celled.grid .faveriate').on('click touch tap', function (e) {
        var dto = {};
        dto.request = "add";
        dto.item = $(this).attr('uid');
        $.ajax({
            url: "/Faveriate/AddFaveriate",
            type: "POST",
            data: JSON.stringify(dto),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dto) {
                console.log(dto);
                if (dto.status == 'OK') {
                }
            },
            error: function (response) {
            }
        });
        e.stopImmediatePropagation();
    });

    $('.search-btn').on('click touch tap', function (e) {
        var val = $(this).parent().find('input[type=text]').val()
        if (val.length < 2) return false;
        $('.search-form #searchText').val(val);
        $('.search-form').submit();
        e.stopImmediatePropagation();
        return false;
    });

    $('.search-text').on('keypress', function (e) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            var val = $(this).parent().find('input[type=text]').val()
            if (val.length < 2) return false;
            $('.search-form #searchText').val(val);
            $('.search-form').submit();
            e.stopImmediatePropagation();
            return false;
        }
        e.stopImmediatePropagation();
    });

    $('.searchform').on('click touch tap', function (e) {
        $('.search-form #searchText').val($(this).attr('searchfor'));
        if ($(this).hasClass('list')) {
            $('.search-form #list').val('List');
        }
        else {
            $('.search-form #list').val('');
        }
        $('.search-form').submit();
        e.stopImmediatePropagation();
        return false;
    });

    $('.contactinfo').on('click touch tap', function (e) {
        var dto = {}
        var obligatorys = [];
        $('.contactform .obligatory').removeClass('red-border');
        $('.contactform .obligatory').each(function (i, item) {
            if ($(item).val() == '') {
                obligatorys.push(item.id);
                $(item).addClass('red-border')
            }
        });
        if (obligatorys.length > 0) {
            var json = { obj: $('.contactform .contactinfo.contact-button').parent(), msg: 'Please complete the required fields underlined in red', left: 0, top: 10, append: $('.contactform .contactinfo.contact-button').parent(), _class: 'pop-msg2' };
            popup_msg(json, 5000);
            e.stopImmediatePropagation();
            return false;
        }
        if (grecaptcha.getResponse(recaptcha3) == '') {
            $('.contactinfo.contact-button').hide();
            $('.captchaContact-div').show();
            e.stopImmediatePropagation();
            return false;
        }
        $('.contactform .input').each(function (i, item) {
            dto[item.id] = $(item).val()
        });
        $(".contactinfo-spinner").children(":first").css('left', $('.contactinfo.contact-button').width() + 59 + 'px');
        $('.contactinfo-spinner').show();

        $.ajax({
            type: 'POST',
            url: '/home/ContactInfo',
            data: JSON.stringify(dto),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (ret) {
                $('.contactinfo-spinner').hide();
                var json = { obj: $('.contactform .contactinfo.contact-button').parent(), msg: 'Contact information send', left: 0, top: 50, append: $('.contactform .button.contactinfo').parent(), _class: 'pop-msg2' };
                popup_msg(json, 5000);
                $('.contactform .input').val('');
            }
        });
        e.stopImmediatePropagation();
        return false;
    });

    $('.special-offers .special-offers-button').on('click touch tap', function (e) {
        var dto = {}
        var obligatorys = [];
        $('.special-offers .input .obligatory').removeClass('red-border');
        $('.special-offers .input .obligatory').each(function (i, item) {
            if ($(item).val() == '') {
                obligatorys.push(item.id);
                $(item).addClass('red-border')
            }
        });
        if (obligatorys.length > 0) {
            var json = { obj: $('.SpecialOffer-spinner').parent().parent(), msg: 'Please complete the required fields underlined in red.', left: 0, top: 0, append: $('.SpecialOffer-spinner').parent().parent(), _class: 'pop-msg2' };
            popup_msg(json, 5000);
            e.stopImmediatePropagation();
            return false;
        }
        if (grecaptcha.getResponse(recaptcha2) == '') {
            $('.special-offers .special-offers-button').hide();
            $('.captchaSpecialOffer-div').show();
            e.stopImmediatePropagation();
            return;
        }
        $('.special-offers .input[type=text]').each(function (i, item) {
            dto[item.id] = $(item).val()
        });
        $(".SpecialOffer-spinner").children(":first").css('left', $('.special-offers .special-offers-button').width() + 59 + 'px');
        $('.SpecialOffer-spinner').show();
        $.ajax({
            type: 'POST',
            url: '/home/EmailSpecialOffer',
            data: JSON.stringify(dto),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (ret) {
                $('.SpecialOffer-spinner').hide();
                var json = { obj: $('.SpecialOffer-spinner').parent().parent(), msg: 'Special Offer information sent.', left: 0, top: 0, append: $('.SpecialOffer-spinner').parent().parent(), _class: 'pop-msg2' };
                popup_msg(json, 5000);
                $('.special-offers input[type=text]').val('');
            }
        });
        e.stopImmediatePropagation();
        return false;
    });

    //$('.special-offers .input[type=text]').on({
    //    mousedown: function () { var json = { obj: $(this).parent(), msg: $(this).attr('placeholder'), left: 0, top: -15, append: $(this).parent(), _class: 'pop-msg-title' }; popup_msg(json, 0); },
    //    mouseleave: function () { $('.special-offers .pop-msg-title').remove() }
    //});
    //$('.billinginformationform .input').on({
    //    mousedown: function () { var json = { obj: $(this).parent(), msg: $(this).attr('placeholder'), left: 0, top: -10, append: $(this).parent(), _class: 'pop-msg-title' }; popup_msg(json, 0); },
    //    mouseleave: function () { $('.billinginformationform .pop-msg-title').remove() }
    //});
    //$('.billing-address-form .input').on({
    //    mousedown: function () { var json = { obj: $(this).parent(), msg: $(this).attr('placeholder'), left: 0, top: -5, append: $(this).parent(), _class: 'pop-msg-title' }; popup_msg(json, 0); },
    //    mouseleave: function () { $('.billing-address-form .pop-msg-title').remove() }
    //});
    //$('.billing-address-form .dropdown.selection').on({//mouseenter
    //    mouseover: function () {var json = { obj: $(this).parent(), msg: 'State', left: 0, top: -5, append: $(this).parent(), _class: 'pop-msg-title' };popup_msg(json, 0);},
    //    mouseleave: function () { $('.billing-address-form .pop-msg-title').remove() }
    //});
    //$('.shipping-address-form .input').on({
    //    mousedown: function () { var json = { obj: $(this).parent(), msg: $(this).attr('placeholder'), left: 0, top: -5, append: $(this).parent(), _class: 'pop-msg-title' }; popup_msg(json, 0); },
    //    mouseleave: function () { $('.shipping-address-form .pop-msg-title').remove() }
    //});
    //$('.shipping-address-form .dropdown.selection').on({//mouseenter
    //    mousedown: function () { var json = { obj: $(this).parent(), msg: 'State', left: 0, top: -5, append: $(this).parent(), _class: 'pop-msg-title' }; popup_msg(json, 0); },
    //    mouseleave: function () { $('.shipping-address-form .pop-msg-title').remove() }
    //});

    $(".shipping-label-form #chain").change(function () {
        if ($(this).val() == 'Other') {
            $("#shippingother").show();
        } else {
            $("#shippingother").hide();
            $("#shippingother").val('');
        }
    });

    $('.shipping-labels .shipping-labels-button').on('click touch tap', function (e) {
        var dto = {}
        var obligatorys = [];
        var count = 0;
        var shippingweight = '';
        var products = '';
        $('.shipping-labels .shipping-labels-form .obligatory').removeClass('red-border');
        $('.shipping-labels .count').removeClass('red-border')
        $('.shipping-labels .other').removeClass('red-border')
        $('.shipping-labels .dropdown.selection').removeClass('red-border')
        $('.shipping-labels .shipping-labels-form .obligatory').each(function (i, item) {
            if ($(item).val() == '' && $(item).is(":visible")) {
                obligatorys.push(item.id);
                if (item.tagName.toLowerCase() == 'select') {
                    $(item).parent().addClass('red-border');
                } else {
                    $(item).addClass('red-border')
                }
            } else {
                if ((item.id.toLowerCase() == 'chain' || item.id.toLowerCase() == 'state') && $(item).val() == '')  {
                    obligatorys.push(item.id);
                    $(item).parent().addClass('red-border');
                }                
            }
        });
        if (obligatorys.length > 0) {
            var json = { obj: $('.shipping-label-spinner').parent().parent(), msg: 'Please complete the required fields underlined in red.', left: 0, top: 10, append: $('.shipping-label-spinner').parent().parent(), _class: 'pop-msg2' };
            popup_msg(json, 5000);
            e.stopImmediatePropagation();
            return false;
        }
        $('.shipping-labels .input').each(function (i, item) {
            if (item.id != 'shippingother') {
                if (item.id == 'chain' && $(item).val() == 'Other') {
                    dto[item.id] = $('.shipping-labels .shippingother').val();
                }
                else {
                    dto[item.id] = $(item).val()
                }
            }
        });
        //$('.shipping-labels .count').each(function (i, item) {
        //    count += parseInt($(item).val());
        //    dto[item.id] = $(item).val()
        //    if ($(item).val() != 0) {
        //        products += (products == '' ? '' : ';') + item.id.replace(/_/g, " ") + ':' + $(item).val()
        //    }
        //});
        //$('.shipping-labels .other').each(function (i, item) {
        //    dto[item.id] = $(item).val()
        //});
        //if ($('.shipping-labels #Other').val() != '' && $('.shipping-labels #Weight').val() != '') {
        //    products += (products == '' ? '' : ';') + $('.shipping-labels #Other').val() + ':' + $('.shipping-labels #Weight').val()
        //    count += parseInt($('.shipping-labels #Weight').val());
        //}
        //if (count == 0 && ($('.shipping-labels #Other').val() == '' && $('.shipping-labels #Weight').val() == '') || $('.shipping-labels #Other').val() != '' && $('.shipping-labels #Weight').val() == '' || $('.shipping-labels #Other').val() == '' && $('.shipping-labels #Weight').val() != '') {
        //    $('.shipping-labels .count').addClass('red-border')
        //    $('.shipping-labels .other').addClass('red-border')
        //    var json = { obj: $('.shipping-label-spinner').parent().parent(), msg: 'Please select a product count or complete the "Other and Weight fields"', left: 0, top: 10, append: $('.shipping-label-spinner').parent().parent(), _class: 'pop-msg2' };
        //    popup_msg(json, 1500);
        //    e.stopImmediatePropagation();
        //    return false;
        //}
        if (!validateemail($('.shipping-labels #email').val())) {
            $('.shipping-labels #email').addClass('red-border')
            var json = { obj: $('.shipping-label-spinner').parent().parent(), msg: 'Please provide a valid email address', left: 0, top: 10, append: $('.shipping-label-spinner').parent().parent(), _class: 'pop-msg2' };
            popup_msg(json, 5000);
            e.stopImmediatePropagation();
            return false;
        }
        if (grecaptcha.getResponse(recaptcha1) == '') {
            $('.shipping-labels .shipping-labels-button').hide();
            $('.captcha-div').show();
            e.stopImmediatePropagation();
            return false;
        }
        dto['comments'] = $('.shipping-labels #comments').val();
        dto['shippingweight'] = count == 0 ? '2' : count.toString();
        dto['products'] = " ";//"products;
        dto['Created_DTS'] = null;//"products;
        dto['captcharesponse'] = grecaptcha.getResponse();
        $(".shipping-label-spinner").children(":first").css('left', $('.shipping-labels .shipping-labels-button').width() + 59 + 'px');
        $('.shipping-label-spinner').show();
        $.ajax({
            type: 'POST',
            url: '/FreeShipping/ReturnLabelJson',
            data: JSON.stringify(dto),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',

            success: function (ret) {
                //gtag_report_conversion();
                //console.log('gtag_report_conversion')

                shippinglabelimg = ret.imageb64;
                $('.shipping-label').html(ret.label);
                labeldto = ret;
                $('.shipping-label .shipping-label-image').attr('src', ret.image);
                //.replace(/\n/g, "<br />")
                $('#lblComments').html($('#lblComments').html().replace(/\n/g, "<br />"));
                $('.shipping-label-form').hide();
                $('.shipping-label').show();
                $('.shipping-label-spinner').hide();

                //var json = { obj: $('.special-offers .input #email').parent(), msg: 'Special Offer information sent', left: 0, top: 50, append: $('.special-offers .input #email').parent(), _class: 'pop-msg2' };
                //popup_msg(json, 5000);
                //$('.special-offers input[type=text]').val('');
                //var pay_temp = JSON.parse(payInfo);
                //Payment_Message = pay_temp.PAYMENT_MESSAGE;
                //Payment_Confirmation = pay_temp.PAYMENT_CONFIRMATION;
            }
        });
        e.stopImmediatePropagation();
        return false;
    });

    $('.purchase-button').on('click touch tap', function (e) {
        if ($(this).html() == 'Add to Cart') {
            var cartdto = {};
            cartdto.product = '';
            cartdto.count = '1';
            cartdto.category = '';
            cartdto.productuid = product.Product_UID;
            cartdto.freight = parseFloat(product.Additional_Freight).toFixed(2).toString();
            cartdto.status = '';
            cartdto.request = 'insert';
            cartdto.productpricetype = new_refurbished;
            cartdto.amount = new_refurbished_price;
            updateCart(cartdto);
            e.stopImmediatePropagation();
            return false;
        } else {
            window.location = '/send-in-a-repair-free-shipping';
        }
        e.stopImmediatePropagation();
    });

    $('.back-to-cart-button').on('click touch tap', function (e) {
        console.log(checkoutdto);
        console.log($('.billinginformationform #firstname').val());
        //setupprogressbar(1);
        $('.progress-bar.segment').hide()
        $('.billinginformationform').hide();
        $('.the-cart').show();
        e.stopImmediatePropagation();
        return false;
    });

    $('.billing-information-button').on('click touch tap', function (e) {
        setupprogressbar(1);
        $('.the-cart').hide();
        $('.progress-bar.segment').show()
        $('.billinginformationform').show();
        $('.billing-information-button').hide();
        e.stopImmediatePropagation();
    });
    $('.billing-address-button').on('click touch tap', function (e) {
        //setupprogressbar(2);
        $('.billinginformation').hide();
        $('.billingaddress').show();
        $('.billing-address-button').hide();
        $('.shipping-address-button').show();
        e.stopImmediatePropagation();
    });
    $('.shipping-address-button').on('click touch tap', function (e) {
        //setupprogressbar(3);
        $('.billingaddress').hide();
        $('.shippingaddress').show();
        $('.shipping-address-button').hide();
        $('.payment-button').show();
        e.stopImmediatePropagation();
    });

    $('.numeric').keypress(function (e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode == 8 || keyCode == 9) return true;
        if (!$.isNumeric(String.fromCharCode(keyCode))) { e.stopImmediatePropagation(); return false; }

    });
    $('.billing-information-form-button').on('click touch tap', function (e) {
        var dto = {}
        var isemail = false;
        var isphone = false
        var obligatorys = [];
        $('.billinginformationform #phonenumber').attr('title', 'Phone Number correct format 3333334444');

        $('.billinginformationform .obligatory').removeClass('red-border');
        $('.billinginformationform .obligatory').each(function (i, item) {
            if (item.id == 'phonenumber') {
                if ($(item).val() == '' || $(item).val().length != 10) {
                    if ($(item).val().length != 10) {
                        isphone = true;
                    }
                    obligatorys.push(item.id);
                    $(item).addClass('red-border')
                    if ($(item).val() != '') {
                        $('.billinginformationform #phonenumber').attr('title', 'Phone Number requires 10 digits, please enter the correct format 3333334444');
                        var json = { obj: $('.billinginformationform #phonenumber').parent(), msg: 'Phone Number requires 10 digits', left: 0, top: -25, append: $('.billinginformationform #phonenumber').parent(), _class: 'pop-msg2' };
                        popup_msg(json, 5000);
                    }
                }
            } else {
                if (item.id == 'email') {
                    if ($(item).val() == '') {
                        obligatorys.push(item.id);
                        $(item).addClass('red-border')
                    } else {
                        if (!validateemail($(item).val())) {
                            isemail = true;
                            obligatorys.push(item.id);
                            $(item).addClass('red-border')
                            var json = { obj: $('.billinginformationform #email').parent(), msg: 'Please enter the correct email format.', left: 0, top: -25, append: $('.billinginformationform #email').parent(), _class: 'pop-msg2' };
                            popup_msg(json, 5000);

                        }
                    }
                }
                else {
                    if ($(item).val() == '') {
                        obligatorys.push(item.id);
                        $(item).addClass('red-border')
                    }
                }
            }
        });
        if (obligatorys.length > 0) {
            if (!isemail && !isphone) {
                var json = { obj: $(this).parent(), msg: 'Please complete the required fields underlined in red', left: 0, top: 10, append: $(this).parent(), _class: 'pop-msg2' };
                popup_msg(json, 5000);
            }
            e.stopImmediatePropagation();
            return false;
        }

        $('.billinginformationform .input').each(function (i, item) {
            dto[item.id] = $(item).val()
        });
        checkoutdto.billinginfo = [];
        checkoutdto.billinginfo.push(dto);
        console.log(checkoutdto);
        setupprogressbar(2);
        $('.billinginformationform').hide();
        $('.billing-address-form').show();
        var binfo = {};
        binfo.billinginfo = dto;
        $.ajax({
            url: "/cart/LogBillingInfo",
            type: "POST",
            data: JSON.stringify(binfo),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dto) {
                $('.conformation-billingInfo').html(dto.billinginfo.firstname + ' ' + dto.billinginfo.lastname + '<br />' + dto.billinginfo.phonenumber + '<br />' + dto.billinginfo.email);
                console.log(dto);
            },
            error: function (response) {
            }
        });
        e.stopImmediatePropagation();
        return false;
    });
    $('.billing-address-form .input').on('paste', function (e) {
        console.log('billing-information-form paste')
        $('.billing-address-form').removeClass('red-border');
    });
    $(".billing-address-form").bind("paste", function (e) {
        console.log('billing-information-form paste')
        $('.billing-address-form').removeClass('red-border');

    });

    $('.billing-address-form-button').on('click touch tap', function (e) {
        var dto = {}
        var obligatorys = [];
        //$('.billing-address-form .obligatory').removeClass('red-border');
        $('.billing-address-form .ui.fluid.dropdown.selection').removeClass('red-border')
        $('.billing-address-form .obligatory').each(function (i, item) {
            if ($(item).val() == '' || $(item).val() == 'state') {
                obligatorys.push(item.id);
                if (item.id == 'state') {
                    $('.billing-address-form .ui.fluid.dropdown.selection').addClass('red-border')
                } else {
                    $(item).addClass('red-border')
                }
            }
        });
        if (obligatorys.length > 0) {
            var json = { obj: $(this).parent(), msg: 'Please complete the required fields underlined in red', left: 0, top: 10, append: $(this).parent(), _class: 'pop-msg2' };
            popup_msg(json, 5000);
            e.stopImmediatePropagation();
            return false;
        }

        $('.billing-address-form .input').each(function (i, item) {
            dto[item.id] = $(item).val()
        });
        checkoutdto.billingaddress = [];
        checkoutdto.billingaddress.push(dto);
        var baddress = {};
        baddress.billingaddress = dto;
        $.ajax({
            url: "/cart/LogBillingAddress",
            type: "POST",
            data: JSON.stringify(baddress),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dto) {
                $('.conformation-billingAddress').html(dto.billingaddress.address + (dto.billingaddress.address2 != null ? '<br />' + dto.billingaddress.address2 : '') + '<br />' + dto.billingaddress.city + ', ' + dto.billingaddress.state + ' ' + dto.billingaddress.zip + '<br />' + dto.billingaddress.country);
                console.log(dto);
            },
            error: function (response) {
            }
        });
        console.log(checkoutdto);
        setupprogressbar(3);
        $('.billing-address-form').hide();
        $('.shipping-address-form').show();
        e.stopImmediatePropagation();
        return false;
    });
    $('.billing-address-form').on('paste', function (e) {
        $('.billing-address-form').removeClass('red-border');
    });

    $('.shipping-address-form-button').on('click touch tap', function (e) {
        var dto = {}
        var obligatorys = [];
        $('.shipping-address-form .obligatory').removeClass('red-border');
        $('.shipping-address-form .ui.fluid.dropdown.selection').removeClass('red-border')

        $('.shipping-address-form .obligatory').each(function (i, item) {
            if ($(item).val() == '' || $(item).val() == 'state') {
                obligatorys.push(item.id);
                if (item.id == 'state') {
                    $('.shipping-address-form .ui.fluid.dropdown.selection').addClass('red-border')
                    //$('.shipping-address-form .ui.fluid dropdown.selection.upward').addClass('red-border')
                } else {
                    $(item).addClass('red-border')
                }
            }
        });
        if (obligatorys.length > 0) {
            var json = { obj: $(this).parent(), msg: 'Please complete the required fields underlined in red', left: 0, top: 10, append: $(this).parent(), _class: 'pop-msg2' };
            popup_msg(json, 5000);
            e.stopImmediatePropagation();
            return false;
        }

        $('.shipping-address-form .input').each(function (i, item) {
            dto[item.id] = $(item).val()
        });
        checkoutdto.shippingaddress = [];
        checkoutdto.shippingaddress.push(dto);
        var saddress = {};
        saddress.shippingaddress = dto;
        if ($('.subtotal.taxdiv').is(":visible")) {
            saddress.shippingaddress.tax = $('.amount.tax').html()
            saddress.shippingaddress.grandtotal = $('.amount.grandtotal').html()
            saddress.shippingaddress.freight = $('.amount.freight').html()
        }
        $.ajax({
            url: "/cart/LogShippingAddress",
            type: "POST",
            data: JSON.stringify(saddress),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (dto) {
                $('.conformation-shippingAddress').html(dto.shippingaddress.company + '<br />' + dto.shippingaddress.address + (dto.shippingaddress.address2 != null ? '<br />' + dto.shippingaddress.address2 : '') + '<br />' + dto.shippingaddress.city + ', ' + dto.shippingaddress.state + ' ' + dto.shippingaddress.zip + '<br />' + dto.shippingaddress.country);
                console.log(dto);
            },
            error: function (response) {
            }
        });
        $('.conformation-total').html($('.total .amount').html());
        console.log(checkoutdto);
        setupprogressbar(4);
        $('.shipping-address-form').hide();
        $('.validation-form').show();
        e.stopImmediatePropagation();
        return false;
    });
    //$('.shipping-address-form').on('paste', function (e) {
    //    $('.shipping-address-form .obligatory').removeClass('red-border');
    //}); 

    $('.x-button').on('click touch tap', function (e) {
        $('.validation-form').hide();
        $('.validation-form').show();
        e.stopImmediatePropagation();
        return false;
    });

    $('.back-to-billing-information-button').on('click touch tap', function (e) {
        setupprogressbar(1);
        $('.billing-address-form').hide();
        $('.billinginformationform').show();
        e.stopImmediatePropagation();
        return false;
    });
    $('.back-to-billing-address-button').on('click touch tap', function (e) {
        setupprogressbar(2);
        $('.shipping-address-form').hide();
        $('.billing-address-form').show();
        e.stopImmediatePropagation();
        return false;
    });
    $('.back-to-shipping-address-button').on('click touch tap', function (e) {
        setupprogressbar(3);
        $('.validation-form').hide();
        $('.shipping-address-form').show();
        e.stopImmediatePropagation();
        return false;
    });
    $('.use-same-address').on('click touch tap', function (e) {
        if ($(this).is(":checked")) {
            $('.shipping-address-form .obligatory').removeClass('red-border');
            $.each(checkoutdto.billingaddress[0], function (key, value) {
                if (key == 'state') {
                    $('.shipping-address-form .dropdown.selection .text').html($(".shipping-address-form #state option[value=" + value + "]").text());
                    $('.shipping-address-form .dropdown.selection .menu .item').addClass('active').addClass('selected')
                    $('.shipping-address-form #state').val(value).change();

                } else {
                    $('.shipping-address-form #' + key).val(value);
                }
            });
        } else {
            //useBillingUnclick = true;
            $('.shipping-address-form .input').each(function (i, item) {
                if (item.id == 'state') {
                    $('.shipping-address-form .dropdown.selection .text').html('State');
                    $('.shipping-address-form .dropdown.selection .menu .item').removeClass('active').removeClass('selected')
                    $(item).val('State')
                    $('.shipping-address-form #state').change();
                } else {
                    $(item).val('')
                }
            });
        }
        e.stopImmediatePropagation();
        return;
    });

    $('#json_login').on('click touch tap', function () {
        var dto = {};
        dto.username = $('.json-username').val();
        dto.password = $('.json-password').val();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: " /Security/JSON_Login",
            data: JSON.stringify(dto),
            dataType: "json",
            success: function (dto) {
                console.log(dto)
                if (dto.status == 'OK') {
                }
                else {
                }
            },
            error: function (jqXHR, ajaxOptions, thrownError) {
                var xx = '222';
            }
        });
        e.stopImmediatePropagation();
    });
    //$('.taxstate').on('change', function (e) {
    $('.shipping-address-form #state').on('change', function (e) {
        var subtotal = parseFloat($('#costs').attr('subtotal')).toFixed(2);
        var tax = 0;
        var statetax = 0;
        var freight = parseFloat($('#costs').attr('freight')).toFixed(2);
        var discount = parseFloat($('#costs').attr('discount')).toFixed(2);
        var total = 0;
        var deccheck
        var statelong = '';
        if ($(this).val() == 'CA' || $(this).val() == 'MO' || $(this).val() == 'IA' || $(this).val() == 'CO') {
            switch ($(this).val()) {
                case 'CA': statetax = parseFloat($('#costs').attr('CA')); statelong = 'California'; break
                case 'MI': statetax = parseFloat($('#costs').attr('MI')); statelong = 'Missouri'; break
                case 'IA': statetax = parseFloat($('#costs').attr('IA')); statelong = 'Iowa'; break
                case 'CO': statetax = parseFloat($('#costs').attr('CO')); statelong = 'Colorado'; break
            }
            tax = parseFloat(((parseFloat(subtotal) - parseFloat(discount)) * parseFloat(statetax))).toFixed(2);
            total = (parseFloat(tax) + parseFloat(freight) + (parseFloat(subtotal) - parseFloat(discount))).toFixed(2).toString();
            deccheck = total.split('.');
            if (deccheck.length == 1) {
                total = deccheck[0] + '.00';
            }
            if (deccheck.length > 1) {
                if (deccheck[1].length == 1)
                    total = deccheck[0] + '.' + deccheck[1] + '0';
            }
            $('.taxdiv .tax').html('$' + tax.toString());
            $('.taxdiv').show();
            $('.total .amount').html('$' + total.toString());
            var json = { obj: $('.messageDiv'), msg: statelong + ' Sales Tax Added', left: 0, top: 0, append: $('.messageDiv'), _class: 'pop-msg2' };
            popup_msg(json, 5000);
        }
        else {
            tax = 0;
            if ($('.taxdiv').is(":visible")) {
                $('.taxdiv').hide();
                $('.total .amount').html('$' + $('#costs').attr('total'));
                var json = { obj: $('.messageDiv'), msg: statelong + ' Sales Tax Removed', left: 0, top: 0, append: $('.messageDiv'), _class: 'pop-msg2' };
                popup_msg(json, 5000);
            }
        }
        if (useBillingUnclick) {
            useBillingUnclick = false;
            //e.stopImmediatePropagation();
        }
    });

    $('.invoice-print-button').on('click touch tap', function (e) {
        var dto = {};
        dto.guid = btnObj;
        dto.Invoice_Session_UID = Cart_Session_UID;
        dto.Invoice_User_Cookie = User_Cookie;
        dto.Invoice_Conformation_ID = Invoice_Conformation_ID;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: " /invoice/PrintCommand",
            data: JSON.stringify(dto),
            dataType: "json",
            success: function (dto) {
                if (dto.status == '0') {
                    $('#jquery_print #print_invoice').html(dto.print)
                    $('#print_invoice').print();
                }
                else {
                }
            },
            error: function (jqXHR, ajaxOptions, thrownError) {
            }
        });
        e.stopImmediatePropagation();
    });

    $('.invoice-download-button').on('click touch tap', function (e) {
        var dto = {};
        dto.guid = btnObj;
        dto.Invoice_Session_UID = Cart_Session_UID;
        dto.Invoice_User_Cookie = User_Cookie;
        dto.Invoice_Conformation_ID = Invoice_Conformation_ID;
        //window.location = '/invoice/DownloadPDFfile?guid=' + btnObj;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: " /invoice/DownloadCommand",
            data: JSON.stringify(dto),
            dataType: "json",
            success: function (dto) {
                if (dto.status == '0') {
                    window.location = '/invoice/DownloadPDFfile?guid=' + dto.guid;
                }
                else {
                }
            },
            error: function (jqXHR, ajaxOptions, thrownError) {
            }
        });
        e.stopImmediatePropagation();
    });

    $('.send-emails-button').on('click touch tap', function (e) {
        if ($('.emails_to_send').val() == '') {
            return false;
        }
        var dto = {};
        dto.guid = btnObj;
        dto.Invoice_Session_UID = Cart_Session_UID;
        dto.Invoice_User_Cookie = User_Cookie;
        dto.Invoice_Conformation_ID = Invoice_Conformation_ID;
        dto.emails = $('.emails_to_send').val();
        var emailsSplit = dto.emails.split(',');
        $.each(emailsSplit, function (index, value) {
            if (!validateemail(value)) {
                var json = { obj: $('#emails_to_send').parent(), msg: 'Please correct email address ' + value + '.', left: 0, top: -30, append: $('#emails_to_send').parent(), _class: 'pop-msg2' };
                popup_msg(json, 5000);
                e.stopImmediatePropagation();
                rturn;
            }
        });

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: " /invoice/SendAdditionalEmails",
            data: JSON.stringify(dto),
            dataType: "json",
            success: function (dto) {
                if (dto.status == '0') {
                    $('.emails_to_send').val('Email(s) have been sent');
                    setTimeout(function () {
                        $('.emails_to_send').val('');
                    }, 3000);
                }
                else {
                    $('.emails_to_send').val('Sending additional Email(s) have not been sent');
                    setTimeout(function () {
                        $('.emails_to_send').val('');
                    }, 3000);
                }
            },
            error: function (jqXHR, ajaxOptions, thrownError) {
            }
        });
        e.stopImmediatePropagation();
    });
});

function updateCart(cartdto) {
    $.ajax({
        type: "POST",
        url: '/cart/AddToCart',
        data: JSON.stringify(cartdto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //window.location = '/cart';
            $('.ce.form #actionButton').hide();
            $('.ce.form #view-cart').show();
            $('.ce.form .item-purchased').html(new_refurbished + ' - ' + product.Product_Short_Description + ' added to your Cart');
            $('.ce.form .item-purchased').show();
            if (!$('.cart-badge').hasClass('active')) {
                $('.cart-badge').addClass('active');
            }
            $('.cart-badge').html(data.totalcartitems);
        }
    });
}

function setCookie(key, value) {
    var date = new Date();
    date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + date.toGMTString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function pagination() {
    $('.pagination .basic .button').on('click touch tap', function (e) {
        if ($(this).hasClass('active')) return;
        $('.pagination .basic .button.pbtn').removeClass('active');
        $(this).addClass('active')
        var pageClicked = parseInt($(this).html());
        page = pageClicked;
        var pageItemsCount = itemsperpage * (pageClicked - 1);
        $('.twelve.wide.column .grid .cell-product').addClass("hide").addClass('no-left-border').removeClass('left-border').addClass('no-bottom-border').removeClass('bottom-border');
        var count = 1;
        var pcount = 1;
        $.each(filtered, function (i, item) {
            if (count > pageItemsCount && count < (pageItemsCount + itemsperpage + 1)) {
                $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass("hide");//.addClass("cell");
                switch (pcount) {
                    case 1:
                    case 2:
                        $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass('no-left-border').addClass('left-border').removeClass('no-bottom-border').addClass('bottom-border');
                        break;
                    case 3:
                        $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass('no-bottom-border').addClass('bottom-border');
                        break;
                    case 4:
                    case 5:
                        $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass('no-left-border').addClass('left-border');
                        break;
                }
                pcount++;
            }
            count++;
        });
        $('.vstart').html(page == 1 ? '1' : ((itemsperpage * page)) - itemsperpage + 1);
        $('.vend').html(filtered.length > (itemsperpage * page) ? (itemsperpage * page) : filtered.length);
        $('.vof').html(filtered.length);
        $(window).scrollTop(0);
        e.stopImmediatePropagation();
    });
}

function pageButtons() {
    $('.pagination .basic .pbtn').remove();
    console.log(pages)
    for (i = 0; i < pages; i++) {
        var btn = i + 1;
        $('.pagination .basic.buttons').append('<div class="ui pbtn' + (i == 0 ? " active " : " ") + 'button">' + btn + '</div>');
    }
    pagination();
}

function productFilter(obj) {
    console.log($(obj))
    var count = 1;
    page = 1;
    var brandfilters = '';
    var modelfilters = '';
    var conditionfilters = '';
    var accessoryfilters = '';
    var checkedLength = $(obj).parents('.menu.filters').find('input:checked').length;
    if (checkedLength > 0) {
        $(obj).parents('.menu.filters').find('input:checked').each(function () {
            if ($(this).attr('filter') == 'brand') {
                var brand = $(this).parent().find("label a").attr('brand');//.html();
                brandfilters += (brandfilters == '' ? '' : ',') + brand;
                console.log(brandfilters);
                //brandfilters = 'CE|3M,CE|HME,CE|Panasonic';
            }

            if ($(this).attr('filter') == 'model') {
                var modeltype = $(this).parent().find("label a").html();
                switch (modeltype) {
                    case 'All-in-One':
                        modelfilters += (modelfilters == '' ? '' : ',') + "'" + 'All-in-One' + "'";
                        break;
                    case 'Wired':
                        modelfilters += (modelfilters == '' ? '' : ',') + "'" + 'Corded' + "'";
                        break;
                }
            }

            if ($(this).attr('filter') == 'accessories') {
                var accessorytype = $(this).parent().find("label a").html().replace("Other ", "");
                accessoryfilters += (accessoryfilters == '' ? '' : ',') + "'" + accessorytype + "'";
                console.log(accessoryfilters)
            }

            if ($(this).attr('filter') == 'condition') {
                var conditonType = $(this).parent().find("label a").html();
                switch (conditonType) {
                    case 'New':
                        conditionfilters += (conditionfilters == '' ? '' : ' and ') + 'Retail_Price > 0';
                        break;
                    case 'Refurbished':
                        conditionfilters += (conditionfilters == '' ? '' : ' and ') + 'Refurbished_Price > 0';
                        break;
                    case 'Repair':
                        conditionfilters += (conditionfilters == '' ? '' : ' and ') + 'Repair_Price > 0';
                        break;
                    case 'Advance Exchange':
                        conditionfilters += (conditionfilters == '' ? '' : ' and ') + 'Exchange_Price > 0';
                        break;
                }
            }
        });
        //console.log(brandfilters)
        var sqlQuery = 'select * from ?';
        if (brandfilters.length > 0) {
            var brandGrp = brandfilters.split(',');
            var brandsql = '';
            $.each(brandGrp, function (index, value) {
                brandsql += (brandsql == '' ? '' : " OR ") + "Manufacturer_Name like '%" + value + "%'";
            });
            sqlQuery += (sqlQuery == 'select * from ?' ? ' where ' : '') + brandsql;//"Manufacturer_Name in (" + brandfilters + ")";
        }
        if (accessoryfilters.length > 0) {
            sqlQuery += (sqlQuery == 'select * from ?' ? ' where ' : ' and ') + "ProductType in (" + accessoryfilters + ")";
        }
        if (conditionfilters.length > 0) {
            sqlQuery += (sqlQuery == 'select * from ?' ? ' where ' : ' and ') + conditionfilters;
        }
        if (modelfilters.length > 0) {
            sqlQuery += (sqlQuery == 'select * from ?' ? ' where ' : ' and ') + "ProductType in (" + modelfilters + ")";
        }

        filtered = alasql(sqlQuery, [manufacturerProducts]);
        select_count = filtered.length;
        pages = Math.ceil(select_count / itemsperpage);// + (select_count % itemsperpage > 0 ? 1 : 0);
        $('.twelve.wide.column .grid .cell-product').addClass("hide").addClass('no-left-border').removeClass('left-border').addClass('no-bottom-border').removeClass('bottom-border');
        count = 1;
        $.each(filtered, function (i, item) {
            if (i < itemsperpage) {
                $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass("hide");
                switch (count) {
                    case 1:
                    case 2:
                        $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass('no-left-border').addClass('left-border').removeClass('no-bottom-border').addClass('bottom-border');
                        break;
                    case 3:
                        $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass('no-bottom-border').addClass('bottom-border');
                        break;
                    case 4:
                    case 5:
                        $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass('no-left-border').addClass('left-border');
                        break;
                }
                count++;
            }
        });
    } else {
        filtered = alasql("select * from ?", [manufacturerProducts]);
        select_count = filtered.length;
        pages = Math.ceil(select_count / itemsperpage);
        $('.twelve.wide.column .grid .cell-product').addClass("hide").addClass('no-left-border').removeClass('left-border').addClass('no-bottom-border').removeClass('bottom-border');
        count = 1;
        $.each(filtered, function (i, item) {
            if (i < itemsperpage) {
                $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass("hide");
                switch (count) {
                    case 1:
                    case 2:
                        $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass('no-left-border').addClass('left-border').removeClass('no-bottom-border').addClass('bottom-border');
                        break;
                    case 3:
                        $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass('no-bottom-border').addClass('bottom-border');
                        break;
                    case 4:
                    case 5:
                        $('.twelve.wide.column .grid .cell-product[uid=' + item.Product_UID + '][brand="' + item.Manufacturer_Name + '"]').removeClass('no-left-border').addClass('left-border');
                        break;
                }
                count++;
            }
        });
    }
    pageButtons();
    $('.vstart').html(page == 1 ? '1' : ((itemsperpage * page)) - itemsperpage + 1);
    $('.vend').html(filtered.length > (itemsperpage * page) ? (itemsperpage * page) : filtered.length);
    $('.vof').html(filtered.length);
    $(window).scrollTop(0);
}

function UpdateCount(cartid) {
    var cartdto = {};
    cartdto.productuid = $('.update-cart .updatecount[cartid="' + cartid + '"]').attr('productuid');
    cartdto.cartid = cartid;
    cartdto.request = 'updatecount';
    cartdto.count = $('.cart.segment').find('.itemcount[cartid="' + cartid + '"]').val();
    cartdto.promodto = PromoLookupJson;
    $.ajax({
        type: "POST",
        url: '/cart/UpdateCartJson',
        data: JSON.stringify(cartdto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            window.location = '/cart';
        }
    });
}

function RemoveCartItem(cartid) {
    var cartdto = {};
    cartdto.cartid = cartid;
    cartdto.request = 'remove';
    cartdto.promodto = {};
    $.ajax({
        type: "POST",
        url: '/cart/UpdateCartJson',
        data: JSON.stringify(cartdto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            window.location = '/cart';
        }
    });
}

$('.ui.counter-cart .inc-count').on('click touch tap', function (e) {
    var itemcount = parseInt($(this).prev().val());
    itemcount++;
    $(this).prev().val(itemcount.toString())
    if ($(this).prev().val() == $(this).prev().attr('currentcount')) {
        $(this).parent().next().hide();
    }
    else {
        $(this).parent().next().show();
    }
    e.stopImmediatePropagation();
});

$('.ui.counter-cart .dec-count').on('click touch tap', function (e) {
    var itemcount = parseInt($(this).next().val());
    if (itemcount > 1) {
        itemcount--;
        $(this).next().val(itemcount.toString())
        if ($(this).next().val() == $(this).next().attr('currentcount')) {
            $(this).parent().next().hide();
        }
        else {
            $(this).parent().next().show();
        }
    }
    e.stopImmediatePropagation();
});

$('.ui.counter-cart input').on('input', function (e) {
    if ($(this).val() == '') {
        $(this).val('0')
    }
    $(this).val(parseInt($(this).val().toString()))
    if ($(this).val() == $(this).attr('currentcount')) {
        $(this).parent().next().hide();
    }
    else {
        $(this).parent().next().show();
    }
    e.stopImmediatePropagation();
});

$('.validation-form-button').on('click touch tap', function (e) {//process-payment
    var cartdto = {};
    cartdto.signalr = chatid;
    cartdto.usercookie = usercookie;
    cartdto.request = 'payment';
    cartdto.amount = $('.order .amount').html().replace('$', '');
    cartdto.billinginfo = checkoutdto;
    cartdto.Invoice_Session_UID = '';
    cartdto.Invoice_User_Cookie = '';
    cartdto.confirmationNumber = '';
    cartdto.PaymentUId = '';
    cartdto.PayPalTransactionResponseDTO = {};

    setupprogressbar(5);
    $('.payment').html('');
    $('.payment').hide();
    $('.validation-form').hide();
    $('.payment-button').hide();

    $.ajax({
        type: "POST",
        url: '/Payment/ProcessPayment',
        data: JSON.stringify(cartdto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data == '-1') {
                window.location.href = '/cart';
            } else {
                $('.payment').html(data);
                $('.payment').show();
                $('.order-back-to-cart').show()
            }

        }
    });
    e.stopImmediatePropagation();
    return false;
});

$('.promo .promo-code').on('click touch tap', function (e) {
    $(this).next().toggle();
    $(this).next().find('input.promocode').val('');
    e.stopImmediatePropagation();
});

$('.button.apply-button').on('click touch tap', function (e) {
    var prmodto = {};
    prmodto.signalr = chatid;
    prmodto.usercookie = usercookie;
    prmodto.prmocode = $(this).parent().prev().find('input.promocode').val();
    prmodto.request = 'prmolookup';
    $('.promo .promo-msg').html('');
    $.ajax({
        type: "POST",
        url: '/cart/PromoLookupJson',
        data: JSON.stringify(prmodto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dto) {
            PromoLookupJson = dto;
            console.log(PromoLookupJson);
            if (dto.message == "OK") {
                window.location = '/cart';
            }
            else {
                $('.promo .promo-msg').html(dto.promomessage);
            }
        }
    });
    e.stopImmediatePropagation();
});

$('.button.clear-button').on('click touch tap', function (e) {
    var prmodto = {};
    prmodto.signalr = chatid;
    prmodto.usercookie = usercookie;
    prmodto.prmocode = $(this).parent().prev().find('input.promocode').val();
    prmodto.request = 'prmoclear';
    $(this).parent().prev().find('input.promocode').val('');
    $.ajax({
        type: "POST",
        url: '/cart/PromoClearJson',
        data: JSON.stringify(prmodto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dto) {
            console.log(dto);
            if (dto.message == "OK") {
                window.location = '/cart';
            }
        }
    });
    e.stopImmediatePropagation();
});

function create_required_msg(msg) {
    return $('<div/>', {
        text: msg,
        class: 'comment-msg',
        title: 'Click to add comment'
    });
}

function popup_msg(json, delay) {
    var ele = create_element(json.msg, json._class);
    var position = getxy($(json.obj));
    $(ele).css({ top: position.top + json.top, left: position.left + json.left });
    $(json.append).append(ele)
    var thread = new handlemsg(delay);
    thread.do(ele);
}

function getxy(obj) {
    var position = {};
    position.left = $(obj).position().left;
    position.top = $(obj).position().top;
    position.x = $(obj).position().left;
    position.y = $(obj).position().top;
    return position;
}

function handlemsg(delay) {
    this.do = function (obj) {
        if (delay > 0) {
            setTimeout(function () {
                $(obj).remove();
            }, delay);
        }
    }
}

function create_element(msg, _class) {
    return $('<div/>', {
        text: msg,
        class: _class
    });
}

function validateemail(email) {
    //var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d!#$%&'*+\-\/=?^_`{|}~\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d!#$%&'*+\-\/=?^_`{|}~\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(email);
}

function clearcartall() {
    $.ajax({
        type: "POST",
        url: '/Payment/ClearSession',
        //data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            window.location = '/cart';
        }
    });
}

function clearcartallAjax() {
    $.ajax({
        type: "POST",
        url: '/Payment/ClearSession',
        //data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //window.location = '/cart';
        }
    });
}

function continueshopping() {
    $.ajax({
        type: "POST",
        url: '/home/ContinueShopping',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data)
            if (data != 'null') {
                window.location = data;
            }
        }
    });
}

function do_json_login() {
    $('.json-username').val('');
    $('.json-password').val('');
    $('.login_error').html('');
}

function setupprogressbar(steps) {
    $('.steps li').removeClass('active');
    for (var i = 1; i <= steps; i++) {
        $('.progress-bar .steps .step-' + i + '-li').addClass('active');
        $('.progress-bar .steps .step-' + i + '-li .step-' + i + '-act').show();
        $('.progress-bar .steps .step-' + i + '-li .step-' + i + '-inact').hide();
    }
    for (i = steps + 1; i <= 5; i++) {
        $('.progress-bar .steps .step-' + i + '-li .step-' + i + '-inact').show();
        $('.progress-bar .steps .step-' + i + '-li .step-' + i + '-act').hide();
    }
}

//function validateemail(email) {
//    //var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
//    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d!#$%&'*+\-\/=?^_`{|}~\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d!#$%&'*+\-\/=?^_`{|}~\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
//    return pattern.test(email);
//}

function doFailure() {
    //$('.the-cart').hide();
    console.log(checkoutdto);
    console.log(salesTax);
    console.log(grandTotal);
    //$('billing-information-button').hide();
    $('.billinginformationform .input').each(function (i, item) {
        if (item.id == 'phonenumber') {
            $(item).val(checkoutdto.billinginfo[0][item.id].replace('(', '').replace(')', '').replace('-', '').replace(' ', ''));
            checkoutdto.billinginfo[0][item.id] = $(item).val();
        }
        else {
            $(item).val(checkoutdto.billinginfo[0][item.id]);
        }
    });

    $('.shipping-address-form .input').each(function (i, item) {
        if (item.id == 'state') {
            $('.shipping-address-form .dropdown.selection .text').html($(".shipping-address-form #state option[value=" + checkoutdto.shippingaddress[0][item.id] + "]").text());
            $('.shipping-address-form .dropdown.selection .menu .item').addClass('active').addClass('selected')
            //$('.shipping-address-form #state').change();
            $('.shipping-address-form #state').val(checkoutdto.shippingaddress[0][item.id]).change();
        }
        else {
            $(item).val(checkoutdto.shippingaddress[0][item.id]);
        }
    });
    $('.billing-address-form .input').each(function (i, item) {
        if (item.id == 'state') {
            $('.billing-address-form .dropdown.selection .text').html($(".billing-address-form #state option[value=" + checkoutdto.billingaddress[0][item.id] + "]").text());
            $('.billing-address-form .dropdown.selection .menu .item').addClass('active').addClass('selected')
            $('.billing-address-form #state').val(checkoutdto.billingaddress[0][item.id]).change();
        }
        else {
            $(item).val(checkoutdto.billingaddress[0][item.id]);
        }
    });
    if (salesTax != '0') {
        $('.taxdiv .tax').html('$' + salesTax);
        $('.taxdiv').show();
    }
    $('.total .amount').html('$' + grandTotal);
    $('.conformation-total').html('$' + grandTotal);
    $('.conformation-shippingAddress').html(checkoutdto.shippingaddress[0].company + '<br />' + checkoutdto.shippingaddress[0].address + (checkoutdto.shippingaddress[0].address2 != null ? '<br />' + checkoutdto.shippingaddress[0].address2 : '') + '<br />' + checkoutdto.shippingaddress[0].city + ', ' + checkoutdto.shippingaddress[0].state + ' ' + checkoutdto.shippingaddress[0].zip + '<br />' + checkoutdto.shippingaddress[0].country);
    $('.conformation-billingInfo').html(checkoutdto.billinginfo[0].firstname + ' ' + checkoutdto.billinginfo[0].lastname + '<br />' + checkoutdto.billinginfo[0].phonenumber + '<br />' + checkoutdto.billinginfo[0].email);
    $('.conformation-billingAddress').html(checkoutdto.billingaddress[0].address + (checkoutdto.billingaddress[0].address2 != null ? '<br />' + checkoutdto.billingaddress[0].address2 : '') + '<br />' + checkoutdto.billingaddress[0].city + ', ' + checkoutdto.billingaddress[0].state + ' ' + checkoutdto.billingaddress[0].zip + '<br />' + checkoutdto.billingaddress[0].country);
    $('.the-cart').hide();
    setupprogressbar(4);
    $('.progress-bar.segment').show()
    $('.validation-form').show();
    var binfo = {};
    binfo.billinginfo = checkoutdto.billinginfo[0];

    $.ajax({
        url: "/cart/LogBillingInfo",
        type: "POST",
        data: JSON.stringify(binfo),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dto) {
        },
        error: function (response) {
        }
    });
    var baddress = {};
    baddress.billingaddress = checkoutdto.billingaddress[0];
    $.ajax({
        url: "/cart/LogBillingAddress",
        type: "POST",
        data: JSON.stringify(baddress),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dto) {
        },
        error: function (response) {
        }
    });
    var saddress = {};
    saddress.shippingaddress = checkoutdto.shippingaddress[0];
    $.ajax({
        url: "/cart/LogShippingAddress",
        type: "POST",
        data: JSON.stringify(saddress),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dto) {
        },
        error: function (response) {
        }
    });
}

function doReset() {
    //$('.the-cart').hide();
    console.log(checkoutdto);
    if (checkoutdto.billinginfo.length > 0) {
        $('.billinginformationform .input').each(function (i, item) {
            if (item.id == 'phonenumber') {
                $(item).val(checkoutdto.billinginfo[0][item.id].replace('(', '').replace(')', '').replace('-', '').replace(' ', ''));
                checkoutdto.billinginfo[0][item.id] = $(item).val();
            }
            else {
                $(item).val(checkoutdto.billinginfo[0][item.id]);
            }
        });
    }
    if (checkoutdto.shippingaddress.length > 0) {
        $('.shipping-address-form .input').each(function (i, item) {
            if (item.id == 'state') {
                $('.shipping-address-form .dropdown.selection .text').html($(".shipping-address-form #state option[value=" + checkoutdto.shippingaddress[0][item.id] + "]").text());
                $('.shipping-address-form .dropdown.selection .menu .item').addClass('active').addClass('selected')
                //$('.shipping-address-form #state').change();
                $('.shipping-address-form #state').val(checkoutdto.shippingaddress[0][item.id]).change();
            }
            else {
                $(item).val(checkoutdto.shippingaddress[0][item.id]);
            }
        });
    }
    if (checkoutdto.billingaddress.length > 0) {
        $('.billing-address-form .input').each(function (i, item) {
            if (item.id == 'state') {
                $('.billing-address-form .dropdown.selection .text').html($(".billing-address-form #state option[value=" + checkoutdto.billingaddress[0][item.id] + "]").text());
                $('.billing-address-form .dropdown.selection .menu .item').addClass('active').addClass('selected')
                $('.billing-address-form #state').val(checkoutdto.billingaddress[0][item.id]).change();
            }
            else {
                $(item).val(checkoutdto.billingaddress[0][item.id]);
            }
        });
    }
    doTax();
    $('.conformation-total').html($('.total .amount').html());

}

function doTax() {
    if (checkoutdto.shippingaddress.length > 0) {
        var subtotal = parseFloat($('#costs').attr('subtotal')).toFixed(2);
        var tax = 0;
        var statetax = 0;
        var freight = parseFloat($('#costs').attr('freight')).toFixed(2);
        var discount = parseFloat($('#costs').attr('discount')).toFixed(2);
        var total = 0;
        var deccheck
        var statelong = '';
        if (checkoutdto.shippingaddress[0].state == 'CA' || checkoutdto.shippingaddress[0].state == 'MO' || checkoutdto.shippingaddress[0].state == 'IA') {
            switch (checkoutdto.shippingaddress[0].state) {
                case 'CA': statetax = parseFloat($('#costs').attr('CA')); statelong = 'California'; break
                case 'MI': statetax = parseFloat($('#costs').attr('MI')); statelong = 'Missouri'; break
                case 'IA': statetax = parseFloat($('#costs').attr('IA')); statelong = 'Iowa'; break
            }
            tax = parseFloat(((parseFloat(subtotal) - parseFloat(discount)) * parseFloat(statetax))).toFixed(2);
            total = (parseFloat(tax) + parseFloat(freight) + (parseFloat(subtotal) - parseFloat(discount))).toFixed(2).toString();
            deccheck = total.split('.');
            if (deccheck.length == 1) {
                total = deccheck[0] + '.00';
            }
            if (deccheck.length > 1) {
                if (deccheck[1].length == 1)
                    total = deccheck[0] + '.' + deccheck[1] + '0';
            }
            $('.taxdiv .tax').html('$' + tax.toString());
            $('.taxdiv').show();
            $('.total .amount').html('$' + total.toString());
            //var json = { obj: $('.messageDiv'), msg: statelong + ' Sales Tax Added', left: 0, top: 0, append: $('.messageDiv'), _class: 'pop-msg2' };
            //popup_msg(json, 5000);
        }
        else {
            tax = 0;
            if ($('.taxdiv').is(":visible")) {
                $('.taxdiv').hide();
                $('.total .amount').html('$' + $('#costs').attr('total'));
                //var json = { obj: $('.messageDiv'), msg: statelong + ' Sales Tax Removed', left: 0, top: 0, append: $('.messageDiv'), _class: 'pop-msg2' };
                //popup_msg(json, 5000);
            }
        }
    }
}

function switchform(obj, progress_bar) {
    $('.cart-form').hide();
    $('.order-back-to-cart').hide()
    if (progress_bar == 0) {
        $('.progress-bar.segment').hide()
        $('.billing-information-button').show();
    } else {
        $('.progress-bar.segment').show()
        setupprogressbar(progress_bar);
    }
    obj.show();
    return false;
}