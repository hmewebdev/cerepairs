﻿$(document).ready(function () {
    if (reqscreen == 'usernamagement') {
        $('#User_Search_Submit').on('click', function () {
            $('.searchform input[type="text"]').each(function (i, item) {
                jsonDTO.usersearchrequest[item.id] = $(item).val() == '' ? null : $(item).val();
            });
            jsonDTO.usersearchrequest.pagenumber = 1;
            //jsonDTO.usersearchrequest.custom = 'default';
            console.log(jsonDTO.usersearchrequest);
            FilterAjax(jsonDTO.usersearchrequest);
        });

        $('#CSP_Per_Page').on('change', function () {
            jsonDTO.usersearchrequest.itemsperpage = parseInt($(this).val());
            jsonDTO.usersearchrequest.pagenumber = 1;
            $('.csp-page-start').html(jsonDTO.usersearchrequest.pagenumber);
            //jsonDTO.usersearchrequest.custom = 'default';
            console.log(jsonDTO.usersearchrequest);
            FilterAjax(jsonDTO.usersearchrequest);
        });

        $('#CSP_Prev_Page').on('click', function () {
            $('#CSP_Next_Page').attr('disabled', false);
            jsonDTO.usersearchrequest.pagenumber = jsonDTO.usersearchrequest.pagenumber - 1;
            if (jsonDTO.usersearchrequest.pagenumber == 1) {
                $('#CSP_Prev_Page').attr('disabled', true);
            }
            //jsonDTO.usersearchrequest.custom = 'default';
            console.log(jsonDTO.usersearchrequest);
            FilterAjax(jsonDTO.usersearchrequest);
        });

        $('#CSP_Next_Page').on('click', function () {
            $('#CSP_Prev_Page').attr('disabled', false);
            jsonDTO.usersearchrequest.pagenumber = jsonDTO.usersearchrequest.pagenumber + 1;
            if ((jsonDTO.usersearchrequest.pagenumber * jsonDTO.usersearchrequest.itemsperpage) >= jsonDTO.usersearchrequest.count) {
                $('#CSP_Next_Page').attr('disabled', true);
            }
            //jsonDTO.usersearchrequest.custom = 'default';
            console.log(jsonDTO.usersearchrequest);
            FilterAjax(jsonDTO.usersearchrequest);
        });

        $(".select-user .sortable").click(function (e) {
            sort_results($(this));
        });

        $('.edit-pending-user #user_type').on('change', function () {
            var user_select_apps = $('.edit-pending-user #user_select_apps').parent();
            $(user_select_apps).find('ul li.active').find('span label').trigger('click');
            getPendingUserTypeRoleList();
        });

        $('.edit-user #user_type').on('change', function () {
            //console.log($('.edit-user input[type=text]#user_select_apps').val());
            //console.log($('.edit-user #user_type').val());
            //var chgArray = alasql("select * from ? where user_type = '" + $('.edit-user #user_type').val() + "'", [user_select_apps_array]);
            ////console.log(chgArray)
            //console.log(previous_user_type)
            //if (user_select_apps_array.length > 0) {
            //    var index = user_select_apps_array.map(function (id) { return id['user_type']; }).indexOf(previous_user_type);
            //    if (index >= 0) {
            //        user_select_apps_array.splice(index, 1);
            //    }
            //}
            //tracker = {};
            //tracker.user_type = previous_user_type;
            //previous_user_type = $('.edit-user #user_type').val();
            //tracker.selected_apps = $('.edit-user input[type=text]#user_select_apps').val().replace(/ /g, '');
            //user_select_apps_array.push(tracker);
            //console.log(user_select_apps_array);

            var user_select_apps = $('.edit-user #user_select_apps').parent();
            $(user_select_apps).find('ul li.active').find('span label').trigger('click');

            getUserTypeRoleList();
        });

        $('.edit-user .input').on('change', function () {
            console.log(this);
            $(this).attr('changed', '1');
        });

        $('.edit-pending-user #user_decline').on('click', function () {
            User_Decline();
        });

        $('.edit-pending-user #user_accept').on('click', function () {
            User_Accept();
        });

        $('.edit-user #btnSubmit').on('click', function () {
            $(window).scrollTop(0);
            UpdateUser();
        });

        populateGrid();
        setTimeout(function () {
            var user_select_apps = $('.edit-pending-user #user_select_apps').parent();
            //$(user_select_apps).find('ul > li:gt(0)').addClass('cust admin emp')
            //$(user_select_apps).find('ul > li:gt(0)').removeClass('cust admin emp')
            $(user_select_apps).find('ul li span:contains( Invoice)').parent().addClass('cust admin emp');
            $(user_select_apps).find('ul li span:contains( Orders)').parent().addClass('cust admin emp');
            $(user_select_apps).find('ul li span:contains( Store)').each(function () {
                switch ($(this).html()) {
                    case '<input type="checkbox"><label></label> Store':
                        $(this).parent().addClass('cust admin emp')
                        break;
                    case '<input type="checkbox"><label></label> Store Management':
                        $(this).parent().addClass('admin')
                        break;
                }
            });
            $(user_select_apps).find('ul li span:contains( Customers)').parent().addClass('admin emp');
            $(user_select_apps).find('ul li span:contains( User Management)').parent().addClass('admin');

            user_select_apps = $('.edit-user #user_select_apps').parent();
            $(user_select_apps).find('ul li span:contains( Invoice)').parent().addClass('cust admin emp');
            $(user_select_apps).find('ul li span:contains( Orders)').parent().addClass('cust admin emp');
            $(user_select_apps).find('ul li span:contains( Store)').each(function () {
                switch ($(this).html()) {
                    case '<input type="checkbox"><label></label> Store':
                        $(this).parent().addClass('cust admin emp')
                        break;
                    case '<input type="checkbox"><label></label> Store Management':
                        $(this).parent().addClass('admin')
                        break;
                }
            });
            $(user_select_apps).find('ul li span:contains( Customers)').parent().addClass('admin emp');
            $(user_select_apps).find('ul li span:contains( User Management)').parent().addClass('admin');
        }, 1000);
    }
    if (reqscreen == 'myaccount') {
        $('.edit-my_account .input').on('change', function () {
            console.log(this);
            $(this).attr('changed', '1');
        });

        $('.edit-my_account #btnSubmit').on('click', function () {
            $(window).scrollTop(0);
            UpdateMyAccount();
        });
    }
});

function populateGrid() {
    $('.csp-page-start').html(jsonDTO.usersearchrequest.pagenumber);
    if ((jsonDTO.usersearchrequest.pagenumber * jsonDTO.usersearchrequest.itemsperpage) > jsonDTO.usersearchrequest.count) {
        $('.csp-page-end').html(jsonDTO.usersearchrequest.count);
    }
    else {
        $('.csp-page-end').html(jsonDTO.usersearchrequest.pagenumber * jsonDTO.usersearchrequest.itemsperpage);

    }
    $('.csp-filter-count').html(jsonDTO.usersearchrequest.count);

    var grid = '';
    $.each(jsonDTO.usermanagementusers, function (i, item) {
        console.log(item)
        var typerole_id = alasql("select * from ? where [id]='usertyperole' and [value] = '" + item.user_type + "'", [jsonDTO.dropdowns])[0].usertype_id;
        var user_type = alasql("select * from ? where [id]='usertype' and [value] = '" + typerole_id + "'", [jsonDTO.dropdowns])[0].text;
        grid += '<tr uid="' + item.user_uid + '">';
        grid += '<td><a href="javascript:void(0);" onclick="editCust(\'' + item.user_uid + '\');">Edit</a></td>';
        grid += '<td style="max-width: 125px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">' + item.user_cust_id + '</td>';
        grid += '<td col="user_firstname">' + item.user_firstname + '</td>';
        grid += '<td col="user_lastname">' + item.user_lastname + '</td>';
        grid += '<td col="user_emailaddress">' + item.user_emailaddress + '</td>';
        grid += '<td col="user_type">' + user_type + '</td>';
        grid += '<td col="user_isactive">';
        grid += '<span class="glyphicon ' + (item.user_isactive == '1' ? 'glyphicon-ok-circle' : 'glyphicon-remove-circle') + ' ' + (item.user_isactive == '1' ? 'greenStatus' : 'redStatus') + '"></span>';
        grid += '</td>';
        grid += '</tr>';
    });
    $('.select-user #User_Results tbody').html(grid);
}

function editCust(cust) {
    ResetUserForm();
    PopulateUserForm(cust);
    $('.select-user').hide();
    $('.edit-user').show();
}

function showusers() {
    $('.edit-user').hide();
    $('.select-user').show();
}

function FilterAjax(dto) {
    $.ajax({
        url: "/UserManagement/UserFilter",
        type: "POST",
        data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dto) {
            console.log(dto)
            if (dto.status == 'login') {
                window.location = '/security/login/';
                //do_json_login();
            }
            if (dto.status == 'OK') {
                jsonDTO.usersearchrequest = dto.usersearchrequest;
                jsonDTO.usermanagementusers = dto.usermanagementusers;
                populateGrid();
            }
        },
        error: function (response) {
            console.log(response.responseText);
            window.location = '/security/login/';
        }
    });
}

function PopulateUserForm(cust) {
    var user = alasql("select * from ? where user_uid = '" + cust + "'", [jsonDTO.usermanagementusers])[0];
    console.log(user)
    var type = '';
    $.each(user, function (key, value) {
        type = $('#' + key).prop('type');
        switch (key) {
            case 'user_address_line1':
            case 'user_address_line2':
            case 'user_brand':
            case 'user_companyname':
            case 'user_country':
            case 'user_cust_id':
            case 'user_emailaddress':
            case 'user_firstname':
            case 'user_id':
            case 'user_lastname':
            case 'user_locality':
            case 'user_phone_number':
            case 'user_postcode':
            case 'user_region':
                //case 'user_select_apps':
                $('.edit-user #' + key).val(value);
                break;
            case 'apply_credit':
                $('.edit-user #apply_credit').prop("checked", value == '1' ? true : false);
                break;
            case 'apply_deposit':
                $('.edit-user #apply_deposit').prop("checked", value == '1' ? true : false);
                break;
            case 'user_uid':
                $('.edit-user #' + key).val(value);
                break;
            case 'user_type':
                var typerole_id = alasql("select * from ? where [id]='usertyperole' and [value] = '" + value + "'", [jsonDTO.dropdowns])[0].usertype_id;
                var user_type = alasql("select * from ? where [id]='usertype' and [value] = '" + typerole_id + "'", [jsonDTO.dropdowns])[0].text;
                if (typeof user_type != "undefined") {
                    console.log(user_type)
                    $('.edit-user #' + key).val(user_type);
                }
                else {
                    $('.edit-user #' + key).val('Customer');
                }
                getUserTypeRoleList();
                break;
            case 'user_isactive':
                $('.edit-user #User_IsActive').val(value);
                $('.edit-user #User_IsActive').attr('prev', value);
                $('.edit-user #User_IsActive_Label').removeClass('btn-success').removeClass('btn-danger').addClass(value == '1' ? 'btn-success' : 'btn-danger').val(value == '1' ? 'Active' : 'Inactive');
                break;
            case 'user_companies':
                if (value.length > 0) {
                    var user_companies = $('.edit-user input[type=text]#user_companies').parent();
                    var companies = value.split(',');
                    var company;
                    $.each(companies, function (i, item) {
                        company = alasql("select * from ? where [id]='company' and [value] = '" + item + "'", [jsonDTO.dropdowns])[0];
                        if (typeof company != 'undefined') {
                            $(user_companies).find('ul li span:contains( ' + company.text + ')').find('label').trigger('click')
                        }
                    });
                }
                break;
            case 'user_company':
                if (value.length > 0) {
                    var user_company = $('.edit-user input[type=text]#user_company').parent();
                    var user_companies = value.split(',');
                    var company;
                    $.each(user_companies, function (i, item) {
                        company = alasql("select * from ? where [id]='company' and [company_code] = '" + item + "'", [jsonDTO.dropdowns])[0];
                        if (typeof company != 'undefined') {
                            $(user_company).find('ul li span:contains( ' + company.text + ')').find('label').trigger('click')
                        }
                     });
                }
                break;
            case 'user_select_apps':
                if (value.length > 0) {
                    var user_select_apps = $('.edit-user #user_select_apps').parent();
                    var user_apps = value.split(',');
                    var app;
                     $.each(user_apps, function (i, item) {
                        app = alasql("select * from ? where [id]='applications' and [value] = '" + item + "'", [jsonDTO.dropdowns])[0];
                        if (app != 'undefined') {
                            if (app.text == 'Store') {
                                $(user_select_apps).find('ul li span:contains( ' + app.text + ')').each(function () {
                                    switch ($(this).html()) {
                                        case '<input type="checkbox"><label></label> Store':
                                            $(this).parent().find('label').trigger('click');
                                            break;
                                    }
                                });
                                //$(user_select_apps).find('ul li span:contains(&nbsp' + app.text + ')').find('label').trigger('click');
                            } else {
                                $(user_select_apps).find('ul li span:contains( ' + app.text + ')').find('label').trigger('click');
                            }
                        }
                    });
                    //$(user_select_apps).find('ul li.active').find('span label').trigger('click');
                    //console.log($(user_select_apps).find('ul li'))
                }
                break;
            default:
                //console.log('default ' + key + '   ' + value)
                break;
        }
    });
    //tracker = {};
    //tracker.user_type = $('.edit-user #user_type').val();
    //tracker.selected_apps = $('.edit-user input[type=text]#user_select_apps').val().replace(/ /g, '');
    //user_select_apps_array.push(tracker);
    //previous_user_type = tracker.user_type;
    //console.log(user_select_apps_array);
}

//var user_select_apps_array = [];

function UpdateUser() {
    var dto = {};
    console.log(dto);
    $('.edit-user .input').removeClass('red-border');
    $('.edit-user .user-message .edit_msg').remove();
    var obligatorys = [];
    $('.edit-user .input.obligatory').each(function () {
        if ($(this).val() == '') {
            obligatorys.push(this.id);
            $(this).addClass('red-border')
        }
    });
    //var user_select_apps = $('.edit-user select#user_select_apps').parent().find('.input[type=text].select-dropdown').val();
    var user_select_apps = $('.edit-user input[type=text]#user_select_apps').val();
    if (user_select_apps == 'Choose your option') {
        //$('.edit-user #user_select_apps').parent().find('.input[type=text].select-dropdown').addClass('red-border');
        $('.edit-user input[type=text]#user_select_apps').addClass('red-border');
        obligatorys.push('user_select_apps');
    }

    //var user_companies = $('.edit-user select#user_company').parent().find('.input[type=text].select-dropdown').val();
    var user_companies = $('.edit-user input[type=text]#user_companies').val();
    if (user_companies == 'Choose your option') {
        //$('.edit-user #user_company').parent().find('.input[type=text].select-dropdown').addClass('red-border');
        $('.edit-user input[type=text]#user_companies').addClass('red-border');
        obligatorys.push('user_companies');
    }

    //var user_company = $('.edit-user select#User_Company_Main').parent().find('.input[type=text].select-dropdown').val();
    var user_company = $('.edit-user input[type=text]#user_company').val();
    if (user_company == 'Choose your option') {
        //$('.edit-user #User_Company_Main').parent().find('.input[type=text].select-dropdown').addClass('red-border');
        $('.edit-user input[type=text]#user_company').addClass('red-border');
        obligatorys.push('user_company');
    }
    if (obligatorys.length > 0) {
        $(window).scrollTop(0);
        var msg = '<div class="edit_msg form-message form-error animated fadeIn"><ul><li>Please complete the items in RED</li></ul></div>';
        $('.edit-user .user-message').append(msg);
        var thread = new handlemsg();
        thread.do($('.edit-user .user-message .edit_msg')); return;
    }
    if ($('.edit-user #User_IsActive').attr('prev') != $('.edit-user #User_IsActive').val()) {
        $('.edit-user #User_IsActive').attr('changed', '1');
    }
    //console.log($('.edit-user #user_company').parent().find('input[type=text]').val())

    var selectedapps = '';
    var apps = user_select_apps.split(',');
    $.each(apps, function (i, item) {
        var app = $.trim(item);
        selectedapps += (selectedapps == '' ? '' : ',') + alasql("select * from ? where [id]='applications' and [text] = '" + app + "'", [jsonDTO.dropdowns])[0].value;
    });

    var selected_user_companies = '';
    user_companies = user_companies.split(',');
    $.each(user_companies, function (i, item) {
        var company = $.trim(item);
        selected_user_companies += (selected_user_companies == '' ? '' : ',') + alasql("select * from ? where [id]='company' and [text] = '" + company + "'", [jsonDTO.dropdowns])[0].value;
    });

    var selected_user_company = '';
    user_company = user_company.split(',');
    $.each(user_company, function (i, item) {
        var company = $.trim(item);
        selected_user_company += (selected_user_company == '' ? '' : ',') + alasql("select * from ? where [id]='company' and [text] = '" + company + "'", [jsonDTO.dropdowns])[0].company_code;
    });

    var user = alasql("select * from ? where user_uid = '" + $('.edit-user input#user_uid').val() + "'", [jsonDTO.usermanagementusers])[0];

    selected_user_company = selected_user_company.split(",").sort().join(",");
    if (selected_user_company != user.user_company.split(",").sort().join(",")) {
        $('.edit-user input[type=text]#user_company').attr('changed', '1');
    }

    selectedapps = selectedapps.split(",").sort().join(",");
    if (selectedapps != user.user_select_apps.split(",").sort().join(",")) {
        $('.edit-user input[type=text]#user_select_apps').attr('changed', '1');
    }

    selected_user_companies = selected_user_companies.split(",").sort().join(",");
    if (selected_user_companies != user.user_companies.split(",").sort().join(",")) {
        $('.edit-user input[type=text]#user_companies').attr('changed', '1');
    }

    var hasChanges = $(".edit-user .input[changed = '1']").length;
    if (hasChanges == 0) {
        var msg = '<div class="edit_msg form-message form-success animated fadeIn"><ul><li>No changes were made.</li></ul></div>';
        $('.edit-user .user-message').append(msg);
        var thread = new handlemsg();
        thread.do($('.edit-user .user-message .edit_msg'));
        return;
    }
    $(".edit-user .input[changed = '1']").each(function () {
        console.log(this.id + ' ' + $(this).prop('type'))
        switch ($(this).prop('type')) {
            case 'button':
                break;
            case 'checkbox':
                dto[this.id] = $('.edit-user #' + this.id).is(":checked") ? '1' : '0';
                break;
            case 'hidden':
                if ([this.id] == 'User_IsActive') {
                    dto['user_isactive'] = $('.edit-user #' + this.id).val();
                }
                break;
            case 'select':
                dto[this.id] = $(this).val();
                break;
            case 'select-one':
                switch (this.id) {
                    case 'user_type':
                        var user_type = alasql("select * from ? where [id]='usertype' and [text] = '" + $(this).val() + "'", [jsonDTO.dropdowns])[0].value;
                        var typerole_id = alasql("select * from ? where [id]='usertyperole' and [usertype_id] = '" + user_type + "'", [jsonDTO.dropdowns])[0].value;
                        dto[this.id] = typerole_id;
                        break;
                    default:
                        dto[this.id] = $(this).val();
                        break;
                }
                break;
            case 'text':
                switch (this.id) {
                    case 'user_select_apps':
                        dto['user_select_apps'] = selectedapps;
                        break;
                    case 'user_companies':
                        dto['user_companies'] = selected_user_companies;
                        break;
                    case 'user_company':
                        dto['user_company'] = selected_user_company;
                        break;
                    case 'user_brand':
                        var val = '';
                        if ($(this).val() == '') {
                            val = ' ';
                        }
                        else {
                            val = $(this).val();
                        }
                        dto[this.id] = val;
                        break;
                    default:
                        dto[this.id] = $(this).val();
                        break;
                }
                break;
            case 'textarea':
                break;
        }
    });
    dto['user_uid'] = $('.edit-user input#user_uid').val()
    dto['user_id'] = $('.edit-user input[type=hidden]#user_id').val()
    console.log(dto)
    UserUpdate(dto, 'Update');
}

function UpdateMyAccount() {
    var dto = {};
    console.log(dto);
    $('.edit-my_account .input').removeClass('red-border');
    $('.edit-my_account .user-message .edit_msg').remove();
    var obligatorys = [];
    $('.edit-my_account .input.obligatory').each(function () {
        if ($(this).val() == '') {
            obligatorys.push(this.id);
            $(this).addClass('red-border')
        }
    });
 
    if (obligatorys.length > 0) {
        $(window).scrollTop(0);
        var msg = '<div class="edit_msg form-message form-error animated fadeIn"><ul><li>Please complete the items in RED</li></ul></div>';
        $('.edit-my_account .user-message').append(msg);
        var thread = new handlemsg();
        thread.do($('.edit-my_account .user-message .edit_msg')); return;
    }


    var hasChanges = $(".edit-my_account .input[changed = '1']").length;
    if (hasChanges == 0) {
        var msg = '<div class="edit_msg form-message form-success animated fadeIn"><ul><li>No changes were made.</li></ul></div>';
        $('.edit-my_account .user-message').append(msg);
        var thread = new handlemsg();
        thread.do($('.edit-my_account .user-message .edit_msg'));
        return;
    }
    $(".edit-my_account .input[changed = '1']").each(function () {
        console.log(this.id + ' ' + $(this).prop('type'))
        switch ($(this).prop('type')) {
            case 'button':
                break;
            case 'checkbox':
                dto[this.id] = $('.edit-my_account #' + this.id).is(":checked") ? '1' : '0';
                break;
            case 'hidden':
                if ([this.id] == 'User_IsActive') {
                    dto['user_isactive'] = $('.edit-my_account #' + this.id).val();
                }
                break;
            case 'select':
                dto[this.id] = $(this).val();
                break;
            case 'select-one':
                switch (this.id) {
                    case 'user_type':
                        var user_type = alasql("select * from ? where [id]='usertype' and [text] = '" + $(this).val() + "'", [jsonDTO.dropdowns])[0].value;
                        var typerole_id = alasql("select * from ? where [id]='usertyperole' and [usertype_id] = '" + user_type + "'", [jsonDTO.dropdowns])[0].value;
                        dto[this.id] = typerole_id;
                        break;
                    default:
                        dto[this.id] = $(this).val();
                        break;
                }
                break;
            case 'text':
                switch (this.id) {
                    case 'user_select_apps':
                        dto['user_select_apps'] = selectedapps;
                        break;
                    case 'user_companies':
                        dto['user_companies'] = selected_user_companies;
                        break;
                    case 'user_company':
                        dto['user_company'] = selected_user_company;
                        break;
                    case 'user_brand':
                        var val = '';
                        if ($(this).val() == '') {
                            val = ' ';
                        }
                        else {
                            val = $(this).val();
                        }
                        dto[this.id] = val;
                        break;
                    default:
                        dto[this.id] = $(this).val();
                        break;
                }
                break;
            case 'textarea':
                break;
        }
    });
    dto['user_uid'] = $('.edit-my_account input#user_uid').val()
    //dto['user_id'] = $('.edit-my_account input[type=hidden]#user_id').val()
    console.log(dto)
    MyAccountUpdate(dto, 'Update');
}

function handlemsg() {
    this.do = function (obj) {
        setTimeout(function () {
            $(obj).remove();
        }, 5000);
    };
}

function ResetUserForm() {
    $('.edit-user select option').attr('selected', false);
    $('.edit-user .select-wrapper.companies ul li.active').each(function (i, item) {
        $(item).find('span label').trigger('click');
    });
    $('.edit-user .apply-check').prop("checked", false);
    $('.edit-user input[type=text].form-control').val('');
    $('.edit-user .select-dropdown').addClass('input');
    $('.edit-user #User_IsActive_Label').removeClass('btn-success').removeClass('btn-danger').addClass('btn-success').val('Active');
    $('.edit-user #User_IsActive').val('1');
    $('.edit-user .input').attr('changed', '0');
    $('.edit-user select#user_company').parent().find('.input[type=text].select-dropdown').attr('id', 'user_companies');
    $('.edit-user select#User_Company_Main').parent().find('.input[type=text].select-dropdown').attr('id', 'user_company');
    $('.edit-user select#user_select_apps').parent().find('.input[type=text].select-dropdown').attr('id', 'user_select_apps');
    $('.edit-user .user-message .edit_msg').remove();
}

function sort_results(obj) {
    $('.header_title').removeClass('sorted');
    $('#' + obj.prop('id') + ' span:first-child').addClass('sorted');
    if (obj.hasClass('sort') || obj.hasClass('desc')) {
        $(".sortable").removeClass('sort').removeClass('desc').removeClass('asc');
        $(".sortable").addClass('sort');
        obj.removeClass('sort').addClass('asc');
        jsonDTO.usersearchrequest.sortorder = 'asc';
    } else {
        $('.sortable').removeClass('sort').removeClass('desc').removeClass('asc');
        $('.sortable').addClass('sort');
        obj.removeClass('sort').addClass('desc');
        jsonDTO.usersearchrequest.sortorder = 'desc';
    }
    jsonDTO.usersearchrequest.sortcolumn = obj.attr('db');
    jsonDTO.usersearchrequest.custom = null;
    jsonDTO.usersearchrequest.pagenumber = 1;
    //jsonDTO.usersearchrequest.custom = 'default';
    FilterAjax(jsonDTO.usersearchrequest);
}

function ViewPendingUsers() {
    PendingUsersAjax();
}

function PendingUsersAjax() {
    $.ajax({
        url: "/UserManagement/PendingUsers",
        type: "POST",
        //data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (dto) {
            console.log(dto);
            if (dto.status == 'login') {
                window.location = '/security/login/';
                //do_json_login();
            }
            if (dto.status == 'OK') {
                PendingUsers = dto;
                console.log(PendingUsers);
                PopulatePendingUsersGrid()
                $('.select-user').hide();
                $('.pending-user').show();
            }
        },
        error: function (response) {
            console.log(response.responseText);
            window.location = '/security/login/';
        }
    });
}

function PopulatePendingUsersGrid(dto) {
    //$('.csp-page-start').html(jsonDTO.usersearchrequest.pagenumber);
    //if ((jsonDTO.usersearchrequest.pagenumber * jsonDTO.usersearchrequest.itemsperpage) > jsonDTO.usersearchrequest.count) {
    //    $('.csp-page-end').html(jsonDTO.usersearchrequest.count);
    //}
    //else {
    //    $('.csp-page-end').html(jsonDTO.usersearchrequest.pagenumber * jsonDTO.usersearchrequest.itemsperpage);

    //}
    //$('.csp-filter-count').html(jsonDTO.usersearchrequest.count);

    var grid = '';
    $.each(PendingUsers.usermanagementusers, function (i, item) {
        console.log(item)
        var user_type = alasql("select * from ? where [id]='usertype' and [value] = '" + '1' + "'", [jsonDTO.dropdowns])[0];
        grid += '<tr uri=' + item.user_uid + '>';
        grid += '<td><a href="javascript:void(0);" onclick="EditPendingUser(\'' + item.user_uid + '\');">Edit</a></td>';
        grid += '<td style="max-width: 125px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;">' + item.user_cust_id + '</td>';
        grid += '<td>' + item.user_firstname + '</td>';
        grid += '<td>' + item.user_lastname + '</td>';
        grid += '<td>' + item.user_emailaddress + '</td>';
        grid += '<td>' + item.user_created_dts + '</td>';
        grid += '<td>';
        grid += '<span class="glyphicon ' + (item.user_isactive == '1' ? 'glyphicon-ok-circle' : 'glyphicon-remove-circle') + ' ' + (item.user_isactive == '1' ? 'greenStatus' : 'redStatus') + '"></span>';
        grid += '</td>';
        grid += '</tr>';
    });
    $('.pending-user #User_Results tbody').html(grid);
}

function showusersfrompending() {
    $('.pending-user').hide();
    $('.select-user').show();
}

function EditPendingUser(cust) {
    ResetPendingUserForm();
    PopulatePendingUserForm(cust)
    $('.pending-user').hide();
    $('.edit-pending-user').show();
}

function ResetPendingUserForm() {
    $('.edit-pending-user select option').attr('selected', false);
    $('.edit-pending-user .select-wrapper.companies ul li.active').each(function (i, item) {
        $(item).find('span label').trigger('click');
    });
    $('.edit-pending-user .apply-check').prop("checked", false);
    $('.edit-pending-user input[type=text].form-control').val('');
    $('.edit-pending-user .select-dropdown').addClass('obligatory');
    $('.edit-pending-user #User_IsActive_Label').removeClass('btn-success').removeClass('btn-danger').addClass('btn-success').val('Active');
    $('.edit-pending-user #User_IsActive').val('1');
    var user_select_apps = $('.edit-pending-user #user_select_apps').parent();
    $(user_select_apps).find('ul > li:gt(0)').hide()
    $(user_select_apps).find('ul li.cust').show();
    $('#user_type').val('Customer');
    $('.edit-pending-user .admin-perm-cont').hide();
    $('.edit-pending-user #apply_credit').prop("checked", false);
    $('.edit-pending-user #apply_deposit').prop("checked", false);

}

function PopulatePendingUserForm(cust) {
    $('.edit-pending-user input').removeClass('red-border');
    var user = alasql("select * from ? where user_uid = '" + cust + "'", [PendingUsers.usermanagementusers])[0];
    var type = '';
    $.each(user, function (key, value) {
        type = $('.edit-pending-user #' + key).prop('type');
        switch (key) {
            case 'user_address_line1':
            case 'user_address_line2':
            case 'user_brand':
            case 'user_companyname':
            case 'user_country':
            case 'user_cust_id':
            case 'user_emailaddress':
            case 'user_firstname':
            case 'user_id':
            case 'user_lastname':
            case 'user_locality':
            case 'user_phone_number':
            case 'user_postcode':
            case 'user_region':
                //case 'user_select_apps':
                $('.edit-pending-user #' + key).val(value);
                break;
            case 'apply_credit':
                $('.edit-pending-user #apply_credit').prop("checked", value == '1' ? true : false);
                break;
            case 'apply_deposit':
                $('.edit-pending-user #apply_deposit').prop("checked", value == '1' ? true : false);
                break;
            case 'user_uid':
                $('.edit-pending-user #' + key).val(value);
                break;
            case 'user_type':
                var user_type = alasql("select * from ? where [id]='usertype' and [value] = '" + value + "'", [jsonDTO.dropdowns])[0];
                if (typeof user_type != "undefined") {
                    console.log(user_type)
                    $('.edit-pending-user #' + key).val(user_type.text);
                }
                else {
                    $('.edit-pending-user #' + key).val('Customer');
                }
                getPendingUserTypeRoleList();
                break;
            case 'user_isactive':
                $('.edit-pending-user #User_IsActive').val(value);
                $('.edit-pending-user #User_IsActive_Label').removeClass('btn-success').removeClass('btn-danger').addClass(value == '1' ? 'btn-success' : 'btn-danger').val(value == '1' ? 'Active' : 'Inactive');
                break;
            case 'user_companies':
                if (value.length > 0) {
                    var user_companies = $('.edit-pending-user #user_company').parent();
                    var companies = value.split(',');
                    var company;
                    $.each(companies, function (i, item) {
                        company = alasql("select * from ? where [id]='company' and [value] = '" + item + "'", [jsonDTO.dropdowns])[0];
                        if (typeof company != 'undefined') {
                            $(user_companies).find('ul li span:contains( ' + company.text + ')').find('label').trigger('click')
                        }
                    });
                }
                break;
            case 'user_company':
                if (value.length > 0) {
                    var user_company = $('.edit-pending-user #User_Company_Main').parent();
                    var user_companies = value.split(',');
                    var company;
                    $.each(user_companies, function (i, item) {
                        company = alasql("select * from ? where [id]='company' and [company_code] = '" + item + "'", [jsonDTO.dropdowns])[0];
                        if (typeof company != 'undefined') {
                            $(user_company).find('ul li span:contains( ' + company.text + ')').find('label').trigger('click');
                        }
                    });
                }
                break;
            case 'user_select_apps':
                if (value.length > 0) {
                    var user_select_apps = $('.edit-pending-user #user_select_apps').parent();
                    var user_apps = value.split(',');
                    var app;
                    $.each(user_apps, function (i, item) {
                        app = alasql("select * from ? where [id]='applications' and [value] = '" + item + "'", [jsonDTO.dropdowns])[0];
                        if (app.text == 'Store') {
                            $(user_select_apps).find('ul li span:contains( ' + app.text + ')').each(function () {
                                switch ($(this).html()) {
                                    case '<input type="checkbox"><label></label> Store':
                                        $(this).parent().find('label').trigger('click');
                                        break;
                                }
                            });
                        } else {
                            $(user_select_apps).find('ul li span:contains( ' + app.text + ')').find('label').trigger('click')
                        }
                    });
                }
                break;
            default:
                //console.log('default ' + key + '   ' + value)
                break;
        }
    });
}

function showpendingFromEdit() {
    $('.edit-pending-user').hide();
    $('.pending-user').show();
}

function getPendingUserTypeRoleList() {
    var UserType = $('.edit-pending-user #user_type').val();
    var user_select_apps = $('.edit-pending-user #user_select_apps').parent();
    $(user_select_apps).find('ul > li:gt(0)').hide()
    switch (UserType) {
        case 'Customer':
            $(user_select_apps).find('ul li.cust').show();
            $('.edit-pending-user .admin-perm-cont').hide();
            $('.edit-pending-user #apply_credit').prop("checked", false);
            $('.edit-pending-user #apply_deposit').prop("checked", false);
            break;
        case 'Employee':
            $(user_select_apps).find('ul li.emp').show();
            $('.edit-pending-user .admin-perm-cont').show();
            break;
        case 'Admin':
        case 'Full Admin':
            $(user_select_apps).find('ul li.admin').show();
            if (UserType === "Full Admin") {
                $('.edit-pending-user #apply_credit').prop("checked", true);
                $('.edit-pending-user #apply_deposit').prop("checked", true);
            }
            $('.edit-pending-user .admin-perm-cont').show();
            break;
    }
}
//var previous_user_type = null;
function getUserTypeRoleList() {
    var UserType = $('.edit-user #user_type').val();
    var user_select_apps = $('.edit-user #user_select_apps').parent();
    $(user_select_apps).find('ul > li:gt(0)').hide()
    $('.edit-user #apply_credit').attr("disabled", false);
    $('.edit-user #apply_deposit').attr("disabled", false);
    //previous_user_type = UserType;
    switch (UserType) {
        case 'Customer':
            $(user_select_apps).find('ul li.cust').show();
            $('.edit-user .admin-perm-cont').hide();
            $('.edit-user #apply_credit').prop("checked", false).attr('changed', '1');
            $('.edit-user #apply_deposit').prop("checked", false).attr('changed', '1');
            break;
        case 'Employee':
            $(user_select_apps).find('ul li.emp').show();
            $('.edit-user .admin-perm-cont').show();
            break;
        case 'Admin':
        case 'Full Admin':
            $(user_select_apps).find('ul li.admin').show();
            if (UserType === "Full Admin") {
                $('.edit-user #apply_credit').prop("checked", true).attr('changed', '1');
                $('.edit-user #apply_deposit').prop("checked", true).attr('changed', '1');
                $('.edit-user #apply_credit').attr("disabled", true);
                $('.edit-user #apply_deposit').attr("disabled", true);
            }
            $('.edit-user .admin-perm-cont').show();
            break;
    }
}

function User_Decline() {
    var user = alasql("select * from ? where user_uid = '" + $('.edit-pending-user input#user_uid').val() + "'", [PendingUsers.usermanagementusers])[0];
    $('.edit-pending-user input').removeClass('red-border');
    var user_cust_id = $('.edit-pending-user input#user_cust_id').val();

    user.user_cust_id = user_cust_id;
    console.log(user);
    UserVerifyUpdate(user, 'Decline')
}

function User_Accept() {
    $('.edit-pending-user .user-message .edit_msg').remove();
    var selectedapps = '';
    var error = false;
    var apply_credit = '0';
    var apply_deposit = '0';
    var user = alasql("select * from ? where user_uid = '" + $('.edit-pending-user input#user_uid').val() + "'", [PendingUsers.usermanagementusers])[0];
    $('.edit-pending-user input').removeClass('red-border');
    console.log(user);
    var user_cust_id = $('.edit-pending-user input#user_cust_id').val();
    if (user_cust_id == '') {
        $('.edit-pending-user input#user_cust_id').addClass('red-border');
        error = true;
    }
    var user_type = alasql("select * from ? where [id]='usertype' and [text] = '" + $('.edit-pending-user #user_type').val() + "'", [jsonDTO.dropdowns])[0].value;
    var usertyperole = alasql("select * from ? where [id]='usertyperole' and [usertype_id] = '" + user_type + "'", [jsonDTO.dropdowns])[0].value;

    //var user_type = $('.edit-pending-user #user_type').val();
    var user_select_apps = $('.edit-pending-user #user_select_apps').parent().find('.select-dropdown').val();
    if (user_select_apps == 'Choose your option') {
        $('.edit-pending-user #user_select_apps').parent().find('.select-dropdown').addClass('red-border');
        error = true;
    }
    else {
        var apps = user_select_apps.split(',');
        $.each(apps, function (i, item) {
            var app = $.trim(item);
            selectedapps += (selectedapps == '' ? '' : ',') + alasql("select * from ? where [id]='applications' and [text] = '" + app + "'", [jsonDTO.dropdowns])[0].value;
        });
    }

    if (error) {
        $(window).scrollTop(0);
        var msg = '<div class="edit_msg form-message form-error animated fadeIn"><ul><li>Please complete the items in RED</li></ul></div>';
        $('.edit-pending-user .user-message').append(msg);
        var thread = new handlemsg();
        thread.do($('.edit-pending-user .user-message .edit_msg'));
        return;
    }
    if ($('.edit-pending-user #user_type').val() != 'Customer') {
        apply_credit = $('.edit-pending-user #apply_credit').is(":checked") ? '1' : '0';
        apply_deposit = $('.edit-pending-user #apply_deposit').is(":checked") ? '1' : '0';
    }
    user.apply_credit = apply_credit;
    user.apply_deposit = apply_deposit;
    user.user_select_apps = selectedapps;
    user.user_cust_id = user_cust_id;
    user.user_type = usertyperole;
    UserVerifyUpdate(user, 'Accept')
}

function UserVerifyUpdate(dto, request) {
    $.ajax({
        url: '/UserManagement/VerifyUpdate',
        type: 'POST',
        data: JSON.stringify(dto),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        headers: { 'requesttype': request },
        success: function (dto) {
            console.log(dto);
            if (dto.status == 'login') {
                window.location = '/security/login/';
                //do_json_login();
            }
            if (dto.status == 'OK') {
                $('.pending-user tr[uri=' + dto.user_uid + ']').remove();
                var index = PendingUsers.usermanagementusers.map(function (uid) { return uid['user_uid']; }).indexOf(dto.user_uid);
                if (index >= 0) {
                    PendingUsers.usermanagementusers.delete(index);
                }
                $(window).scrollTop(0);
                var msg = '<div class="edit_msg form-message form-success animated fadeIn"><ul><li>Your changes have been saved.</li></ul></div>';
                $('.edit-pending-user .user-message').append(msg);
                var thread = new handlemsg();
                thread.do($('.edit-pending-user .user-message .edit_msg'));
            }
        },
        error: function (response) {
            console.log(response.responseText);
            window.location = '/security/login/';
        }
    });
}

function UserUpdate(dto, request) {
    $.ajax({
        url: '/UserManagement/UserUpdate',
        type: 'POST',
        data: JSON.stringify(dto),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        headers: { 'requesttype': request },
        success: function (dto) {
            console.log(dto);
            if (dto.status == 'login') {
                window.location = '/security/login/';
                //do_json_login();
            }
            if (dto.status == 'OK') {
                var index = jsonDTO.usermanagementusers.map(function (uid) { return uid['user_uid']; }).indexOf(dto.user_uid);
                if (index >= 0) {
                    var tr = $('.select-user table tbody tr[uid=' + dto.user_uid + ']');
                    if (dto.user_firstname != null) {
                        $(tr).find('td[col=user_firstname]').html(dto.user_firstname);
                    }
                    if (dto.user_lastname != null) {
                        $(tr).find('td[col=user_lastname]').html(dto.user_lastname);
                    }
                    if (dto.user_emailaddress != null) {
                        $(tr).find('td[col=user_emailaddress]').html(dto.user_emailaddress);
                    }
                    if (dto.user_isactive != null) {
                        $(tr).find('td[col=user_isactive] span').removeClass('glyphicon-remove-circle redStatus glyphicon-ok-circle greenStatus').addClass(dto.user_isactive == '1' ? 'glyphicon-ok-circle greenStatus' : 'glyphicon-remove-circle redStatus');
                    }
                    if (dto.user_type != null) {
                        var typerole_id = alasql("select * from ? where [id]='usertyperole' and [value] = '" + dto.user_type + "'", [jsonDTO.dropdowns])[0].usertype_id;
                        var user_type = alasql("select * from ? where [id]='usertype' and [value] = '" + typerole_id + "'", [jsonDTO.dropdowns])[0].text;
                        $(tr).find('td[col=user_type]').html(user_type);
                    }
                }
                $.each(dto, function (key, value) {
                    if (value != null) {
                        jsonDTO.usermanagementusers[index][key] = value == ' ' ? null : value;
                    }
                })
                $('.edit-user .input').attr('changed', '0');
                var msg = '<div class="edit_msg form-message form-success animated fadeIn"><ul><li>Your account changes have been saved.</li></ul></div>';
                $('.edit-user .user-message').append(msg);
                var thread = new handlemsg();
                thread.do($('.edit-user .user-message .edit_msg'));
                //$('.pending-user tr[uri=' + dto.user_uid + ']').remove();
                //var index = PendingUsers.usermanagementusers.map(function (uid) { return uid['user_uid']; }).indexOf(dto.user_uid);
                //if (index >= 0) {
                //    PendingUsers.usermanagementusers.delete(index);
                //}
            }
        },
        error: function (response) {
            console.log(response.responseText);
            window.location = '/security/login/';
        }
    });
}

function MyAccountUpdate(dto, request) {
    $.ajax({
        url: '/UserManagement/MyAccountUpdate',
        type: 'POST',
        data: JSON.stringify(dto),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        headers: { 'requesttype': request },
        success: function (dto) {
            console.log(dto);
            if (dto.status == 'login') {
                window.location = '/security/login/';
                //do_json_login();
            }
            if (dto.status == 'OK') {
                var index = jsonDTO.usermanagementusers.map(function (uid) { return uid['user_uid']; }).indexOf(dto.user_uid);
                if (index >= 0) {
                    $.each(dto, function (key, value) {
                        if (value != null) {
                            jsonDTO.usermanagementusers[index][key] = value == ' ' ? null : value;
                        }
                    })
                }
                $('.edit-my_account .input').attr('changed', '0');
                var msg = '<div class="edit_msg form-message form-success animated fadeIn"><ul><li>Your account changes have been saved.</li></ul></div>';
                $('.edit-my_account .user-message').append(msg);
                var thread = new handlemsg();
                thread.do($('.edit-user .user-message .edit_msg'));
            }
        },
        error: function (response) {
            console.log(response.responseText);
            window.location = '/security/login/';
        }
    });
}