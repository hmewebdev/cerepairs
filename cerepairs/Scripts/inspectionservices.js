﻿
$(document).ready(function () {
    //$(".input-date").datepicker({ dateFormat: 'dd/mm/yy' }).val();
    $(".input-date").datepicker().val();
    $('.numeric').keypress(function (e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode == 8 || keyCode == 9) return true;
        if (!$.isNumeric(String.fromCharCode(keyCode))) { return false; }
    });
    $('.noEnterKey').keypress(function (e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode == 13) { return false; }
    });
    $('form input[type=text]').change(function (e) {
        $(this).attr('changed', $(this).val().length > 0 ? '1' : '0');
        console.log(this)
    });
    $('form textarea').change(function (e) {
        $(this).attr('changed', $(this).val().length > 0 ? '1' : '0');
        console.log(this)
    });
    $('form select').change(function (e) {
        $(this).attr('changed', $(this).prop('selectedIndex') > 0 ? '1' : '0');
        console.log(this)
    });
    $('form input[type=checkbox]').change(function (e) {
        $(this).attr('changed', this.checked ? '1' : '0');
        console.log(this)
    });
});

function validate() {
    $('.form-control').removeClass('redborders');
    var errors = '';
    if ($('#company1').prop('selectedIndex') == 0) {
        $('#company1').addClass('redborders');
        errors += (errors.length > 0 ? '<br>' : '') + 'Please complete HSC OR CLEAR-COM dropdown'
    }
    if ($('#itemnumber').val() == '') {
        $('#itemnumber').addClass('redborders');
        errors += (errors.length > 0 ? '<br>' : '') + 'Please provide an Item Number'
    }
    if ($('#revision').val() == '') {
        $('#revision').addClass('redborders');
        errors += (errors.length > 0 ? '<br>' : '') + 'Please provide a Revision'
    }

    if ($('#datereceived').val() == '') {
        $('#datereceived').addClass('redborders');
        errors += (errors.length > 0 ? '<br>' : '') + 'Please provide a Date Given To Inspection';
    }

    if ($('#needdate').val() == '') {
        $('#needdate').addClass('redborders');
        errors += (errors.length > 0 ? '<br>' : '') + 'Please provide a Date Needed';
    }

    if ($('#reasonforfai').prop('selectedIndex') == 0) {
        $('#reasonforfai').addClass('redborders');
        errors += (errors.length > 0 ? '<br>' : '') + 'Please provide a Reason For Inspection';
    }

    if ($('#failevel').prop('selectedIndex') == 0) {
        $('#failevel').addClass('redborders');
        errors += (errors.length > 0 ? '<br>' : '') + 'Please provide an Inspection Level';
    }

    if ($('#riqtyinspected').val() == '') {
        $('#riqtyinspected').addClass('redborders');
        errors += (errors.length > 0 ? '<br>' : '') + 'Please provide How Many Yoy Would Like Inspected';
    }

    if ($('#qtyprovided').val() == '') {
        $('#qtyprovided').addClass('redborders');
        errors += (errors.length > 0 ? '<br>' : '') + 'Please provide the Quantity Parts Provided to Inspection';
    }

    if (errors.length > 0) {
        $('.alerteModalMsg').html(errors);
        $('#alerteModal').modal('show');
        return false;
    }
    
    var count = $("form [changed = '1']").length;
    //if (count == 0 )return false;
    var dto = {};
    $('input[type=checkbox]').each(function () {
        dto[this.id] = this.checked ? '1' : '0';
    });
    $('input[type=text]').each(function () {
        dto[this.id] = $(this).val();
    });
    $('select').each(function () {
        dto[this.id] = $(this).val();
    });
    $('textarea').each(function () {
        dto[this.id] = $(this).val();
    });
    $('#jsonobj').val(JSON.stringify(dto));
    dto['jsonobj'] = $('#jsonobj').val();
    console.log(dto)
    return true;
}