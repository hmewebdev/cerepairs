$('.ui.dropdown')
    .dropdown()
    ;

$('.ui.radio.checkbox')
    .checkbox()
    ;

$('.ui.checkbox')
    .checkbox()
    ;

$('.info.segment .item')
    .tab({
        context: '.info.segment'
    })
    ;

$('.ui.accordion')
    .accordion({
        exclusive: false
    })
    ;

var mySwiper = new Swiper('.swiper-container', {
    // Optional parameters

    loop: true,
    slidesPerView: 1,
    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
        el: '.swiper-scrollbar',
    },
});

$('.counter .button').on('click', function () {

    var $button = $(this);
    var oldValue = parseInt($button.parent().find('input').val());
    var $dec = $button.parent().find('.dec');
    var newVal = 0;

    if ($button.hasClass('inc')) {
        //Any NaN will be converted to 1
        newVal = isNaN(oldValue) ? 1 : oldValue + 1;
        if ($dec.hasClass('disable')) {
            $dec.toggleClass('disable');
        }
    } else if ($button.hasClass('dec')) {
        // Don't allow decrementing below zero
        if (oldValue > 0) {
            newVal = oldValue - 1;
        } else {
            newVal = 0;
        }
        if (newVal === 0) { $button.addClass('disable'); }
    }

    $button.parent().find('input').val(newVal);

});


function rows(columns) {

    var items = $('.grid .cell').length;
    var lastRowLen = items % columns;
    var itemIdx = items - lastRowLen;

    for (var i = 0; i < lastRowLen; i++) {
        $('.grid .cell:nth(' + (i + itemIdx) + ')').addClass('last-row');
    }

}

rows(3);
$('#actionButton').hide();
$('#phoneNo').hide();
$('.price').hide();


function hideAll() {
    $('#actionButton').hide();
    $('#phoneNo').hide();
    $('.price').hide();
}


$('.ce.radio').on('click', function (e) {
    var radioValue = $(this).find('input[type=\'radio\']').val();
    var buttons = null;
    if (radioValue === 'repair') {
        $('.fields').removeClass('visible');
        $('#repair').addClass('visible');
        buttons = $('.group.condition #repair .ce.radio.checkbox');
        if (buttons.length == 1) {
            doclick($(buttons[0]).find('input[type=radio]'));
        }
    } else {
        if (radioValue === 'buy') {
            $('.fields').removeClass('visible');
            $('#buy').addClass('visible');
            buttons = $('.group.condition #buy .ce.radio.checkbox');
            if (buttons.length == 1) {
                doclick($(buttons[0]).find('input[type=radio]'));
            }
        }
    }
    updatePrice(radioValue);
    updateDetails(radioValue, 'name');
});

function doclick(el) {
    setTimeout(function () {
        $(el).click();
    }, 50);

}


function updatePrice(e) {
    new_refurbished = '';
    new_refurbished_price = '';
    $('.ce.form #actionButton').hide(); $('.ce.form #view-cart').hide(); $('.ce.form .item-purchased').html(''); $('.ce.form .item-purchased').hide();

    var a = "";
    switch (e) {
        case "advance": a = '$' + parseFloat(Exchange_Price, 10).toFixed(2), $(".price").show(), $("#phoneNo").show(), $("#actionButton").hide(); break;
        case "standard": a = '$' + parseFloat(Repair_Price, 10).toFixed(2) + '-$' + parseFloat(Recondition_Price, 10).toFixed(2), $(".price").show(), $("#phoneNo").hide(), $("#actionButton").html("Send it In"), $("#actionButton").show(); break;
        case "new": a = '$' + parseFloat(Retail_Price, 10).toFixed(2), $("#phoneNo").hide(), $(".price").show(), $("#actionButton").html("Add to Cart"), $("#actionButton").show(); new_refurbished = 'New'; new_refurbished_price = parseFloat(Retail_Price, 10).toFixed(2); $('.ce.form #actionButton').show(); break;
        case "refurbished": a = '$' + parseFloat(Refurbished_Price, 10).toFixed(2), $(".price").show(), $("#phoneNo").hide(), $("#actionButton").html("Add to Cart"), $("#actionButton").show(); new_refurbished = 'Refurbished'; new_refurbished_price = parseFloat(Refurbished_Price, 10).toFixed(2); $('.ce.form #actionButton').show(); break;
    }
    $(".amount").html(a);
}
function updateModal(e, a) {
    var n = "";
    $('.details .power-supply-span').hide();
    switch (e) {
        case "advance": n = "If you can't wait for a repair, CE will overnight you a refurbished unit in exchange for your broken unit. Once you receive the replacement unit, return your broken unit in the same box. A minimum $35 shipping fee will apply. Additional charges may apply for expedited shipping needs."; break;
        case "standard": n = "Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician's assessment. In either case, a reconditioned replacement item may be returned to you.";
            //$('.details .power-supply-span').show();
            break;
        case "new": n = "These products are straight from the factory and carry the OEM warranty. CE is a fully authorized distributor of this equipment."; break;
        case "refurbished": n = "This is a previously used product. Our technicians thoroughly check, repair, and upgrade to ensure like-new operation. Your purchase of these products includes a 4-month warranty. Please note that all refurbished items are subject to equipment availability.";
            $('.details .power-supply-span').show();
            break;
    } $(".ui.modal .header").html(a), $(".details .details-span").html(n);
}

function updateDetails(prodCond, name) {
    var msg = '';
    $('.details .power-supply-span').hide();
    switch (prodCond) {
        case 'advance':
            msg = 'If you can\'t wait for a repair, CE will overnight you a refurbished unit in exchange for your broken unit. Once you receive the replacement unit, return your broken unit in the same box. A minimum $35 shipping fee will apply. Additional charges may apply for expedited shipping needs.';
            break;
        case 'standard':
            msg = 'Basic functional issues will incur a repair fee. Repairs needing new case parts, button, gaskets or headset cords will be charged a reconditioning fee based on technician\'s assessment. In either case, a reconditioned replacement item may be returned to you.';
            //$('.details .power-supply-span').show();
           break;
        case 'new':
            msg = 'These products are straight from the factory and carry the OEM warranty. CE is a fully authorized distributor of this equipment.';
            break;
        case 'refurbished':
            msg = 'This is a previously used product. Our technicians thoroughly check, repair, and upgrade to ensure like-new operation. Your purchase of these products includes a 4-month warranty. Please note that all refurbished items are subject to equipment availability.';
            $('.details .power-supply-span').show();
            break;
    }

    $('.ui.modal .header').html(name);
    $('.details .details-span').html(msg);
}


// Scroll to a certain element
$('.condition .ce.radio').on('click', function () {
    // alert('hi');
    document.querySelector('.condition').scrollIntoView({
        behavior: 'smooth'
    });
});
