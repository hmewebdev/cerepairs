﻿function popup_msg(json) {
    var ele = create_element(json.msg, json._class);
    var position = getxy($(json.obj));
    $(ele).css({ top: position.top + (json.top + 20), left: position.left + json.left });
    $(json.append).append(ele);
    var thread = new handlemsg();
    thread.do(ele);
}

function getxy(obj) {
    var position = {};
    position.left = $(obj).position().left;
    position.top = $(obj).position().top;
    position.x = $(obj).position().left;
    position.y = $(obj).position().top;
    return position;
}

function handlemsg(delay) {
    if (delay > 0) {
        this.do = function (obj) {
            setTimeout(function () {
                $(obj).remove();
            }, 5000);
        };
    }
}

function create_element(msg, _class) {
    return $('<div/>', {
        text: msg,
        class: _class
    });
}
