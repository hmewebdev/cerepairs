﻿
var validationpressed = 0;
var submitpressed = 0;
var submitted = 0;
function payxeroscript() {
    //document.addEventListener("DOMContentLoaded", function () {
    $('.not-numeric').on('keypress keyup', function (e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (e.type === 'keypress') {
            if (jQuery.isNumeric(String.fromCharCode(keyCode))) {
                return false;
            }
        }

    });
    CollectJS.configure({
        paymentSelector: "#demoPayButton",
        variant: "inline",
        styleSniffer: true,
        googleFont: "Montserrat:400",
        customCss: {

        },
        invalidCss: {
            color: "white",
            "background-color": "red",
        },
        validCss: {
            color: "black",
            "background-color": "transparent",
        },
        placeholderCss: {
            color: "black",
            "background-color": "transparent",
        },
        focusCss: {
            color: "white",
            "background-color": "gray",
        },
        fields: {
            ccnumber: {
                selector: "#demoCcnumber",
                title: "Card Number",
                placeholder: "0000 0000 0000 0000",
            },
            ccexp: {
                selector: "#demoCcexp",
                title: "Card Expiration",
                placeholder: "00 / 00",
            },
            cvv: {
                display: "show",
                selector: "#demoCvv",
                title: "CVV Code",
                placeholder: "***",
            },
        },
        validationCallback: function (field, status, message) {
            if (status) {
                var message = field + " is now OK: " + message;
            } else {
                var message = field + " is now Invalid: " + message;
            }
            if (submitted == 0 && $('#demoPayButton').html() == 'Submit') {
                $('#demoPayButton').html('Confirm Card')
                $('.surcharge-message').hide();
            }
            console.log(message);
        },
        timeoutDuration: 10000,
        timeoutCallback: function () {
            console.log(
                "The tokenization didn't respond in the expected timeframe.  This could be due to an invalid or incomplete field or poor connectivity"
            );
        },
        fieldsAvailableCallback: function () {
            console.log("Collect.js loaded the fields onto the form");
        },
        callback: function (response) {
            // This is the representative token of the card data
            submitted = 0;
            $('.errorMessage').hide();
            $('.red_border').removeClass('redborder');
            if ($('#demoPayButton').html() == 'Confirm Card' && validationpressed == 1) {
                return;
            }
            if ($('#demoPayButton').html() == 'Submit' && submitpressed == 1) {
                return;
            }
            var error = false;
            var nameValue = document.querySelector('#name').value;
            var zipValue = document.querySelector('#zip').value;
            if (nameValue == '') {
                error = true;
                $('#name').addClass('redborder');
            }
            if (zipValue == '') {
                error = true;
                $('#zip').addClass('redborder');
            }
            if (error) {
                return;
            }
            $('.spinner').show();

            console.log(response.card)
            // This is sample data in a convinient format for our api to process
            // notes the card data at the bottom of the object.

            var jsonData = JSON.stringify({
                clientId: document.getElementById("custIDs").value,
                amount: document.getElementById("total").value,
                surcharge: 0,
                contact: {
                    email: document.getElementById("email").value,
                    phone: ''
                },
                billingAddress: {
                    address: '',
                    city: '',
                    country: '',
                    state: '',
                    zipCode: zipValue,//32405
                },
                shippingAddress: {},
                order: {
                    amount: document.getElementById("total").value,
                    shipping: 0,
                    tax: 0,
                    poNumber: '',
                    discount: 0
                },
                capture: true,
                card: {
                    name: nameValue,
                    zipCode: zipValue,
                    paymentToken: response.token,
                    number: response.card
                },
                SECURETOKENID: document.getElementById("SECURETOKENID").value,
                SECURETOKEN: document.getElementById("SECURETOKEN").value
            })

            var jsonDataInput = document.createElement("input")
            jsonDataInput.type = "hidden";
            jsonDataInput.name = "jsonDataInput";
            jsonDataInput.value = jsonData;

            var form = document.getElementsByTagName("form")[0];
            form.appendChild(jsonDataInput);
            if ($('#demoPayButton').html() == 'Confirm Card') {
                validateCreditCard(jsonData);
                return;
            }
            processCreditCard(jsonData);
            //form.submit();
        },
        //    });
    });
}

function processCreditCard(jsonData) {
    submitpressed = 1;
    $.ajax({
        type: "POST",
        url: "/PayXeroAjax/PayXeroChargeAjax",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: jsonData,
        success: function (dto) {
            console.log(dto);
            $('.spinner').hide();
            submitpressed = 0;
            if (dto.actionstatus == "LogOff") {
                window.location = "/Security/LogOff";
            }
            if (dto.actionstatus == "Failed") {
                if (dto.response != null) {
                    $('.errorMessage').html(dto.response.text);
                    $('.errorMessage').show();
                }
                else {
                    //$('.errorMessage').html(dto.Response.text);
                    $('.errorMessage').show();

                }
                return;

            }
            if (dto.actionstatus == 'OK') {
                //if (dto.data.surcharge != null && dto.data.surcharge != 0) {
                //    $('.nosurcharge').hide();
                //    $('.surcharge').show();
                //    $('.surchargefee').html(dto.data.surcharge);
                //    $('.totalcosts').html(dto.data.amount);
                //}
                $('.payxeroconformation').html(dto.data.confirmationNumber);
                $('.ccResponse').html(dto.content);
                $('#searchCustomer').hide();
                $('#searchCustomer').html('');

                $('.payxeroresp').show();
                $('.cart-badge').html('').removeClass('active');
                clearcartallAjax();
                $('.ui.grid.the-cart.cart-form').html('<div>No Items in your Cart</div>');
                $('.four.wide.column.gray').hide();
                //$.ajax({
                //    type: "POST",
                //    url: "/PayXeroAjax/SendEmailsAjax",
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    data: JSON.stringify(dto),
                //    success: function (dto) {

                //    },
                //    error: function () {
                //        alert("Something went wrong");
                //    }
                //});
            }
        },
        error: function (jqXHR, ajaxOptions, thrownError) {
            submitpressed = 0;
            $('.spinner').hide();
            alert("Something went wrong");
        }
    });
}

function validateCreditCard(jsonData) {
    validationpressed = 1;
    $.ajax({
        type: "POST",
        url: "/PayXeroAjax/PayXeroValidateAjax",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: jsonData,
        success: function (dto) {
            validationpressed = 0;
            console.log();
            if (dto.actionstatus == "Failed") {
                $('.errorMessage').html('An Error Occured : ' + dto.message);
                $('.errorMessage').show();
                $('.spinner').hide();
                return;
            }
            if (dto.data.surcharge == 0) {
                processCreditCard(dto.jsonData);
                return;
            }
            $('.spinner').hide();
            var surchargeTotal = $('.amount.grandtotal').html().replace('$', '');
            surchargeTotal = parseFloat(surchargeTotal);
            var surcharge = (parseFloat(dto.data.surcharge) / 100);
            $('.amount.surcharge').html('$' + surcharge.toString());
            console.log(surcharge);
            var newtotal = surchargeTotal + surcharge;
            console.log(newtotal.toFixed(2));
            $('.withSurcharge').html(newtotal.toFixed(2));
            $('.amount.grandtotal').html('$' + newtotal.toFixed(2).toString())
            var message = 'Please be aware that there will be a surcharge fee of 3.0% <span style="color:blue">$' + parseFloat(dto.data.surcharge) / 100 + '</span> added to this payment. This Surcharge is not greater than our total cost of accepting credit cards. There is no surcharge for Debit Card Payments. By clicking the Submit button you are accepting the surcharge';
            $('.surcharge-message').html(message).show();
            $('.surcharge.column').show();
            $('#demoPayButton').html('Submit');
            //jsCommon.showProgress(false);
        },
        error: function (jqXHR, ajaxOptions, thrownError) {
            validationpressed = 0;
            $('.spinner').hide();
            alert("Something went wrong");
        }
    });
}


function backtopayment() {
    $('#ccProcess').hide();
    $('#ccProcess').html('');
    $('#payment').show();
}

