﻿function addtocart(product, count, category) {
    var dto = {
        product: product,
        count: count,
        category: category
    };
    $.ajax({
        type: "POST",
        async: true,
        url: "/cart/AddToCart",
        data: JSON.stringify(dto),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data)
            window.location.href = '/cart';
        }
    });
    return false;
}
$(document).ready(function () {
    $('.products a').each(function () {
        //this.href = "javascript:void(0);";
        $(this).attr('onclick', "return addtocart('" + $(this).attr('product') + "', 1, '" + $(this).attr('category') + "');");
        console.log($(this).attr('title'));
        console.log($(this).attr('onclick'));
    });
});