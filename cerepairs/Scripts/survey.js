﻿var dto = {};
var validEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
$(document).ready(function () {
    $("#surveychain").change(function () {
        if ($(this).val() == 'Other') {
            $("#surveyother").show();
        } else {
            $("#surveyother").hide();
            $("#surveyother").val('');
        }
    });

    $("#sbt_btn").click(function (e) {
        dto = {};
        $('.surveytop').find('*').removeClass('yellobackground');
        $('#surveymessage').hide();
        //$('#invalidemailaddress').hide();

        //if (grecaptcha.getResponse() == '') {
        //    //$('.surveycaptcha').addClass('red');
        //    //displaymessage();
        //    return;
        //}
        //else {
        //    //dto.grecaptcha = grecaptcha.getResponse();
        //}

        //var othersOption = $('#Store_Name').find('option:selected');

        if (!$("input[name='quickly']:checked").val()) {
            $('.group1').addClass('yellobackground');
        }
        else {
            dto.quickly = $("input[name='quickly']:checked").val();
        }

        if (!$("input[name='expectations']:checked").val()) {
            $('.group2').addClass('yellobackground');
        }
        else {
            dto.expectations = $("input[name='expectations']:checked").val();
        }

        if (!$("input[name='goodvalue']:checked").val()) {
            $('.group3').addClass('yellobackground');
        }
        else {
            dto.goodvalue = $("input[name='goodvalue']:checked").val();
        }

        if (!$("input[name='drivethru']:checked").val()) {
            $('.group4').addClass('yellobackground');
        }
        else {
            dto.drivethru = $("input[name='drivethru']:checked").val();
        }

        if ($('input.surveyname').val() == '') {
            $('input.surveyname').addClass('yellobackground');
        }
        else {
            dto.name = $('input.surveyname').val();
        }

        if ($('select.surveychain').val() == '') {
            $('.ui.fluid.dropdown.selection').addClass('yellobackground');
        } else {
            if ($('select.surveychain').find('option:selected').val() == 'Other') {
                if ($('input.surveyother').val() == '') {
                    $('input.surveyother').addClass('yellobackground');
                } else {
                    dto.chain = $('input.surveyother').val();
                }
            } else {
                dto.chain = $('select.surveychain').find('option:selected').val();
            }
        }

        if ($('input.surveystorenumber').val() == '') {
            $('input.surveystorenumber').addClass('yellobackground');
        }
        else {
            dto.storenumber = $('input.surveystorenumber').val();
        }


        if ($('input.surveyemail').val() == '') {
            $('input.surveyemail').addClass('yellobackground');
        }
        else {
            if (!validEmail.test($('input.surveyemail').val())) {
                $('input.surveyemail').addClass('yellobackground');
               // $('#invalidemailaddress').show();
            }
            else {
                dto.email = $('input.surveyemail').val();
            }
        }

        dto.receivenews = $('#surveyreceivenews').is(":checked") ? '1' : '0';
        dto.comments = $('#surveycomments').val();
        dto.phone = $('#surveyphone').val();
        //console.log(dto);
        if ($('.yellobackground').length >= 0) {
            if (grecaptcha.getResponse(recaptchaSurvey) == '') {
                $('#sbt_btn').hide();
                $('.grecaptchaSurvey-div').show();
                e.stopImmediatePropagation();
                return;
            }
            $('.grecaptchaSurvey-div').hide();
            $('#sbt_btn').show();

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/home/Survey",
                data: JSON.stringify(dto),
                dataType: "json",
                success: function (data) {
                    if (data.results == 'Yes') {
                        $('.ui.padded.segment').hide();
                        $('.thankyou').show();
                        $(window).scrollTop(0);
                    }
                },
                error: function (jqXHR, ajaxOptions, thrownError) {
                }
            });

        }
        else {
            displaymessage()
        }
        return false;
    });

    $('.special-offers .special-offers-button').on('click touch tap', function (e) {
        var dto = {}
        var obligatorys = [];
        $('.special-offers .input .obligatory').removeClass('red-border');
        $('.special-offers .input .obligatory').each(function (i, item) {
            if ($(item).val() == '') {
                obligatorys.push(item.id);
                $(item).addClass('red-border')
            }
        });
        if (obligatorys.length > 0) {
            var json = { obj: $('.SpecialOffer-spinner').parent().parent(), msg: 'Please complete the required fields underlined in red.', left: 0, top: 0, append: $('.SpecialOffer-spinner').parent().parent(), _class: 'pop-msg2' };
            popup_msg(json, 5000);
            e.stopImmediatePropagation();
            return false;
        }
        if (grecaptcha.getResponse(recaptcha2) == '') {
            $('.special-offers .special-offers-button').hide();
            $('.captchaSpecialOffer-div').show();
            e.stopImmediatePropagation();
            return;
        }
        $('.special-offers .input[type=text]').each(function (i, item) {
            dto[item.id] = $(item).val()
        });
        $(".SpecialOffer-spinner").children(":first").css('left', $('.special-offers .special-offers-button').width() + 59 + 'px');
        $('.SpecialOffer-spinner').show();
        $.ajax({
            type: 'POST',
            url: '/home/EmailSpecialOffer',
            data: JSON.stringify(dto),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (ret) {
                $('.SpecialOffer-spinner').hide();
                var json = { obj: $('.SpecialOffer-spinner').parent().parent(), msg: 'Special Offer information sent.', left: 0, top: 0, append: $('.SpecialOffer-spinner').parent().parent(), _class: 'pop-msg2' };
                popup_msg(json, 5000);
                $('.special-offers input[type=text]').val('');
            }
        });
        e.stopImmediatePropagation();
        return false;
    });

});

function displaymessage() {
    $('#message').show();
}

function popup_msg(json, delay) {
    var ele = create_element(json.msg, json._class);
    var position = getxy($(json.obj));
    $(ele).css({ top: position.top + json.top, left: position.left + json.left });
    $(json.append).append(ele)
    var thread = new handlemsg(delay);
    thread.do(ele);
}

function create_element(msg, _class) {
    return $('<div/>', {
        text: msg,
        class: _class
    });
}

function handlemsg(delay) {
    this.do = function (obj) {
        if (delay > 0) {
            setTimeout(function () {
                $(obj).remove();
            }, delay);
        }
    }
}

function getxy(obj) {
    var position = {};
    position.left = $(obj).position().left;
    position.top = $(obj).position().top;
    position.x = $(obj).position().left;
    position.y = $(obj).position().top;
    return position;
}