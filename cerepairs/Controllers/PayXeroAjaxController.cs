﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PayXero.Models;
using System.Web.Script.Serialization;
//using CustomerPortal.Classes;
using System.Text;
using Newtonsoft.Json;
using System.Web.Services;
using System.Net.Http;
using System.Net.Http.Headers;
//using CustomerPortal.DAL;
//using CustomerPortal.BAL.Enums;
//using CustomerPortal.Common;
//using CustomerPortal.Model;
//using CustomerPortal.BAL;
//using CustomerPortal.Models;
using System.Threading;
using hmeUtil;
using cerepairs;
using PaymentModel.Models;
using cart.Models;
using qalab;

namespace CustomerPortal.Controllers
{
    public class PayXeroAjaxController : BaseController
    {
        //[CustomAuthorizeAttribute()]
        [HttpPost]
        public async System.Threading.Tasks.Task<JsonResult> PayXeroChargeAjax(PayXeroFromResponse dto)
        {
            //ServicePointManager.Expect100Continue = true;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            getPayZero_Account();
            Encryptor encryptor = new Encryptor();
            PayXeroModels model = new PayXeroModels();
            JavaScriptSerializer json = new JavaScriptSerializer();
            PayXeroFromResponse response = dto;
            ProcessPayment Payment_dto = new ProcessPayment();
            PayXeroCharge charge = model.BuildCharge(response);

            Dup dupTrans = new Dup();
            List<Dup> dupTransList = new List<Dup>();
            dupTrans.Amount = charge.amount.ToString();
            dupTrans.CreditCard = charge.card.number;
            dupTrans.date = DateTime.Now.ToString();
            string isdup = RuntimeCaching.CacheGetDupTrans(charge.amount.ToString() + charge.card.number);
            if (isdup != null)
            {
                List<Dup> dupObj = json.Deserialize<List<Dup>>(isdup);
                Dup dup = dupObj.FirstOrDefault(a => a.Amount == charge.amount.ToString() && a.CreditCard == charge.card.number);
                if (dup != null)
                {
                    int wtime = DateTime.Compare(DateTime.Parse(dup.date).AddMinutes(5), DateTime.Now);
                    if (wtime >= 0)
                    {
                        PayXeroChargeData payXeroChargeRespDup = new PayXeroChargeData();
                        payXeroChargeRespDup.actionstatus = "Failed";
                        return Json(payXeroChargeRespDup);
                    }
                }
                else
                {
                    dupTransList.Add(dupTrans);
                    RuntimeCaching.CacheDupTrans(charge.amount.ToString() + charge.card.number, json.Serialize(dupTransList));
                }
            }
            else
            {
                dupTransList.Add(dupTrans);
                RuntimeCaching.CacheDupTrans(charge.amount.ToString() + charge.card.number, json.Serialize(dupTransList));
            }

            string ccCharge = json.Serialize(charge);
            var ccChargeJson = JsonConvert.SerializeObject(charge);
            var data = new StringContent(ccChargeJson, Encoding.UTF8, "application/json");

            string Referer = HttpContext.Request.Headers["Referer"];
            string User_Agent = HttpContext.Request.Headers["User-Agent"];
            Referer = "https://cerepairs.com/cart";

            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("~") + @"PayXeroError.txt");
            System.IO.StreamWriter sw = System.IO.File.AppendText(file.ToString());
            sw.WriteLine(DateTime.Now.ToShortDateString() + "  " + ccChargeJson);
            sw.Close();
            sw.Dispose();
            var url = "https://api.paywithzero.net/v1/public/payment/charge";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.TryAddWithoutValidation("key-hash", Session["key-hash"].ToString());
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Session["payxero-authorization"].ToString());
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Referer", Referer);
            client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", User_Agent);


            var apiResponse = await client.PostAsync(url, data);
            string result = apiResponse.Content.ReadAsStringAsync().Result;
            ViewBag.result = result;
            PayXeroChargeDataDeclined payXeroChargeDeclined = null;
            PayXeroChargeData payXeroChargeResp = null;
            //if (result.ToLower().Contains("declined") || result.ToLower().Contains("decline"))
            //{
            //    payXeroChargeDeclined = (PayXeroChargeDataDeclined)json.Deserialize(result, typeof(PayXeroChargeDataDeclined));
            //    payXeroChargeDeclined.actionstatus = "Declined";
            //    return Json(payXeroChargeDeclined);
            //}
            //else
            //{

            System.IO.FileInfo xfile = new System.IO.FileInfo(Server.MapPath("~") + @"PayXeroError.txt");
            System.IO.StreamWriter xsw = System.IO.File.AppendText(file.ToString());
            xsw.WriteLine(DateTime.Now.ToShortDateString() + "  " + result);
            xsw.Close();
            xsw.Dispose();

            payXeroChargeResp = (PayXeroChargeData)json.Deserialize(result, typeof(PayXeroChargeData));
            var confirmationNumber = GenerateRandomToken(TokenLength.Bit16);
            //}
            if (payXeroChargeResp != null && payXeroChargeResp.data == null)
            {
                System.IO.FileInfo yfile = new System.IO.FileInfo(Server.MapPath("~") + @"PayXeroError.txt");
                System.IO.StreamWriter ysw = System.IO.File.AppendText(yfile.ToString());
                ysw.WriteLine(DateTime.Now.ToShortDateString() + "  " + result);
                ysw.Close();
                ysw.Dispose();

                payXeroChargeResp.actionstatus = "Failed";
                return Json(payXeroChargeResp);
            }
            else
            {
                SessionManagement();
                CartModels cartmodel = new CartModels();
                string securityToken = RuntimeCaching.CacheGetSecurityToken(dto.SECURETOKENID);

                Payment_dto = json.Deserialize<ProcessPayment>(securityToken);//error on cache timeout
                //Payment_dto.model = new PaymentModel.Models.PaymentModel();
                Payment_dto.confirmationnumber = confirmationNumber;
                Payment_dto.model.confirmationNumber = confirmationNumber;
                Payment_dto.User_Created_IP = User_IP;
                Payment_dto.User_Created_Agent = User_Agent;
                Payment_dto.currency = "USD";
                Payment_dto.user = User.Identity.Name;
                Payment_dto.Token = dto.SECURETOKEN;
                Payment_dto.TokenID = dto.SECURETOKENID;
                Payment_dto.model.PaymentToken = dto.SECURETOKEN;
                Payment_dto.model.PaymentTokenId = dto.SECURETOKENID;

                Payment_dto.model.PaymentConfirmation = confirmationNumber;
                Payment_dto.model.Invoice_Transaction_ID = dto.SECURETOKEN; ;
                Payment_dto.model.Invoice_Correlation_ID = GenerateRandomToken(TokenLength.Bit16);
                Payment_dto.model.ipAddress = User_IP;
                Payment_dto.model.surcharge = (float.Parse(payXeroChargeResp.data.surcharge) / 100).ToString();
                Payment_dto.model.grandtotal = (float.Parse(Payment_dto.model.grandtotal) + float.Parse(Payment_dto.model.surcharge)).ToString();
                InvoiceThreading invoiceThreading = new InvoiceThreading();
                Thread invoice_Thread = new Thread(invoiceThreading.UpdateSalesInfo);
                invoice_Thread.Start(Payment_dto);
                Confirmation_Number paymentResponse = new Confirmation_Number();
                paymentResponse.Status = "OK";
                paymentResponse.ConfirmationNumber = confirmationNumber;

                payXeroChargeResp.data.baseAmount = String.Format("{0:0.00}", Convert.ToDecimal(payXeroChargeResp.data.baseAmount) / 100);
                payXeroChargeResp.data.surcharge = String.Format("{0:0.00}", Convert.ToDecimal(payXeroChargeResp.data.surcharge) / 100);
                payXeroChargeResp.data.amount = String.Format("{0:0.00}", Convert.ToDecimal(payXeroChargeResp.data.amount) / 100);
                payXeroChargeResp.actionstatus = "OK";
                payXeroChargeResp.data.confirmationNumber = paymentResponse.ConfirmationNumber;
                payXeroChargeResp.token = response.SECURETOKENID;
                payXeroChargeResp.currency = "USD";
                payXeroChargeResp.paymentstatus = paymentResponse.Status;
                payXeroChargeResp.PaymentCreditCardNumber = charge.card.number;

            }

            ViewBag.Cart_Session_UID = Payment_dto.Invoice_Session_UID;// Cart_Session_UID;
            ViewBag.btnObj = Payment_dto.model.Invoice_Transaction_ID;
            ViewBag.SECURETOKENID = dto.SECURETOKENID;
            ViewBag.User_Cookie = User_Cookie;
            ViewBag.PaymentConfirmation = confirmationNumber;
            ViewBag.status = "Approved";

            string viewResponse = RenderPartialViewToString("~/Views/Payment/PaymentConfirmationAjax.cshtml");
            payXeroChargeResp.content = viewResponse;
            return Json(payXeroChargeResp);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<JsonResult> PayXeroValidateAjax(PayXeroFromResponse dto)
        {
            //ServicePointManager.Expect100Continue = true;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            PayXeroModels model = new PayXeroModels();
            JavaScriptSerializer json = new JavaScriptSerializer();
            PayXeroFromResponse response = dto;//(PayXeroFromResponse)json.Deserialize(dto, typeof(PayXeroFromResponse));
            var client = new HttpClient();
            string url = "https://api.paywithzero.net/v1/public/payment/rate?amount=" + dto.amount + "&binCard=" + dto.card.number.bin;
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();

            //client.DefaultRequestHeaders.TryAddWithoutValidation("key-hash", Session["payxero-authorization"].ToString());
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Session["key-hash"].ToString());
            var ttt = Session["payxero-authorization"].ToString();

            string Referer = HttpContext.Request.Headers["Referer"];
            string User_Agent = HttpContext.Request.Headers["User-Agent"];
            Referer = "https://cerepairs.com/cart";


            client.DefaultRequestHeaders.TryAddWithoutValidation("key-hash", Session["key-hash"].ToString());
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Session["payxero-authorization"].ToString());
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Referer", Referer);
            client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", User_Agent);
            var apiResponse = await client.GetAsync(url);
            string result = apiResponse.Content.ReadAsStringAsync().Result;
            CreditCardRateDTO CreditCardRateDTO = new CreditCardRateDTO();
            CreditCardRate creditCardRate = (CreditCardRate)json.Deserialize(result, typeof(CreditCardRate));
            if (creditCardRate != null && creditCardRate.data == null)
            {
                creditCardRate.actionstatus = "Failed";
            }
            else
            {
                creditCardRate.actionstatus = "OK";
                creditCardRate.data.formatamount = String.Format("{0:0.00}", Convert.ToDecimal(creditCardRate.data.amount) / 100);
                creditCardRate.data.formatsurcharge = String.Format("{0:0.00}", Convert.ToDecimal(creditCardRate.data.surcharge) / 100);
                creditCardRate.data.formattotalAmount = String.Format("{0:0.00}", Convert.ToDecimal(creditCardRate.data.totalAmount) / 100);
            }
            //CreditCardRateDTO.data = creditCardRate.data;
            creditCardRate.jsonData = json.Serialize(dto);

            return Json(creditCardRate);
        }


        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private string GenerateRandomToken(TokenLength bit)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, (int)bit)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
        private string RenderPartialViewToString(string viewName)
        {
            //ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext,
                    viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
    }

    internal class Viewbag
    {
    }
}