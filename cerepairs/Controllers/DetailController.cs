﻿using cerepairs.DetailsModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using cart.Models;

namespace cerepairs.Controllers
{
    public class DetailController : BaseController
    {
        // GET: Detail
        public ActionResult Index()
        {
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            return View();
        }

        public ActionResult Product(string id)
        {
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            ViewBag.Detail = true;
            DetailModels model = new DetailModels();
            string user_cookie = User_Cookie;
            string history_cookie = History_Cookie;
            //string sessionUID = User_Session_UID;
            //string cartUserId = Cart_Session_UID;

            DetailModels.ProductDetail_dto dto = model.GetProductDetail(id);
            //model.GetProductWarranty(dto);

            DetailModels.History_dto historydto = new DetailModels.History_dto();
            if (dto.productdetails.Count > 0)
            {
                historydto.product_uid = dto.productdetails[0].Product_UID;
            }
            historydto.user_cookie = User_Cookie;
            historydto.user_sessionuid = User_Session_UID;
            model.UpdateHistory(historydto);


            //CartModels cartmodel = new CartModels();
            //dto.cartitems = cartmodel.GetCart(User_Session_UID, Cart_Session_UID);
            //CartLookup cartitems = cartmodel.GetCart("9U8NGPDS6XQQBRK4N5L2991QITHIBUDE", "1YT1H5QWVF7WBL7JHK7OKQNG7190OSNF", Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
            //dto.cartitems = cartitems.cartitems;
            JavaScriptSerializer json = new JavaScriptSerializer();
            if (dto.productdetails.Count > 0)
            {
                ViewBag.Product = json.Serialize(dto.productdetails[0]);
            }
            else
            {
                ViewBag.Product = json.Serialize(dto.productdetails);

            }
            //ViewBag.jsoncartitems = json.Serialize(dto.cartitems);
            //ViewBag.cartitems = dto.cartitems;

            ViewBag.id = id;
            ViewBag.Meta_Keywords = dto.productdetails[0].metaKeywords == ""? null : dto.productdetails[0].metaKeywords;
            ViewBag.Meta_Description = dto.productdetails[0].metaDescription == "" ? null : dto.productdetails[0].metaDescription; ;
            return View("ProductDetails",dto);
        }

        // GET: Detail/Details/5
        public ActionResult Details(string pdt)
        {
            ViewBag.Detail = true;
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            DetailModels model = new DetailModels();
            DetailModels.ProductDetail_dto dto = new DetailModels.ProductDetail_dto();
            dto.shortdescription = pdt.Replace("-", " ").Replace("__", "/").Replace("_", "-");
            dto.user_cookie = User_Cookie;
            dto.history_cookie = History_Cookie;
            dto.user_sessionuid = User_Session_UID;
            model.GetProductDetailByShortDescription(dto);
            if (dto.status == "OK")
            {
                dto.product_uid = dto.productdetails[0].Product_UID;
                model.GetPromo(dto);
                return View(dto);
            }
            return View(dto);
        }
    }
}
