﻿using cerepairs.ProductsModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace cerepairs.Controllers
{
    public class SearchController : BaseController
    {


        // POST: Search/Create
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            string xx = collection["list"].ToLower();
            string list = collection["list"] == null ? null : collection["list"].ToLower();

            ViewBag.Calalog = true;
            ViewBag.Brand = string.Empty;
            ViewBag.BrandFilter = false;
            ViewBag.AccessoryType = false;
            ViewBag.Model1 = false;
            ViewBag.Model2 = false;
            ViewBag.Condition = false;
            ViewBag.Printer = false;

            ProductModels productModels = new ProductModels();
            ProductModels.ManufacturerProduct_dto dto = new ProductModels.ManufacturerProduct_dto();
            string searchText = collection["searchText"].ToString();
            ViewBag.SearchText = searchText;
            productModels.Search(dto, searchText);
            ViewBag.Isbooglebolt = isbooglebolt;
            ViewBag.DisplayType = string.IsNullOrEmpty(list) ? "detail" : list;
            string user_cookie = User_Cookie;
            @ViewBag.breadcrum_current = "Search / " + searchText;
            if (dto.searchProducts.Count > 0)
            {
                ViewBag.BrandFilter = true;
                ViewBag.Condition = true;
            }
            JavaScriptSerializer json = new JavaScriptSerializer();
            ViewBag.manufacturerProducts = json.Serialize(dto.searchProducts);
            return View("Search", dto);
        }

        [HttpGet]
        public ActionResult Search(string req, string p1, string p2, string p3)
        {
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            string list = null;
            string searchText = string.Empty;
            if (req == "Brand")
            {
                searchText = p1;
                ViewBag.SearchText = searchText;
                list = p2 != null ? p1.ToLower() : p2;
            }
            else
            {
                searchText = req;
                ViewBag.SearchText = searchText;
                list = p1 != null ? p1.ToLower() : p1;
            }

            ViewBag.Calalog = true;
            ViewBag.Brand = string.Empty;
            ViewBag.BrandFilter = false;
            ViewBag.AccessoryType = false;
            ViewBag.Model1 = false;
            ViewBag.Model2 = false;
            ViewBag.Condition = false;
            ViewBag.Printer = false;

            ProductModels productModels = new ProductModels();
            ProductModels.ManufacturerProduct_dto dto = new ProductModels.ManufacturerProduct_dto();
            productModels.Search(dto, searchText);
            ViewBag.Isbooglebolt = isbooglebolt;
            ViewBag.DisplayType = string.IsNullOrEmpty(list) ? "detail" : list;
            string user_cookie = User_Cookie;
            @ViewBag.breadcrum_current = "Search / " + searchText;
            if (dto.searchProducts.Count > 0)
            {
                ViewBag.BrandFilter = true;
                ViewBag.Condition = true;
            }
            JavaScriptSerializer json = new JavaScriptSerializer();
            ViewBag.manufacturerProducts = json.Serialize(dto.searchProducts);
            return View("Search", dto);
        }
    }
}
