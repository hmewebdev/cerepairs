﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using hmeUtil;
using System.Collections.Generic;
using RazorEngine;
using cerepairs;
using RazorEngine.Templating;
using cerepairs.Models;
using cerepairs.Classes;
using HiQPdf;

namespace ups
{
    public class FreeShippingController : BaseController
    {
        public ActionResult Index()
        {
            SessionManagement();
            return View();
        }

        [HttpPost]
        public ActionResult ReturnLabel(UpsLabelModels.upsLabelModel model)
        {
            SessionManagement();
            upsLabes ups = new upsLabes();
            labelinfo labelinfo = new labelinfo();
            labelinfo.destination = "cerepairs";
            labelinfo.address1 = "741; Avenida Amigo";
            labelinfo.address2 = "";
            labelinfo.city = "San Marcos";
            labelinfo.state = "CA";
            labelinfo.zipcode = "92069";
            labelinfo.country = "US";
            labelinfo.firstname = "Bill";
            labelinfo.lastname = "Jones";
            labelinfo.companyname = "KFC";
            labelinfo.storename = "KFC";
            labelinfo.storenumber = "1234";
            labelinfo.rmanumber = "";
            labelinfo.products = "Head Phones";
            labelinfo.weight = "10";
            labelinfo.phone = "17602345960";
            labelinfo.attention = "RMA #" + labelinfo.rmanumber;

            ShipmentResponseObj response = ups.ReturnLabel(model);
            //JavaScriptSerializer json = new JavaScriptSerializer();
            //ViewBag.shipmentResponse = json.Serialize(response);

            byte[] bytes = Convert.FromBase64String(response.ShipmentResponse.ShipmentResults.PackageResults.ShippingLabel.GraphicImage);
            using (Image image = Image.FromStream(new MemoryStream(bytes)))
            {
                //image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                Image imageSized = ups.resizeImage(image, new Size(651, 392));
                //string imagename = System.Guid.NewGuid().ToString("N");
                //string imagePath = Server.MapPath(ConfigurationManager.AppSettings["upsLabelImagePath"]) + imagename + ".gif";
                //imageSized.Save(imagePath, System.Drawing.Imaging.ImageFormat.Gif);
                using (MemoryStream m = new MemoryStream())
                {
                    imageSized.Save(m, System.Drawing.Imaging.ImageFormat.Gif);
                    byte[] imageBytes = m.ToArray();
                    ViewBag.image = String.Format("data:image/gif;base64,{0}", Convert.ToBase64String(imageBytes));
                    ViewBag.image_gif = Convert.ToBase64String(imageBytes);
                }
            }
            return View(labelinfo);
        }

        [HttpPost]
        public JsonResult ReturnLabelJson(UpsLabelModels.upsLabelModel model)
        {
            SessionManagement();
            upsLabes ups = new upsLabes();
            bool isCaptchaValid = ValidateCaptcha.CheckCaptcha(model.captcharesponse);

            model.attention = "RMA #";
            model.destination = "cerepairs";
            model.country = "US";

            ShipmentResponseObj response = ups.ReturnLabel(model);
            //JavaScriptSerializer json = new JavaScriptSerializer();
            //ViewBag.shipmentResponse = json.Serialize(response);

            byte[] bytes = Convert.FromBase64String(response.ShipmentResponse.ShipmentResults.PackageResults.ShippingLabel.GraphicImage);
            using (Image image = Image.FromStream(new MemoryStream(bytes)))
            {
                //image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                Image imageSized = ups.resizeImage(image, new Size(651, 392));
                //string imagename = System.Guid.NewGuid().ToString("N");
                //string imagePath = Server.MapPath(ConfigurationManager.AppSettings["upsLabelImagePath"]) + imagename + ".gif";
                //imageSized.Save(imagePath, System.Drawing.Imaging.ImageFormat.Gif);
                using (MemoryStream m = new MemoryStream())
                {
                    imageSized.Save(m, System.Drawing.Imaging.ImageFormat.Gif);
                    byte[] imageBytes = m.ToArray();
                    ViewBag.image = String.Format("data:image/gif;base64,{0}", Convert.ToBase64String(imageBytes));
                    ViewBag.image_gif = Convert.ToBase64String(imageBytes);
                }
            }
            FreeShippingModels freeshipping = new FreeShippingModels();
            UPS_Insert upsinsert = new UPS_Insert();
            upsinsert.ups = new List<upsupdate>();
            upsupdate _ups = new upsupdate();
            _ups.address_1 = model.address1;
            _ups.address_2 = model.address2;
            _ups.city = model.city;
            _ups.contact_name = model.firstname+" " +model.lastname;
            _ups.email = model.email;
            _ups.HME_Company = "CE";
            _ups.Packing_Label = Guid.NewGuid().ToString("N")+".gif";
            _ups.phone_number = model.phonenumber;
            _ups.products = model.products;
            _ups.product_desc = "";
            _ups.rma_number = "";
            _ups.state = model.state;
            _ups.store_name = model.chain;
            _ups.store_number = model.storenumber;
            _ups.tracking_number = response.ShipmentResponse.ShipmentResults.ShipmentIdentificationNumber;
            _ups.zip_code = model.zipcode;
            _ups.firstname = model.firstname;
            _ups.lastname = model.lastname;
            if (!string.IsNullOrEmpty(model.comments))
            {
                _ups.comments = model.comments;//.Replace("\n", Environment.NewLine);
            }
            upsinsert.ups.Add(_ups);
            freeshipping.UpdateUPS(upsinsert);

            string emailDist = "CStockton@ceinstl.com,RTurner@ceinstl.com,mkapler@ceinstl.com,abaer@hme.com,RKarsten@hme.com,bfelix@hme.com,mbusch@cerepairs.com,JBellin@ceinstl.com,afletcher@cerepairs.com";
            email email = new email();
            email.emailaddress emailaddress;
            email.attachments = new List<email.attachment>();
            email.from = "no-reply@cerepairs.com";
            email.subject = _ups.HME_Company+" UPS Electronic Return Label - User Information - " + _ups.store_name +"-" + _ups.store_number;
            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = model.email;// emailDist;
            email.to.Add(emailaddress);

            email.bcc = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = "Web_Support@hme.com";
            email.bcc.Add(emailaddress);

            string[] additionalBCC = emailDist.Split(',');
            foreach(string bcc in additionalBCC)
            {
                emailaddress = new email.emailaddress();
                emailaddress.address = bcc;
                email.bcc.Add(emailaddress);
            }

            string viewpath = Server.MapPath(@"~/Views/FreeShipping/ReturnlabelNotification.cshtml");
            email.body = RunCompile(viewpath, null, _ups);
            bool ret = email.sendemail();
            email.Dispose();

            model.trackingnumber = _ups.tracking_number;
            viewpath = Server.MapPath(@"~/Views/FreeShipping/ReturnLabeJson.cshtml");
            string shippingLabel = RunCompile(viewpath, null, model);
            model.imageb64 = ViewBag.image_gif;
            model.image = ViewBag.image;
            model.label = shippingLabel;
            //model.trackingnumber = _ups.tracking_number;
            return Json(model); ;
        }

        [HttpPost]
        public JsonResult EmailLabel(EmailLabel_dto dto)
        {
            SessionManagement();
            FreeShippingModels freeshipping = new FreeShippingModels();
            JavaScriptSerializer json = new JavaScriptSerializer();
            byte[] imageBytes = Convert.FromBase64String(dto.label);
            email email = new email();
            email.emailaddress emailaddress;
            email.attachments = new List<email.attachment>();
            email.from = "no-reply@hme.com";
            email.subject = "UPS Electronic Return Label";
            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = dto.email;
            email.attachment attachment = new email.attachment();
            attachment.type = email.AttachmentType.byteArray;
            attachment.bytearray = imageBytes;
            attachment.filename = "label.gif";
            attachment.inline = true;
            attachment.inlineid = "~label~";
            email.attachments.Add(attachment);
            email.to.Add(emailaddress);
            upsget labelinfo = freeshipping.GetUPS(dto.trackingnumber);
            string viewpath = Server.MapPath(@"~/Views/EmailShippingLabel.cshtml");
            var template = System.IO.File.ReadAllText(viewpath);
            email.body = Razor.Parse(template, dto);
            viewpath = Server.MapPath(@"~/Views/FreeShipping/ReturnLablepackingSlip.cshtml");
            template = System.IO.File.ReadAllText(viewpath);
            string pkslip = Razor.Parse(template, labelinfo);
            byte[] pdfBuffer = null;
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            htmlToPdfConverter.Document.PageSize = HiQPdf.PdfPageSize.A4;
            htmlToPdfConverter.Document.PageOrientation = HiQPdf.PdfPageOrientation.Portrait;
            pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(pkslip, ConfigurationManager.AppSettings["ceURL"]);
            email.attachment mailattachment = new email.attachment();
            mailattachment = new email.attachment();
            mailattachment.type = email.AttachmentType.byteArrayPDF;
            mailattachment.bytearray = pdfBuffer;
            mailattachment.filename = "PackingSlip" + "_"+ dto.trackingnumber + "_" + string.Format("{0:M.d.yyyy_HH.mm.ss_ffff}", DateTime.Now) + ".pdf";
            email.attachments.Add(mailattachment);
            bool ret = false;
            int retryCount = 0;
            while (!ret && retryCount < 5)
            {
                ret = email.sendemail();
                retryCount++;
            }

            email.Dispose();
            return Json(dto);
        }

        public static string RunCompile(string viewpath, string templatekey, object model)
        {
            string result = string.Empty;
            var template = System.IO.File.ReadAllText(viewpath);
            if (string.IsNullOrEmpty(templatekey))
            {
                templatekey = Guid.NewGuid().ToString();
            }
            result = Engine.Razor.RunCompile(template, templatekey, null, model);
            //result = Engine.Razor.RunCompile(template, null, model);
            return result;
        }
    }
    public class EmailLabel_dto
    {
        public string label { get; set; }
        public string image { get; set; }
        public string imageb64 { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string storename { get; set; }
        public string storenumber { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string chain { get; set; }
        public string HME_EOS_Battery_Charge { get; set; }
        public string All_in_one_headset { get; set; }
        public string Communicator { get; set; }
        public string Timer_SYS30 { get; set; }
        public string Timer_DASH { get; set; }
        public string Timer_Zoom { get; set; }
        public string Zoom_TSP { get; set; }
        public string Battery_Charger { get; set; }
        public string Other { get; set; }
        public string Weight { get; set; }
        public string email { get; set; }
        //public string comments { get; set; }
        public string trackingnumber { get; set; }
    }

}
