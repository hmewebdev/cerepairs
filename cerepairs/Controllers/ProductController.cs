﻿using System.Drawing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using cerepairs.ProductsModels;

namespace cerepairs
{
    public class ProductController : BaseController
    {
        // GET: Product
        public ActionResult Index()
        {
            SessionManagement();
            return View();
        }

        public ActionResult ManufacturerProducts(string pmu, string p1, string p2, string p3)
        {

            string list = null;
            ViewBag.Root = "Manufacturers";
            ViewBag.Manufacturer = pmu;
            ViewBag.Calalog = true;
            ViewBag.Brand = string.Empty;
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.BrandFilter = false;
            ViewBag.AccessoryType = true;
            ViewBag.Model1 = false;
            ViewBag.Model2 = false;
            ViewBag.Condition = true;
            ViewBag.Printer = false;
            ViewBag.Isbooglebolt = isbooglebolt;
            ViewBag.DisplayType = string.IsNullOrEmpty(list) ? "detail" : list;
            string user_cookie = User_Cookie;

            ProductModels productModels = new ProductModels();
            ProductModels.ManufacturerProduct_dto dto = new ProductModels.ManufacturerProduct_dto();
            dto.manufacturername = pmu;
            productModels.GetManufacturerProducts(dto);
            if (dto.manufacturerProducts == null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.ManufacturerDescription = dto.manufacturerdescription;
            JavaScriptSerializer json = new JavaScriptSerializer();
            ViewBag.manufacturerProducts = json.Serialize(dto.manufacturerProducts);

            //return View("ManufacturerProducts", dto);
            return View("~/Views/Catalog/Catalog.cshtml", dto);
        }

        public ActionResult Products(string ptu, string p1, string p2, string p3)
        {
            string Accessory_Type = null;
            string list = null;
            if (p1 != null)
            {
                if(p1.ToLower() == "list")
                {
                    list = p1.ToLower();
                }
                else
                {
                    list = p2 == null ? null : p2.ToLower();
                    Accessory_Type = p1;
                }
            }
            ViewBag.Root = "ManufacturerProducts";
            ViewBag.Link_Base = "Product";
            ViewBag.Manufacturer = ptu;
            ViewBag.Calalog = true;
            ViewBag.Brand = string.Empty;
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.BrandFilter = false;
            if(ptu == "CE-Products")
            {
                ViewBag.AccessoryType = true;
            }
            else
            {
                ViewBag.AccessoryType = false;
            }
            ViewBag.Model1 = false;
            ViewBag.Model2 = false;
            ViewBag.Condition = true;
            ViewBag.Printer = false;
            ViewBag.Isbooglebolt = isbooglebolt;
            ViewBag.DisplayType = string.IsNullOrEmpty(list) ? "detail" : list;
            string user_cookie = User_Cookie;
            ProductModels productModels = new ProductModels();
            ProductModels.ManufacturerProduct_dto dto  = new ProductModels.ManufacturerProduct_dto();
            dto.producttype = ptu;
            dto.ProductClass = Accessory_Type;
            productModels.ProductsByProductType(dto,User_Cookie);
            if (dto.manufacturerProducts == null)
            {
                return RedirectToAction("Index", "Home");
            }

            //return View(dto);
            JavaScriptSerializer json = new JavaScriptSerializer();
            ViewBag.manufacturerProducts = json.Serialize(dto.manufacturerProducts);
            return View("~/Views/Catalog/Catalog.cshtml", dto);
        }

        public ActionResult Product(string ptu, string ptl)
        {
            SessionManagement();
            ProductModels productModels = new ProductModels();
            ProductModels.ManufacturerProduct_dto dto = new ProductModels.ManufacturerProduct_dto();
            dto.producttype = ptu;
            dto.ProductClass = ptl;
            productModels.ProductsByProductType(dto, User_Cookie);
            if (dto.manufacturerProducts == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(dto);
        }

        public ActionResult ProductSystems(string pmu)
        {
            SessionManagement();
            //ProductModels productModels = new ProductModels();
            //ProductModels.Manufacturer manufacturer = productModels.GetManufacturerName(pmu);
            return View();
        }

        public static Image resizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }
    }
}
