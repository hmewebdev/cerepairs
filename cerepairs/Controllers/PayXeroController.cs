﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PayXero.Models;
using System.Web.Script.Serialization;
//using CustomerPortal.Classes;
using System.Text;
using Newtonsoft.Json;
using System.Web.Services;
using System.Net.Http;
using System.Net.Http.Headers;

using hmeUtil;
using cerepairs;

namespace CustomerPortal.Controllers
{
    public class PayXeroController : BaseController
    {
        public string[] userInfo;
        //private Payment _paymentDAL;
        // GET: PayXero
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Transaction(string recipientEmails = "")
        {

            return View();

        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> PayXeroResponse(string jsonDataInput)
        {
            Encryptor encryptor = new Encryptor();

            getPayZero_Account();

            PayXeroModels model = new PayXeroModels();
            JavaScriptSerializer json = new JavaScriptSerializer();
            PayXeroFromResponse response = (PayXeroFromResponse)json.Deserialize(jsonDataInput, typeof(PayXeroFromResponse));
 
            PayXeroCharge charge = model.BuildCharge(response);
            string ccCharge = json.Serialize(charge);
            var ccChargeJson = JsonConvert.SerializeObject(charge);
            var data = new StringContent(ccChargeJson, Encoding.UTF8, "application/json");

            var url = "https://api.paywithzero.net/v1/public/payment/charge";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.TryAddWithoutValidation("key-hash", Session["key-hash"].ToString());
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Session["payxero-authorization"].ToString());
            //client.DefaultRequestHeaders.TryAddWithoutValidation("key-hash", "2WKXFAWYL3GB2AMJGY78");
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "aHNjb3R0QGhtZS5jb206SnVseTIwMjIh");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");

            var apiResponse = await client.PostAsync(url, data);
            string result = apiResponse.Content.ReadAsStringAsync().Result;
            ViewBag.result = result;
            PayXeroChargeDataDeclined payXeroChargeDeclined = null;
            PayXeroChargeData payXeroChargeResp = null;
            if (result.Contains("Declined"))
            {
                payXeroChargeDeclined = (PayXeroChargeDataDeclined)json.Deserialize(result, typeof(PayXeroChargeDataDeclined));
            }
            else
            {
                payXeroChargeResp = (PayXeroChargeData)json.Deserialize(result, typeof(PayXeroChargeData));
            }
            if (payXeroChargeResp != null && payXeroChargeResp.data == null)
            {
                //string status = payXeroChargeResp.status.ToString();
                //string msg = payXeroChargeResp.message;
            }
            else
            {
                url = "https://api.paywithzero.net/v1/public/payment/" + payXeroChargeResp.data.paymentId;
                client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("key-hash", Session["key-hash"].ToString());
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Session["payxero-authorization"].ToString());
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");

                apiResponse = await client.GetAsync(url);
                result = apiResponse.Content.ReadAsStringAsync().Result;
                PayXeroPaymentResp payXeroPaymentResp = (PayXeroPaymentResp)json.Deserialize(result, typeof(PayXeroPaymentResp));
            }
            //ViewBag.result = result;

            return View();
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private string GenerateRandomToken(TokenLength bit)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, (int)bit)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
    }
}





//ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
//HttpWebRequest payReq = (HttpWebRequest)WebRequest.Create("https://api.paywithzero.net/v1/public/payment/charge");
////payReq.Headers.Add("Content-Type", "application/json");
//payReq.Headers.Add("key-hash", "2WKXFAWYL3GB2AMJGY78");
//payReq.Headers.Add("Authorization", "Basic aHNjb3R0QGhtZS5jb206SnVseTIwMjIh");
//payReq.Method = "POST";
//payReq.ContentType = "application/json";

//byte[] bytes = UTF8Encoding.UTF8.GetBytes(ccCharge.ToString());
//payReq.ContentLength = bytes.Length;

//using (var stream = payReq.GetRequestStream())
//{
//    stream.Write(bytes, 0, bytes.Length);
//}

//payReq.ContentLength = ccCharge.Length;
//StreamWriter sw = new StreamWriter(payReq.GetRequestStream());
//sw.Write(ccCharge);
//sw.Close();
////GET PAYFLOW RESPONSE
//HttpWebResponse payResp = (HttpWebResponse)payReq.GetResponse();
//StreamReader sr = new StreamReader(payResp.GetResponseStream());
//string resp = sr.ReadToEnd();
//sr.Close();

//var obj = new
//{            
//        name = "John",
//        age = 21
//};
