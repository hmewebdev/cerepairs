﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cerepairs.Controllers
{
    public class tech_supportController : Controller
    {
        // GET: tech_support
        public ActionResult Index()
        {
            return View();
        }

        // GET: tech_support/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: tech_support/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tech_support/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: tech_support/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: tech_support/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: tech_support/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: tech_support/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
