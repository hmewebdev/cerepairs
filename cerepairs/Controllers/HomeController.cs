﻿using System;
using System.Collections.Generic;
using HME.SQLSERVER.DAL;
using hmeUtil;
using System.Web.Mvc;
using System.Data;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using cerepairs.ProductsModels;
using cerepairs.HomeModel;
using cerepairs.DetailsModels;
using System.Web.Script.Serialization;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Net.Http;
//using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using shipper;
using System.Threading.Tasks;
using axapi;
using System.IO;
using hme;
using static hme.SecurityController;
using RazorEngine;
using cart.Models;

namespace cerepairs.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController()/* : base(Session)*/
        {
        }

        public ActionResult Index()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            JavaScriptSerializer json = new JavaScriptSerializer();
            SessionManagement();
            CartModels cartmodel = new CartModels();
            CartLookup cartitems = cartmodel.GetCart(Cart_Session_UID, User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
            Session["CartCount"] = null;
            Session["CartActive"] = null;
            Session["PromoDiscount"] = null;
            if (cartitems.cartitems != null && !string.IsNullOrEmpty(cartitems.totalcartitems))
            {
                Session["CartCount"] = cartitems.totalcartitems;
                Session["CartActive"] = "active";
            }

            if (Session["Cart_Promo"] != null)
            {
                ViewBag.promo = Session["Cart_Promo"].ToString();
                ViewBag.promomessage = Session["Cart_Promo_Message"].ToString();
                if (cartitems.promo != null)
                {
                    Session["PromoDiscount"] = cartitems.promo;
                    ViewBag.discount = cartitems.promo;
                }
            }
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            string vbvb = Session.SessionID.ToString();
            string dd = User_Session_UID;
            string cc = User_Cookie;
            string ff = UpdateType;
            ViewBag.Title = "Drive-Thru Equiptment Repairs, Sales and Accessory";
            var queryCollection = new FormCollection(Request.QueryString);

            //AXapi axapi = new AXapi();
            //List<AXServices.CustomerModel> customer = await axapi.Get_CustomerInfo("jtch", "187563");

            //List<StoreItemsModel> storeitems = await axapi.GetStoreItems("jtch");
            //List<CustomerModel> customerinfo = await axapi.GetCustomerInfo("jtch", "187563");

            if (queryCollection["pg"] != null)
            {
                if(queryCollection["pg"].ToLower() == "customersurvey")
                {
                    return RedirectPermanent("/survey");

                }
                string qry = queryCollection["pg"].ToLower();
                switch (queryCollection["pg"].ToLower())
                {
                    case "product":
                        ProductModels productModels = new ProductModels();
                        //ProductModels.Product_Type producttype;
                        ProductModels.Product_dto dto;

                        if (queryCollection["ptu"] != null)
                        {
                            if (queryCollection["ptl"] != null)
                            {
                                dto = new ProductModels.Product_dto();
                                dto.ProductType_UID = queryCollection["ptu"];
                                dto.ProductClass_ID = queryCollection["ptl"];
                                productModels.ProductType(dto, User_Cookie);
                                if (dto.product_type != null)
                                {
                                    return RedirectPermanent("/Product/" + dto.product_type.urlProductType + "/" + (string.IsNullOrEmpty(dto.ProductClass) ? "" : dto.ProductClass.Replace(" ","-").Replace("/","-")));
                                }
                            }

                            dto = new ProductModels.Product_dto();
                            dto.ProductType_UID = queryCollection["ptu"];
                            productModels.ProductType(dto, User_Cookie);
                            if (dto.product_type != null)
                            {
                                return RedirectPermanent("/Product/" + dto.product_type.urlProductType);
                            }
                        }
                        if (queryCollection["pmu"] != null)
                        {
                            ProductModels.Manufacturer manufacturer = productModels.GetManufacturerName(queryCollection["pmu"]);
                            if (manufacturer != null)
                            {
                                return RedirectPermanent("/Product/" + manufacturer.Manufacturer_Name + "/Products");
                            }

                        }
                        return View("Index");
                    case "detail":
                        if (queryCollection["pdt"] != null)
                        {
                            // return RedirectPermanent("/Detail/" + queryCollection["pdt"]);
                            DetailModels model = new DetailModels();
                            DetailModels.History_dto historydto = new DetailModels.History_dto();
                            historydto.product_uid = queryCollection["pdt"];
                            historydto.user_cookie = User_Cookie;
                            historydto.user_sessionuid = User_Session_UID;
                            model.UpdateHistory(historydto);

                            DetailModels.ProductDetail_dto productdetail = model.GetProductDetail(queryCollection["pdt"]);
                            return RedirectPermanent("/Detail/Product/" + productdetail.productdetails[0].Product_Name);//.Replace("-", "_").Replace(" ", "-").Replace(@"/", "__"));
                        }
                        return View();
                    case "sendrepair":
                        //return RedirectPermanent("/send-in-a-repair-free-shipping");
                        return View("sendinarepair") ;
                    case "cart":
                        return RedirectPermanent("/cart");
                    case "privacy":
                        return RedirectPermanent("/CE_Online_Privacy_Policy.pdf");
                    default:
                        return View();
                }
            }

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; //TLS 1.2
            Random rnd = new Random();
            int code = rnd.Next(100000, 999999);  // creates a number between 1 and 12
            //TwilioClient.Init("AC2c8afba952e4f09884b5c0f323c9db63", "14f3964cf3c88101ae1f7a99becf17b8");
            //var message = MessageResource.Create(
            //    body: "Validation Code: "+ code.ToString(),
            //    from: new Twilio.Types.PhoneNumber("14422455507"),
            //    to: new Twilio.Types.PhoneNumber("7606506891")
            //);

            ViewBag.Title = "Drive-Thru Repair, Drive-Thru Headset Repair, Drive-Thru Repairs | CE";
            ViewBag.Meta_Keywords = "HME, Panasonic, Drive-thru, communications, electronics";
            ViewBag.Meta_Description = "The leader in drive-thru repair for over 40 years. Trained technicians specialize in drive-thru headset repair and timer repair for HME and Panasonic.";
            ViewBag.fromIndex = "1";
            return View();
        }

        public ActionResult testpage()
        {
            return View();
        }

        public ActionResult Page()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            string url = Request.RawUrl;
            string page = Request.CurrentExecutionFilePath.Replace("/", "").Replace(@"\", "");

            HomeModels homeModels = new HomeModels();
            Page pageinfo = homeModels.GetPage(page);
            if (pageinfo == null)
            {
                return View("Index");
            }
            ViewBag.page_style = "single";
            if (pageinfo.Page_BackgroundImage != "" && System.IO.File.Exists(Server.MapPath(@"~/images/pages/" + pageinfo.Page_BackgroundImage)))
            {
                ViewBag.page_style = "double";
            }
            @ViewBag.Title = pageinfo.Page_MetaTitle;
            string page_template = pageinfo.Page_Template == "" ? "Page" : pageinfo.Page_Template.Replace("_", "").Replace(".cfm", "");
            return View(page_template, pageinfo);
        }

        public async Task<JsonResult> TrackingHistory(Tracking_History_dto dto)
        {
            Shippo_Tracking_Response tracking = await ShipperTracking.shippo(dto.carrier, dto.tracking_number, "1234567");
            dto.tracking = tracking;
            return Json(tracking);
        }

        //public ActionResult Product(string pg, string ptu)
        //{
        //    return View();
        //}

        // GET: Home/Details/5
        public ActionResult Details(int id)
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            return View();
        }

        public ActionResult About()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "About CE";
            ViewBag.Meta_Keywords = "CE Repairs, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair, CE Drive-thru Repairs.";
            ViewBag.Meta_Description = "The leader in drive thru repairs for over 50 years. Quick repairs to get your drive thru up and running. Send in a Repair Today!";
            return View();
        }
        public ActionResult OurRepairs()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "About CE Drive-Thru Repairs";
            ViewBag.Meta_Keywords = "CE Repairs, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair, CE Drive-thru Repairs";
            ViewBag.Meta_Description = "CE offers a 100% guarantee on our services. With factory-authorized drive-thru repairs, free inbound shipping and more, we are sure you won't be disappointed.";
            return View();
        }
        public ActionResult TechSupport()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | Technical Support";
            ViewBag.Meta_Keywords = "CE Repairs Tech Support, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair";
            ViewBag.Meta_Description = "Fix your equipment with CE's knowledgeable drive-thru repair tech support team. Or send in your drive-thru equipment on us with a free shipping label.";

            return View();
        }
        public ActionResult RepairWarranty()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | Warranty and Maintenance Program";
            ViewBag.Meta_Keywords = "Drive Thru Warranty, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair";
            ViewBag.Meta_Description = "CE always provides a 4-month drive-thru repair warranty as well as EMAs and advance exchange programs so your drive-thru equipment is always up and running.";

            return View();
        }
        public ActionResult SameDay()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | Same-Day Service";
            ViewBag.Meta_Keywords = "CE Repairs, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair, CE Drive-thru Repairs";
            ViewBag.Meta_Description = "CE is committed to providing the best drive-thru repairs in the industry. Send in your drive-thru repair, and we'll get you back up and running in a day."; return View();
        }
        public ActionResult Protection_Plan()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            ViewBag.Title = "CE Repairs | Drive-Thru Protection Plan";
            ViewBag.Meta_Keywords = "CE Repairs, Protection Plan, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair ";
            ViewBag.Meta_Description = "CE Protection Plans keep your drive-thru headsets and timers in working order, providing you peace of mind and helping you avoid surprise expenses.";
            return View();
        }
        public ActionResult Advance_Exchange()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | Advance Exchange Program";
            ViewBag.Meta_Keywords = "CE Repairs, Advance Exchange, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair";
            ViewBag.Meta_Description = "Avoid drive-thru downtime. With CE's advance exchange program, we'll send replacement equipment overnight. Send us your broken equipment at your convenience.";
            return View();
        }
        public ActionResult Nationwide_Installation()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | Nationwide Installation";
            ViewBag.Meta_Keywords = "CE Repairs, Drive Thru Installation, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair";
            ViewBag.Meta_Description = "Drive-thru equipment installation anywhere you are. Skilled drive-thru installation technicians nationwide are close by to set up your drive-thru systems."; return View();
        }
        public ActionResult Four_month_warranty()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | 4-Month Repair Warranty";
            ViewBag.Meta_Keywords = "CE Repairs, Drive Thru Warranty, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair";
            ViewBag.Meta_Description = "Every CE drive-thru repair is covered with a 4-month warranty, with an additional two months offered on select HME headsets.";
            return View();
        }
        //[Route("drive-thru-faqs")]
        //[Route("Drive-Thru-Repairs-Frequency-Asked-Questions")]
        public ActionResult FAQ()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | FAQs";
            ViewBag.Meta_Keywords = "Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair";
            ViewBag.Meta_Description = "Drive-Thru Repair FAQs. We've provided a list of frequently asked questions about drive-thru repairs and products for your convenience.";
            return View();
        }
        public ActionResult Multistorediscount()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Drive-Thru Repair Multiple Store Discount";
            ViewBag.Meta_Keywords = "";
            ViewBag.Meta_Description = "Get special discounts on your drive-thru repairs with the CE multiple store discount. Receive quality drive-thru repairs at low prices from CE today.";
            return View();
        }
        public ActionResult bonusbucks()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Drive-Thru Repairs Bonus Bucks";
            ViewBag.Meta_Keywords = "drive-thru repair, drive-thru repairs";
            ViewBag.Meta_Description = "Get a reward for sending CE your drive-thru repairs. Spend $100 on your HME, 3M, or Panasonic drive-thru repair at CE and receive $5 in bonus bucks.  ";
            return View();
        }
        public ActionResult PriceMatching()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "Drive-Thru Repair | Drive-Thru Repair Price Match Savings Coupon";
            ViewBag.Meta_Keywords = "drive-thru repair coupon, drive-thru repair";
            ViewBag.Meta_Description = "We'll beat our competitors' drive-thru repair coupon. Find a lower price somewhere else? Send CE the coupon and we'll match the price on your drive-thru repair.";
            return View();
        }
        public ActionResult Contact()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "Drive-Thru Repairs, Drive-Thru Headset Repairs, Drive-Through Repairs";
            ViewBag.Meta_Keywords = "drive-thru repair";
            ViewBag.Meta_Description = "CE repairs all brands of headsets with fast, reliable service. For quality drive-thru repair service at the lowest prices contact CE today. ";
            var queryCollection = new FormCollection(Request.QueryString);
            if (queryCollection["pg"] != null)
            {
                string newRedirect = HandleQueryString();
                if (newRedirect != null)
                {
                    return RedirectPermanent(newRedirect);
                }
            }
            return View();
        }
        public ActionResult Special_Offers()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | Special Offers";
            ViewBag.Meta_Keywords = "Drive Thru Special Offers, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair ";
            ViewBag.Meta_Description = "CE offers special discounts and offers on drive-thru repairs for new and existing customers. Choose CE for the best deals on drive-thru headset repairs."; return View();
        }
        public ActionResult SendInRepair()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "Drive-Thru Repair | Drive-Thru Headset Repair | Drive-Thru Repairs";
            ViewBag.Meta_Keywords = "drive-thru repair, drive-thru headset repair, drive-thru repairs";
            ViewBag.Meta_Description = "CE provides free inbound shipping for your drive-thru repair orders. Send us your drive-thru repairs, and we'll send it back within 24 hours.";
            return View();
        }
        public ActionResult SendInARepair()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | Send in a Drive-Thru Repair";
            ViewBag.Meta_Keywords = "CE Repairs, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair";
            ViewBag.Meta_Description = "Send in a Drive-Thru Repair. Drive-Thru Repairs - Easy Online Form. Fill out a quick form and we'll provide free inbound shipping labels.";
            return View();
        }
        public ActionResult FactoryAuthorizedRepairs()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "HME Factory Authorized Repairs from CE";
            ViewBag.Meta_Keywords = "CE Repairs, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair, CE Drive-thru Repairs.";
            ViewBag.Meta_Description = "CE is the only independent HME factory-approved and trained facility in the U.S. 100% satisfaction guaranteed on drive-thru repairs. ";
            return View();
        }
        public ActionResult HelpCenter()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "CE Repairs | Help Center";
            ViewBag.Meta_Keywords = "CE Repairs, Drive Thru Repair, Drive Thru Headset Repair, Drive Thru Equipment, Drive Thru Parts, Drive Thru Accessories, HME Headset Repair, System Repair";
            ViewBag.Meta_Description = "Everything you need to know about your drive-thru, we've got you covered with the CE Drive-Thru Repair Help Center.";
            return View();
        }
        public ActionResult TechnicalSupport()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "Drive-Thru Equipment Technical Support | Drive-Thru Intercom Systems";
            ViewBag.Meta_Keywords = "drive-thru repair";
            ViewBag.Meta_Description = "Fix your drive-thru repair needs with CE's knowledgeable drive-thru repair tech support team. Or send in your drive-thru repair with a free shipping label.";

            return View();
        }
        public ActionResult freebattery()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "Drive-Thru Battery, Drive-Thru Batteries, Drive-Thru Battery Pack | CE";
            ViewBag.Meta_Keywords = "drive-thru battery, drive-thru discounts, drive-thru repairs";
            ViewBag.Meta_Description = "Get 1 free CE branded drive-thru battery with the purchase of 3 CE branded batteries. Get other drive-thru discounts on drive-thru repairs and equipment at CE.";
            return View();
        }
        public ActionResult free_headset_repair()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "Drive-Thru Equipment Technical Support | Drive-Thru Intercom Systems";
            ViewBag.Meta_Keywords = "drive-thru repair";
            ViewBag.Meta_Description = "Fix your drive-thru repair needs with CE's knowledgeable drive-thru repair tech support team. Or send in your drive-thru repair with a free shipping label.";

            return View();

        }

        public ActionResult survey()
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            ViewBag.Title = "We want your feedback";
            ViewBag.survey = true;
            return View();
        }
        public ActionResult Privacy()
        {
            return RedirectPermanent("/CE_Online_Privacy_Policy.pdf");
        }
        private string HandleQueryString()
        {
            var queryCollection = new FormCollection(Request.QueryString);
            string qry = queryCollection["pg"].ToLower();
            switch (queryCollection["pg"].ToLower())
            {
                case "product":
                    ProductModels productModels = new ProductModels();
                    //ProductModels.Product_Type producttype;
                    ProductModels.Product_dto dto;

                    if (queryCollection["ptu"] != null)
                    {
                        if (queryCollection["ptl"] != null)
                        {
                            dto = new ProductModels.Product_dto();
                            dto.ProductType_UID = queryCollection["ptu"];
                            dto.ProductClass_ID = queryCollection["ptl"];
                            productModels.ProductType(dto, User_Cookie);
                            if (dto.product_type != null)
                            {
                                return "/Product/" + dto.product_type.urlProductType + "/" + dto.ProductClass;
                            }
                        }

                        dto = new ProductModels.Product_dto();
                        dto.ProductType_UID = queryCollection["ptu"];
                        productModels.ProductType(dto, User_Cookie);
                        if (dto.product_type != null)
                        {
                            return "/Product/" + dto.product_type.urlProductType;
                        }
                    }
                    if (queryCollection["pmu"] != null)
                    {
                        string pmu = queryCollection["pmu"];
                        ProductModels.Manufacturer manufacturer = productModels.GetManufacturerName(queryCollection["pmu"]);
                        if (manufacturer != null)
                        {
                            return "/Product/" + manufacturer.Manufacturer_Name + "/Products";
                        }

                    }
                    break;
                case "detail":
                    if (queryCollection["pdt"] != null)
                    {
                        // return RedirectPermanent("/Detail/" + queryCollection["pdt"]);
                        DetailModels model = new DetailModels();
                        DetailModels.History_dto historydto = new DetailModels.History_dto();
                        historydto.product_uid = queryCollection["pdt"];
                        historydto.user_cookie = User_Cookie;
                        historydto.user_sessionuid = User_Session_UID;
                        model.UpdateHistory(historydto);

                        DetailModels.ProductDetail_dto productdetail = model.GetProductDetail(queryCollection["pdt"]);
                        return "/Detail/Product/" + productdetail.productdetails[0].Product_Name;
                    }
                    return null;
                case "sendrepair":
                    return "/send-in-a-repair-free-shipping";
                case "cart":
                    return "/cart";
                default:
                    return null;
            }
            return null;
        }
        public void usermanuals()
        {
            Response.Redirect("https://www.hme.com/qsr/drive-thru-user-manuals/", true);
        }
        public void cebatteries()
        {
            Response.Redirect("/Product/CE-Products/Batteries", true);
        }

        [HttpPost]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public void KeepAlive()
        {
            SessionManagement();
        }

        [HttpPost]
       [System.Web.Services.WebMethod(EnableSession = true)]
        public string ceHeader()
        {
            ViewBag.blog = "1";
            string CartActive = Session["CartActive"] == null ? "" : Session["CartActive"].ToString();
            string viewResponse = RenderPartialViewToString("~/Views/shared/_header.cshtml");
            return viewResponse;
        }

        [HttpPost]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public JsonResult Survey(Servey dto)
        {
            HomeModels model = new HomeModels();
            string results = model.Survey(dto);
            dto.results = "Yes";
            return Json(dto);
        }

        [HttpPost]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public JsonResult ContinueShopping()
        {
            if (Session["ContinueShopping"] != null)
            {
                return Json(Session["ContinueShopping"].ToString());
            }
            else
            {
                return Json("null");
            }
        }

        [HttpPost]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public JsonResult GetUserInfo()
        {
            SessionManagement();
            HomeModels.UserInfo userinfo = new HomeModels.UserInfo();
            userinfo.isAdmin = ViewBag.isAdmin;
            userinfo.isFullAdmin = ViewBag.isFullAdmin;
            userinfo.isUser = ViewBag.isUser;
            return Json(userinfo);
        }

        [HttpPost]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public JsonResult ContactInfo(ContactInfoDTO dto)
        {
            SessionManagement();
            HomeModels homeModels = new HomeModels();
            string path = Server.MapPath(@"~/Views/Shared/ContactEmail.cshtml");
            string imagepath = Server.MapPath(@"~/images/");
            string stat = homeModels.SaveContactInfo(dto, path, imagepath);
            return Json("OK");
        }

        [HttpPost]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public JsonResult EmailSpecialOffer(SignUpDTO dto)
        {
            SessionManagement();
            HomeModels homeModels = new HomeModels();
            string path = Server.MapPath(@"~/Views/Shared/EmailSpecialOffer.cshtml");
            string imagepath = Server.MapPath(@"~/images/");
            string stat = homeModels.EmailSpecialOffer(dto, path, imagepath);
            return Json("OK");
        }
        private string RenderPartialViewToString(string viewName)
        {
            //ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext,
                    viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
    }

    public static class Extensions
    {
        public static void SetProperty(this object obj, string propertyName, object value)
        {
            var propertyInfo = obj.GetType().GetProperty(propertyName);
            if (propertyInfo == null) return;
            propertyInfo.SetValue(obj, value);
        }
    }
    enum storelistColumns
    {
        NATL_STR_NU = 0,
        ADDRESS = 1,
        CITY = 2,
        STATE = 3,
        ZIP = 4,
        [Display(Name = "CITY STATE ZIP")]
        CITY_STATE_ZIP = 5,
        SITE_PHNE = 6,
    }
    enum enumstorelist
    {
        natlstoreno = 0,
        address = 1,
        city = 2,
        state = 3,
        zip = 4,
        citystatezip = 5,
        sitephone = 6,
    }

    public class storelist
    {
        public string natlstoreno { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string citystatezip { get; set; }
        public string sitephone { get; set; }
    }
}



//TokenHash tokenHash = new TokenHash();
//string token = tokenHash.GetMD5Hash("tttttttttttttttttt");
//bool validate = tokenHash.ValidateMD5Hash("tttttttttttttttttt", token);
//string token2 = tokenHash.GetSHA1Hash("tttttttttttttttttt");
//bool validate2 = tokenHash.ValidateSHA1Hash("tttttttttttttttttt", token2);
//Encryptor encryptor = new Encryptor();
//string salt = "fwesdfsfs904sdkw-ldfgpsd-wgf";
//string encrypt = encryptor.Encrypt("tttttttttttttttttt", salt);
//string decrypt = encryptor.Decrypt(encrypt, salt);
//xlsxReader xlsxReader = new xlsxReader();
//DataSet ds = xlsxReader.readxlsx(@"c:\dev\McD WWC Order Form and Date 2016.xlsx");
//foreach (DataTable table in ds.Tables)
//{
//    if (table.TableName.ToLower() == "store list")
//    {
//        List<storelist> storelist = new List<storelist>();
//        for (int i = 1; i < table.Rows.Count; i++)
//        {
//            storelist store = new storelist();
//            for (int col = 0; col < Enum.GetNames(typeof(storelistColumns)).Length; col++)
//            {
//                store.SetProperty(((enumstorelist)col).ToString(), table.Rows[i].ItemArray[(int)((storelistColumns)col)].ToString());
//            }
//            storelist.Add(store);
//        }
//        break;
//    }
//}
//HttpContext.Current.Session.SessionID