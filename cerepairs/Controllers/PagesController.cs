﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cerepairs.Controllers
{
    public class PagesController : Controller
    {
        //[Route("same-day-service")]
        public ActionResult Index()
        {
            string url = Request.RawUrl;
            string page = Request.CurrentExecutionFilePath.Replace("/","").Replace(@"\","");

            return View();
        }
    }
}
