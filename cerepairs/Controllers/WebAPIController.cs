﻿using hme.Models;
using RazorEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace cerepairs.Controllers
{
    public class WebAPIController : ApiController
    {
        // GET: api/WebAPI
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/WebAPI/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]
        [Route("api/WebAPI/post")]

        public HttpResponseMessage Post([FromBody]ResetPasswordViewModel dto)
        {
            dynamic model = new { Name = "Howard" };
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            string viewpath = HttpContext.Current.Server.MapPath(@"~/Views/Home/theview.cshtml");
            var template = File.ReadAllText(viewpath);
            string parsedView = Razor.Parse(template,model);
            response.Content = new StringContent(parsedView);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
            return response;
        }

        // PUT: api/WebAPI/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/WebAPI/5
        public void Delete(int id)
        {
        }
    }
}
