﻿using System.Drawing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using cerepairs.ProductsModels;

namespace cerepairs.Controllers
{
    //[RouteArea("AreaName", RoutePrefix("Catalog")]
    [RouteArea("AreaName", AreaPrefix = "Catalog")]
    public class CatalogController : BaseController
    {
        // GET: Catalog
        public ActionResult Index()
        {
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            return View();
        }

        //[Route("Headsets/{displayType: string}")]
        //public ActionResult xHeadSets(string displayType)
        //{
        //    SessionManagement();
        //    bool isbooglebolt = IsGoogleBolt();
        //    ProductModels productModels = new ProductModels();
        //    ProductModels.ManufacturerProduct_dto dto = new ProductModels.ManufacturerProduct_dto();
        //    dto.category = "headsets";
        //    productModels.SelectCatalog(dto);
        //    if (dto.manufacturerProducts == null)
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }
        //    JavaScriptSerializer json = new JavaScriptSerializer();
        //    ViewBag.manufacturerProducts = json.Serialize(dto.manufacturerProducts);
        //    ViewBag.Title = "Drive-Thru Headset Repairs";
        //    ViewBag.CatReq = "HeadSets";
        //    ViewBag.Isbooglebolt = isbooglebolt;
        //    ViewBag.DisplayType = "detail";// string.IsNullOrEmpty(displayType) ? "detail" : displayType;
        //    string user_cookie = User_Cookie;
        //    return View("Catalog", dto);
        //}

        //[Route("HeadSets/{id:string}")] // Matches GET app/accounts/list/45646
        //public ActionResult HeadSets(string id, string DisplayType)
        //{
        //    SessionManagement();
        //    bool isbooglebolt = IsGoogleBolt();
        //    ProductModels productModels = new ProductModels();
        //    ProductModels.ManufacturerProduct_dto dto = new ProductModels.ManufacturerProduct_dto();
        //    dto.category = "headsets";
        //    dto.manufacturername = id == null ? null : id;
        //    productModels.SelectCatalog(dto);
        //    if (dto.manufacturerProducts == null)
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }
        //    JavaScriptSerializer json = new JavaScriptSerializer();
        //    ViewBag.manufacturerProducts = json.Serialize(dto.manufacturerProducts);
        //    ViewBag.Title = "Drive-Thru Headset Repairs";
        //    ViewBag.CatReq = "HeadSets";
        //    ViewBag.Isbooglebolt = true;// isbooglebolt;
        //    ViewBag.DisplayType =  string.IsNullOrEmpty(DisplayType) ? "detail" : DisplayType;
        //    string user_cookie = User_Cookie;
        //    return View("Catalog", dto);
        //}

        public ActionResult Catitems(string req, string p1, string p2, string p3)//Catalogs/HeadSets/brand/hme/list
        {
            string list = null;
            ViewBag.Root = "Catalog";
            ViewBag.Link_Base = "Catalog";
            ViewBag.Calalog = true;
            ViewBag.Brand = string.Empty;
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ProductModels productModels = new ProductModels();
            ProductModels.ManufacturerProduct_dto dto = new ProductModels.ManufacturerProduct_dto();
            dto.category = req;
            dto.user_cookie = User_Cookie;
            if (p1 != null && p1.ToLower() == "brand")
            {
                list = p3 == null ? null : p3.ToLower();
                dto.manufacturername = p2 == null ? null : p2;
                ViewBag.Brand = p2 == null ? "" : p2;
            }
            else
            {
                list = p1 == null ? null : p1.ToLower();
            }
            productModels.SelectCatalog(dto);
            if (dto.manufacturerProducts == null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.BrandFilter = false;
            ViewBag.AccessoryType = false;
            ViewBag.Model1 = false;
            ViewBag.Model2 = false;
            ViewBag.Condition = false;
            ViewBag.Printer = false;
            ViewBag.AccessoryType = false;

            switch (req.ToLower())
            {
                case "headsets":
                    ViewBag.Title = "Drive-Thru Headset";
                    ViewBag.CatReq = "Headsets";
                    ViewBag.breadcrum_current = "Headsets";
                    ViewBag.BrandFilter = true;
                    ViewBag.Model2 = true;
                    ViewBag.Condition = true;
                    break;
                case "belt-pacs":
                    ViewBag.Title = "Drive-Thru Belt Pacs";
                    ViewBag.CatReq = "Belt-Pacs";
                    ViewBag.breadcrum_current = "Belt Pacs";
                    ViewBag.BrandFilter = true;
                    ViewBag.Condition = true;
                    break;
                case "accessories":
                    ViewBag.Title = "Drive-Thru Accessories and Replacement Parts";
                    ViewBag.CatReq = "Accessories";
                    ViewBag.breadcrum_current = "Accessories";
                    ViewBag.BrandFilter = true;
                    ViewBag.Condition = true;
                    ViewBag.AccessoryType = true;
                    break;
                case "base-stations":
                    ViewBag.Title = "Drive-Thru Base Stations";
                    ViewBag.CatReq = "Base-Stations";
                    ViewBag.breadcrum_current = "Base Stations";
                    ViewBag.BrandFilter = true;
                    ViewBag.Condition = true;
                    break;
                case "headset-systems":
                    ViewBag.Title = "Drive-Thru Headset Systems";
                    ViewBag.CatReq = "Headset-Systems";
                    ViewBag.breadcrum_current = "Headset Systems";
                    ViewBag.BrandFilter = true;
                    ViewBag.Condition = true;
                    break;
                case "timers":
                    ViewBag.Title = "Drive-Thru Timers & Accessories";
                    ViewBag.CatReq = "Timers";
                    ViewBag.breadcrum_current = "Timers";
                    ViewBag.BrandFilter = true;
                    ViewBag.Condition = true;
                    break;
                case "cabled-intercoms":
                    ViewBag.Title = " Drive-Thru Intercoms Systems";
                    ViewBag.CatReq = "Cabled-Intercoms";
                    ViewBag.breadcrum_current = "Cabled Intercoms";
                    ViewBag.BrandFilter = true;
                    ViewBag.Condition = true;
                    break;
                case "batteries":
                    ViewBag.Title = " Batteries";
                    ViewBag.CatReq = "Batteries";
                    ViewBag.breadcrum_current = "Batteries";
                    ViewBag.BrandFilter = true;
                    //ViewBag.Condition = true;
                    break;
            }
            ViewBag.Isbooglebolt = isbooglebolt;
            ViewBag.DisplayType = string.IsNullOrEmpty(list) ? "detail" : list;
            string user_cookie = User_Cookie;
            JavaScriptSerializer json = new JavaScriptSerializer();
            ViewBag.manufacturerProducts = json.Serialize(dto.manufacturerProducts);
            //Session["ContinueShopping"] = "catalog~" + req.ToLower();
            return View("Catalog", dto);
        }
    }
}
