﻿using cerepairs.Models;
using hme;
using hmeUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static hme.SecurityController;

namespace cerepairs.Controllers
{
    public class FaveriateController : BaseController
    {

        [HttpPost]
        public async Task<JsonResult> AddFaveriate(Faveriate_dto dto)
        {
            Encryptor encryptor = new Encryptor();
            JavaScriptSerializer json = new JavaScriptSerializer();
            SessionManagement();
            string user_cookie = User_Cookie;
            if (User != null && User.Identity.IsAuthenticated)
            {
                user_info = json.Deserialize<User_Info>(encryptor.Decrypt(User.Identity.Name, SecurityController.salt));
                string loginType = user_info.logintype;
                switch (loginType)
                {
                    case "ldap":
                        dto.faveriate_id = user_info.SamAccountname;
                        break;
                    case "customerservice":
                        dto.faveriate_id = user_info.useruid;
                        break;
                }
            }
            else
            {
                dto.faveriate_id = user_cookie;
            }
            FaveriateModels model = new FaveriateModels();
            await model.AddFaveriate(dto);
            return Json(dto);
        }
    }
}
