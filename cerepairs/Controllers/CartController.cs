﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using cart.Models;
using cerepairs.DetailsModels;
using System.IO;
using qalab;
using PaymentModel.Models;
using System.Web.Script.Serialization;
using System.Configuration;
using hmeUtil;

namespace cerepairs.Controllers
{
    public class CartController : BaseController
    {
        // GET: Cart
        public ActionResult Index()
        {
            Encryptor encryptor = new Encryptor();
            string Salt = "kds45kdfdgmckeYy$m*Hke>";
            SessionManagement();
            JavaScriptSerializer json = new JavaScriptSerializer();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            string sessionUID = User_Session_UID;
            string cartUserId = Cart_Session_UID;
            DetailModels model = new DetailModels();
            DetailModels.ProductDetail_dto dto = model.GetHistory(User_Cookie);
 
            CartModels cartmodel = new CartModels();
            if (Session["Cart_Promo"] != null && Session["PromoDiscount"] == null)// && Session["PromoDiscount"] != null
            {
                cartmodel.PromoLookup(Session["Cart_Promo"].ToString(), Cart_Session_UID, User_Cookie);
            }


            CartLookup cartitems = cartmodel.GetCart(Cart_Session_UID, User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
            ViewBag.cartitems = null;
            ViewBag.cartitems = cartitems.cartitems;
            ViewBag.totalcartitems = cartitems.totalcartitems;
            ViewBag.freight = cartitems.freight;
            ViewBag.AdditionalFreight = cartitems.AdditionalFreight;
            ViewBag.usercookie = User_Cookie;
            ViewBag.promo = null;
            ViewBag.discount = null;
            Session["CartCount"] = null;
            Session["CartActive"] = null;
            Session["PromoDiscount"] = null;
            ViewBag.promo = Session["Cart_Promo"];
            if (cartitems.cartitems!= null && !string.IsNullOrEmpty(cartitems.totalcartitems))
            {
                Session["CartCount"] = cartitems.totalcartitems;
                Session["CartActive"] = "active";
            }

            if (Session["Cart_Promo"] != null)
            {
                ViewBag.promo = Session["Cart_Promo"].ToString();
                ViewBag.promomessage = Session["Cart_Promo_Message"].ToString();
                if (cartitems.promo != null)
                {
                    Session["PromoDiscount"] = cartitems.promo;
                    ViewBag.discount = cartitems.promo;
                }
            }
            string log = RuntimeCaching.CacheGetBillingLogToken(Cart_Session_UID + User_Cookie + "info");
            string failureToken = RuntimeCaching.CacheGetFailureToken(Cart_Session_UID + User_Cookie + "failure");
            if (failureToken != null)
            {
                ProcessPayment failuredto = json.Deserialize<ProcessPayment>(failureToken);
                ViewBag.failure = "1";
                ViewBag.billingaddress = json.Serialize(failuredto.billinginfo.billingaddress[0]);
                ViewBag.shippingaddress = json.Serialize(failuredto.billinginfo.shippingaddress[0]);
                ViewBag.billinginfo = json.Serialize(failuredto.billinginfo.billinginfo[0]);
                ViewBag.payment = failuredto.model.grandtotal.ToString();
                ViewBag.tax = failuredto.model.tax == null ? "0.0" : failuredto.model.tax.ToString();
                ViewBag.grandtotal = failuredto.model.grandtotal.ToString();
            }
            else
            {
                if (log != null)
                {
                    RuntimeCaching.CacheBillingLogToken(Cart_Session_UID + User_Cookie + "info",log);
                    BillingInfoLog billingLog = json.Deserialize<BillingInfoLog>(log);
                    ViewBag.billingLog = billingLog;
                    ViewBag.billingaddresslog = billingLog.billingaddress != null ? json.Serialize(billingLog.billingaddress) : null;
                    ViewBag.billinginfolog = billingLog.billinginfo != null ? json.Serialize(billingLog.billinginfo) : null;
                    if (billingLog.shippingaddress != null)
                    {
                        ViewBag.payment = billingLog.shippingaddress.grandtotal != null ? billingLog.shippingaddress.grandtotal.ToString().Replace("$", "") : "";
                        ViewBag.tax = billingLog.shippingaddress.tax == null ? "0" : billingLog.shippingaddress.tax.ToString().Replace("$","");
                        ViewBag.grandtotal = ViewBag.payment;
                        ShippingAddress shipping = new ShippingAddress();
                        shipping.address = billingLog.shippingaddress.address;
                        shipping.address2 = billingLog.shippingaddress.address2;
                        shipping.city = billingLog.shippingaddress.city;
                        shipping.company = billingLog.shippingaddress.company;
                        shipping.country = billingLog.shippingaddress.country;
                        shipping.state = billingLog.shippingaddress.state;
                        shipping.zip = billingLog.shippingaddress.zip;
                        ViewBag.shippingaddresslog = json.Serialize(shipping);
                    }
                    else
                    {
                        ViewBag.shippingaddresslog = null;
                    }
                }
            }
            var cerepairs_tokenizationkey = encryptor.Encrypt("W5jaxk-sH6S28-8A5xdR-JmUUwV", Salt);
            var cerepairs_key_hash = encryptor.Encrypt("LKEHSTL7F2DFJCN5ZLUM", Salt);
            var cerepairs_Authorization = encryptor.Encrypt(Base64Encode("integration@cerepairs.com:Welcome@2"), Salt);
            var b64 = Base64Encode("integration@cerepairs.com:Welcome@2").ToString();
            if (ConfigurationManager.AppSettings["Environment"].ToString().ToLower() == "test")
            {
                Session["tokenizationkey"] = encryptor.Decrypt(ConfigurationManager.AppSettings["test tokenization-key"], Salt);
                Session["payxero-authorization"] = encryptor.Decrypt(ConfigurationManager.AppSettings["test authorization"], Salt);
                Session["key-hash"] = encryptor.Decrypt(ConfigurationManager.AppSettings["test key-hash"], Salt);
            }
            else
            {
                var yy = ConfigurationManager.AppSettings["cerepairs " + "authorization"];
                var xx = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs " + "authorization"], Salt);
                Session["tokenizationkey"] = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs " + "tokenization-key"], Salt);
                Session["payxero-authorization"] = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs " + "authorization"], Salt);
                Session["key-hash"] = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs " + "key-hash"], Salt);
                var yyy= Session["payxero-authorization"].ToString();

            }
            return View("Index", dto);
        }

        [HttpPost]
        public JsonResult AddToCart(AddProductDTO dto)
        {
            SessionManagement();
            string sessionUID = User_Session_UID;
            string cartUserId = Cart_Session_UID;
            CartModels model = new CartModels();
            DetailModels dedtailmodel = new DetailModels();
            model.UpdateCart(Cart_Session_UID, User_Cookie, dto);
            DetailModels.ProductDetail_dto historydto = dedtailmodel.GetHistory(User_Cookie);

            CartLookup cartitems = model.GetCart(Cart_Session_UID, User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
            ViewBag.totalcartitems = cartitems.totalcartitems;
            ViewBag.cartitems = cartitems.cartitems;
            Session["CartCount"] = null;
            Session["CartActive"] = null;
            if (cartitems.cartitems != null && !string.IsNullOrEmpty(cartitems.totalcartitems))
            {
                Session["CartCount"] = cartitems.totalcartitems;
                Session["CartActive"] = "active";
            }
            dto.totalcartitems = cartitems.totalcartitems;
            return Json(dto);
        }

        [HttpPost]
        public JsonResult AddToCartJson(AddProductDTO dto)
        {
            SessionManagement();
            string sessionUID = User_Session_UID;
            string cartUserId = Cart_Session_UID;
            dto.status = "OK";
            CartModels model = new CartModels();
            model.UpdateCart(Cart_Session_UID, User_Cookie, dto);
            CartLookup cartitems = model.GetCart(Cart_Session_UID, User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
            Session["CartCount"] = null;
            Session["CartActive"] = null;
            if (cartitems.cartitems != null && !string.IsNullOrEmpty(cartitems.totalcartitems))
            {
                Session["CartCount"] = cartitems.totalcartitems;
                Session["CartActive"] = "active";
            }
            dto.count = cartitems.cartitems.Count.ToString();
            dto.totalcartitems = cartitems.totalcartitems;
            return Json(dto);
        }

        [HttpPost]
        public JsonResult UpdateCartJson(AddProductDTO dto)
        {
            SessionManagement();
            string sessionUID = User_Session_UID;
            string cartUserId = Cart_Session_UID;
            dto.status = "OK";
            CartModels model = new CartModels();
            model.CartUpdate(Cart_Session_UID, User_Cookie, dto, Session["Cart_Promo"] == null ? null :Session["Cart_Promo"].ToString());
            CartLookup cartitems = model.GetCart(Cart_Session_UID, User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
            float AdditionalFreight = 0;
            cartitems.AdditionalFreight = 0;
            if (cartitems.cartitems != null) {
                foreach (var item in cartitems.cartitems)
                {
                    AdditionalFreight += model.AdditionalFreight(item.Manufacturer_Part_Number, Convert.ToInt32(item.Cart_Product_QTY));
                }
            }
            cartitems.AdditionalFreight += Convert.ToDecimal(AdditionalFreight);

            Session["CartCount"] = null;
            Session["CartActive"] = null;
            if (cartitems.cartitems != null && !string.IsNullOrEmpty(cartitems.totalcartitems))
            {
                Session["CartCount"] = cartitems.totalcartitems;
                Session["CartActive"] = "active";
            }
            else
            {
            }
            dto.totalcartitems = cartitems.totalcartitems;
            return Json(dto);

        }

        [HttpPost]
        public JsonResult PromoLookupJson(PromoDTO dto)
        {
            SessionManagement();
            Session["Cart_Promo"] = null;
            string sessionUID = User_Session_UID;
            string cartUserId = Cart_Session_UID;
            dto.usercookie = User_Cookie;
            dto.status = "OK";
            CartModels model = new CartModels();
            model.PromoLookupJson(dto, Cart_Session_UID, User_Cookie);
            if (dto.message == "OK")
            {
                Session["Promo_ProductUID"] = dto.pormo_productUID;
                Session["Cart_Promo"] = dto.prmocode;
                Session["Cart_Promo_Message"] = dto.message;
            }
            ViewBag.promoLookup = "1";
            return Json(dto);
        }

        [HttpPost]
        public JsonResult PromoClearJson(PromoDTO dto)
        {
            Session["Cart_Promo"] = null;
            Session["Promo_ProductUID"] = null;
            Session["CartCount"] = null;
            Session["CartActive"] = null;
            Session["PromoDiscount"] = null;
            dto.status = "OK";
            dto.message = "OK";
            return Json(dto);
        }


        [HttpPost]
        public JsonResult LogBillingInfo(BillingInfoLog dto)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            BillingInfoLog billingLog = new BillingInfoLog();
            string log = RuntimeCaching.CacheGetBillingLogToken(Cart_Session_UID + User_Cookie + "info");
            if(log != null)
            {
                billingLog = json.Deserialize<BillingInfoLog>(log);
            }
            //else
            //{
            //    billingLog.billinginfo = new BillingInfo();
            //    billingLog.billingaddress = new BillingAddress();
            //    billingLog.shippingaddress = new ShippingAddressLog();
            //}
            billingLog.billinginfo = dto.billinginfo;
            RuntimeCaching.CacheBillingLogToken(Cart_Session_UID + User_Cookie + "info", json.Serialize(billingLog));
            dto.status = "OK";
            return Json(dto);
        }
        [HttpPost]
        public JsonResult LogBillingAddress(BillingInfoLog dto)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            BillingInfoLog billingLog = new BillingInfoLog();
            string log = RuntimeCaching.CacheGetBillingLogToken(Cart_Session_UID + User_Cookie + "info");
            if (log != null)
            {
                billingLog = json.Deserialize<BillingInfoLog>(log);
                billingLog.billingaddress = dto.billingaddress;
                RuntimeCaching.CacheBillingLogToken(Cart_Session_UID + User_Cookie + "info", json.Serialize(billingLog));
                dto.status = "OK";
                return Json(dto);
            }
            dto.status = "Failed";
            return Json(dto);
        }

        [HttpPost]
        public JsonResult LogShippingAddress(BillingInfoLog dto)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            BillingInfoLog billingLog = new BillingInfoLog();
            string log = RuntimeCaching.CacheGetBillingLogToken(Cart_Session_UID + User_Cookie + "info");
            if (log != null)
            {
                billingLog = json.Deserialize<BillingInfoLog>(log);
                billingLog.shippingaddress = dto.shippingaddress;
                RuntimeCaching.CacheBillingLogToken(Cart_Session_UID + User_Cookie + "info", json.Serialize(billingLog));
                dto.status = "OK";
                return Json(dto);
            }
            dto.status = "Failed";
            return Json(dto);
        }
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private string RenderPartialViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext,
                    viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

    }
}
