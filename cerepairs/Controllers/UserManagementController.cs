﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cerepairs.Models;
using System.Web.Script.Serialization;
using System.Net;
using System.Threading.Tasks;
using qalab;
using System.Data;
using System.Configuration;
using hme;
using System.Web.Security;
using hmeUtil;
using static hme.SecurityController;

namespace cerepairs.Controllers
{
    public class UserManagementController : BaseController
    {
        // GET: UserManagement
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index()
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            if (User != null && User.Identity.IsAuthenticated)
            {
                UserManagementModels model = new UserManagementModels();
                UserManagementDTO dto = new UserManagementDTO();
                dto.usersearchrequest = new UserSearchRequest();
                dto.usersearchrequest.custom = "default";
                dto.usersearchrequest.pagenumber = 1;
                dto.usersearchrequest.itemsperpage = 5;
                await model.UsersSearch(dto);
                ViewBag.json = json.Serialize(dto);
                return View("UserManagement");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "User")]
        public async Task<ActionResult> MyAccount()
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            if (User != null && User.Identity.IsAuthenticated)
            {
                UserManagementModels model = new UserManagementModels();
                UserManagementDTO dto = new UserManagementDTO();
                dto.usersearchrequest = new UserSearchRequest();
                dto.usersearchrequest.custom = "default";
                dto.usersearchrequest.pagenumber = 1;
                dto.usersearchrequest.itemsperpage = 1;
                dto.usersearchrequest.user_emailaddress = user_info.email;
                await model.UsersSearch(dto);
                ViewBag.json = json.Serialize(dto);
                return View(dto.usermanagementusers[0]);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<JsonResult> UserFilter(UserSearchRequest searchdto)
        {
            Encryptor encryptor = new Encryptor();
            JavaScriptSerializer json = new JavaScriptSerializer();
            UserManagementModels model = new UserManagementModels();
            UserManagementDTO dto = new UserManagementDTO();
            dto.usersearchrequest = searchdto;
            if (User != null && User.Identity != null && User.Identity.IsAuthenticated)
            {
                user_info = json.Deserialize<User_Info>(encryptor.Decrypt(User.Identity.Name, SecurityController.salt));
                bool isAdmin = user_info.isadmin.ToLower() == "true" ? true : false;
                bool isFullAdmin = user_info.isfulladmin.ToLower() == "true" ? true : false;
                if (isAdmin)
                {
                    await model.UsersSearch(dto);
                }
                else
                {
                    dto.status = "login";
                }
            }
            else
            {
                dto.status = "login";
            }
            return Json(dto);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<JsonResult> PendingUsers()
        {
            Encryptor encryptor = new Encryptor();
            JavaScriptSerializer json = new JavaScriptSerializer();
            UserManagementModels model = new UserManagementModels();
            UserManagementDTO dto = new UserManagementDTO();
            UserSearchRequest searchdto = new UserSearchRequest();
            searchdto.sortcolumn = "user_cust_id";
            searchdto.sortorder = "asc";
            searchdto.pagenumber = 1;
            searchdto.itemsperpage = 50;
            searchdto.custom = "pendingusers";
            searchdto.user_company = Session["Company_Master"].ToString();
            dto.usersearchrequest = searchdto;
            if (User != null && User.Identity != null && User.Identity.IsAuthenticated)
            {
                user_info = json.Deserialize<User_Info>(encryptor.Decrypt(User.Identity.Name, SecurityController.salt));
                bool isAdmin = user_info.isadmin.ToLower() == "true" ? true : false;
                bool isFullAdmin = user_info.isfulladmin.ToLower() == "true" ? true : false;
                if (isAdmin)
                {
                    await model.PendingUsers(dto);
                }
                else
                {
                    dto.status = "login";
                }
            }
            else
            {
                dto.status = "login";
            }
            return Json(dto);
        }

        //[Authorize]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<JsonResult> VerifyUpdate(UserManagementUser dto)
        {
            UserManagementModels model = new UserManagementModels();
            string requesttype = Request.Headers["requesttype"];
            await model.VerifyUpdate(dto, requesttype);
            if (dto.status == "OK" && requesttype == "Accept")
            {
                string siteURL = ConfigurationManager.AppSettings["ceURL"] + @"/security/ResetPassword/";
                string siteURLLogin = ConfigurationManager.AppSettings["ceURL"] + @"/security/login/";
                string viewpath = Server.MapPath(@"~/Views/emailtemplate.cshtml");
                string imagepath = Server.MapPath(@"~\images\emailtemplate");
                SendEmail.sendenotifymail(imagepath,viewpath, "New User Login Confirmation", "newuserloginconformation", "url", "linktag", dto.user_emailaddress, "Customer Service Portal: New User Login Confirmation", siteURL, siteURLLogin);
            }
            return Json(dto);
        }

        //[Authorize]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<JsonResult> UserUpdate(UserManagementUser dto)
        {
            UserManagementModels model = new UserManagementModels();
            string requesttype = Request.Headers["requesttype"];
            await model.UserUpdate(dto, requesttype);
            return Json(dto);
        }

        [Authorize]
        [HttpPost]
        public async Task<JsonResult> MyAccountUpdate(UserManagementUser dto)
        {
            SessionManagement();
            bool isbooglebolt = IsGoogleBolt();
            ViewBag.Isbooglebolt = isbooglebolt;
            UserManagementModels model = new UserManagementModels();
            string requesttype = Request.Headers["requesttype"];
            await model.UserUpdate(dto, requesttype);
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                if (authTicket != null)
                {
                    var roles = authTicket.UserData.Split(',');
                    user_info.email = dto.user_emailaddress == null ? user_info.email : dto.user_emailaddress;
                    user_info.givenname = dto.user_firstname == null ? user_info.givenname : dto.user_firstname;
                    user_info.sn = dto.user_lastname == null ? user_info.sn : dto.user_lastname;
                    user_info.cn = user_info.givenname + " " + user_info.sn;
                    Encryptor encryptor = new Encryptor();
                    JavaScriptSerializer json = new JavaScriptSerializer();
                    string token = encryptor.Encrypt(json.Serialize(user_info), SecurityController.salt);

                    //string Name = userInfo[0] + "|" + userInfo[1] + "|" + userInfo[2] + "|" + userInfo[3] + "|" + userInfo[4] + "|" + userInfo[5] + "|" + userInfo[6] + "|" + userInfo[7] + "|" + userInfo[8];
                    var new_authTicket = new FormsAuthenticationTicket(1, token, DateTime.Now, DateTime.Now.AddMinutes(30), false, authTicket.UserData);
                    string encryptedTicket = FormsAuthentication.Encrypt(new_authTicket);
                    var new_authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    Response.Cookies.Add(new_authCookie);
                }
             }
            return Json(dto);
        }
    }
}
