﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using hmeUtil;
using System.Collections.Generic;
using RazorEngine;

namespace ups
{
    public class UpsLabelController : Controller
    {
        [HttpPost]
        public JsonResult ReturnLabel(UpsLabelModels.upsLabelModel dto)
        {
            upsLabes ups = new upsLabes();
            ShipmentResponseObj response = ups.ReturnLabel(dto);
            byte[] bytes = Convert.FromBase64String(response.ShipmentResponse.ShipmentResults.PackageResults.ShippingLabel.GraphicImage);
            using (Image image = Image.FromStream(new MemoryStream(bytes)))
            {
                //image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                Image imageSized = ups.resizeImage(image, new Size(651, 392));
                using (MemoryStream m = new MemoryStream())
                {
                    imageSized.Save(m, System.Drawing.Imaging.ImageFormat.Gif);
                    byte[] imageBytes = m.ToArray();
                    ViewBag.image = String.Format("data:image/gif;base64,{0}", Convert.ToBase64String(imageBytes));
                    ViewBag.image_gif = Convert.ToBase64String(imageBytes);
                }
            }
            dto.label = ViewBag.image;
            dto.status = "OK";
            return Json(dto);
        }

        [HttpPost]
        public JsonResult EmailLabel(EmailLabel_dto dto)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();

            byte[] imageBytes = Convert.FromBase64String(dto.label);
            email email = new email();
            email.emailaddress emailaddress;
            email.attachments = new List<email.attachment>();
            email.from = "no-reply@hme.com";
            email.subject = "UPS Electronic Return Label";
            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = "hscott@hme.com";
            email.attachment attachment = new email.attachment();
            attachment.type = email.AttachmentType.byteArray;
            attachment.bytearray = imageBytes;
            attachment.filename = "label.gif";
            attachment.inline = true;
            attachment.inlineid = "~label~";
            email.attachments.Add(attachment);
            email.to.Add(emailaddress);

            string viewpath = Server.MapPath(@"~/Views/EmailShippingLabel.cshtml");
            var template = System.IO.File.ReadAllText(viewpath);
            email.body = Razor.Parse(template, dto);

            bool ret = email.sendemail();
            email.Dispose();
            dto.label = "";
            return Json(dto);
        }
    }
}
