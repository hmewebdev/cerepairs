﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ce.InvoiceModels;
using RazorEngine;
using RazorEngine.Templating;
using HiQPdf;
using hmeUtil;
using System.Configuration;
using System.Threading;
using Spire.Pdf;
using Spire.Pdf.Graphics;
using System.IO;
using System.Web.Script.Serialization;
using qalab;

namespace cerepairs.Controllers
{
    public class InvoiceController : BaseController
    {
        // GET: Invoice
        public ActionResult Index()
        {
            ViewBag.Title = "Invoice";
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            return null;
        }

        public string InvoiceConformationIDBuildPDF(string Conformation)
        {
            //////if (Request.Browser.Browser == "InternetExplorer" || Request.Browser.Browser == "IE") { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            //////if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            JavaScriptSerializer json = new JavaScriptSerializer();
            string path = Server.MapPath(@"~/Views/invoice.cshtml");
            string guid = Guid.NewGuid().ToString("N");
            InvoiceModels model = new InvoiceModels();

            Invoices inv = model.GetInvoice(Conformation, "conformation");
            string cart_session_uid = inv.Invoice_Session_UID;// "f53fc374de5f475ebf387c983b2c8db5";
            string user_cookie = inv.Invoice_User_Cookie;// "870cc576f0c4455c8c981dd68086fa64";
            string transaction_id = inv.Invoice_Transaction_ID;// "YNU4M0Q3IB7JJIF29U0LEYJYJ8CXYFPW";
            string comformation_id = Conformation;// "ZHRX42IG0M75PUMT";
            User user = model.GetUser(transaction_id);

            var first = DateTime.Parse(inv.invoices.Take(1).FirstOrDefault().Invoice_Created_DTS).ToString("MM/dd/yyyy");
            string page = RazorEngineClass.RazorEngine.RunCompile(path, null, inv);
            byte[] pdfBuffer = null;
            HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(page, ConfigurationManager.AppSettings["ceURL"]);
            Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
            pdf.LoadFromBytes(pdfBuffer);

            for (int i = 0; i < pdf.Pages.Count; i++)
            {
                PdfPageBase originalPage = pdf.Pages[i];
                if (originalPage.IsBlank())
                {
                    pdf.Pages.Remove(originalPage);
                    i--;
                }
            }

            MemoryStream PDFstream = new MemoryStream();
            pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
            byte[] bytes = PDFstream.ToArray();
            //bytes = bytes;
            //token.pdf = pdf;
            //InvoiceModels model = new InvoiceModels();45bc134752e14e95ae04425e4368ef79
            //model.SavePDF(bytes, token.Cart_Session_UID, token.User_Cookie, token.SECURETOKENID, token.confirmationNumber);
 
            model.SavePDF(bytes, cart_session_uid, user_cookie, transaction_id, comformation_id);



            //InvoiceModels model = new InvoiceModels();
            pdf = new Spire.Pdf.PdfDocument();
            //byte[] bytes;
            //byte[] pdfBuffer;
            string xbody = string.Empty;
            string pngImages = string.Empty;
            string header = string.Empty;
            string urlPath = Server.MapPath(@"~\images");
            byte[] ximage;
            //string cart_session_uid = null, string user_cookie = null, string transaction_id = null, string comformation_id = null
            bytes = model.GetPDF(null,null,null,Conformation);
            if (bytes != null)
            {
                pdf.LoadFromBytes(bytes);
            }
            PDFstream = new MemoryStream();
            pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
            pdfBuffer = PDFstream.ToArray();

            System.Drawing.Image image = pdf.SaveAsImage(0, PdfImageType.Bitmap, 296, 296);
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ximage = ms.ToArray();
            }

            List<pdfImage> pdfpages = model.GetPDFpages(pdf);
            email email = new email();
            email.emailaddress emailaddress;
            email.attachments = new List<email.attachment>();
            email.from = "no-reply@cerepairs.com";
            email.subject = "Invoice of Order - "+ comformation_id;
            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = "hscott@hme.com";
            email.to.Add(emailaddress);
            //string base64 = Convert.ToBase64String(pdfBuffer);
            email.body = string.Empty;
            email.attachment attachment = new email.attachment();
            attachment = new email.attachment();
            attachment.type = email.AttachmentType.byteArrayPDF;
            attachment.bytearray = pdfBuffer;
            attachment.filename = "Invoice_" + string.Format("{0:M.d.yyyy_HH.mm.ss_ffff}", DateTime.Now) + ".pdf";
            email.attachments.Add(attachment);

            int count = 0;
            foreach (var p in pdfpages)
            {
                attachment = new email.attachment();
                attachment.type = email.AttachmentType.byteArray;
                attachment.bytearray = ximage;
                attachment.inline = true;
                attachment.inlineid = "~attachment" + count.ToString() + "~";
                email.attachments.Add(attachment);
                pngImages += "<div><img src='cid:~attachment" + count.ToString() + "~' style='width: 100%;'></div>";
                xbody += "<div><img src='" + p.base64 + "' style='width: 100%;'></div>";
            }

            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = urlPath + @"\celogo.png";
            // urlPath+ "/images/celogo.png";
            attachment.inline = true;
            attachment.inlineid = "~logo~";
            //email.attachments.Add(attachment);

            header = "";// "<div style='text-align:left;margin-left:10px;'><img src='cid:~logo~'/><div style='margin-left:10px;font-size:19px;'>Invoice</div></div>";
            email.body = header + pngImages;
            bool ret = email.sendemail();
            email.Dispose();
            xbody = "<div style='text-align:left;margin-left:10px;'><img src='" + ConfigurationManager.AppSettings["ceURL"] + "/images/celogo.png'/><div style='margin-left:10px;font-size:19px;'>Invoice</div></div>" + xbody;
            return xbody;
        }

        public string InvoiceTransactionID(string transactionID)
        {
            //////if (Request.Browser.Browser == "InternetExplorer" || Request.Browser.Browser == "IE") { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            //////if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            JavaScriptSerializer json = new JavaScriptSerializer();

            //cerepairs.Classes.token secureToken = new cerepairs.Classes.token();
            //string token = secureToken.CreateToken("123456");
            //cerepairs.Classes.Token tokendto = secureToken.ValidateToken(token, 20, "seconds");

            //securetokendto securetoken_dto = new securetokendto();
            //securetoken_dto.SECURETOKENID = transactionID;
            //cerepairs.Classes.token secureToken = new cerepairs.Classes.token();
            //string securetoken = secureToken.CreateToken(json.Serialize(securetoken_dto));
            //cerepairs.Classes.Token tokendto = secureToken.ValidateToken(securetoken, 20, "seconds");
            //securetokendto xx = json.Deserialize<securetokendto>(tokendto.obj);


            //InvoiceModels model = new InvoiceModels();
            //MemoryStream PDFstream = new MemoryStream();
            //Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
            //pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
            //byte[] bytes = PDFstream.ToArray();
            //model.SavePDF(bytes, "123456");
            //byte[] bytes;
            //bytes = model.GetPDF(null, null, transactionID);
            //if (bytes != null)
            //{
            //    pdf.LoadFromBytes(bytes);
            //    List<pdfImage> pdf_pages = model.GetPDFpages(pdf);
            //    string invoice_Image = " <div style='text-align:left;margin-left:10px;'><img src='https://www.cerepairs.com/images/celogo.png' /><div style='margin-left:10px;font-size:19px;'>Invoice</div></div>";
            //    foreach (var p in pdf_pages)
            //    {
            //        invoice_Image += "<div><img src='" + p.image + "' style='width: 100%;'></div>";
            //    }
            //    Invoices inv = model.GetInvoice(transactionID, "transactionID");
            //    var first = DateTime.Parse(inv.invoices.Take(1).FirstOrDefault().Invoice_Created_DTS).ToString("MM/dd/yyyy");
            //    var date = first.Invoice_Created_DTS;
            //    string page = RazorEngineClass.RazorEngine.RunCompile(Server.MapPath(@"~/Views/invoice.cshtml"), null, inv);
            //    model.SendInvoiceEmail("Invoice of Order -" + transactionID, transactionID, "hscott@hme.com", page, null, null, null, null, pdf);

            //}
            //else
            //{
            //    return null;
            //}
            //List<pdfImage> pdfpages = model.GetPDFpages(pdf);
            //string invoiceImage = " <div style='text-align:left;margin-left:10px;'><img src='https://www.cerepairs.com/images/celogo.png' /><div style='margin-left:10px;font-size:19px;'>Invoice</div></div>";
            //foreach (var p in pdfpages)
            //{
            //    invoiceImage += "<div><img src='" + p.base64 + "' style='width: 100%;'></div>";
            //}
            //return invoiceImage;




            //InvoiceModels invoice = new InvoiceModels();
            //Invoices inv = invoice.GetInvoice(transactionID, "transactionID");
            //var first = DateTime.Parse(inv.invoices.Take(1).FirstOrDefault().Invoice_Created_DTS).ToString("MM/dd/yyyy");
            //var date = first.Invoice_Created_DTS;
            //ViewBag.Title = "Invoice";
            //string viewpath = Server.MapPath(@"~/Views/invoice.cshtml");
            //string page = RunCompile(viewpath, null, inv);
            //byte[] pdfBuffer = null;
            //HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
            //htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
            ////htmlToPdfConverter.Document.PageSize = HiQPdf.PdfPageSize.A4;
            ////htmlToPdfConverter.Document.PageOrientation = HiQPdf.PdfPageOrientation.Portrait;
            //pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(page, ConfigurationManager.AppSettings["ceURL"]);
            //string base64 = Convert.ToBase64String(pdfBuffer);
            //pdf.LoadFromBytes(pdfBuffer);

            InvoiceModels model = new InvoiceModels();
            Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
            byte[] bytes;
            byte[] pdfBuffer;
            string xbody = string.Empty;
            string pngImages = string.Empty;
            string header = string.Empty;
            string urlPath = Server.MapPath(@"~\images");
            byte[] ximage;

            bytes = model.GetPDF(transactionID);
            if (bytes != null)
            {
                pdf.LoadFromBytes(bytes);
            }
            MemoryStream PDFstream = new MemoryStream();
            pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
            pdfBuffer = PDFstream.ToArray();

            System.Drawing.Image image = pdf.SaveAsImage(0, PdfImageType.Bitmap, 296, 296);
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ximage = ms.ToArray();
            }

            List<pdfImage> pdfpages = model.GetPDFpages(pdf);
            email email = new email();
            email.emailaddress emailaddress;
            email.attachments = new List<email.attachment>();
            email.from = "no-reply@cerepairs.com";
            email.subject = "Invoice of Order - BQPP6E6473DE";
            email.to = new List<email.emailaddress>();
            emailaddress = new email.emailaddress();
            emailaddress.address = "hscott@hme.com";
            email.to.Add(emailaddress);
            //string base64 = Convert.ToBase64String(pdfBuffer);
            email.body = string.Empty;
            email.attachment attachment = new email.attachment();
            attachment = new email.attachment();
            attachment.type = email.AttachmentType.byteArrayPDF;
            attachment.bytearray = pdfBuffer;
            attachment.filename = "Invoice_" + string.Format("{0:M.d.yyyy_HH.mm.ss_ffff}", DateTime.Now) + ".pdf";
            email.attachments.Add(attachment);

            int count = 0;
            foreach (var p in pdfpages)
            {
                attachment = new email.attachment();
                attachment.type = email.AttachmentType.byteArray;
                attachment.bytearray = ximage;
                attachment.inline = true;
                attachment.inlineid = "~attachment" + count.ToString() + "~";
                email.attachments.Add(attachment);
                pngImages += "<div><img src='cid:~attachment" + count.ToString() + "~' style='width: 100%;'></div>";
                xbody += "<div><img src='" + p.base64 + "' style='width: 100%;'></div>";
            }

            attachment = new email.attachment();
            attachment.type = email.AttachmentType.filePath;
            attachment.filepath = urlPath + @"\celogo.png";
            // urlPath+ "/images/celogo.png";
            attachment.inline = true;
            attachment.inlineid = "~logo~";
            //email.attachments.Add(attachment);

            header = "";// "<div style='text-align:left;margin-left:10px;'><img src='cid:~logo~'/><div style='margin-left:10px;font-size:19px;'>Invoice</div></div>";
            email.body = header + pngImages;
            bool ret = email.sendemail();
            email.Dispose();
            xbody = "<div style='text-align:left;margin-left:10px;'><img src='" + ConfigurationManager.AppSettings["ceURL"] + "/images/celogo.png'/><div style='margin-left:10px;font-size:19px;'>Invoice</div></div>" + xbody;
            return xbody;
        }
        public ActionResult InvoiceSessionUID(string sessionUID)
        {
            if (Request.Browser.Browser == "InternetExplorer" && Request.Browser.MajorVersion <= 11) { return View("~/Views/Shared/InternetExplorer.cshtml"); }
            if (Request.Browser.Browser == "Safari" && Request.Browser.Platform == "WinNT") { return View("~/Views/Shared/SafariWindows.cshtml"); }
            SessionManagement();
            InvoiceModels invoice = new InvoiceModels();
            Invoices inv = invoice.GetInvoice(sessionUID, "invoicesessionID");
            ViewBag.Title = "Invoice";

            return View("~/Views/invoice.cshtml", inv);
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public JsonResult SendAdditionalEmails(additionalEmails dto)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
            //string cmd = RuntimeCaching.CacheGetButtonCommandsToken(dto.guid);
            //string cmd = string.Empty;
            //for (int i = 0; i < 10; i++)
            //{
            //    cmd = RuntimeCaching.CacheGetButtonCommandsToken(dto.guid);
            //    if (cmd == null)
            //    { Thread.Sleep(500); }
            //    else { break; }
            //}
            //if (cmd == null)
            //{
            //    //string logFile = "~/" + "testlog.txt";
            //    //logFile = Server.MapPath(logFile);
            //    //StreamWriter sw = new StreamWriter(logFile, true);
            //    //sw.WriteLine("cmd = Null");
            //    //sw.Close();

            //    dto.status = "-1";
            //    return Json(dto);
            //}
            //RuntimeCaching.CacheButtonCommandsToken(dto.guid, cmd);
            //buttonCommands securintObjs = json.Deserialize<buttonCommands>(cmd);
            //if (securintObjs != null)
            //{
            buttonCommands buttonCommands = new buttonCommands();
            buttonCommands.Invoice_Transaction_ID = dto.guid;
            buttonCommands.Invoice_Session_UID = dto.Invoice_Session_UID;
            buttonCommands.Invoice_User_Cookie = dto.Invoice_User_Cookie;
            buttonCommands.Invoice_Conformation_ID = dto.Invoice_Conformation_ID;
            InvoiceModels model = new InvoiceModels();
                Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
                byte[] bytes = null;
                for (int i = 0; i < 10; i++)
                {
                    bytes = model.GetPDF(buttonCommands.Invoice_Session_UID, buttonCommands.Invoice_User_Cookie, buttonCommands.Invoice_Transaction_ID);
                    if (bytes == null)
                    { Thread.Sleep(500); }
                    else { break; }
                }
                if (bytes == null)
                {
                    dto.status = "-1";
                    return Json(dto);
                }
                else
                {
                    string path = Server.MapPath(@"~/Views/invoice.cshtml");
                    pdf.LoadFromBytes(bytes);
                    bool ret = false;
                    int count = 0;
                    while (count < 5)
                    {
                        ret = model.SendInvoiceEmail("Invoice of Order -" + buttonCommands.Invoice_Conformation_ID, buttonCommands.Invoice_Transaction_ID, buttonCommands.Invoice_Conformation_ID, Server.MapPath(@"~\images"), dto.emails, null, null, pdf, null);
                        if (ret)
                        {
                            break;
                        }
                        Thread.Sleep(500);
                        count++;
                    }
                    if (ret == true)
                    {
                        dto.status = "0";
                        return Json(dto);
                    }
                    else
                    {
                        dto.status = "-3";
                        return Json(dto);
                    }
                }
            //}
            //else
            //{
            //    dto.status = "-2";
            //    return Json(dto);
            //}
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public JsonResult PrintCommand(bcmd dto)
        {
            JavaScriptSerializer json = new JavaScriptSerializer();
 
            InvoiceModels model = new InvoiceModels();
            buttonCommands buttonCommands = new buttonCommands();
            buttonCommands.Invoice_Transaction_ID = dto.guid;
            buttonCommands.Invoice_Session_UID = dto.Invoice_Session_UID;
            buttonCommands.Invoice_User_Cookie = dto.Invoice_User_Cookie;
            buttonCommands.Invoice_Conformation_ID = dto.Invoice_Conformation_ID;
            dto.print = model.Print(buttonCommands);
            dto.status = "0";
            return Json(dto);

        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        public JsonResult DownloadCommand(bcmd dto)
        {
            //JavaScriptSerializer json = new JavaScriptSerializer();
            //string cmd = string.Empty;
            //for (int i = 0; i < 10; i++)
            //{
            //    cmd = RuntimeCaching.CacheGetButtonCommandsToken(dto.guid);
            //    if (cmd == null)
            //    { Thread.Sleep(500); }
            //    else { break; }
            //}
            //if (cmd == null)
            //{
            //    dto.status = "-1";
            //    return Json(dto);
            //}
            //RuntimeCaching.CacheButtonCommandsToken(dto.guid, cmd);
            //buttonCommands securintObjs = json.Deserialize<buttonCommands>(cmd);
            //if (securintObjs != null)
            //{
            buttonCommands buttonCommands = new buttonCommands();
            buttonCommands.Invoice_Transaction_ID = dto.guid;
            buttonCommands.Invoice_Session_UID = dto.Invoice_Session_UID;
            buttonCommands.Invoice_User_Cookie = dto.Invoice_User_Cookie;
            buttonCommands.Invoice_Conformation_ID = dto.Invoice_Conformation_ID;
            string guid = Guid.NewGuid().ToString("N");
            InvoiceModels model = new InvoiceModels();
            Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
            byte[] bytes;
            bytes = model.GetPDF(buttonCommands.Invoice_Session_UID, buttonCommands.Invoice_User_Cookie, buttonCommands.Invoice_Transaction_ID);
            if (bytes == null)
            {
                dto.status = "-2";
                return Json(dto);
            }
            else
            {
                string handle = guid;// Guid.NewGuid().ToString();
                dto.guid = handle;
                pdf.LoadFromBytes(bytes);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    pdf.SaveToStream(memoryStream);
                    memoryStream.Position = 0;
                    TempData[dto.guid] = memoryStream.ToArray();
                    dto.status = "0";
                    return Json(dto);
                }
            }
            //}
            //else
            //{
            //    dto.status = "-3";
            //    return Json(dto);
            //}
        }

        [HttpGet]
        [System.Web.Services.WebMethod(EnableSession = true)]
        public virtual ActionResult DownloadPDFfile(string guid)
        {
            //JavaScriptSerializer json = new JavaScriptSerializer();
            //string cmd = string.Empty;
            //for (int i = 0; i < 10; i++)
            //{
            //    cmd = RuntimeCaching.CacheGetButtonCommandsToken(guid);
            //    if (cmd == null)
            //    { Thread.Sleep(500); }
            //    else { break; }
            //}
            ////if (cmd == null)
            ////{
            ////    dto.status = "-1";
            ////    return Json(dto);
            ////}
            //RuntimeCaching.CacheButtonCommandsToken(guid, cmd);
            //buttonCommands securintObjs = json.Deserialize<buttonCommands>(cmd);
            byte[] data = TempData[guid] as byte[];
            //return File(data, "application/pdf", "Invoice_" + securintObjs.CompanyName + "_" + securintObjs.Invoice_Conformation_ID + "_" + string.Format("{0:M.d.yyyy_HH.mm.ss_ffff}", DateTime.Now) + ".pdf");
            return File(data, "application/pdf", "Invoice_" + string.Format("{0:M.d.yyyy_HH.mm.ss_ffff}", DateTime.Now) + ".pdf");

            //JavaScriptSerializer json = new JavaScriptSerializer();
            //string cmd = string.Empty;
            //for (int i = 0; i < 10; i++)
            //{
            //    cmd = RuntimeCaching.CacheGetButtonCommandsToken(guid);
            //    if (cmd == null){ Thread.Sleep(500); }else { break; }
            //}
            //if(cmd == null)
            //{
            //    return null;
            //}
            //RuntimeCaching.CacheButtonCommandsToken(guid, cmd);
            //buttonCommands securintObjs = json.Deserialize<buttonCommands>(cmd);
            //if (securintObjs != null)
            //{
            //    InvoiceModels model = new InvoiceModels();
            //    Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
            //    byte[] bytes;
            //    bytes = model.GetPDF(securintObjs.Invoice_Session_UID, securintObjs.Invoice_User_Cookie, securintObjs.Invoice_Transaction_ID);
            //    if (bytes == null)
            //    {
            //        return null;
            //    }
            //    else
            //    {
            //        pdf.LoadFromBytes(bytes);
            //        using (MemoryStream memoryStream = new MemoryStream())
            //        {
            //            pdf.SaveToStream(memoryStream, Spire.Pdf.FileFormat.PDF);
            //            memoryStream.Position = 0;
            //            return File(memoryStream.ToArray(), "application/pdf", "fileName.pdf");
            //        }
            //    }
            //}
            //else
            //{
            //    return null;
            //}
        }

        public static string RunCompile(string viewpath, string templatekey, object model)
        {
            string result = string.Empty;
            var template = System.IO.File.ReadAllText(viewpath);
            if (string.IsNullOrEmpty(templatekey))
            {
                templatekey = Guid.NewGuid().ToString();
            }
            result = Engine.Razor.RunCompile(template, templatekey, null, model);
            //result = Engine.Razor.RunCompile(template, null, model);
            return result;
        }
    }
}