﻿using cerepairs.DetailsModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using cart.Models;
using System.Collections.Specialized;
using System.Configuration;
using System.Net;
using System.IO;
using PaymentModel.Models;
using qalab;
using signalR_Hub;
using System.Reflection;
using RazorEngine;
using RazorEngine.Templating;
using System.Threading;
using hmeUtil;
using System.Web.SessionState;
using ce.InvoiceModels;
using Spire.Pdf;
using HiQPdf;

namespace cerepairs.Controllers
{
    public class PaymentController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PaymentConfirmation()
        {
            CartModels cartmodel = new CartModels();
            SessionIDManager manager = new SessionIDManager();
            PayPalResponse paypalresponse = new PayPalResponse();
            bool isAdded = false; bool redirected = false;
            ViewBag.PaymentConfirmation = "";
            ViewBag.PaymentResult = "";
            PayPalTransactionResponse token = new PayPalTransactionResponse();
            JavaScriptSerializer json = new JavaScriptSerializer();
            if (Request.QueryString["conformation"] != null)
            {
                string securityToken = RuntimeCaching.CacheGetResponseToken(Request.QueryString["conformation"].ToString());
                if (securityToken == null)
                {
                    ViewBag.PaymentResult = "";
                    return View();
                }
                token = json.Deserialize<PayPalTransactionResponse>(securityToken);
            }
            if (token != null)
            {
                ViewBag.status = token.status;
                if (token.status != "Approved")
                {
                    manager.SaveSessionID(System.Web.HttpContext.Current, token.User_Session_UID, out redirected, out isAdded);
                    CartLookup cartitems = cartmodel.GetCart(token.Cart_Session_UID, token.User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
                    Session["CartCount"] = null;
                    Session["CartActive"] = null;
                    if (cartitems.cartitems != null && !string.IsNullOrEmpty(cartitems.totalcartitems))
                    {
                        Session["CartCount"] = cartitems.totalcartitems;
                        Session["CartActive"] = "active";
                    }
                    ViewBag.PaymentResult = token.RESULT;
                    ViewBag.PaymentConfirmation = string.Empty;

                    var tokenInfo = RuntimeCaching.CacheGetFailureToken(token.Cart_Session_UID + token.User_Cookie + "failure");
                    if (tokenInfo != null)
                    {
                        RuntimeCaching.CacheFailureToken(token.Cart_Session_UID + token.User_Cookie + "failure", tokenInfo);
                        ProcessPayment tokenInfoJson = json.Deserialize<ProcessPayment>(tokenInfo);
                        ViewBag.PaymentConfirmation = "";
                        ViewBag.chatid = json.Serialize(tokenInfoJson.signalr);
                        ViewBag.usercookie = json.Serialize(tokenInfoJson.User_Cookie);
                        ViewBag.billingaddress = json.Serialize(tokenInfoJson.billinginfo.billingaddress[0]);
                        ViewBag.shippingaddress = json.Serialize(tokenInfoJson.billinginfo.shippingaddress[0]);
                        ViewBag.billinginfo = json.Serialize(tokenInfoJson.billinginfo.billinginfo[0]);
                        ViewBag.grandtotal = tokenInfoJson.model.grandtotal.ToString();
                        ViewBag.tax = tokenInfoJson.model.tax;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(token.confirmationNumber))
                    {
                        ViewBag.PaymentConfirmation = token.confirmationNumber;
                        ViewBag.PaymentResult = token.RESULT;
                        ViewBag.SECURETOKENID = token.SECURETOKENID;
                        string path = Server.MapPath(@"~/Views/invoice.cshtml");
                        string guid = Guid.NewGuid().ToString("N");



                        //InvoiceModels model = new InvoiceModels();

                        //Invoices inv = model.GetInvoice(token.SECURETOKENID, "transactionID");
                        //var first = DateTime.Parse(inv.invoices.Take(1).FirstOrDefault().Invoice_Created_DTS).ToString("MM/dd/yyyy");
                        ////var date = first.Invoice_Created_DTS;
                        //string page = RazorEngineClass.RazorEngine.RunCompile(path, null, inv);
                        //byte[] pdfBuffer = null;
                        //HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
                        //htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
                        //pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(page, ConfigurationManager.AppSettings["ceURL"]);
                        //Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
                        //pdf.LoadFromBytes(pdfBuffer);

                        //for (int i = 0; i < pdf.Pages.Count; i++)
                        //{
                        //    PdfPageBase originalPage = pdf.Pages[i];
                        //    if (originalPage.IsBlank())
                        //    {
                        //        pdf.Pages.Remove(originalPage);
                        //        i--;
                        //    }
                        //}

                        //MemoryStream PDFstream = new MemoryStream();
                        //pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
                        //byte[] bytes = PDFstream.ToArray();
                        ////bytes = bytes;
                        ////token.pdf = pdf;
                        ////InvoiceModels model = new InvoiceModels();
                        //model.SavePDF(bytes, token.Cart_Session_UID, token.User_Cookie, token.SECURETOKENID, token.confirmationNumber);






                        @ViewBag.securetoken = guid;
                        @ViewBag.Cart_Session_UID = token.Cart_Session_UID;
                        @ViewBag.User_Cookie = token.User_Cookie;
                        //InvoiceThread invoiceThread = new InvoiceThread();
                        //invoiceThread.guid = guid;
                        //invoiceThread.directoryPath = Server.MapPath(@"~\images");
                        //invoiceThread.dto = token;
                        //invoiceThread.viewpath = path;
                        //invoiceThread.bytes = bytes;
                        //invoiceThread.pdf = pdf;
                        //InvoiceThreading invoiceThreading = new InvoiceThreading();
                        //Thread invoice_Thread = new Thread(invoiceThreading.InvoiceConformation);
                        //invoice_Thread.Start(invoiceThread);
                        Session["Cart_Session_UID"] = null;
                        Session["Cart_User_UID"] = null;
                        //new Thread(() =>
                        //{
                        //    ce.InvoiceModels.InvoiceModels invoiceModel = new ce.InvoiceModels.InvoiceModels();
                        //    invoiceModel.ConformationInvoice(guid, token, path, Server.MapPath(@"~\images"));
                        //}).Start();
                    }
                }
            }
            else
            {
                //    manager.SaveSessionID(System.Web.HttpContext.Current, token.User_Session_UID, out redirected, out isAdded);
                //    SessionManagement();
                //    CartLookup cartitems = cartmodel.GetCart(token.Cart_Session_UID, User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
                //    Session["CartCount"] = null;
                //    Session["CartActive"] = null;
                //    if (cartitems.cartitems != null && !string.IsNullOrEmpty(cartitems.totalcartitems))
                //    {
                //        Session["CartCount"] = cartitems.totalcartitems;
                //        Session["CartActive"] = "active";
                //    }
            }
            return View();
        }

        [HttpPost]
        public JsonResult ProcessPayment(ProcessPayment dto)
        {
            Encryptor encryptor = new Encryptor();
            string Salt = "kds45kdfdgmckeYy$m*Hke>";
            dto.billinginfo.billinginfo[0].phonenumber = String.Format("{0:(###) ###-####}", double.Parse(dto.billinginfo.billinginfo[0].phonenumber));
            string SECURETOKENID = Guid.NewGuid().ToString("N");
            ViewBag.SECURETOKENID = SECURETOKENID;
            string SECURETOKEN = Guid.NewGuid().ToString("N");
            ViewBag.SECURETOKEN = SECURETOKEN;

            SessionManagement();
            dto.User_Session_UID = User_Session_UID;
            dto.Cart_Session_UID = Cart_Session_UID;
            dto.Promo_ProductUID = Session["Promo_ProductUID"] == null ? null : Session["Promo_ProductUID"].ToString();
            CartModels cartmodel = new CartModels();
            double freight = 0;
            double tax = 0;
            double discount = 0;
            decimal totalPurchases = 0;
            string promo = Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString();
            CartLookup cartitems = cartmodel.GetCart(Cart_Session_UID, User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
            //totalPurchases = cartitems.cartitems.Sum(r => (Convert.ToInt32(r.Cart_Product_QTY) + Convert.ToInt32(r.Cart_Promo_Discount)));
            //discount = cartitems.cartitems.Sum(r => Math.Round((Convert.ToDouble(r.Cart_Product_Price_SubTotal) * (r.Cart_Promo_Discount == "0" ? 0 : Convert.ToInt32(r.Cart_Promo_Discount))),2));
            if (cartitems.cartitems == null)
            {
                return Json("-1");
            }

            float AdditionalFreight = 0;
            cartitems.AdditionalFreight = 0;

            foreach (var item in cartitems.cartitems)
            {
                totalPurchases += Convert.ToDecimal(item.Cart_Product_Price_SubTotal) * (Convert.ToInt32(item.Cart_Product_QTY) + Convert.ToInt32(item.Cart_Promo_Discount));
                if (promo != null)
                {
                    discount += Convert.ToDouble(item.Cart_Product_Price_SubTotal.ToString()) * Convert.ToDouble(item.Cart_Promo_Discount.ToString());
                    //discount += Convert.ToDouble(cartitems.promo.discount.ToString()) + (Convert.ToDecimal(cartitems.promo.discount.ToString()) == 0 ? 0 : Convert.ToInt32(cartitems.promo.discount.ToString()));
                }

                AdditionalFreight += cartmodel.AdditionalFreight(item.Supplier_Part_Number,Convert.ToInt32(item.Cart_Product_QTY));
            }
            cartitems.AdditionalFreight += Convert.ToDecimal(AdditionalFreight);
            var freightObj = cartitems.freight.Where(x => totalPurchases >= x.ProductFreight_Low && totalPurchases <= x.ProductFreight_High).ToList();
            freight = Math.Round(Convert.ToDouble(freightObj[0].ProductFreight), 2);
            double TaxRate = 0;
            double MO_TaxRate = 0.08363;
            double CA_TaxRate = 0.08000;
            double CO_TaxRate = 0.02900;
            double IA_TaxRate = 0.06000;
            double grandtotal;
            bool istaxableState = false;
            switch (dto.billinginfo.shippingaddress[0].state.ToLower())
            {
                case "ca":
                    TaxRate = CA_TaxRate;
                    istaxableState = true;
                    break;
                case "mo":
                    TaxRate = MO_TaxRate;
                    istaxableState = true;
                    break;
                case "ia":
                    TaxRate = IA_TaxRate;
                    istaxableState = true;
                    break;
                case "co":
                    TaxRate = CO_TaxRate;
                    istaxableState = true;
                    break;
            }
            double totalPurchases_double = Math.Round(Convert.ToDouble(totalPurchases), 2);
            if (istaxableState)
            {
                tax = Math.Round(((totalPurchases_double - discount) * TaxRate), 2);
            }
            freight += Convert.ToDouble(AdditionalFreight);
            grandtotal = Math.Round(((totalPurchases_double + tax + freight) - discount), 2);

            PaymentRequestModel paymentRequestModel = new PaymentRequestModel();
            dto.paypalresponse = new PayPalTransactionResponseDTO();
            string environment = ConfigurationManager.AppSettings["Environment"]; // Change to "live" to process real transactions.
            string comments1 = "";
            string _mode = "TEST";
            string currency = "USD";
            paymentRequestModel.TotalAmount = Convert.ToDecimal(string.Format("{0:N}", grandtotal));
            PaypalInfo paypallInfo = GetPaypalInfo("USD");
            //NameValueCollection request = new NameValueCollection() {
            //    {"PARTNER", paypallInfo.Partner},
            //    {"VENDOR", paypallInfo.Vendor},
            //    {"USER", paypallInfo.User},
            //    {"PWD", paypallInfo.Password},
            //    {"TRXTYPE", "S"},
            //    {"AMT", string.Format("{0:N}", grandtotal.ToString()) },
            //    {"CURRENCY", currency},
            //    {"CREATESECURETOKEN", "Y"},
            //    {"SECURETOKENID", GenerateRandomToken(TokenLength.Bit32)},
            //    {"RETURNURL", paypallInfo.ReturnURL},
            //    {"CANCELURL", paypallInfo.CancelURL},
            //    {"ERRORURL", paypallInfo.ErrorURL},
            //    {"COMMENT1", comments1.TrimEnd(',') },
            //    {"COMMENT2", "" }
            //};

            //string environment = ConfigurationManager.AppSettings["Environment"]; // Change to "live" to process real transactions.
            //string nvpstring = "";
            //foreach (string key in request)
            //{
            //    var val = request[key];
            //    nvpstring += key + "[ " + val.Length + "]=" + val + "&";
            //}
            //string urlEndpoint;
            //if (environment == "pilot" || environment == "test" || environment == "sandbox")
            //{
            //    urlEndpoint = "https://pilot-payflowpro.paypal.com/";
            //}
            //else
            //{
            //    urlEndpoint = "https://payflowpro.paypal.com";
            //}
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            //HttpWebRequest payReq = (HttpWebRequest)WebRequest.Create(urlEndpoint);
            //payReq.Method = "POST";
            //payReq.ContentLength = nvpstring.Length;
            //payReq.ContentType = "application/x-www-form-urlencoded";
            //StreamWriter sw = new StreamWriter(payReq.GetRequestStream());
            //sw.Write(nvpstring);
            //sw.Close();

            ////GET PAYFLOW RESPONSE
            //HttpWebResponse payResp = (HttpWebResponse)payReq.GetResponse();
            //StreamReader sr = new StreamReader(payResp.GetResponseStream());
            //string response = sr.ReadToEnd();
            //sr.Close();

            //NameValueCollection dict = new NameValueCollection();
            //foreach (string nvp in response.Split('&'))
            //{
            //    string[] keys = nvp.Split('=');
            //    if (keys.Length > 1)
            //    {
            //        dict.Add(keys[0], keys[1]);
            //    }
            //}
            string IPAddress = (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] == null) ? Request.UserHostAddress : Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            var PaymentUId = GenerateRandomToken(TokenLength.Bit32);
            dto.PaymentUId = PaymentUId;
            dto.User_Session_UID = User_Session_UID;
            dto.Cart_Session_UID = Cart_Session_UID;
            dto.User_Cookie = User_Cookie;
            dto.Invoice_Session_UID = Cart_Session_UID;
            dto.Invoice_User_Cookie = User_Cookie;
            dto.currency = currency;
            dto.Promo = promo;
            //dto.Promo_ProductUID = Session["Promo_ProductUID"] == null ? null : Session["Promo_ProductUID"].ToString();
            JavaScriptSerializer json = new JavaScriptSerializer();
            PaymentModel.Models.PaymentModel model = new PaymentModel.Models.PaymentModel();
            model.JsonObj = json.Serialize(paymentRequestModel);
            //var xx = json.Deserialize<PaymentRequestModel>(model.JsonObj);
            //model.PaymentTokenId = dict["SECURETOKENID"];
            //model.PaymentToken = dict["SECURETOKEN"];
            model.PaymentAmount = grandtotal.ToString();
            model.PaymentMemo = (!string.IsNullOrEmpty(paymentRequestModel.Comments) ? paymentRequestModel.Comments : string.Empty);
            model.promo = promo;
            model.freight = freight.ToString();
            model.discount = discount.ToString() == "0" ? null : Convert.ToDecimal(string.Format("{0:N}", discount)).ToString();
            model.taxrate = TaxRate.ToString();
            model.ipAddress = User_IP;
            model.promo_productUID = dto.Promo_ProductUID;
            model.PaymentCompany = Company;
            model.PaymentType = paymentRequestModel.PaymentType;
            model.SessionUserId = User_Session_UID;
            model.User_Cookie = dto.usercookie;
            model.Invoice_Session_UID = dto.Invoice_Session_UID;
            model.Invoice_User_Cookie = dto.Invoice_User_Cookie;
            model.PaymentUId = PaymentUId;
            model.PaymentCreatedBy = IPAddress;
            model.ipAddress = IPAddress;
            model.PaymentCreatedDateTime = DateTime.Now;
            model.PaymentComplete = "false";
            model.IsPaymentApplied = "false";
            model.PaymentJournal = string.Empty;
            model.PaymentCardType = string.Empty;
            model.grandtotal = grandtotal.ToString();
            model.freight = freight.ToString();
            model.tax = Convert.ToDecimal(tax) == 0 ? "0" : tax.ToString();
            //model.PaymentSubTotal = totalPurchases_double.ToString();
            model.PaymentCurrency = currency;
            model.promo_productUID = Session["Promo_ProductUID"] == null ? null : Session["Promo_ProductUID"].ToString();
            model.User_Created_Agent = User_Agent;
            //model.AdditionalFreight = cartitems.AdditionalFreight.ToString();

            dto.model = model;

            RuntimeCaching.CacheSaveCurrency("currency" + SECURETOKENID, currency);
            RuntimeCaching.CacheSaveIdenity("idenity" + SECURETOKENID, User.Identity.Name);
            RuntimeCaching.CacheSecurityToken(SECURETOKENID, json.Serialize(dto));

            IList<PropertyInfo> propertiesList;
            propertiesList = typeof(Sessions).GetProperties().ToList();
            Sessions sessions = new Sessions();
            try
            {
                foreach (var property in propertiesList)
                {
                    try
                    {
                        if (Session[property.Name] != null)
                            sessions.SetProperty(property.Name, Session[property.Name].ToString());
                    }
                    catch { }
                }
            }
            catch { }
            string save_sessions = json.Serialize(sessions);
            RuntimeCaching.CacheSession("sessions" + SECURETOKENID, save_sessions);
            if (ConfigurationManager.AppSettings["Environment"].ToString().ToLower() == "test")
            {
                Session["tokenizationkey"] = encryptor.Decrypt(ConfigurationManager.AppSettings["test tokenization-key"], Salt);
                Session["payxero-authorization"] = encryptor.Decrypt(ConfigurationManager.AppSettings["test authorization"], Salt);
                Session["key-hash"] = encryptor.Decrypt(ConfigurationManager.AppSettings["test key-hash"], Salt);
            }
            else
            {
                var yy = ConfigurationManager.AppSettings["cerepairs " + "authorization"];
                var xxx = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs " + "authorization"], Salt); ;
                Session["tokenizationkey"] = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs tokenization-key"], Salt);
                Session["payxero-authorization"] = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs authorization"], Salt);
                Session["key-hash"] = encryptor.Decrypt(ConfigurationManager.AppSettings["cerepairs key-hash"], Salt);
                var jjj = Session["payxero-authorization"].ToString();
            }
            ViewBag.SECURETOKENID = SECURETOKENID;
            ViewBag.SECURETOKEN = SECURETOKEN;

            ViewBag.custIDs = "44444";


            ViewBag.email = dto.billinginfo.billinginfo[0].email;


            //ViewBag.email = "hscott@hme.com";
            string viewResponse = RenderPartialViewToString("~/Views/Payment/_ProocessPaymentAjax.cshtml", paymentRequestModel);
            //string viewResponse = RenderPartialViewToString("~/Views/Payment/PaymentConfirmation.cshtml", paymentRequestModel);
            return Json(viewResponse);

        }

        [HttpPost]
        public ActionResult Transaction()
        {
            SessionManagement();
            CartModels cartmodel = new CartModels();
            SessionIDManager manager = new SessionIDManager();
            string confirmationNumber = GenerateRandomToken(TokenLength.Bit16);
            bool isAdded = false; bool redirected = false;
            cerepairsHub.ceHub hubpush = new cerepairsHub.ceHub();
            string guid = Guid.NewGuid().ToString("N");
            JavaScriptSerializer json = new JavaScriptSerializer();
            var paypalCollection = Request.Form;
            string securityToken = RuntimeCaching.CacheGetSecurityToken(paypalCollection["SECURETOKENID"].ToString());
            //RuntimeCaching.CacheSecurityToken(paypalCollection["SECURETOKENID"].ToString(), securityToken);

            ProcessPayment dto = json.Deserialize<ProcessPayment>(securityToken);//error on cache timeout
            //RuntimeCaching.CacheSecurityToken(dto.model.PaymentTokenId, json.Serialize(securityToken));
            PayPalResponse paypalresponse = new PayPalResponse();

            if (paypalCollection != null
                && paypalCollection["SECURETOKENID"] != null
                && paypalCollection["RESPMSG"].ToString().ToLower() == "approved"
                && paypalCollection["RESULT"].ToString() == "0")
            {
                dto.confirmationnumber = confirmationNumber;
                dto.model.confirmationNumber = confirmationNumber;
                IList<PropertyInfo> propertiesList;
                propertiesList = typeof(PayPalResponse).GetProperties().ToList();
                PayPalResponse presp = new PayPalResponse();
                try
                {
                    foreach (var property in propertiesList)
                    {
                        try
                        {
                            presp.SetProperty(property.Name, Request.Form[property.Name].ToString());//if (s != "FPS_PREXMLDATA")
                        }
                        catch { }
                    }
                }
                catch { }

                propertiesList = typeof(Sessions).GetProperties().ToList();
                Sessions sessions = new Sessions();
                try
                {
                    foreach (var property in propertiesList)
                    {
                        try
                        {
                            if (Session[property.Name] != null)
                                sessions.SetProperty(property.Name, Session[property.Name].ToString());
                        }
                        catch { }
                    }
                }
                catch { }
                string save_sessions = json.Serialize(sessions);
                RuntimeCaching.CacheSession("sessions" + confirmationNumber, save_sessions);


                PayPalTransactionResponse resp = new PayPalTransactionResponse();
                resp.SECURETOKENID = Request.Form["SECURETOKENID"];
                resp.status = "Approved";
                resp.paypalresponse = presp;
                resp.confirmationNumber = confirmationNumber;
                resp.RESPMSG = paypalCollection["RESPMSG"] == null ? "" : paypalCollection["RESPMSG"];
                resp.Cart_Session_UID = dto.Cart_Session_UID;
                resp.User_Cookie = dto.User_Cookie;
                resp.User_Email = dto.billinginfo.billinginfo[0].email;
                resp.CompanyName = dto.billinginfo.shippingaddress[0].company;
                dto.User_Created_IP = User_IP;
                dto.User_Created_Agent = User_Agent;
                dto.User_Created_Agent = User_Agent;
                dto.currency = RuntimeCaching.CacheGetCurrency("currency" + resp.SECURETOKENID.ToString());
                dto.user = RuntimeCaching.CacheGetIdenity("idenity" + resp.SECURETOKENID.ToString());
                dto.Token = Request.Form["SECURETOKEN"];
                dto.TokenID = Request.Form["SECURETOKENID"];
                dto.model.PaymentToken = Request.Form["SECURETOKEN"];
                dto.model.PaymentTokenId = Request.Form["SECURETOKENID"];
                dto.model.PaymentConfirmation = confirmationNumber;
                dto.model.Invoice_Transaction_ID = Request.Form["SECURETOKENID"].ToString();
                dto.model.Invoice_Correlation_ID = Request.Form["CORRELATIONID"].ToString();
                dto.model.ipAddress = dto.User_Created_IP;
                //dto.model.tax = dto.tax;
                //dto.model.freight = dto.;
                dto.presp = presp;
                InvoiceThreading invoiceThreading = new InvoiceThreading();
                Thread invoice_Thread = new Thread(invoiceThreading.UpdateSalesInfo);
                invoice_Thread.Start(dto);

                //new Thread(() =>
                //{
                //    UpdateSales payments = new UpdateSales();
                //    payments.UpdateSalesInfo(dto);
                //}).Start();


                RuntimeCaching.CacheResponseToken(confirmationNumber, json.Serialize(resp));
                RuntimeCaching.CacheSecurityToken(Request.Form["SECURETOKENID"].ToString(), json.Serialize(dto));

                //string token = encryptor.Encrypt(json.Serialize(resp), salt);
                ViewBag.token = confirmationNumber;
                ViewBag.PaymentResult = paypalCollection["RESULT"] == null ? "" : paypalCollection["RESULT"];
                @ViewBag.PaymentConfirmation = confirmationNumber;
                //manager.SaveSessionID(System.Web.HttpContext.Current, dto.User_Session_UID, out redirected, out isAdded);
                Session["CartCount"] = null;
                Session["CartActive"] = null;
                Session["Cart_Session_UID"] = null;
                return View();
            }
            else
            {
                PayPalTransactionResponse resp = new PayPalTransactionResponse();
                if (paypalCollection != null
                && paypalCollection["SECURETOKENID"] != null
                && paypalCollection["RESULT"].ToString() != "0")
                {
                    resp.status = "Failed";
                    resp.confirmationNumber = guid;
                    resp.User_Session_UID = dto.User_Session_UID;
                    resp.Cart_Session_UID = dto.Cart_Session_UID;
                    resp.User_Cookie = dto.User_Cookie;
                    RuntimeCaching.CacheResponseToken(confirmationNumber, json.Serialize(resp));
                    ViewBag.token = confirmationNumber;
                    ViewBag.PaymentResult = string.Empty;
                }
                else
                {
                    manager.SaveSessionID(System.Web.HttpContext.Current, dto.User_Session_UID, out redirected, out isAdded);
                    SessionManagement();
                    CartLookup cartitems = cartmodel.GetCart(dto.Cart_Session_UID, dto.User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
                    Session["CartCount"] = null;
                    Session["CartActive"] = null;
                    if (cartitems.cartitems != null && !string.IsNullOrEmpty(cartitems.totalcartitems))
                    {
                        Session["CartCount"] = cartitems.totalcartitems;
                        Session["CartActive"] = "active";
                    }
                    if (paypalCollection == null)
                    {
                        resp.status = "Null Response";
                        resp.confirmationNumber = confirmationNumber;
                        resp.User_Session_UID = dto.User_Session_UID;
                        resp.Cart_Session_UID = dto.Cart_Session_UID;
                        resp.User_Cookie = dto.User_Cookie;
                        RuntimeCaching.CacheResponseToken(confirmationNumber, json.Serialize(resp));
                        ViewBag.token = confirmationNumber;
                        ViewBag.PaymentResult = string.Empty;
                    }
                    else
                    {
                        resp.status = "Failed";
                        resp.confirmationNumber = confirmationNumber;
                        resp.Cart_Session_UID = dto.Cart_Session_UID;
                        resp.User_Session_UID = dto.User_Session_UID;
                        resp.User_Cookie = dto.User_Cookie;
                        RuntimeCaching.CacheResponseToken(confirmationNumber, json.Serialize(resp));
                        ViewBag.token = confirmationNumber;
                        ViewBag.PaymentResult = string.Empty;
                    }
                }
                @ViewBag.PaymentConfirmation = "";
                return View();
            }
        }



        //[HttpPost]
        public ActionResult PaymentFailure(string SECURETOKEN = null, string SECURETOKENID = null)
        {
            var paypalCollection = Request.Form;
            CartModels cartmodel = new CartModels();
            SessionIDManager manager = new SessionIDManager();
            string confirmationNumber = GenerateRandomToken(TokenLength.Bit16);
            bool isAdded = false; bool redirected = false;
            string paypalresponsejson = string.Empty;
            string guid = Guid.NewGuid().ToString("N");
            ViewBag.token = confirmationNumber;
            Encryptor encryptor = new Encryptor();
            PayPalResponse paypalresponse = new PayPalResponse();

            cerepairsHub.ceHub hubpush = new cerepairsHub.ceHub();
            JavaScriptSerializer json = new JavaScriptSerializer();
            string response = string.Empty;
            string SECURETOKEN_ID = null;
            if (SECURETOKENID != null)
            {
                SECURETOKEN_ID = SECURETOKENID;
            }
            if (Request.Form["SECURETOKENID"] != null)
            {
                SECURETOKEN_ID = paypalCollection["SECURETOKENID"].ToString();
            }
            if (SECURETOKEN_ID != null)
            {
                string securityToken = RuntimeCaching.CacheGetSecurityToken(SECURETOKEN_ID);
                //string logFile = "~/" + "testlog.txt";
                //logFile = Server.MapPath(logFile);
                //StreamWriter sw = new StreamWriter(logFile, true);
                //sw.WriteLine("CacheGetSecurityToken: " + securityToken);
                //sw.Close();
                if (securityToken == null)
                {
                    return View("~/Views/Payment/Error.cshtml");
                }
                //RuntimeCaching.CacheSecurityToken(paypalCollection["SECURETOKENID"].ToString(), securityToken);
                ProcessPayment token = json.Deserialize<ProcessPayment>(securityToken);
                manager.SaveSessionID(System.Web.HttpContext.Current, token.User_Session_UID, out redirected, out isAdded);
                //SessionManagement();
                CartLookup cartitems = cartmodel.GetCart(token.Cart_Session_UID, token.User_Cookie, Session["Cart_Promo"] == null ? null : Session["Cart_Promo"].ToString());
                Session["CartCount"] = null;
                Session["CartActive"] = null;
                if (cartitems.cartitems != null && !string.IsNullOrEmpty(cartitems.totalcartitems))
                {
                    Session["CartCount"] = cartitems.totalcartitems;
                    Session["CartActive"] = "active";
                }
                SessionManagement();
                IList<PropertyInfo> propertiesList;
                propertiesList = typeof(PayPalResponse).GetProperties().ToList();
                PayPalResponse presp = new PayPalResponse();
                try
                {
                    foreach (var property in propertiesList)
                    {
                        try
                        {
                            presp.SetProperty(property.Name, Request.Form[property.Name].ToString());
                            //if (s != "FPS_PREXMLDATA")
                        }
                        catch { }
                    }
                }
                catch { }

                //propertiesList = typeof(Sessions).GetProperties().ToList();
                //Sessions sessions = new Sessions();
                //try
                //{
                //    foreach (var property in propertiesList)
                //    {
                //        try
                //        {
                //            sessions.SetProperty(property.Name, Session[property.Name].ToString());
                //        }
                //        catch { }
                //    }
                //}
                //catch { }
                //sessions.Cart_Session_UID = token.Cart_Session_UID;
                //sessions.User_Cookie = token.User_Cookie;
                //string save_sessions = json.Serialize(sessions);
                //RuntimeCaching.CacheSession("sessions" + confirmationNumber, save_sessions);


                PayPalTransactionResponse resp = new PayPalTransactionResponse();
                resp.SECURETOKENID = SECURETOKEN_ID;
                resp.status = "Failure";
                resp.paypalresponse = presp;
                resp.User_Session_UID = token.User_Session_UID;
                resp.Cart_Session_UID = token.Cart_Session_UID;
                resp.User_Cookie = token.User_Cookie;
                ViewBag.token = confirmationNumber;
                ViewBag.PaymentResult = paypalCollection["RESULT"] == null ? "" : paypalCollection["RESULT"];
                RuntimeCaching.CacheResponseToken(confirmationNumber, json.Serialize(resp));
                RuntimeCaching.CacheFailureToken(token.Cart_Session_UID + token.User_Cookie + "failure", securityToken);

                ViewBag.PaymentConfirmation = confirmationNumber;
                ViewBag.chatid = json.Serialize(token.signalr);
                ViewBag.usercookie = json.Serialize(token.User_Cookie);
                ViewBag.billingaddress = json.Serialize(token.billinginfo.billingaddress[0]);
                ViewBag.shippingaddress = json.Serialize(token.billinginfo.shippingaddress[0]);
                ViewBag.billinginfo = json.Serialize(token.billinginfo.billinginfo[0]);
                ViewBag.grandtotal = token.model.grandtotal.ToString();
                ViewBag.tax = token.model.tax;
                return View("PaymentFailure");

                //hubpush.Push(dto.dto.signalr, hubdto);
            }
            //return response;
            ViewBag.PaymentResult = paypalCollection["RESULT"] == null ? "" : paypalCollection["RESULT"]; ;
            ViewBag.token = confirmationNumber;
            ViewBag.PaymentConfirmation = confirmationNumber;
            ViewBag.billingaddress = "";
            ViewBag.shippingaddress = "";
            ViewBag.billinginfo = "";
            return View("PaymentFailure");
        }

        [HttpPost]
        public JsonResult ClearSession()
        {
            Session["Cart_Session_UID"] = null;

            Session["Cart_Promo"] = null;
            Session["Promo_ProductUID"] = null;
            Session["CartCount"] = null;
            Session["CartActive"] = null;
            Session["PromoDiscount"] = null;

            SessionManagement();
            return Json("OK");
        }


        [HttpPost]
        public JsonResult BillingInfo(BillingInfoDTO dto)
        {
            return Json("OK");
        }

        public static string salt = "Ek#JkkL-KSDFfort%93K,&MKmowmfom;spw,mfv";

        private string GenerateRandomToken(TokenLength bit)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, (int)bit)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
        [HttpPost]

        public static string RunCompile(string viewpath, string templatekey, object model)
        {
            string result = string.Empty;
            var template = System.IO.File.ReadAllText(viewpath);
            if (string.IsNullOrEmpty(templatekey))
            {
                templatekey = Guid.NewGuid().ToString();
            }
            result = Engine.Razor.RunCompile(template, templatekey, null, model);
            //result = Engine.Razor.RunCompile(template, null, model);
            return result;
        }
    }
}


public enum TokenLength
{
    Bit16 = 16,
    Bit32 = 32
}

public class InvoiceThreading
{
    public void InvoiceConformation(object data)
    {
        InvoiceThread dto = (InvoiceThread)data;
        ce.InvoiceModels.InvoiceModels invoiceModel = new ce.InvoiceModels.InvoiceModels();

        //InvoiceModels model = new InvoiceModels();

        //Invoices inv = model.GetInvoice(dto.dto.SECURETOKENID, "transactionID");
        //var first = DateTime.Parse(inv.invoices.Take(1).FirstOrDefault().Invoice_Created_DTS).ToString("MM/dd/yyyy");
        ////var date = first.Invoice_Created_DTS;
        //string page = RazorEngineClass.RazorEngine.RunCompile(dto.viewpath, null, inv);
        //byte[] pdfBuffer = null;
        //HtmlToPdf htmlToPdfConverter = new HtmlToPdf();
        //htmlToPdfConverter.SerialNumber = "mtLzy8r+-/Nbz+Oj7-6OOrqrSq-uqu6qLqs-o6y6qau0-q6i0o6Oj-ow==";
        //pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(page, ConfigurationManager.AppSettings["ceURL"]);
        //Spire.Pdf.PdfDocument pdf = new Spire.Pdf.PdfDocument();
        //pdf.LoadFromBytes(pdfBuffer);

        //for (int i = 0; i < pdf.Pages.Count; i++)
        //{
        //    PdfPageBase originalPage = pdf.Pages[i];
        //    if (originalPage.IsBlank())
        //    {
        //        pdf.Pages.Remove(originalPage);
        //        i--;
        //    }
        //}

        //MemoryStream PDFstream = new MemoryStream();
        //pdf.SaveToStream(PDFstream, Spire.Pdf.FileFormat.PDF);
        //byte[] bytes = PDFstream.ToArray();
        //dto.bytes = bytes;
        //dto.pdf = pdf;
        ////InvoiceModels model = new InvoiceModels();
        //model.SavePDF(dto.bytes, dto.dto.Cart_Session_UID, dto.dto.User_Cookie, dto.dto.SECURETOKENID, dto.dto.confirmationNumber);



        invoiceModel.ConformationInvoice(dto);
        //invoiceModel.ConformationInvoice(dto.guid, dto.dto, dto.viewpath, dto.directoryPath);

    }
    public void UpdateSalesInfo(object data)
    {
        ProcessPayment dto = (ProcessPayment)data;
        UpdateSales payments = new UpdateSales();
        payments.UpdateSalesInfo(dto);
    }

}
public class PaypalInfo
{
    public string ReturnURL { get; set; }
    public string CancelURL { get; set; }
    public string ErrorURL { get; set; }
    public string Partner { get; set; }
    public string Vendor { get; set; }
    public string User { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public string Company { get; set; }
    public string Currency { get; set; }
    public string COMMENT1 { get; set; }
    public string COMMENT2 { get; set; }

}

public class PayPalResponse
{
    public string SECURETOKEN { get; set; }
    public string AVSDATA { get; set; }
    public string AMT { get; set; }
    public string ACCT { get; set; }
    public string CORRELATIONID { get; set; }
    public string AUTHCODE { get; set; }
    public string TENDER { get; set; }
    public string ZIP { get; set; }
    public string RESULT { get; set; }
    public string IAVS { get; set; }
    public string EXPDATE { get; set; }
    public string RESPMSG { get; set; }
    public string CARDTYPE { get; set; }
    public string PROCCVV2 { get; set; }
    public string TYPE { get; set; }
    public string PROCAVS { get; set; }
    public string TAX { get; set; }
    public string BILLTOZIP { get; set; }
    public string AVSZIP { get; set; }
    public string CVV2MATCH { get; set; }
    public string PNREF { get; set; }
    public string PPREF { get; set; }
    public string TRXTYPE { get; set; }
    public string AVSADDR { get; set; }
    public string SECURETOKENID { get; set; }
    public string METHOD { get; set; }
    public string TRANSTIME { get; set; }
    public string LASTNAME { get; set; }

}

public class securetokendto
{
    public string SECURETOKENID { get; set; }
    public string Cart_Session_UID { get; set; }
    public string User_Cookie { get; set; }
}
public class PayPalTransactionResponse
{
    public string SECURETOKENID { get; set; }
    public string confirmationNumber { get; set; }
    public string status { get; set; }
    public PayPalResponse paypalresponse { get; set; }
    public string RESULT { get; set; }
    public string RESPMSG { get; set; }
    public string User_Session_UID { get; set; }
    public string Cart_Session_UID { get; set; }
    public string User_Cookie { get; set; }
    public string User_Email { get; set; }
    public string CompanyName { get; set; }

}

//public class PaymentModel
//{
//    public int PaymentID { get; set; }
//    public string SessionUserId { get; set; }
//    public string PaymentUId { get; set; }
//    public string PaymentCurrency { get; set; }
//    public string PaymentCompany { get; set; }
//    public string CustomerId { get; set; }
//    public string PaymentConfirmation { get; set; }
//    public string PaymentAmount { get; set; }
//    public string PaymentNumber { get; set; }
//    public string PaymentType { get; set; }
//    public DateTime? InvoiceDate { get; set; }
//    public bool IsPaymentApplied { get; set; }
//    public DateTime PaymentCreatedDateTime { get; set; }
//    public string PaymentCreatedBy { get; set; }
//    public string PaymentTransactionId { get; set; }
//    public string PaymentJournal { get; set; }
//    public string PaymentCardType { get; set; }
//    public string PaymentMemo { get; set; }
//    public bool PaymentComplete { get; set; }
//    public string PaymentToken { get; set; }
//    public string PaymentTokenId { get; set; }
//    public string JsonObj { get; set; }
//}

//public class PaymentRequestModel
//{
//    //public List<PaymentRequest> PaymentRequests { get; set; }
//    public string Comments { get; set; }
//    public string Currency { get; set; }
//    public string Company { get; set; }
//    public string PaymentRefNum { get; set; }
//    public string PaymentLabel { get; set; }
//    public string PaymentType { get; set; }
//    public string PaymentCardType { get; set; }
//    public string PaymentCardNumber { get; set; }
//    public string PaymentNumber { get; set; }
//    public string PaymentValues { get; set; }
//    public decimal TotalAmount { get; set; }
//    public string PayPalURL { get; set; }
//    public string ConfirmationMessage { get; set; }
//    public string BCCEmail { get; set; }
//    public string PaymentSessionUserId { get; set; }
//    public string UserIPaddress { get; set; }
//    public string PaymentConfirmation { get; set; }
//    public DateTime? PaymentCreatedDateTime { get; set; }
//    //public List<InvoiceModel> Credits { get; set; }
//    public string SignalR { get; set; }
//}


//hubdto hubdto = new hubdto();
//string viewpath = Server.MapPath(@"~/Views/Payment/PaymentResponses.cshtml");
//transactionresponse = new PayPalTransactionResponse();
//transactionresponse.response = "decline";
//transactionresponse.conformation = GenerateRandomToken(TokenLength.Bit16);
//response = RunCompile(viewpath, null, transactionresponse);
//hubdto.request = "paypal";
//hubdto.success = "false";
//hubdto.data = response;
//hubdto.paymenttype = "";
//hubdto.paypal = "0";


