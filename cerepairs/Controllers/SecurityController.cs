﻿using System;
using System.Web;
using System.Web.Mvc;
using hme.Models;
using System.Web.Security;

using hmeUtil;
using HME.SQLSERVER.DAL;
using Microsoft.AspNet.Identity;
using System.Linq;
using cerepairs.Models;
using System.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using RazorEngine;
using qalab;
using cerepairs;
using System.Web.Script.Serialization;

namespace hme
{
    public class SecurityController : Controller
    {
        public static string salt = "Ek#JkkL-KSDFfort%93K,&MKmowmfom;spw,mfv";

        public object ExceptionUtility { get; private set; }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult JSON_Login(login_dto dto)
        {
            bool isAdmin = false;
            bool isFullAdmin = false;
            string loginType = "ldap";
            string userUID = "";
            User user = null;
            ViewBag.loginerror = "";

            AccountViewModels accountViewModels = new AccountViewModels();
            UserLogin userlogin = accountViewModels.User_Login(dto.username, dto.password);
            if (userlogin != null)
            {
                user = new User() { Username = dto.username.Split('@')[0], Password = dto.password };
                loginType = "customerservice";
                user.Email = dto.username;
                user.CN = userlogin.CN;
                userUID = userlogin.User_UID;
                user.Givenname = userlogin.Givenname;
                user.SN = userlogin.SN;
                //user.Roles = "Users,Admin";
                if (userlogin.UserType_ID == "1")
                {
                    isAdmin = true;
                }
                if (userlogin.UserType_ID == "4")
                {
                    isAdmin = true;
                    isFullAdmin = true;
                }
                user.Roles = "User" + (isAdmin ? ",Admin" : "") + (isFullAdmin ? ",FullAdmin" : "");

            }
            if (user != null)
            {
                User_Info user_info = new User_Info();
                user_info.cn = user.CN;
                //user_info.company_master = Session["Company_Master"].ToString();
                user_info.email = user.Email;
                user_info.givenname = user.Givenname;
                user_info.sn = user.SN;
                user_info.isadmin = isAdmin.ToString().ToLower();
                user_info.isfulladmin = isFullAdmin.ToString().ToLower();
                user_info.logintype = loginType;
                user_info.useruid = userUID;
                user_info.SamAccountname = user.SamAccountname;
                return Json(user_info);

            }
            else
            {
                //ModelState.AddModelError("", "Invalid login attempt.");
                //ViewBag.loginerror = "Invalid login attempt.";
                return null;
            }
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (RuntimeCaching.GetLoginAttempts(Request.ServerVariables["REMOTE_ADDR"].ToString() + "delay") != 0)
            {
                return View("LoginAttemptsExceeded");
            }
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.loginerror = "";
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (RuntimeCaching.GetLoginAttempts(Request.ServerVariables["REMOTE_ADDR"].ToString() + "delay") != 0)
            {
                return View("LoginAttemptsExceeded");
            }
            RuntimeCaching.CacheLoginAttempts(Request.ServerVariables["REMOTE_ADDR"].ToString());
            bool isAdmin = false;
            bool isFullAdmin = false;
            string loginType = "ldap";
            string userUID = "";
            ldap ldap = new ldap();
            User user;
            ViewBag.loginerror = "";

            if (!ModelState.IsValid)
            {
                ViewBag.loginerror = "Invalid login attempt.";
                return View(model);
            }

            AccountViewModels accountViewModels = new AccountViewModels();
            UserLogin userlogin = accountViewModels.User_Login(model.Username, model.Password);
            if (userlogin != null)
            {
                user = new User() { Username = model.Username.Split('@')[0], Password = model.Password };
                loginType = "customerservice";
                user.Email = model.Username;
                user.CN = userlogin.CN;
                userUID = userlogin.User_UID;
                user.Givenname = userlogin.Givenname;
                user.SN = userlogin.SN;
                //user.Roles = "Users,Admin";
                if (userlogin.UserType_ID == "1")
                {
                    isAdmin = true;
                }
                if (userlogin.UserType_ID == "4")
                {
                    isAdmin = true;
                    isFullAdmin = true;
                }
                user.Roles = "User" + (isAdmin ? ",Admin" : "") + (isFullAdmin ? ",FullAdmin" : "");

            }
            else
            {
                user = new User() { Username = model.Username.Split('@')[0], Password = model.Password };
                user = ldap.GetUserDetails(user);
            }

            if (user != null)
            {
                RuntimeCaching.ClearCache(Request.ServerVariables["REMOTE_ADDR"].ToString());
                FormsAuthentication.SetAuthCookie(model.Username, false);
                if (loginType == "ldap")
                {
                    isAdmin = isFullAdmin = user.MemberOf.Where(x =>
                          x.Contains("# HMEAll") && Session["Company_Master"].ToString() == "hsc"
                       || x.Contains("# HSCAll") && Session["Company_Master"].ToString() == "hsc"
                       || x.Contains("# JTECH All") && Session["Company_Master"].ToString() == "jtch"
                       || x.Contains("# Commercial Electronics") && Session["Company_Master"].ToString() == "cel"
                       || x.Contains("# CC_Order_Portal_Admin") && Session["Company_Master"].ToString() == "clrc"
                       || x.Contains("# Executive Staff")
                       || x.Contains("# HME Officers")
                       || x.Contains("IT Department")
                    ).ToList().Count > 0 ? true : false;
                    user.Roles = "User" + (isAdmin ? ",Admin,FullAdmin" : "");
                }
                User_Info user_info = new User_Info();
                user_info.cn = user.CN;
                user_info.company_master = Session["Company_Master"].ToString();
                user_info.email = user.Email;
                user_info.givenname = user.Givenname;
                user_info.sn = user.SN;
                user_info.isadmin = isAdmin.ToString().ToLower();
                user_info.isfulladmin = isFullAdmin.ToString().ToLower();
                user_info.logintype = loginType;
                user_info.useruid = userUID;
                user_info.SamAccountname = user.SamAccountname;
                hmeUtil.Encryptor encryptor = new Encryptor();
                JavaScriptSerializer json = new JavaScriptSerializer();
                string token = encryptor.Encrypt(json.Serialize(user_info), salt);

                var authTicket = new FormsAuthenticationTicket(1, token, DateTime.Now, DateTime.Now.AddMinutes(30), false, user.Roles);
                //var authTicket = new FormsAuthenticationTicket(1, user.Email + "|" + userUID + "|" + user.CN + "|" + loginType + "|" + isAdmin.ToString().ToLower() + "|" + isFullAdmin.ToString().ToLower() + "|" + Session["Company_Master"].ToString() + "|" + user.Givenname + "|" + user.SN, DateTime.Now, DateTime.Now.AddMinutes(30), false, user.Roles);
                string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Response.Cookies.Add(authCookie);
                string decodedUrl = "";
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    decodedUrl = Server.UrlDecode(returnUrl);
                }

                if (Url.IsLocalUrl(decodedUrl))
                {
                    return Redirect(decodedUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
                //return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Invalid login attempt.");
                ViewBag.loginerror = "Invalid login attempt.";
                return View(model);
            }
        }


        //
        // POST: /Security/LogOff
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            try
            {
                FormsAuthentication.SignOut();
            }
            catch { }
            return RedirectToAction("Index", "Home");
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {

            if (RuntimeCaching.CacheIsForgotPassword(model.Code))
            {
                AccountViewModels accountViewModels = new AccountViewModels();
                string user_uid = accountViewModels.Update_Pass_Reset(model.Code);
                string salt = string.Empty;
                string User_PasswordHash;
                if (user_uid != null)
                {
                    UserSearchRequest searchdto = new UserSearchRequest();
                    searchdto.user_uid = user_uid;
                    UserManagementModels user_model = new UserManagementModels();
                    UserManagementUser user = await user_model.SingleUsersSearch(searchdto);
                    salt = await user_model.GetUserSalt(user_uid);
                    string password = model.Password;

                    User_PasswordHash = TokenHash.GenerateSHA512String(password + salt);
                    for (int i = 1; i <= 2048; i++)
                    {
                        User_PasswordHash = TokenHash.GenerateSHA512String(User_PasswordHash + salt);
                    }
                    await user_model.SetNewPassword(user_uid, User_PasswordHash);

                    string viewpath = Server.MapPath(@"~/Views/emailtemplate.cshtml");
                    string imagepath = Server.MapPath(@"~\images\emailtemplate");
                    ViewBag.URL = ConfigurationManager.AppSettings["ceURL"];

                    //await Task.Factory.StartNew(() => SendEmail.sendenotifymail(imagepath, viewpath, "Password Reset Confirmation", "resetpasswordonformation", "", "linktag", token.email, "Customer Service Portal: Password Reset Confirmation", ConfigurationManager.AppSettings["ceURL"], ""));
                    SendEmail.sendenotifymail(imagepath, viewpath, "Password Reset Confirmation", "resetpasswordconformation", "", "linktag", user.user_emailaddress, "Customer Service Portal: Password Reset Confirmation", ConfigurationManager.AppSettings["ceURL"], "");
                    return View("PasswordResetConfirmed");
                }
            }
            //}
            return View("ResetPasswordExpired");
        }

        //
        // GET: /Security/Register
        [AllowAnonymous]
        public ActionResult ResetPassword(string uid)
        {
            ResetPasswordViewModel model = new ResetPasswordViewModel();
            //model.Code = Guid.NewGuid().ToString("N");
            model.Code = uid;
            return View(model);
        }

        //
        // GET: /Security/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            RegisterViewModel model = new RegisterViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {

            //Microsoft.Dynamics.BusinessConnectorNet.Axapta DynAx = new Microsoft.Dynamics.BusinessConnectorNet.Axapta();
            //Microsoft.Dynamics.BusinessConnectorNet.AxaptaRecord DynRec;
            //System.Net.NetworkCredential netCred = new System.Net.NetworkCredential("ax-bcproxy", "H,wn3Vsg");
            //string strUserName = "ax-bcproxy";

            //try
            //{
            //    DynAx.Logon("hsc", "en-us", "DCAXDEVTEST1", "CUS.axc");
            //}
            //catch (Exception ex)
            //{
            //    string err = ex.Message;
            //}

            //try
            //{
            //    DynAx.LogonAs(strUserName.Trim(), "HME", netCred, "hsc", "en-us", "DCAXDEVTEST1", "CUS.axc");
            //}
            //catch(Exception ex){
            //    string err = ex.Message;
            //}


            model.User_UID = Guid.NewGuid().ToString("N");
            model.User_PasswordSalt = TokenHash.GetRandomAlphanumericString(44) + "=";
            model.User_PasswordHash = TokenHash.GenerateSHA512String(model.User_Password1 + model.User_PasswordSalt);
            model.User_Created_By = Request.ServerVariables["REMOTE_ADDR"];
            model.User_Created_DTS = DateTime.Now;
            for (int i = 1; i <= 2048; i++)
            {
                model.User_PasswordHash = TokenHash.GenerateSHA512String(model.User_PasswordHash + model.User_PasswordSalt);
            }
            AccountViewModels accountViewModels = new AccountViewModels();
            if (accountViewModels.IsEmailAddressInUse(model.User_EmailAddress))
            {
                ViewBag.Error = "<li>Email Address is in use</li>";
                return View(model);
            }

            if (!string.IsNullOrEmpty(model.User_Cust_ID))
            {
                string dbAvanteResponse = accountViewModels.CheckCustomerID(model);
                if (dbAvanteResponse != "OK")
                {
                    ViewBag.Error = "<li>" + dbAvanteResponse + "</li>";
                    return View(model);
                }
            }

            string dbResponse = accountViewModels.User_Tbl_Update(model);
            if (dbResponse != "OK")
            {
                ViewBag.Error = dbResponse;
                return View(model);
            }
            string viewpath = Server.MapPath(@"~/Views/emailtemplate.cshtml");
            string siteURL = ConfigurationManager.AppSettings["ceURL"] + @"/security/login/";
            string imagepath = Server.MapPath(@"~\images\emailtemplate");
            Task.Factory.StartNew(() => SendEmail.sendenotifymail(imagepath, viewpath, "New User Login Request", "newuserloginrequest", "url", "linktag", "hscott@hme.com", "Customer Service Portal: New User Login Request", siteURL, ""));
            Task.Factory.StartNew(() => SendEmail.sendenotifymail(imagepath, viewpath, "Registration Notification", "registration", "url", "linktag", model.User_EmailAddress, "Customer Service Portal: Registration Notification", siteURL, ""));
            return View("RegisterResponse");
        }


        //
        // GET: /Security/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            ForgotPasswordViewModel model = new ForgotPasswordViewModel();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            ViewBag.Email = model.Email;
            ViewBag.URL = ConfigurationManager.AppSettings["ceURL"];
            string guid = Guid.NewGuid().ToString("N");

            string userAgent = Request.UserAgent;
            AccountViewModels accountViewModels = new AccountViewModels();
            string uid = accountViewModels.Get_User_UID(model.Email);
            if (uid != null)
            {
                accountViewModels.Pass_Reset_Insert(guid, uid, Request.ServerVariables["REMOTE_ADDR"], userAgent);
                RuntimeCaching.CacheForgotPassword(guid);
                string viewpath = Server.MapPath(@"~/Views/emailtemplate.cshtml");
                string imagepath = Server.MapPath(@"~\images\emailtemplate");
                Task.Factory.StartNew(() => SendEmail.sendenotifymail(imagepath, viewpath, "Password Reset Requested", "resetpassword", guid, "linktag", model.Email, "Customer Service Portal: Password Reset Requested", ConfigurationManager.AppSettings["ceURL"], ""));
                return View("ResetPasswordResponse");

            }
            ViewBag.Forogot_URL = ViewBag.URL + "/security/ForgotPassword";
            return View("ResetPasswordEmailInvalid");
        }

        public class MemberOf
        {
            public string item { get; set; }
        }
        public class MemberOf_List
        {
            public List<MemberOf> memberof { get; set; }
        }

        public class User_Info
        {
            public string email { get; set; }
            public string useruid { get; set; }
            public string cn { get; set; }
            public string logintype { get; set; }
            public string isadmin { get; set; }
            public string isfulladmin { get; set; }
            public string company_master { get; set; }
            public string givenname { get; set; }
            public string sn { get; set; }
            public string SamAccountname { get; set; }
        }

        public class login_dto
        {
            public string username { get; set; }
            public string password { get; set; }
            public string status { get; set; }
        }

        public enum _user
        {
            Email,
            name,
            username,
            firstname,
            lastname,
            roles,
            id,
            company
        }
    }
}

