﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(hme.Startup))]
namespace hme
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
